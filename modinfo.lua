-- This information tells other players more about the mod
name = "Playable Pets Modded"
author = "Leonardo Coxington"
version = "1.21.5"
description = "Play as Pets/Monsters from the community! \nPress the z,h,j,k,l keys when playing as mobs, they might do something special! \n\nNOTE: You will need Playable Pets Essentials enabled to be able to run this mod. \nVersion:"..version
-- This is the URL name of the mod's thread on the forum; the part after the ? and before the first & in the url
forumthread = "/topic/73911-playable-pets/"

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
    name = name.." Dev."
end

-- This lets other players know if your mod is out of date, update it to match the current version in the game
api_version = 10

dst_compatible = true
dont_starve_compatible = false
reign_of_giants_compatible = false
all_clients_require_mod = true
forge_compatible = true

icon_atlas = "modicon.xml"
icon = "modicon.tex"

priority = -308

server_filter_tags = {
"mob", "mobs", "playable", "monsters", "PP", "playable pets", "modded"
}

---------------------------------
--            DATA             --
---------------------------------

-- For initializing configuration settings to disable each mob
local availableMobs = {
	"Enslaved Pig",
	"Ham Breaker",
	"Grave Pig",
	"Goliath Garlic",
	--"Necrommander",
	"Roach Beetle",
	"Zombie Pigman",
	"Zombie Were-Pig",
	"Pig Zombie Carrier",
	"Big Bird",
	"Judgement Bird",
	"Punishing Bird",
	"Apoc Bird",
	"CENSORED",
	"Schadenfreude",
	"Scavantula",
	"Fumo",
	"Princess Peach",
	"Bristle",
	"Dry Bones",
	"Duplighost",
	"Shroob",
	"Scamp",
	"Swordman",
	"Bomb Scout",
	"Healer",
	--SK
	"Dust Zombie", "Slag Walker", "Frozen Shambler", "Droul", "Frankenzom", "Carnavon", 
	"Bombie", "Burning Bombie", "Freezing Bombie", "Choking Bombie", "Surging Bombie",
	"Spookat", "Statikat", "Hurkat", "Pepperkat", "Bloogato", "Black Kat", "Mew Kat", "Grimalkin", "Margrel"
}

---------------------------------
--       CONFIG OPTIONS        --
---------------------------------

configuration_options = {
	{
		name = "sk_difficulty",
		label = "Difficulty", 
		options = 
		{
			{description = "Easy",  data = 0.5,  hover = "SK mob strength will be 50%"},
			{description = "Advanced", data = 1, hover = "SK mob strength will be 100%"},
			{description = "Elite", data = "1.5", hover = "SK mob strength will be 150%"},
			
		},
		default = 1
	},
	{
		name = "sk_curse_enable",
		label = "Players can Curse?", 
		options = 
		{
			{description = "Yes",  data = true,  hover = "Curse can be inflicted by players"},
			{description = "No", data = false, hover = "Curse cannot be inflicted by players"},
			
		},
		default = true
	},
}

---------------------------------
-- TABLE POPULATION CODE BELOW --
---------------------------------

-- Automatically populate enable/disable configuration settings for mobs
local settingEnable = {
	{description = "Enabled", data = "Enable"},
	{description = "Disabled", data = "Disable"},
}

for i = 1, #availableMobs do	
	local configOption = {}
	configOption.name = availableMobs[i]
	configOption.label = availableMobs[i]
	configOption.options = settingEnable
	configOption.default = "Enable"
	
	configuration_options[#configuration_options + 1] = configOption
end