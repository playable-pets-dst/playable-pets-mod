
------------------------------------------------------------------
-- Variables
------------------------------------------------------------------

local require = GLOBAL.require
local Ingredient = GLOBAL.Ingredient

local PlayablePets = GLOBAL.PlayablePets
local SETTING = GLOBAL.PP_SETTINGS
local MOBTYPE = GLOBAL.PP_MOBTYPES

PlayablePets.Init(env.modname)

local STRINGS = require("ppm_strings")

GLOBAL.PPM_FORGE = require("ppm_forge")

------------------------------------------------------------------
-- Configuration Data
------------------------------------------------------------------

local TUNING = require("ppm_tuning") --has to be here to catch configuration settings.

------------------------------------------------------------------
-- Prefabs
------------------------------------------------------------------

PrefabFiles = {
	"lc_debuff_enchantlure",
	"lc_buff_revenge",
	"scavantula_projectilep",
	"pig_stakep",
	"pig_corpse_pilep",
	"lc_blackforestportal",
	"hangingtrap",
	"pm_bristle",
	"pm_shroobfo",
	"pw_banner",
	"pw_cages",
	"drybones_projectiles",
	"ppm_skeletons",
	"cotl_bombp",
	"cotl_enemyspawnp",
	"cotl_summoningcirclep",
	"lc_censoredbaby_npc",
	"sk_zombie",
	"sk_bombie",
	"sk_spookat",
	"sk_grimalkin",
	------Misc-------
	"sk_grim_totem",
	"sk_lantern",
	"sk_gravestone",
	"pp_floor_button",
	"gatesp",
	"pp_switch",
	-------FX--------
	"sk_debuffs",
	"sk_bullet",
	"sk_zombie_breath_fx",
	"sk_fx",
	"sk_auras",
	"sk_marg_util",
	"pm_shroob_laser_projectile",
	"custom_basalts",
	"goliath_cloud",
	"goliath_projectile",
}

-- Centralizes a number of related functions about playable mobs to one convenient table
-- Name is the prefab name of the mob
-- Fancyname is the name of the mob, as seen in-game
-- Gender is fairly self-explanatory
GLOBAL.PP_MobCharacters = {
	goliath_garlicp         = { fancyname = "Goliath Garlic",           gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	pig_slavep              = { fancyname = "Enslaved Pig",             gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	gravepigp               = { fancyname = "Grave Pig",                gender = "MALE",      mobtype = {}, skins = {}, forge = false, mod_id = "2633870801"},
	necrommanderp           = { fancyname = "Necrommander",             gender = "MALE",      mobtype = {}, skins = {}, forge = false, mod_id = "2633870801"},
	roach_beetlep           = { fancyname = "Roach Beetle",             gender = "MALE",      mobtype = {}, skins = {}, forge = false, mod_id = "2633870801"},
	tiddles_zombiepigp      = { fancyname = "Zombie Pigman",            gender = "MALE",      mobtype = {}, skins = {}, forge = false, mod_id = "1982562290"},
	tiddles_zombiepig_werep = { fancyname = "Zombie Were-Pig",          gender = "MALE",      mobtype = {}, skins = {}, forge = false, mod_id = "1982562290"},
	tiddles_zombiepig_fatp  = { fancyname = "Pig Zombie Carrier",       gender = "MALE",      mobtype = {}, skins = {}, forge = false, mod_id = "1982562290"},
	lc_bigbirdp             = { fancyname = "Big Bird",                 gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	lc_judgementbirdp       = { fancyname = "Judgement Bird",           gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	lc_punishingbirdp       = { fancyname = "Punishing Bird",           gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	lc_censoredp            = { fancyname = "CENSORED",                 gender = "ROBOT",     mobtype = {}, skins = {}, forge = false},
	lc_schadenfreudep       = { fancyname = "Schadenfreude",            gender = "ROBOT",     mobtype = {}, skins = {}, forge = false},
	lc_apocbirdp            = { fancyname = "Apoc Bird",                gender = "MALE",      mobtype = {}, skins = {}, forge = false, hidden = true},
	scavantulap             = { fancyname = "Scavantula",               gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	fumop                   = { fancyname = "Fumo",                     gender = "FEMALE",    mobtype = {}, skins = {}, forge = false},
	cotl_scampp             = { fancyname = "Scamp",                    gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	cotl_swordmanp          = { fancyname = "Swordman",                 gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	cotl_bomb_scoutp        = { fancyname = "Bomb Scout",               gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	cotl_healerp            = { fancyname = "Healer",                   gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	pm_peachp               = { fancyname = "Princess Peach",           gender = "FEMALE",    mobtype = {}, skins = {}, forge = false},
	pm_bristlep             = { fancyname = "Bristle",                  gender = "ROBOT",     mobtype = {}, skins = {}, forge = false},
	pm_drybonesp            = { fancyname = "Dry Bones",                gender = "ROBOT",     mobtype = {}, skins = {}, forge = false},
	pm_shroobp              = { fancyname = "Shroob",                   gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	pm_duplighostp          = { fancyname = "Duplighost",               gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	--prinnyp                 = { fancyname = "Prinny",                   gender = "MALE",      mobtype = {}, skins = {}, forge = false},
	zombie_dustp            = { fancyname = "Dust Zombie",              gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_firep            = { fancyname = "Slag Walker",              gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_freezep          = { fancyname = "Frozen Shambler",          gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_poisonp          = { fancyname = "Droul",                    gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_shockp           = { fancyname = "Frankenzom",               gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	zombie_cursep           = { fancyname = "Carnavon",                 gender = "NEUTRAL",   mobtype = {}, skins = {}, forge = true},
	bombie_dustp            = { fancyname = "Bombie",                   gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_firep            = { fancyname = "Burning Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_freezep          = { fancyname = "Freezing Bombie",          gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_poisonp          = { fancyname = "Choking Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	bombie_shockp           = { fancyname = "Surging Bombie",           gender = "NEUTRAL",   mobtype = {}, skins = {}},
	--
	spookat_nrmp            = { fancyname = "Spookat",                  gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_shockp          = { fancyname = "Statikat",                 gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_poisonp         = { fancyname = "Hurkat",                   gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_firep           = { fancyname = "Pepperkat",                gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_freezep         = { fancyname = "Pepperkat",                gender = "NEUTRAL",   mobtype = {}, skins = {}},
	spookat_blackp          = { fancyname = "Black Kat",                gender = "NEUTRAL",   mobtype = {}, skins = {}},
	mewkatp                 = { fancyname = "Mew Kat",                  gender = "NEUTRAL",   mobtype = {}, skins = {}},
	grimalkinp              = { fancyname = "Grimalkin",                gender = "NEUTRAL",   mobtype = {}, skins = {}},
	grimalkin_cursep        = { fancyname = "Margrel",                  gender = "NEUTRAL",   mobtype = {}, skins = {}},
	--
	--citypig_footsoldierp       = { fancyname = "Trot Soldier",           gender = "MALE",   mobtype = {}, skins = {}, forge = false, mod_id = "1547216819"}, --requires hamlet.
	pig_knight_fatp         = { fancyname = "Ham Breaker",              gender = "MALE",   mobtype = {}, skins = {}},
}

-- Necessary to ensure a specific order when adding mobs to the character select screen. This table is iterated and used to index the one above
PP_Character_Order = {
	"pig_slavep",
	"pig_knight_fatp",
	"gravepigp", 
	"necrommanderp",
	"roach_beetlep",
	"goliath_garlicp",
	"tiddles_zombiepigp",
	"tiddles_zombiepig_werep",
	"tiddles_zombiepig_fatp",
	"lc_bigbirdp",
	"lc_judgementbirdp",
	"lc_punishingbirdp",
	"lc_apocbirdp",
	"lc_censoredp",
	"lc_schadenfreudep",
	"scavantulap",
	"fumop",
	"pm_peachp",
	"pm_bristlep",
	"pm_drybonesp",
	"pm_duplighostp",
	"pm_shroobp",
	"cotl_scampp",
	"cotl_swordmanp",
	"cotl_bomb_scoutp",
	"cotl_healerp",
	"zombie_dustp", 
	"zombie_firep", 
	"zombie_freezep", 
	"zombie_poisonp", 
	"zombie_shockp", 
	"zombie_cursep", 
	"bombie_dustp", 
	"bombie_firep", 
	"bombie_freezep", 
	"bombie_poisonp", 
	"bombie_shockp",
	"spookat_nrmp", 
	"spookat_shockp", 
	"spookat_poisonp", 
	"spookat_firep", 
	"spookat_freezep", 
	"spookat_blackp", 
	"mewkatp", 
	"grimalkinp", 
	"grimalkin_cursep",
	--"prinnyp",
}

------------------------------------------------------------------
-- SK stuff
------------------------------------------------------------------
--TODO we're going to have a pp version of combat to handle stuff like this... probably.
GLOBAL.SK_FAMILIES = {
	GREMLIN = 
	{
		family = "gremlin", --variable
		tag = "gremlin", --might not give to players?
		piercing_stat = 1,
		elemental_stat = 0.5,
		normal_stat = 1,
		shadow_stat = 2
	},
	SLIME = 
	{
		family = "slime", --variable
		tag = "slime", --might not give to players?
		piercing_stat = 0.5,
		elemental_stat = 1,
		normal_stat = 1,
		shadow_stat = 2,
		
		stun_resist = 0.5,
	},
	BEAST = 
	{
		family = "beast", --variable
		tag = "beast", --might not give to players?
		piercing_stat = 2,
		elemental_stat = 0.5,
		normal_stat = 1,
		shadow_stat = 1
	},
	UNDEAD = 
	{
		family = "undead", --variable
		tag = "undead", --might not give to players?
		piercing_stat = 1,
		elemental_stat = 2,
		normal_stat = 1,
		shadow_stat = 0.5,
		
		curse_resist = 1,
	},
	FIEND = 
	{
		family = "fiend", --variable
		tag = "fiend", --might not give to players?
		piercing_stat = 2,
		elemental_stat = 1,
		normal_stat = 1,
		shadow_stat = 0.5
	},
	CONSTRUCT = 
	{
		family = "construct", --variable
		tag = "construct", --might not give to players?
		piercing_stat = 0.5,
		elemental_stat = 2,
		normal_stat = 1,
		shadow_stat = 1,
		
		sleep_resist = 0,
	},
	UNKNOWN = --default or used for custom classes, like Vanaduke. 
	{
		family = "", --these top two won't be used
		tag = "", 
		piercing_stat = 1,
		elemental_stat = 1,
		normal_stat = 1,
		shadow_stat = 1
	},
	
}

GLOBAL.SK_DIFFICULTY = GetModConfigData("sk_difficulty")
GLOBAL.SK_CURSE_ENABLE = GetModConfigData("sk_curse_enable")
GLOBAL.SK_HEALTH = require("sk_health")

------------------------------------------------------------------
-- Assets
------------------------------------------------------------------

Assets = {
    --------------------------------------------------------------------
	--Copy of Reforged Assets that will be required by its expansions.
	Asset("ANIM", "anim/lavaarena_player_teleport_colorable.zip"),
	Asset("ANIM", "anim/lavaarena_boaron_undead_actions.zip"),
	Asset("ANIM", "anim/lavaarena_peghook_cultist_actions.zip"),
	Asset("ANIM", "anim/undead_ground_fx.zip"),
	Asset("ANIM", "anim/warg_dire_actions.zip"),
	Asset("ANIM", "anim/warg_dire_build.zip"),
	--------------------------------------------------------------------
	--Lobcorp--
	Asset("SOUNDPACKAGE", "sound/lobcorp.fev"),
	Asset( "SOUND", "sound/lobcorp_bank00.fsb"), 
	--------------------------------------------------------------------
	--Paper Mario--
	Asset("SOUNDPACKAGE", "sound/pm.fev"),
	Asset( "SOUND", "sound/pm_bank01.fsb"), 
	--------------------------------------------------------------------
	--Spiral Knights--
	Asset("SOUNDPACKAGE", "sound/sk.fev"), --make sure to update this every time SW updates.
	Asset( "SOUND", "sound/sk.fsb"), --Needed until SW gets ported to DST.
	--------------------------------------------------------------------
	--Custom--
	Asset("SOUNDPACKAGE", "sound/pig_knight_fat.fev"),
	Asset( "SOUND", "sound/pig_knight_fat_bank02.fsb"), 
}
------------------------------------------------------------------
--Fonts
------------------------------------------------------------------
GLOBAL.TALKINGFONT_SHROOB = "talkingfont_shroob"
local TheSim = GLOBAL.TheSim
AddSimPostInit(function()
	TheSim:UnloadFont(GLOBAL.TALKINGFONT_SHROOB)
	TheSim:UnloadPrefabs({"pp_fonts"})

	local Assets = {
		Asset("FONT", GLOBAL.resolvefilepath("fonts/talkingfont_shroob.zip")),
	}
	local FontsPrefab = GLOBAL.Prefab("pp_fonts", function() return GLOBAL.CreateEntity() end, Assets)
	GLOBAL.RegisterPrefabs(FontsPrefab)
	TheSim:LoadPrefabs({"pp_fonts"})
	TheSim:LoadFont(GLOBAL.resolvefilepath("fonts/talkingfont_shroob.zip"), GLOBAL.TALKINGFONT_SHROOB)
end)

GLOBAL.c_gate = function(inst, id)
	local gate = GLOBAL.SpawnPrefab("pp_gate_base")
	gate.Transform:SetPosition(inst:GetPosition():Get())
	gate.Transform:SetRotation(inst.Transform:GetRotation())
	gate._trigger_id = id
end
------------------------------------------------------------------
-- Custom Recipes
------------------------------------------------------------------

-- Pig house
--local pigrecipe = AddRecipe("pighouse_player", {Ingredient("boards", 4),Ingredient("cutstone", 2)}, GLOBAL.RECIPETABS.TOWN, GLOBAL.TECH.SCIENCE_ONE, "pighouse_placer", nil, nil, 1, "pig", "images/inventoryimages/houseplayer.xml", "houseplayer.tex")
------------------------------------------------------------------
-- Component Overrides
------------------------------------------------------------------

------------------------------------------------------------------
-- PostInits
------------------------------------------------------------------

-------------------------------------------------------
--Wardrobe stuff--

--Skin Puppet Stuff
local MobPuppets = require("ppm_puppets")
local MobSkins = require("ppm_skins")
PlayablePets.RegisterPuppetsAndSkins(PP_Character_Order, MobPuppets, MobSkins)
------------------------------------------------------------------
-- Commands
------------------------------------------------------------------

------------------------------------------------------------------
-- Asset Population
------------------------------------------------------------------

local assetPaths = { "bigportraits/", "images/map_icons/", "images/avatars/avatar_", "images/avatars/avatar_ghost_" }
local assetTypes = { {"IMAGE", "tex"}, {"ATLAS", "xml"} }

-- Iterate through the player mob table and do the following:
-- 1. Populate the PrefabFiles table with the mob prefab names and their skin prefabs (if applicable)
-- 2. Add an atlas and image for the mob's following assets:
-- 2.1 Character select screen portraits
-- 2.2 Character map icons
-- 2.3 ??? FIXME
-- 2.4 ??? FIXME
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PP_Character_Order) do
	local mob = GLOBAL.PP_MobCharacters[prefab]
	if PlayablePets.MobEnabled(mob, env.modname) then --don't load characters if they aren't enabled.
		table.insert(PrefabFiles, prefab)
	end
	
	-- Add custom skin prefabs, if available
	-- Example: "dragonplayer_formal"
	
	for _, path in ipairs(assetPaths) do
		for _, assetType in ipairs(assetTypes) do
			--print("Adding asset: "..assetType[1], path..prefab.."."..assetType[2])
			table.insert( Assets, Asset( assetType[1], path..prefab.."."..assetType[2] ) )
		end
	end
end
------------------------------------------------------------------
-- Mob Character Instantiation
------------------------------------------------------------------

-- Adds a mod character based on an individual mob
-- prefab is the prefab name (e.g. clockwork1player)
-- mob.fancyname is the mob's ingame name (e.g. Knight)
-- mob.gender is fairly self-explanatory
--for prefab, mob in pairs(GLOBAL.PP_MobCharacters) do
for _, prefab in ipairs(PP_Character_Order) do
	local mob = GLOBAL.PP_MobCharacters[prefab]
	PlayablePets.SetGlobalData(prefab, mob)
	if PlayablePets.MobEnabled(mob, env.modname) then
		AddMinimapAtlas("images/map_icons/"..prefab..".xml")
		AddModCharacter(prefab, mob.gender)
		if mob.hidden then
			table.insert(GLOBAL.MODCHARACTEREXCEPTIONS_DST, prefab)
			table.insert(GLOBAL.SEAMLESSSWAP_CHARACTERLIST, prefab)
		end
	end
end