require "behaviours/chaseandattack"
require "behaviours/wander"
require "behaviours/faceentity"
require "behaviours/follow"
require "behaviours/standstill"
require "behaviours/runaway"
require "behaviours/doaction"
require "behaviours/panic"
require "behaviours/leash"

local RUN_START_DIST = 5
local RUN_STOP_DIST = 15

local SEE_FOOD_DIST = 10
local MAX_WANDER_DIST = 40
local MAX_CHASE_TIME = 10

local MIN_FOLLOW_DIST = 8
local MAX_FOLLOW_DIST = 15
local TARGET_FOLLOW_DIST = (MAX_FOLLOW_DIST+MIN_FOLLOW_DIST)/2
local MAX_PLAYER_STALK_DISTANCE = 40

local LEASH_RETURN_DIST = 40
local LEASH_MAX_DIST = 80

local MIN_FOLLOW_LEADER = 2
local MAX_FOLLOW_LEADER = 4
local TARGET_FOLLOW_LEADER = (MAX_FOLLOW_LEADER+MIN_FOLLOW_LEADER)/2

local START_FACE_DIST = MAX_FOLLOW_DIST
local KEEP_FACE_DIST = MAX_FOLLOW_DIST

local function GetLeader(inst)
    return inst.components.follower ~= nil and inst.components.follower.leader or nil
end

local function GetHome(inst)
    return inst.components.homeseeker ~= nil and inst.components.homeseeker.home or nil
end

local function GetHomePos(inst)
    local home = GetHome(inst)
    return home ~= nil and home:GetPosition() or nil
end

local function GetNoLeaderLeashPos(inst)
    return GetLeader(inst) == nil and GetHomePos(inst) or nil
end

local function GetWanderPoint(inst)
    local target = GetLeader(inst) or inst:GetNearestPlayer(true)
    return target ~= nil and target:GetPosition() or nil
end


local SK_BulletChaserBrain = Class(Brain, function(self, inst)
    Brain._ctor(self, inst)
end)

function SK_BulletChaserBrain:OnStart()
    local root = PriorityNode(
    {
        WhileNode(function() return self.inst.components.combat.target end, "Chase", ChaseAndAttack(self.inst, 100)),
		Wander(self.inst, GetHomePos, MAX_WANDER_DIST),
    }, .25)

    self.bt = BT(self.inst, root)
end

return SK_BulletChaserBrain
