local assets =
{
    Asset("ANIM", "anim/pig_corpse_pile.zip"),
}

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local x, y, z = inst.Transform:GetWorldPosition()
    SpawnPrefab("ash").Transform:SetPosition(x, y, z)
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(x, y, z)
    fx:SetMaterial("stone")
    inst:Remove()
end

local function onhit(inst, worker)
    inst.AnimState:PlayAnimation("hit")
    inst.AnimState:PushAnimation("idle")
end

local function onextinguish(inst)
    if inst.components.fueled ~= nil then
        inst.components.fueled:InitializeFuelLevel(0)
    end
end

local function ontakefuel(inst)
    inst.SoundEmitter:PlaySound("dontstarve/common/fireAddFuel")
end

local function updatefuelrate(inst)
    inst.components.fueled.rate = TheWorld.state.israining and 1 + TUNING.FIREPIT_RAIN_RATE * TheWorld.state.precipitationrate or 1
end

local function onupdatefueled(inst)
    if inst.components.burnable ~= nil and inst.components.fueled ~= nil then
        updatefuelrate(inst)
        inst.components.burnable:SetFXLevel(inst.components.fueled:GetCurrentSection(), inst.components.fueled:GetSectionPercent())
    end
end

local function onfuelchange(newsection, oldsection, inst, doer)
    if newsection <= 0 then
        inst.components.burnable:Extinguish()
		if inst.queued_charcoal then
			inst.components.lootdropper:SpawnLootPrefab("charcoal")
			inst.queued_charcoal = nil
		end
    else
        if not inst.components.burnable:IsBurning() then
            updatefuelrate(inst)
            inst.components.burnable:Ignite(nil, nil, doer)
        end
        inst.components.burnable:SetFXLevel(newsection, inst.components.fueled:GetSectionPercent())

		if newsection == inst.components.fueled.sections then
			inst.queued_charcoal = not inst.disable_charcoal
		end
    end
end

local SECTION_STATUS =
{
    [0] = "OUT",
    [1] = "EMBERS",
    [2] = "LOW",
    [3] = "NORMAL",
    [4] = "HIGH",
}
local function getstatus(inst)
    return SECTION_STATUS[inst.components.fueled:GetCurrentSection()]
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("place")
    inst.AnimState:PushAnimation("idle", false)
    inst.SoundEmitter:PlaySound("dontstarve/common/fireAddFuel")
end

local function OnHaunt(inst, haunter)
    if math.random() <= TUNING.HAUNT_CHANCE_RARE and
        inst.components.fueled ~= nil and
        not inst.components.fueled:IsEmpty() then
        inst.components.fueled:DoDelta(TUNING.MED_FUEL)
        inst.components.hauntable.hauntvalue = TUNING.HAUNT_SMALL
        return true
    --#HAUNTFIX
    --elseif math.random() <= TUNING.HAUNT_CHANCE_HALF and
        --inst.components.workable ~= nil and
        --inst.components.workable:CanBeWorked() then
        --inst.components.workable:WorkedBy(haunter, 1)
        --inst.components.hauntable.hauntvalue = TUNING.HAUNT_SMALL
        --return true
    end
    return false
end

local function OnInit(inst)
    if inst.components.burnable ~= nil then
        inst.components.burnable:FixFX()
    end
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()

	MakeObstaclePhysics(inst, 1)

    inst.AnimState:SetBank("pig_corpse_pile")
	inst.AnimState:SetBuild("pig_corpse_pile")
    inst.AnimState:PlayAnimation("idle_loop", true)
    inst.AnimState:SetSaturation(0)
    inst.AnimState:SetMultColour(0.3, 0.3, 0.3, 1)

	inst:AddTag("corpse_pile")

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
		return inst
	end

    inst:DoTaskInTime(0, function(inst)
        local fx = SpawnPrefab("corpse_pile_firep")
        fx.entity:SetParent(inst.entity)
        fx.entity:AddFollower()
	    fx.Follower:FollowSymbol(inst.GUID, "swap_fire", 0, 0, 0)
    end)

	return inst
end

local function GetHeat(inst)
    return 240
end

local function firefn()
    local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()

    inst.entity:SetPristine()

    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetBank("fire")
	inst.AnimState:SetBuild("fire")
    inst.AnimState:PlayAnimation("level4", true)

    inst.entity:AddLight()
	inst.Light:SetIntensity(0.9)
	inst.Light:SetRadius(5)
	inst.Light:SetFalloff(0.2)
	inst.Light:Enable(true)
	inst.Light:SetColour(255/255,190/255,121/255)

    inst.AnimState:SetFinalOffset(-1)

    if not TheWorld.ismastersim then
		return inst
	end

    inst.persists = false

    inst.SoundEmitter:PlaySound("dontstarve/common/forestfire", "campfire")

    inst:AddComponent("heater")
    inst.components.heater.heatfn = GetHeat

    return inst
end

return Prefab("corpse_pile_firepitp", fn, assets, prefabs),
Prefab("corpse_pile_firep", firefn, assets, prefabs)

