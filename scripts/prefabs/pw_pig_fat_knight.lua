local brain = require("brains/pigsoldierbrain")

local unitstats = TUNING.PIGWARS.UNITS.PIG_FAT_KNIGHT
local tuning_values = TUNING.PIG_KNIGHT_FATP

local assets =
{
	Asset("ANIM", "anim/pig_knight_fat.zip"),
	Asset("ANIM", "anim/pig_knight_fat_loyalist.zip"),
}

local prefabs =
{
    "meat",
    "pigskin",
}

local s_path = "pig_knight_fat/creatures/pig_knight_fat/"
local sounds = {
    --t  = "path",
    talk = s_path.."grunt",
	grunt = s_path.."grunt",
	hurt = s_path.."hurt",
	step = s_path.."step",
	swing = s_path.."swing",
	attack = s_path.."axe_attack",
	smash = s_path.."smash",
	bodyfall = s_path.."bodyfall",
	death = s_path.."death",
}

local MAX_TARGET_SHARES = 5
local SHARE_TARGET_DIST = 30

local target_dist = 17

----------------------------------------------
--Experimental setup for weapons
----------------------------------------------
--This method's quirks:
--Cannot have the weapon be stolen or forced off.
--Switching back to unarmed might be complicated.
local function FireProjectile(inst, target)
    if target and target:IsValid() then
		local pos = inst:GetPosition()
        local posx, posy, posz = inst.Transform:GetWorldPosition()
		local angle = -inst.Transform:GetRotation() * DEGREES
		local offset = 2
		local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
        local spit = SpawnPrefab("blunderbuss_projectile")
		spit.Transform:SetPosition(targetpos.x, 0, targetpos.z)
        --Blunderbuss FX--
        local fx = SpawnPrefab("toadstool_cap_releasefx")
        fx.AnimState:SetSaturation(0)
        fx.Transform:SetPosition(targetpos.x, 2, targetpos.z)
		inst:DoTaskInTime(0, function(inst) spit.components.projectile.owner = inst end)
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end

local function GiveWeapon(inst, prefab)
    local wpn = TUNING.PIGWARS.WEAPONS[string.upper(prefab)]
    if not wpn then
        print("Failed giving "..prefab.." weapon to "..inst.prefab)
        return
    end

    inst.components.combat:SetDefaultDamage(wpn.DAMAGE)
    inst.components.combat:SetRange(wpn.ATTACK_RANGE, wpn.HIT_RANGE)
    inst.components.combat:SetAttackPeriod(wpn.ATTACK_PERIOD)
    inst._attack_period = wpn.ATTACK_PERIOD

    inst.AnimState:OverrideSymbol("swap_object", "swap_"..prefab, "swap_"..prefab)
	inst.AnimState:Show("ARM_carry")
	inst.AnimState:Hide("ARM_normal")
end

-----------------------------------------------------
--Talker
-----------------------------------------------------

local function getSpeechType(inst,speech)
    local line = speech.DEFAULT

    if inst.talkertype and speech[inst.talkertype] then
        line = speech[inst.talkertype]
    end

    if type(line) == "table" then
        line = line[math.random(#line)]
    end

    return line
end

local function sayline(inst, line, mood)
	inst.SoundEmitter:PlaySound(inst.sounds.talk)
    inst.components.talker:Say(line, 1.5, nil, true, mood)
end

local function ontalk(inst, script, mood)
    inst.SoundEmitter:PlaySound(inst.sounds.talk)
end

---------------------------------------------------
--Sleeper
---------------------------------------------------
local function NormalShouldSleep(inst)
    --[[
    if inst.components.follower and inst.components.follower.leader then
        local fire = FindEntity(inst, 6, function(ent)
            return ent.components.burnable
                   and ent.components.burnable:IsBurning()
        end, {"campfire"})
        return DefaultSleepTest(inst) and fire and (not inst.LightWatcher or inst.LightWatcher:IsInLight())
    else
        return DefaultSleepTest(inst) 
    end]]
    return false --soldiers don't sleep
end
---------------------------------------------------
--Faction
---------------------------------------------------
local builds = {
    gold = "loyalist",
    red = "shadow",
    blue = "lunar",
    purple = "undead"
}

local function OnFactionChanged(inst, data)
    if data.faction and builds[data.faction] then
        inst.AnimState:SetBuild("pig_knight_fat_"..builds[data.faction])
    end
end

local axe_attacks = {
	weapon_slam = {
		stats = 
		{
			blunt = 0, 
			slash = 240, 
		 	piercing = 0
		}, 
		poise = 40, 
		offset = tuning_values.AXE_RANGE_OFFSET, 
		range = 3
	},
	butt_slam = {stats = {blunt = tuning_values.MAX_JUMP_DAMAGE, slash = 0, piercing = 0, poise = 80}, range = 5}
}
---------------------------------------------------
--Combat
---------------------------------------------------
local function OnAttackedByDecidRoot(inst, attacker)
    local fn = function(dude) return dude:HasTag("pig") and not dude:HasTag("werepig") and not dude:HasTag("guard") end

    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = nil
    ents = TheSim:FindEntities(x,0,z, SHARE_TARGET_DIST / 2)
    
    if ents then
        local num_helpers = 0
        for k,v in pairs(ents) do
            if v ~= inst and v.components.combat and not (v.components.health and v.components.health:IsDead()) and fn(v) then
                if v:PushEvent("suggest_tree_target", {tree=attacker}) then
                    num_helpers = num_helpers + 1
                end
            end
            if num_helpers >= MAX_TARGET_SHARES then
                break
            end     
        end
    end
end

local function IsPlayerFriendly(target)
    return target:HasTag("player") or target:HasTag("companion") or target:HasTag("abigail")
end

local function IsMyAlly(inst, target)
    if target.components.factionp then
        return (IsPlayerFriendly(target) and target.components.factionp.faction == "none" and inst.components.factionp.faction == "gold") or inst.components.factionp.faction == target.components.factionp.faction
    else
        if inst.components.factionp == TUNING.PIGWARS.FACTIONS.LOYALIST then
            --Loyalists are only hostile to members outside their faction and hostile creatures.
            return not (target:HasTag("hostile") or target:HasTag("monster")) or (target:HasTag("player") or target:HasTag("companion"))
        else
            --Every other faction doesn't like any 'intelligent' creature outside their faction.
            return not (target:HasTag("hostile") or target:HasTag("monster") or target:HasTag("player") or target:HasTag("companion") or target:HasTag("character"))
        end
    end
end

local RETARGET_CANT_TAGS = {"wall", "civilian", "lureplant"}
--TODO commonize this.
local function retargetfn(inst)
    local leader = inst.components.follower.leader or nil
    if leader and leader.components.combat.target and leader.components.combat.target:IsValid() then
        return leader.components.combat.target
    end
    return FindEntity(
                inst,
                target_dist,
                function(guy)
                    return guy ~= leader and not IsMyAlly(inst, guy)
                end,
                {},
                RETARGET_CANT_TAGS
            )
        or nil
end

local function keeptargetfn(inst, target)
    --give up on dead guys, or guys in the dark, or werepigs
    return inst.components.combat:CanTarget(target)
           and (not target.LightWatcher or target.LightWatcher:IsInLight())
           and not (target.sg and target.sg:HasStateTag("transform") )
           and not (target.components.factionp and target.components.factionp.faction == inst.components.factionp.faction)
end

local function ShouldShareTarget(inst, dude)
    if dude.components.factionp then
        return dude.components.factionp.faction == inst.components.factionp.faction
    else
        return false
    end
end

local function OnAttacked(inst, data)
	if data.attacker and data.attacker:IsValid() then
		inst.components.combat:SetTarget(data.attacker)
		inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return ShouldShareTarget(inst, dude) and not dude.components.health:IsDead() end, 30)
    end
end

--------------------------------------------
--Inits
--------------------------------------------

local function common_fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    ------------------------------------
    --Physics and Transform--
    MakeCharacterPhysics(inst, 450, 1.5)
    inst.DynamicShadow:SetSize(4, 2.5)
	inst.Transform:SetFourFaced()
    ------------------------------------
    --AnimState--
    inst.AnimState:SetBank("pig_knight_fat")
    inst.AnimState:SetBuild("pig_knight_fat_loyalist")
    inst.AnimState:PlayAnimation("idle_loop", true)
    ------------------------------------
    --Tags--
    inst:AddTag("character")
    inst:AddTag("pig")
    inst:AddTag("civilized")
    inst:AddTag("breaker")
    inst:AddTag("scarytoprey")
    ------------------------------------
    --Client Components--
    inst:AddComponent("talker")
    inst.components.talker.ontalk = ontalk
    inst.components.talker.fontsize = 35
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0,-600,0)
    inst.talkertype = "pigman_royalguardp"

    inst.sayline = sayline
    ------------------------------------
    inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end
	-----------------------------------
    --Components--
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(unitstats.HEALTH)

    inst:AddComponent("factionp")
    inst.components.factionp:SetFaction("gold")

    inst:AddComponent("combat")
    inst.components.combat:SetRetargetFunction(1, retargetfn)
    inst.components.combat:SetKeepTargetFunction(keeptargetfn)
    inst.components.combat:SetDefaultDamage(10)
    inst.components.combat.hiteffectsymbol = "torso"

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = unitstats.RUN_SPEED --5
    inst.components.locomotor.walkspeed = unitstats.WALK_SPEED --3

    inst:AddComponent("leader")
    inst:AddComponent("follower")

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetSleepTest(NormalShouldSleep)
    inst.components.sleeper:SetWakeTest(DefaultWakeTest)
    inst.components.sleeper:SetResistance(2)

    inst:AddComponent("groundpounder")
	inst.components.groundpounder.destroyer =  MOBFIRE == "Enable"
	inst.components.groundpounder.groundpounddamagemult = 1
	inst.components.groundpounder.damageRings = 0
	inst.components.groundpounder.destructionRings = MOBFIRE == "Enable" and 0 or 2
	inst.components.groundpounder.platformPushingRings = 3
	inst.components.groundpounder.numRings = 3
	inst.components.groundpounder.radiusStepDistance = 2
	inst.components.groundpounder.ringWidth = 1.5

    inst:AddComponent("inventory")
    inst:AddComponent("lootdropper")
    inst:AddComponent("knownlocations")
    inst:AddComponent("trader")
    inst:AddComponent("eater")
    inst:AddComponent("inspectable")
    --------------------------------------
    --Variables--
    inst.sounds = sounds
    inst.type = "normal"

    inst._resists = unitstats.RESISTS
    --------------------------------------
    --Events--
    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("factionchange", OnFactionChanged)
    --------------------------------------
    MakeMediumFreezableCharacter(inst, "torso")
    MakeMediumBurnableCharacter(inst, "torso")
    --------------------------------------
    --Stategraph and Brain--
    inst:SetStateGraph("SGpig_knight_fat")
    inst:SetBrain(brain)
    --------------------------------------

    return inst
end

return Prefab("pig_knight_fat", common_fn, assets, prefabs)
