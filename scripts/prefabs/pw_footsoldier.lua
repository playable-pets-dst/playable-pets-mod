local brain = require("brains/pigsoldierbrain")

local unitstats = TUNING.PIGWARS.UNITS.FOOTSOLDIER

local assets =
{
	Asset("SOUND", "sound/pig.fsb"),
    Asset("ANIM", "anim/townspig_blunderbuss_actions.zip"),

    Asset("ANIM", "anim/pig_footsoldier_blue.zip"),
    Asset("ANIM", "anim/pig_footsoldierp_red.zip"),
    Asset("ANIM", "anim/swap_silver_spear.zip"),
    Asset("ANIM", "anim/swap_loyalist_blunderbuss.zip"),
}

local prefabs =
{
    "meat",
    "poop",
    "tophat",
    "pigskin",
    "halberdp",
    "strawhat",
    "monstermeat",
    --"pigman_shopkeeper_deskp",
}

local sounds = {
    talk = "dontstarve/pig/grunt",
    scream = "dontstarve_DLC003/creatures/city_pig/scream",
    death = "dontstarve_DLC003/creatures/city_pig/death",
    alert = "dontstarve_DLC003/creatures/city_pig/guard_alert",
    armorhit = "dontstarve_DLC003/movement/armour/hit",
    foley = "dontstarve_DLC003/movement/armour/foley",
    sleep = "dontstarve/pig/sleep",
    gun_pre = "pp_sounds/creatures/pig_footsoldier/gun_pre",
    gun_fire = "dontstarve_DLC003/common/items/weapon/blunderbuss_shoot",
}

local MAX_TARGET_SHARES = 5
local SHARE_TARGET_DIST = 30

local target_dist = 17

----------------------------------------------
--Experimental setup for weapons
----------------------------------------------
--This method's quirks:
--Cannot have the weapon be stolen or forced off.
--Switching back to unarmed might be complicated.
local function FireProjectile(inst, target)
    if target and target:IsValid() then
		local pos = inst:GetPosition()
        local posx, posy, posz = inst.Transform:GetWorldPosition()
		local angle = -inst.Transform:GetRotation() * DEGREES
		local offset = 2
		local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
        local spit = SpawnPrefab("blunderbuss_projectile")
		spit.Transform:SetPosition(targetpos.x, 0, targetpos.z)
        --Blunderbuss FX--
        local fx = SpawnPrefab("toadstool_cap_releasefx")
        fx.AnimState:SetSaturation(0)
        fx.Transform:SetPosition(targetpos.x, 2, targetpos.z)
		inst:DoTaskInTime(0, function(inst) spit.components.projectile.owner = inst end)
		spit.components.projectile.owner = inst
		spit.components.projectile:Throw(inst, target)
    end
end

local function GiveWeapon(inst, prefab)
    local wpn = TUNING.PIGWARS.WEAPONS[string.upper(prefab)]
    if not wpn then
        print("Failed giving "..prefab.." weapon to "..inst.prefab)
        return
    end

    inst.components.combat:SetDefaultDamage(wpn.DAMAGE)
    inst.components.combat:SetRange(wpn.ATTACK_RANGE, wpn.HIT_RANGE)
    inst.components.combat:SetAttackPeriod(wpn.ATTACK_PERIOD)
    inst._attack_period = wpn.ATTACK_PERIOD

    inst.AnimState:OverrideSymbol("swap_object", "swap_"..prefab, "swap_"..prefab)
	inst.AnimState:Show("ARM_carry")
	inst.AnimState:Hide("ARM_normal")
end

-----------------------------------------------------
--Talker
-----------------------------------------------------

local function getSpeechType(inst,speech)
    local line = speech.DEFAULT

    if inst.talkertype and speech[inst.talkertype] then
        line = speech[inst.talkertype]
    end

    if type(line) == "table" then
        line = line[math.random(#line)]
    end

    return line
end

local function sayline(inst, line, mood)
	inst.SoundEmitter:PlaySound(inst.sounds.talk)
    inst.components.talker:Say(line, 1.5, nil, true, mood)
end

local function ontalk(inst, script, mood)
    inst.SoundEmitter:PlaySound(inst.sounds.talk)
end

---------------------------------------------------
--Sleeper
---------------------------------------------------
local function NormalShouldSleep(inst)
    --[[
    if inst.components.follower and inst.components.follower.leader then
        local fire = FindEntity(inst, 6, function(ent)
            return ent.components.burnable
                   and ent.components.burnable:IsBurning()
        end, {"campfire"})
        return DefaultSleepTest(inst) and fire and (not inst.LightWatcher or inst.LightWatcher:IsInLight())
    else
        return DefaultSleepTest(inst) 
    end]]
    return false --soldiers don't sleep
end
---------------------------------------------------
--Combat
---------------------------------------------------
local function OnAttackedByDecidRoot(inst, attacker)
    local fn = function(dude) return dude:HasTag("pig") and not dude:HasTag("werepig") and not dude:HasTag("guard") end

    local x,y,z = inst.Transform:GetWorldPosition()
    local ents = nil
    ents = TheSim:FindEntities(x,0,z, SHARE_TARGET_DIST / 2)
    
    if ents then
        local num_helpers = 0
        for k,v in pairs(ents) do
            if v ~= inst and v.components.combat and not (v.components.health and v.components.health:IsDead()) and fn(v) then
                if v:PushEvent("suggest_tree_target", {tree=attacker}) then
                    num_helpers = num_helpers + 1
                end
            end
            if num_helpers >= MAX_TARGET_SHARES then
                break
            end     
        end
    end
end

local function IsMyAlly(inst, target)
    if target.components.pigfaction then
        return inst.components.pigfaction.faction == target.components.pigfaction.faction
    else
        return not (target:HasTag("hostile") or target:HasTag("monster"))
    end
end

local RETARGET_CANT_TAGS = {"wall", "civilian", "lureplant"}
--TODO commonize this.
local function retargetfn(inst)
    local leader = inst.components.follower.leader or nil
    if leader and leader.components.combat.target and leader.components.combat.target:IsValid() then
        return leader.components.combat.target
    end
    return FindEntity(
                inst,
                target_dist,
                function(guy)
                    return guy ~= leader and not IsMyAlly(inst, guy) and not guy:HasTag("player")
                end,
                {},
                RETARGET_CANT_TAGS
            )
        or nil
end

local function keeptargetfn(inst, target)
    --give up on dead guys, or guys in the dark, or werepigs
    return inst.components.combat:CanTarget(target)
           and (not target.LightWatcher or target.LightWatcher:IsInLight())
           and not (target.sg and target.sg:HasStateTag("transform") )
           and not (target.components.pigfaction and target.components.pigfaction.faction == inst.components.pigfaction.faction)
end

local function ShouldShareTarget(inst, dude)
    if dude.components.pigfaction then
        return dude.components.pigfaction.faction == inst.components.pigfaction.faction
    else
        return false
    end
end

local function OnAttacked(inst, data)
	if data.attacker and data.attacker:IsValid() then
		inst.components.combat:SetTarget(data.attacker)
		inst.components.combat:ShareTarget(data.attacker, 30, function(dude) return ShouldShareTarget(inst, dude) and not dude.components.health:IsDead() end, 30)
    end
end

local function common_fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    ------------------------------------
    --Physics and Transform--
    MakeCharacterPhysics(inst, 50, .5)
    inst.Transform:SetFourFaced()
    inst.DynamicShadow:SetSize( 1.5, .75 )
    ------------------------------------
    --AnimState--
    inst.AnimState:SetBank("townspig")
    inst.AnimState:SetBuild("pig_footsoldier_blue")
    inst.AnimState:PlayAnimation("idle_loop", true)
    inst.AnimState:Hide("ARM_carry")

    ------------------------------------
    --Tags--
    inst:AddTag("character")
    inst:AddTag("pig")
    inst:AddTag("civilized")
    inst:AddTag("footsoldier")
    inst:AddTag("scarytoprey")
    ------------------------------------
    --Client Components--
    inst:AddComponent("talker")
    inst.components.talker.ontalk = ontalk
    inst.components.talker.fontsize = 35
    inst.components.talker.font = TALKINGFONT
    inst.components.talker.offset = Vector3(0,-600,0)
    inst.talkertype = "pigman_royalguardp"

    inst.sayline = sayline
    ------------------------------------
    inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end
	-----------------------------------
    --Components--
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(250)

    inst:AddComponent("pigfaction")

    inst:AddComponent("combat")
    inst.components.combat:SetRetargetFunction(1, retargetfn)
    inst.components.combat:SetKeepTargetFunction(keeptargetfn)
    inst.components.combat:SetDefaultDamage(10)
    inst.components.combat.hiteffectsymbol = "torso"

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.runspeed = unitstats.RUN_SPEED --5
    inst.components.locomotor.walkspeed = unitstats.WALK_SPEED --3

    inst:AddComponent("leader")
    inst:AddComponent("follower")

    inst:AddComponent("sleeper")
    inst.components.sleeper:SetSleepTest(NormalShouldSleep)
    inst.components.sleeper:SetWakeTest(DefaultWakeTest)
    inst.components.sleeper:SetResistance(2)

    inst:AddComponent("inventory")
    inst:AddComponent("lootdropper")
    inst:AddComponent("knownlocations")
    inst:AddComponent("trader")
    inst:AddComponent("eater")
    inst:AddComponent("inspectable")
    --------------------------------------
    --Variables--
    inst.sounds = sounds
    inst.type = "normal"
    --------------------------------------
    --Events--
    inst:ListenForEvent("attacked", OnAttacked)
    --------------------------------------
    MakeMediumFreezableCharacter(inst, "torso")
    MakeMediumBurnableCharacter(inst, "torso")
    --------------------------------------
    --Stategraph and Brain--
    inst:SetStateGraph("SGpigsoldierp")
    inst:SetBrain(brain)
    --------------------------------------

    return inst
end

local function blue_fn()
    local inst = common_fn()

	if not TheWorld.ismastersim then
		return inst
	end

    GiveWeapon(inst, "halberd")

    inst.components.pigfaction.faction = "blue"
    
    return inst
end

local function bluegun_fn()
    local inst = common_fn()

    inst:SetPrefabNameOverride("pig_footsoldier_blue")
    inst:AddTag("_blunderbuss")

	if not TheWorld.ismastersim then
		return inst
	end
    inst._ismonster = true

    GiveWeapon(inst, "loyalist_blunderbuss")

    inst.FireProjectile = FireProjectile
    inst.components.combat.ignorehitrange = true

    inst.components.pigfaction.faction = "blue"
    
    return inst
end

local function red_fn()
    local inst = common_fn()

    inst.AnimState:SetBuild("pig_footsoldierp_red")

	if not TheWorld.ismastersim then
		return inst
	end

    inst.components.pigfaction.faction = "red"

    GiveWeapon(inst, "halberd")
    
    return inst
end

local function redgun_fn()
    local inst = common_fn()

    inst:SetPrefabNameOverride("pig_footsoldier_red")
    inst:AddTag("_blunderbuss")

    inst.AnimState:SetBuild("pig_footsoldierp_red")

	if not TheWorld.ismastersim then
		return inst
	end
    inst._ismonster = true

    GiveWeapon(inst, "loyalist_blunderbuss")

    inst.FireProjectile = FireProjectile
    inst.components.combat.ignorehitrange = true

    inst.components.pigfaction.faction = "red"
    
    return inst
end

return Prefab("pig_footsoldier_blue", blue_fn, assets, prefabs),
Prefab("pig_footsoldier_gun_blue", bluegun_fn, assets, prefabs),
Prefab("pig_footsoldier_red", red_fn, assets, prefabs),
Prefab("pig_footsoldier_gun_red", redgun_fn, assets, prefabs)
