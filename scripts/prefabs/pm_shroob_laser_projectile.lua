local assets=
{
	Asset("ANIM", "anim/pm_shroob_laser.zip"),
}

local prefabs = {}

function RotateToTarget(dest)
    local direction = (dest - inst:GetPosition()):GetNormalized()
    local angle = math.acos(direction:Dot(Vector3(1, 0, 0))) / DEGREES
    inst.Transform:SetRotation(angle)
    inst:FacePoint(dest)
end

function OnUpdate(dt)
    local target = target
    if homing and target ~= nil and target:IsValid() and not target:IsInLimbo() then
        dest = target:GetPosition()
    end
    local current = inst:GetPosition()
    if range ~= nil and distsq(start, current) > range * range then
        Miss(target)
    elseif not homing then
        if target ~= nil and target:IsValid() and not target:IsInLimbo() then
            local range = target:GetPhysicsRadius(0) + hitdist
            -- V2C: this is 3D distsq (since combat range checks use 3D distsq as well)
            -- NOTE: used < here, whereas combat uses <=, just to give us tiny bit of room for error =)
            if distsq(current, target:GetPosition()) < range * range then
                Hit(target)
            end
        end
    elseif target ~= nil
        and target:IsValid()
        and not target:IsInLimbo()
        and target.entity:IsVisible()
        and (target.sg == nil or
            not (target.sg:HasStateTag("flight") or
                target.sg:HasStateTag("invisible"))) then
        local range = target:GetPhysicsRadius(0) + hitdist
        -- V2C: this is 3D distsq (since combat range checks use 3D distsq as well)
        -- NOTE: used < here, whereas combat uses <=, just to give us tiny bit of room for error =)
        if distsq(current, target:GetPosition()) < range * range then
            Hit(target)
        else
            local direction = (dest - current):GetNormalized()
            local projectedSpeed = speed * TheSim:GetTickTime() * TheSim:GetTimeScale()
            local projected = current + direction * projectedSpeed
            if direction:Dot(dest - projected) >= 0 then
                RotateToTarget(dest)
            else
                Hit(target)
            end
        end
    elseif owner == nil or
        not owner:IsValid() or
        owner.components.combat == nil or
        inst.components.weapon == nil or
        inst.components.weapon.attackrange == nil then
        -- Lost our target, e.g. bird flew away
        Miss(target)
    else
        -- We have enough info to make our weapon fly to max distance before "missing"
        local range = owner.components.combat.attackrange + inst.components.weapon.attackrange
        if distsq(owner:GetPosition(), current) > range * range then
            Miss(target)
        end
    end
end

local function OnHit(inst, owner, target)
    if target ~= nil and target:IsValid() then
        --target.components.combat:GetAttacked(owner, 50)
    end
	inst.DoTrail:Cancel()
	if inst.type then
		local fx = SpawnPrefab("fireball_projectile_hitp")
		if inst.projectilehue then
			fx.AnimState:SetHue(inst.projectilehue)
		end
		fx.Transform:SetPosition(inst:GetPosition():Get())
		fx:DoTaskInTime(fx.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, fx.Remove)
		inst:Remove()
	else
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_hit")
		inst.AnimState:PlayAnimation("blast")
		inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, inst.Remove)
	end	
end

local function OnMiss(inst)
	inst.DoTrail:Cancel()
	if inst.type and inst.type == "fire" then
		local fx = SpawnPrefab("fireball_projectile_hitp")
		if inst.projectilehue then
			fx.AnimState:SetHue(inst.projectilehue)
		end
		fx.Transform:SetPosition(inst:GetPosition():Get())
		inst:Remove()
	else
		inst.SoundEmitter:PlaySound("dontstarve/impacts/lava_arena/poison_hit")
		inst.AnimState:PlayAnimation("blast")
		inst:DoTaskInTime(inst.AnimState:GetCurrentAnimationLength() + 2 * FRAMES, inst.Remove)
	end	
end

local function OnThrown(inst)
	
end

local function onremove(inst)
	if inst.TrackHeight then
		inst.TrackHeight:Cancel()
		inst.TrackHeight = nil
		inst.trailtask = nil
	end
end

local function SearchTarget(inst)
	local target = FindEntity(inst, inst.range, nil, {"_combat", "_health"}, {"notarget", "playerghost", "invisible"})
	if not inst.hit and target and not target.components.health:IsDead() and target ~= inst.owner then
		target.components.combat:GetAttacked(inst.owner or inst, inst.damage)
		inst.hit = true
		inst:Remove()
	end
end

local function fn()
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("pm_shroob_laser")
	inst.AnimState:SetBuild("pm_shroob_laser")
	inst.AnimState:PlayAnimation("idle_loop", true)
	inst:SetPrefabNameOverride("pm_shroob")

	inst:AddTag("thrown")
	inst:AddTag("projectile")

	inst.AnimState:SetScale(0.8, 0.8)

	inst.AnimState:SetLightOverride(1)
	
	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end

	inst.life_task = inst:DoPeriodicTask(0, SearchTarget)
	inst.DoTrail = inst:DoPeriodicTask(0.1, function()
		local pos = inst:GetPosition()
		
		local trail = SpawnPrefab("shroob_laser_tail")
		trail.Transform:SetPosition(pos.x, pos.y, pos.z)	
		if inst.projectilehue then
			trail.AnimState:SetHue(inst.projectilehue)
			trail.AnimState:SetSaturation(1.3)
		end
		
		inst.Physics:ClearCollisionMask()
		inst.Physics:CollidesWith(COLLISION.CHARACTERS)
	end)

	inst.range = 1
	inst.damage = 50

	inst.Physics:SetMotorVel(20, 1, 0)

	inst:DoTaskInTime(5, inst.Remove)
	
	inst.OnRemoveEntity = onremove

	inst.persists = false

	return inst
end

local function CreateTail2()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)
	
	inst.AnimState:SetBank("pm_shroob_laser")
	inst.AnimState:SetBuild("pm_shroob_laser")
	inst.AnimState:PlayAnimation("trail")
	inst.AnimState:SetFinalOffset(-1)
	inst.AnimState:SetLightOverride(1)
	inst.AnimState:SetScale(0.8, 0.8)
	
	inst.entity:SetPristine()
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false
	
	inst:ListenForEvent("animover", inst.Remove)

	return inst
end

local function chargefn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	
	inst.AnimState:SetBank("pm_shroob_laser")
	inst.AnimState:SetBuild("pm_shroob_laser")
	inst.AnimState:PlayAnimation("charge")
	inst.AnimState:PushAnimation("charge_loop", false)

	--inst.AnimState:SetFinalOffset(-1)
	inst.AnimState:SetLightOverride(1)
	inst.AnimState:SetScale(1.5, 1.5)

	inst.AnimState:SetDeltaTimeMultiplier(3)
	
	inst.entity:SetPristine()
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.persists = false

	--inst:DoTaskInTime(5, inst.Remove)
	
	inst:ListenForEvent("animqueueover", inst.Remove)

	return inst
end

local function fn_fire_hit()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

	inst.AnimState:SetBank("fireball_fx")
	inst.AnimState:SetBuild("deer_fire_charge")
	inst.AnimState:PlayAnimation("blast")

	inst.AnimState:SetLightOverride(1)
	
	inst.entity:SetPristine()

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst.OnRemoveEntity = onremove

	inst:DoTaskInTime(2, function(inst) inst:Remove() end)
	
	inst.persists = false

	return inst
end

return Prefab("pm_shroob_laser_projectile", fn, assets, prefabs),
Prefab("pm_shroob_laser_hit", fn_fire_hit, assets, prefabs),
Prefab("shroob_laser_tail", CreateTail2),
Prefab("shroob_laser_charge", chargefn)