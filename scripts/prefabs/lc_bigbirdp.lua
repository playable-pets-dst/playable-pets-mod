local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "lc_bigbirdp"

local assets = 
{
	Asset("ANIM", "anim/lc_bigbird.zip"),
}

local getskins = {}

local prefabs = 
{	

}

local tuning_values = TUNING.PP.LC_BIGBIRDP
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.RUNSPEED,
	walkspeed = tuning_values.WALKSPEED,
	damage = tuning_values.DAMAGE,
	range = tuning_values.RANGE,
	hit_range = tuning_values.ATTACK_RANGE,
	attackperiod = tuning_values.ATTACK_PERIOD,
	bank = "lc_bigbird",
	build = "lc_bigbird",
	scale = 1,
	stategraph = "SGlc_bigbirdp",
	minimap = "lc_bigbirdp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
	{'monstermeat',             1.00},
	{'monstermeat',             1.00},
})

local FORGE_STATS = PPM_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local soundpath = "lobcorp/creatures/bigbird/"
local sounds =
{
    lure = soundpath.."lure",
	step = soundpath.."step",
	attack = soundpath.."attack_fast",
	attack_hit = soundpath.."damage",
	attack2 = soundpath.."attack",
	attack2_hit = soundpath.."bigdamage",
}

local function OnHitOther(inst, data)
	if inst.sg:HasStateTag("biting") then
		inst.SoundEmitter:PlaySound(inst.sounds.attack_hit)
	elseif inst.sg:HasStateTag("bigbiting") then
		inst.SoundEmitter:PlaySound(inst.sounds.attack2_hit)
	end
	if inst.sg:HasStateTag("biting") or inst.sg:HasStateTag("bigbiting") then
		if data.target and data.target:IsValid() and data.target.components.sanity and data.damage then
			data.target.components.sanity:DoDelta(data.damage/2, false)
		end
	end
end

local function CalcSanityAura(inst, observer)
    return (inst.components.health ~= nil and not inst.components.health:IsDead() and -TUNING.SANITYAURA_LARGE)
        or 0
end

--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		
	end
end

local function OnSave(inst, data)
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("lobcorp_mob")
	inst:AddTag("bigbird")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Variables			
	inst.isshiny = 0
	
	inst.mobsleep = true
	inst.taunt = true
	inst.shouldwalk = true

	inst.lightcolour = {255/255, 221/255, 0}
	inst.lightcolour_enraged = {1, 0, 0}
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.AnimState:Hide("totallyflameholder")
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
    inst.components.burnable:SetOnBurntFn(nil)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 9999) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater.strongstomach = true
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, 1)
    inst.DynamicShadow:SetSize(3, 2)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	inst:ListenForEvent("onhitother", OnHitOther)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	inst.entity:AddLight()
	inst.Light:SetIntensity(0.7)
	inst.Light:SetRadius(2.2)
	inst.Light:SetFalloff(0.7)
	inst.Light:Enable(true)
	inst.Light:SetColour(unpack(inst.lightcolour))
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
		inst.Light:SetIntensity(0.7)
		inst.Light:SetRadius(2.2)
		inst.Light:SetFalloff(0.7)
		inst.Light:Enable(true)
		inst.Light:SetColour(unpack(inst.lightcolour))
    end)
	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
	--PlayablePets.SetCommonSaveAndLoad(inst, OnSave, OnLoad)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
