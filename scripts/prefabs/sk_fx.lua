
local prefabs = 
{
}

local assets =
{
    Asset("ANIM", "anim/sk_enemy_shield.zip"),
	Asset("ANIM", "anim/sk_rez_circle.zip"),
	
	Asset("ANIM", "anim/lavaarena_hit_sparks_fx.zip"),
	Asset("ANIM", "anim/sk_bite_fx.zip"),
	
	Asset("ANIM", "anim/sk_static_spark.zip"),
	-----Status-----
	Asset("ANIM", "anim/status_text.zip"),
	
	Asset("ANIM", "anim/status_curse_fx.zip"),
	Asset("ANIM", "anim/sk_curse_hit.zip"),
	
	Asset("ANIM", "anim/mossling_spin_fx.zip"),
	Asset("ANIM", "anim/lavaarena_hammer_attack_fx.zip"),
	Asset("ANIM", "anim/sk_shock_fx.zip"),
	
	Asset("ANIM", "anim/poison.zip"),
	Asset("ANIM", "anim/fire.zip"),
	---
	Asset("ANIM", "anim/sk_curse_aura.zip"),
}

local function DoFadeOut(inst)
	inst.components.colourtweener:StartTween({0, 0, 0, 0}, inst.duration, inst.Remove)
end

local function DoFadeOut(inst)
	inst.components.colourtweener:StartTween({0, 0, 0, 0}, inst.duration, inst.Remove)
end

local function OnSave(inst, data)
	data.rot = inst.Transform:GetRotation()
end

local function OnLoad(inst, data)
	if data then
		inst.Transform:SetRotation(data.rot)
	end
end

--MakeFX(name, bank, build, anim, animloop, loop, sound, isflat)
local function MakeFX(name, data)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddNetwork()

        inst.AnimState:SetBank(data.bank)
        inst.AnimState:SetBuild(data.build)
        inst.AnimState:PlayAnimation(data.anim)
		if data.animloop then
			inst.AnimState:PushAnimation(data.animloop, true)
		end
		if data.foffset then
			inst.AnimState:SetFinalOffset(2)
		end
		if data.variations then
			inst.AnimState:PlayAnimation(math.random(1, data.variations))
		end
		if data.multcolour then
			inst.AnimState:SetMultColour(data.multcolour[1], data.multcolour[2], data.multcolour[3], data.multcolour[4])
		end
		if data.bloom then
			inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
		end
		if data.scale then
			inst.AnimState:SetScale(data.scale, data.scale)
		end
		if data.layer then
			inst.AnimState:SetLayer(data.layer)
		end

        inst:AddTag("FX")
		inst:AddTag("NOCLICK")
		
		if data.faced then
			if data.faced == 4 then
				inst.Transform:SetFourFaced()
			elseif data.faced == 6 then
				inst.Transform:SetSixFaced()
			elseif data.faced == 8 then
				inst.Transform:SetEightFaced()
			else
				inst.Transform:SetTwoFaced()
			end
		end
		
		if data.order then
			inst.AnimState:SetSortOrder(data.order)
		end
		
		if data.lightoverride then
			inst.AnimState:SetLightOverride(data.lightoverride)
		end
		
		if data.isflat then
			--ideally we want them to just be under characters and above everything else
			inst.AnimState:SetRayTestOnBB(true)
			inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
			inst.AnimState:SetLayer(LAYER_BACKGROUND)
			inst.AnimState:SetSortOrder(3)
		end

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
		
		if not data.persists then
			inst.persists = false
		end
		
		if data.random_dir then
			inst.Transform:SetRotation(math.random(1, 360))
		end
		
		if not data.loop then --Bad name, TODO rename this. Anything with this variable is killed outside of this.
			inst:DoTaskInTime(1, inst.Remove)
			inst:ListenForEvent("animqueueover", inst.Remove)
		end
		
		if data.fadeout and data.duration then
			inst.duration = data.duration
			inst:AddComponent("colourtweener")
			inst:ListenForEvent("animqueueover", DoFadeOut)
		end
		
		inst.OnSave = OnSave
		inst.OnLoad = OnLoad

        return inst
    end

    return Prefab(name.."p", fn, assets)
end

return MakeFX("sk_enemy_shield", {bank = "sk_enemy_shield", build = "sk_enemy_shield", anim = "front", loop = true, isflat = true, lightoverride = 1}),
MakeFX("sk_rez_circle", {bank = "sk_rez_circle", build = "sk_rez_circle", anim = "spawn", loop = true, isflat = true, lightoverride = 1}),
MakeFX("sk_hit_sparks", {bank = "hit_sparks", build = "lavaarena_hit_sparks_fx", anim = "hit_3"}),
MakeFX("sk_poison_fx", {bank = "poison", build = "poison",  anim = "level1_pre", animloop = "level1_loop", loop = true}),
MakeFX("sk_curse_hit", {bank = "sk_status_hit", build = "sk_curse_hit", anim = "idle"}),
MakeFX("sk_curse_fx", {bank = "status_curse_fx", build = "status_curse_fx", anim = "start"}),
MakeFX("sk_shock_fx", {bank = "lavaarena_hammer_attack_fx", build = "sk_shock_fx", anim = "crackle_loop"}),
MakeFX("sk_fire_fx", {bank = "fire", build = "fire", anim = "level4", animloop = "level4", loop = true}),
MakeFX("sk_status_text", {bank = "status_text", build = "status_text", anim = "debuff"}),
MakeFX("sk_bite_fx", {bank = "sk_bite_fx", build = "sk_bite_fx", anim = "idle_loop", faced = 4, loop = true, fadeout = true, duration = 0.5, lightoverride = 1}),
MakeFX("sk_static_spark", {bank = "sk_static_spark", build = "sk_static_spark", anim = "1", loop = true, fadeout = true, duration = 0.5, lightoverride = 1}),
MakeFX("sk_lantern_light", {bank = "sk_lantern_light", build = "sk_lantern_light", anim = "idle_loop", animloop = "idle_loop", loop = true, lightoverride = 1, bloom = true, foffset = true}),
MakeFX("sk_gravestone_cracks", {bank = "sk_gravestone", build = "sk_gravestone", anim = "cracks1", animloop = "crack1", loop = true, lightoverride = 1, bloom = true}),
MakeFX("sk_graveyard_path", {bank = "sk_graveyard_path", build = "sk_graveyard_path", anim = "1", scale = 1.2, loop = true, isflat = true, variations = 4, persists = true, multcolour = {125/255,143/255,150/255, 1}}),
MakeFX("sk_bullet_explode", {bank = "sk_bullet_fx", build = "sk_bullet_fx", anim = "explode", scale = 3, lightoverride = 1}),
MakeFX("sk_bullet_trail", {bank = "sk_bullet_fx", build = "sk_bullet_fx", anim = "trail", scale = 3, lightoverride = 1, order = -1})
