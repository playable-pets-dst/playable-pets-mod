local assets = {
	Asset("ANIM", "anim/sk_curse_aura.zip"),
	Asset("ANIM", "anim/moonstorm_lightningstrike.zip"),
	Asset("ANIM", "anim/moonstorm_groundlight.zip"),
}

local prefabs = {}

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","kat", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	end
end

local LIGHTNING_MAX_DIST_SQ = 140*140

local function PlayThunderSound(lighting)
	if not lighting:IsValid() or TheFocalPoint == nil then
		return
	end

    local pos = Vector3(lighting.Transform:GetWorldPosition())
    local pos0 = Vector3(TheFocalPoint.Transform:GetWorldPosition())
   	local diff = pos - pos0
    local distsq = diff:LengthSq()

	local k = math.max(0, math.min(1, distsq / LIGHTNING_MAX_DIST_SQ))
	local intensity = math.min(1, k * 1.1 * (k - 2) + 1.1)
	if intensity <= 0 then
		return
	end

    local minsounddist = 10
    local normpos = pos
   	if distsq > minsounddist * minsounddist then
       	--Sound needs to be played closer to us if lightning is too far from player
        local normdiff = diff * (minsounddist / math.sqrt(distsq))
   	    normpos = pos0 + normdiff
    end

    local inst = CreateEntity()

    --[[Non-networked entity]]

    inst.entity:AddTransform()
    inst.entity:AddSoundEmitter()

    inst.Transform:SetPosition(normpos:Get())
    inst.SoundEmitter:PlaySound("dontstarve/rain/thunder_close", nil, intensity, true)

    inst:Remove()
end

local function StartFX(inst)
	for i, v in ipairs(AllPlayers) do
		local distSq = v:GetDistanceSqToInst(inst)
		local k = math.max(0, math.min(1, distSq / LIGHTNING_MAX_DIST_SQ))
		local intensity = -(k-1)*(k-1)*(k-1)				--k * 0.8 * (k - 2) + 0.8

		--print("StartFX", k, intensity)
		if intensity > 0 then
			v:ScreenFlash(intensity <= 0.05 and 0.05 or intensity)
			v:ShakeCamera(CAMERASHAKE.FULL, .7, .02, intensity / 3)
		end
	end
end

local function lightningfn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst.entity:AddSoundEmitter()

    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetLightOverride(1)
    inst.AnimState:SetBank("Moonstorm_LightningStrike")
    inst.AnimState:SetBuild("moonstorm_lightningstrike")
    inst.AnimState:PlayAnimation("strike")
	
	inst.AnimState:SetMultColour(180/255, 0/255, 52/255, 1)

    inst.SoundEmitter:PlaySound("dontstarve/rain/thunder_close", nil, nil, true)

    inst:AddTag("FX")

    --Dedicated server does not need to spawn the local sfx
    if not TheNet:IsDedicated() then
		inst:DoTaskInTime(0, PlayThunderSound)
	end

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:DoTaskInTime(0, StartFX) -- so we can use the position to affect the screen flash

    inst.entity:SetCanSleep(false)
    inst.persists = false
    inst:DoTaskInTime(.5, inst.Remove)

    return inst
end

local ENDPOINTS = 5

local function convertnodelist(data)
    local newdata = {}
    for i,entry in ipairs(data)do
        newdata[entry] = true
    end
    return newdata
end

local function checkspawn(inst)

    if not ThePlayer then
        return
    end

    local pos = Vector3(inst.Transform:GetWorldPosition())

    local radius = 8
    local anglemod = 0

    if inst.anglemod then
        anglemod = inst.anglemod
    else
        anglemod = (math.random()*40 -20) *DEGREES
    end

    local angle = (inst.Transform:GetRotation() * DEGREES) + (PI/2) + anglemod
    local newpos = Vector3(pos.x + math.cos(angle) * radius, 0, pos.z - math.sin(angle) * radius)

    if not TheWorld.Map:IsVisualGroundAtPoint(newpos.x, 0, newpos.z) then
        return false
    end

    local dist = ThePlayer:GetDistanceSqToPoint(newpos.x, 0, newpos.z)

    local node_index = TheWorld.Map:GetNodeIdAtPoint(newpos.x, 0, newpos.z)
    local nodes = TheWorld.net.components.moonstorms._moonstorm_nodes:value()

    local test = false
    for i, node in pairs(nodes) do
        if node == node_index then
            test = true
            break
        end
    end
    if dist < 30*30 and test then
        local newfx = SpawnPrefab("sk_marg_lightning_ground_fx")
        newfx.Transform:SetPosition(newpos.x,newpos.y,newpos.z)
        newfx.Transform:SetRotation(inst.Transform:GetRotation() + (anglemod/DEGREES))
        newfx.anglemod = anglemod
    end

end

local function groundfn()
    local inst = CreateEntity()

	inst.entity:AddNetwork()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()

    inst.AnimState:SetBuild("moonstorm_groundlight")
    inst.AnimState:SetBank("moonstorm_groundlight")
    local anim = math.random() < 0.5 and "strike" or "strike2"
	
	inst.AnimState:SetMultColour(180/255, 0/255, 52/255, 1)

    inst.AnimState:PlayAnimation(anim)
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)

    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    -- From watersource component
    inst:AddTag("fx")
    inst:AddTag("NOCLICK")
	
	if not TheNet:IsDedicated() then
		return inst
	end

    inst.entity:SetPristine()
	inst.Transform:SetRotation(math.random()*360)

    inst:DoTaskInTime(13*FRAMES,function() checkspawn(inst) end)
    inst:ListenForEvent("animover", function() inst:Remove() end)

    inst.SoundEmitter:PlaySound("moonstorm/common/moonstorm/electricity")

    inst.persists = false

    return inst
end

local function bullet_spawnfn()
    local inst = CreateEntity()

	inst.entity:AddNetwork()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()

    inst.AnimState:SetBuild("sk_bullet_fx")
    inst.AnimState:SetBank("sk_bullet_fx")
	
	inst.AnimState:SetScale(2.2, 2.2)
	
	inst.AnimState:SetMultColour(180/255, 0/255, 52/255, 1)

    inst.AnimState:PlayAnimation("spawn")

    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    -- From watersource component
    inst:AddTag("fx")
    inst:AddTag("NOCLICK")
	
	if not TheNet:IsDedicated() then
		return inst
	end

    inst.entity:SetPristine()
	inst.Transform:SetRotation(math.random()*360)

    inst.persists = false

    return inst
end

return Prefab("sk_marg_lightning", lightningfn, assets, prefabs),
Prefab("sk_marg_lightning_ground_fx", groundfn, assets, prefabs),
Prefab("sk_marg_bullet_spawner", bullet_spawnfn, assets, prefabs)