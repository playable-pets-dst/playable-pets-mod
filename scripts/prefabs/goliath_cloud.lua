
local assets =
{
    Asset("ANIM", "anim/goliath_cloud.zip"),
}

local prefabs =
{
    
}

local sounds =
{
	
}

local function common_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.AnimState:SetBank("sporecloud")
    inst.AnimState:SetBuild("goliath_cloud")
    inst.AnimState:PlayAnimation("sporecloud_pre", false)
    inst.AnimState:PushAnimation("sporecloud_loop", false)
    inst.AnimState:PushAnimation("sporecloud_pst", false)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:ListenForEvent("animqueueover", inst.Remove)

    return inst
end

return Prefab("goliath_cloudp", common_fn, assets, prefabs)
