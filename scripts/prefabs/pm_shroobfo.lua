local assets=
{
	Asset("ANIM", "anim/pm_shroobfo.zip"),
    Asset("ANIM", "anim/pm_shroobfo_build.zip")
}

local prefabs =
{

}

local function OnWorkFinished(inst, worker)
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    local fx = SpawnPrefab("collapse_big")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    --inst.components.lootdropper:DropLoot()
    inst:Remove()
end

local function OnWorked(inst, worker)
    if not inst:HasTag("burnt") then
        --inst.AnimState:PlayAnimation("hit")
        --inst.AnimState:PushAnimation("mound_idle", true)
    end
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
    inst.entity:AddDynamicShadow()

    MakeObstaclePhysics(inst, 1)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetEightFaced()

	inst.AnimState:SetBank("pm_shroobfo")
	inst.AnimState:SetBuild("pm_shroobfo_build")
	inst.AnimState:PlayAnimation("inactive", true)

	inst:AddTag("shroob")

	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("lootdropper")
	
	inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetMaxWork(4)
    inst.components.workable:SetWorkLeft(4)
    inst.components.workable:SetOnFinishCallback(OnWorkFinished)
    inst.components.workable:SetOnWorkCallback(OnWorked)

	return inst
end

return Prefab("pm_shroobfo", fn, assets, prefabs)
