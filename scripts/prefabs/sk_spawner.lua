--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_zombie.zip"),
	Asset("ANIM", "anim/sk_zombie_dust.zip"),
	Asset("ANIM", "anim/sk_zombie_freeze.zip"),
	Asset("ANIM", "anim/sk_zombie_shock.zip"),
	Asset("ANIM", "anim/sk_zombie_poison.zip"),
	Asset("ANIM", "anim/sk_zombie_fire.zip"),
	Asset("ANIM", "anim/sk_zombie_curse.zip"),
	-------
	Asset("ANIM", "anim/sk_bombie_dust.zip"),
	Asset("ANIM", "anim/sk_bombie_freeze.zip"),
	Asset("ANIM", "anim/sk_bombie_shock.zip"),
	Asset("ANIM", "anim/sk_bombie_poison.zip"),
	Asset("ANIM", "anim/sk_bombie_fire.zip"),
}

local prefabs =
{
    
}

local spring_variants =
{
	"_poison",
	"_shock",
}

local summer_variants =
{
	"_fire",
}

local autumn_variants =
{
	"",
	"_poison",
}

local winter_variants =
{
	"_freeze",
}

local function OnSeasonChange(inst)
	inst:DoTaskInTime(0, function(inst)
		local childspawner = inst.components.childspawner
		if TheWorld.state.isspring then
			childspawner.child = childspawner.child..""..spring_variants[math.random(1, #spring_variants)]
			if childspawner.rarechild then
				childspawner.rarechild = childspawner.rarechild..""..spring_variants[math.random(1, #spring_variants)]
			end
		elseif TheWorld.state.issummer then
			childspawner.child = childspawner.child..""..summer_variants[math.random(1, #summer_variants)]
			if childspawner.rarechild then
				childspawner.rarechild = childspawner.rarechild..""..summer_variants[math.random(1, #summer_variants)]
			end
		elseif TheWorld.state.isautumn then
			childspawner.child = childspawner.child..""..autumn_variants[math.random(1, #autumn_variants)]
			if childspawner.rarechild then
				childspawner.rarechild = childspawner.rarechild..""..autumn_variants[math.random(1, #autumn_variants)]
			end		
		elseif TheWorld.state.iswinter then
			childspawner.child = childspawner.child..""..winter_variants[math.random(1, #winter_variants)]
			if childspawner.rarechild then
				childspawner.rarechild = childspawner.rarechild..""..winter_variants[math.random(1, #winter_variants)]
			end
		end	
	end)
	
end

local function MakeSpawner(name, data)
	local function fn()
		local inst = CreateEntity()
		inst.entity:AddTransform()
		inst.entity:AddSoundEmitter()
		inst.entity:AddNetwork()

		inst:AddTag("spawner")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end
	
		inst:AddComponent("childspawner")
		inst.components.childspawner.child = data.child or "sk_zombie"
		inst.components.childspawner.rarechild = data.rarechild or nil
		inst.components.childspawner.rarechildchance = data.rarechildchance or 0.1
		inst.components.childspawner.maxchildren = data.max or 3
		inst.components.childspawner.regenperiod = data.regenperiod or 480*3
		inst.components.childspawner.spawnradius = data.radius or 0

		if data.seasonal then
			inst:WatchWorldState("season", OnSeasonChange)
		end
		
		if data.automatic then
			inst.components.childspawner.spawning = true
			inst.components.childspawner.spawnperiod = data.spawnperiod
			inst.components.childspawner.spawnvariance = data.spawnvariance
			inst.components.childspawner.spawnoffscreen = data.offscreen or false
		else
			inst:AddComponent("playerprox")
			
		end
		
		return inst
	end
	return Prefab(name, fn, assets, prefabs)
end

return MakeSpawner("sk_zombiespawner", {child = "sk_zombie", rarechild = "sk_bombie", rarechildchance = 0.1, radius = 0, max = 1, regenperiod = 480*3, seasonal = true})
