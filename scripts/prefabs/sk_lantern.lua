--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_lantern.zip"),
	Asset("ANIM", "anim/sk_lantern_light.zip"),
}

local prefabs =
{
    
}

local sounds =
{
	
}

SetSharedLootTable('sk_lantern',
{
    {"rocks",   			 1.00},
	{"rocks",   			 1.00},
	{"rocks",   			 1.00},
})

local function ChangeColours(inst)
	if inst.colour then
		inst.AnimState:SetMultColour(inst.colour[1], inst.colour[2], inst.colour[3], inst.colour[4])
	end
	if inst.light_colour then
		inst.Light:SetColour(inst.light_colour[1], inst.light_colour[2], inst.light_colour[3])
	end
	if inst.fx and inst.light_colour then
		inst.fx.AnimState:SetMultColour(inst.light_colour[1], inst.light_colour[2], inst.light_colour[3], 1)
	end
end

local function SetColours(inst, colour, light_colour)
	inst.colour = colour or {125/255,143/255,150/255, 1}
	inst.light_colour = light_colour or {147/255, 255/255, 64/255}
	ChangeColours(inst)
end

local function SetRandomColour(inst)
	inst.colour = {math.random(), math.random(), math.random(), 1}
	inst.light_colour = {math.random(), math.random(), math.random()}
	ChangeColours(inst)
end

local function OnPhaseChanged(inst)
	if TheWorld.state.isday	and not TheWorld:HasTag("caves") then
		if inst.fx then
			inst.fx:Remove()
			inst.fx = nil
		end
		inst.Light:Enable(false)
	else
		inst.Light:Enable(true)
		if not inst.fx then
			inst.fx = SpawnPrefab("sk_lantern_lightp")
			inst.fx.entity:SetParent(inst.entity)
			inst.fx.entity:AddFollower()
			inst.fx.Follower:FollowSymbol(inst.GUID, "swap_light", 0, -10, 0)
			inst.fx.Transform:SetPosition(0, 0, 0)
			if inst.light_colour then
				inst.fx.AnimState:SetMultColour(inst.light_colour[1], inst.light_colour[2], inst.light_colour[3], 1)
			end
		end
	end
end

local function OnSave(inst, data)
	data.colour = inst.colour or {1, 1, 1, 1}
	data.light_colour = inst.light_colour or {1, 1, 1}
end

local function OnLoad(inst, data)
	if data then
		inst.colour = data.colour
		inst.light_colour = data.light_colour
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 1)

    --inst.Transform:SetOneFaced()
	local scale = 1
	inst.Transform:SetScale(scale, scale, scale)

    inst:AddTag("structure")
	inst:AddTag("sk_lantern")
	inst:AddTag("noclick")
	
	inst.AnimState:Hide("swap_light")

    inst.AnimState:SetBank("sk_lantern")
    inst.AnimState:SetBuild("sk_lantern")
    inst.AnimState:PlayAnimation("idle_loop", true)
	
	inst.Light:Enable(false)
	inst.Light:SetRadius(3)
	inst.Light:SetFalloff(1.6)
	inst.Light:SetIntensity(0.25)
	inst.Light:SetColour(1, 1, 1)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("inspectable")
	
	SetColours(inst)
	inst:DoTaskInTime(0, ChangeColours)
	
	inst.SetColours = SetColours
	inst.SetRandomColour = SetRandomColour
	
	inst:WatchWorldState("isday", OnPhaseChanged)
  	inst:WatchWorldState("isdusk", OnPhaseChanged)
	inst:WatchWorldState("isnight", OnPhaseChanged)
	inst:DoTaskInTime(0, function(inst)
		OnPhaseChanged(inst)
	end)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("sk_lantern", fn, assets, prefabs)
