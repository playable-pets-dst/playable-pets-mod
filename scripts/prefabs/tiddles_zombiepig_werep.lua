local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "tiddles_zombiepig_werep"

local assets = 
{
	Asset("ANIM", "anim/tiddle_pigzombie.zip"),
    Asset("ANIM", "anim/tiddle_pigzombie_fat.zip"),
    Asset("ANIM", "anim/tiddle_werepigzombie.zip"),
    Asset("ANIM", "anim/pig_tiddled.zip"),
}

local getskins = {}

local prefabs = 
{	

}

local tuning_values = TUNING.TIDDLE_PIGZOMBIE_WERE
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.runspeed,
	walkspeed = tuning_values.walkspeed,
	damage = tuning_values.damage,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "pigman",
	build = "tiddle_werepigzombie",
	scale = 1,
	--build2 = "chester_shadow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGtiddles_zombiepigp",
	minimap = "tiddles_pigzombiep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
})

local FORGE_STATS = PPM_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local sounds =
{
    scream = "dontstarve/pig/scream",
    hit = "dontstarve/quagmire/creature/swamppig_elder/talk",
    attack = "dontstarve/quagmire/creature/swamp_pig/talk",
    death = "dontstarve/quagmire/creature/swamppig_elder/sleep_in",
    revive = "dontstarve/quagmire/creature/swamp_pig/talk",
}

local weresounds =
{
    scream = "dontstarve/creatures/werepig/howl",
    hit = "dontstarve/creatures/werepig/hurt",
    attack = "dontstarve/creatures/werepig/attack",
    death = "dontstarve/creatures/werepig/grunt",
    revive = "dontstarve/creatures/werepig/grunt",
}

local function UpdateSpeed(inst)
    if TheWorld.state.phase == "day" then
		inst.components.locomotor.runspeed = tuning_values.runspeed
    else
        inst.components.locomotor.runspeed = tuning_values.nightrunspeed
    end
end

local function OnAttacked(inst, data)
    if data.attacker ~= nil and not data.attacker:HasTag("tiddleplaguebearer") then
    	inst.components.combat:SetTarget(data.attacker)
    	inst.components.combat:ShareTarget(data.attacker, 30, function(dude)
            return dude:HasTag("tiddlezombie")
                and not dude.components.health:IsDead()
    	    end, 10)
    end
end

local function spread(inst, data)
    local target = data.target
	if inst.components.tiddlespreader.enabled then
		if target ~= nil and target.components.health ~= nil and not target:HasTag("TiddleVirus") and not inst:HasTag("tiddleswept") then
			if target.components.tiddlevirus == nil then
				target:AddComponent("tiddlevirus")
			end
			target.components.tiddlevirus:PushVirus(TUNING.TIDDLEVIRUS_LARGE, "physical")
		end
	else
		if math.random() <= 0.2 then
			PlayablePets.SetPoison(inst, data.target)
		end
	end
end

local function BecomeAlive(inst)
--    SpawnPrefab("slurper_respawn").Transform:SetPosition(inst.Transform:GetWorldPosition())
--    SpawnPrefab("pandorachest_reset").Transform:SetPosition(inst.Transform:GetWorldPosition())
    inst:RemoveComponent("burnable")
    inst:RemoveComponent("propagator")
    inst.components.timer:StopTimer("decay")
    MakeMediumBurnableCharacter(inst, "pig_torso")
    inst.components.burnable.flammability = TUNING.SPIDER_FLAMMABILITY
    inst.revived = true
end

local function Erode(inst)
    if MOBGHOST == "Enable" then
        inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
	else
		TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
	end
end

local function onignight(inst)
	DefaultBurnFn(inst)
	inst.components.timer:StopTimer("decay")
end

local function onextinguish(inst)
    DefaultExtinguishFn(inst)
    Erode(inst)
end

local function BecomeCorpse(inst)
    if not (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        inst:RemoveComponent("burnable")
        inst:RemoveComponent("propagator")

    	MakeMediumBurnable(inst, TUNING.MED_BURNTIME)
    	inst.components.burnable:SetOnIgniteFn(onignight)
    	inst.components.burnable:SetOnExtinguishFn(onextinguish)
        inst.components.burnable:SetOnBurntFn(nil)
    	MakeSmallPropagator(inst)
    	inst.components.timer:StartTimer("decay", TUNING.TIDDLE_PIGZOMBIE_DECAYDELAY + math.random())
    else
        inst.components.burnable:SetOnBurntFn(onextinguish)
    end
end

local function CalcSanityAura(inst, observer)
    return (inst.components.health ~= nil and not inst.components.health:IsDead() and -TUNING.SANITYAURA_LARGE)
        or 0
end

--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.isalt = data.isalt or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
    inst:AddTag("houndfriend")
    inst:AddTag("soulless")
    --inst:AddTag("scarytoprey")
	inst:AddTag("were")
    inst:AddTag("monster")
    inst:AddTag("tiddlezombie")
	inst:AddTag("tiddlevirusimmune")
    inst:AddTag("tiddleplaguebearer")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = weresounds
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Variables			
	inst.isshiny = 0
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.Erode = Erode
	inst.BecomeAlive = BecomeAlive
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.burnable.flammability = TUNING.SPIDER_FLAMMABILITY
    inst.components.burnable:SetOnBurntFn(nil)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 0) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, 'tiddlezombiepig') --inst, prefaboverride
	--inst:AddComponent("sanityaura")
	--inst.components.sanityaura.aura = CalcSanityAura
	
	inst:AddComponent("tiddlespreader")
	inst.components.tiddlespreader.noflies = true
	inst.components.tiddlespreader.spreadrange = TUNING.TIDDLEVIRUS_RANGESMALL
	
	inst:AddComponent("timer")
	inst:ListenForEvent("timerdone", Erode)
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater.strongstomach = true
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.75, 0.75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	inst:ListenForEvent("equip", function(inst) inst.AnimState:Hide("swap_object") end)
	
	inst:ListenForEvent("onhitother", spread)
    inst:ListenForEvent("attacked", OnAttacked)
	
	inst:ListenForEvent("death", BecomeCorpse)
    inst:ListenForEvent("tiddlerevived", BecomeAlive)
	
	inst:WatchWorldState("phase", UpdateSpeed)
	UpdateSpeed(inst, TheWorld.state.phase)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
