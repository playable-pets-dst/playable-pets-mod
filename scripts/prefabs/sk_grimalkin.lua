--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_grimalkin.zip"),
	Asset("ANIM", "anim/sk_grimalkin_nrm.zip"),
}

local prefabs =
{
    
}

local brain = require("brains/sk_zombiebrain")

SetSharedLootTable('grimalkin',
{
    
})

local sounds =
{
	--grunt = "sk/creatures/grimalkin/growl",
	moan = "sk/creatures/grimalkin/moan",
    attack = "sk/creatures/grimalkin/bite",
	spawn = "sk/creatures/grimalkin/spawn",
}

local t_values = TUNING.SK.GRIMALKIN

local function GetExcludeTags(inst)
	if TheNet:GetServerGameMode() == "lavaarena" or inst:HasTag("companion") or (inst.components.follower.leader and inst.components.follower.leader:HasTag("player") and not TheNet:GetPVPEnabled()) then
		return	{ "wall", "player", "companion", "_isinheals", "smallcreature", "insect"}
	else
		return { "wall", "zombie", "structure", "sk_enemy", "smallcreature", "insect", "undead" }
	end
end


local function retargetfn(inst)
    local leader = inst.components.follower.leader
    if leader and leader.components.combat and not leader.components.combat.target then
		return 
	elseif leader and leader.components.combat and leader.components.combat.target then
		return leader.components.combat.target
	end
	return FindEntity(
                inst,
                t_values.TARGET_RANGE,
                function(guy)
                    return inst.components.combat:CanTarget(guy) and guy ~= leader
                end,
                nil,
                GetExcludeTags(inst)
            )
end

local function KeepTarget(inst, target)
    local leader = inst.components.follower.leader
    return inst.components.combat:CanTarget(target)
		and target ~= leader
        and not (TheNet:GetServerGameMode() == "lavaarena" and target:HasTag("_isinheals"))
end

local function OnSave(inst, data)

end

local function OnLoad(inst, data)

end

local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		if inst.status and inst.status == "freeze" and math.random() <= t_values.INFLICT_CHANCE then
			if other.components.freezable then
				other.components.freezable:Freeze(8)
			end
		elseif inst.status then
			if inst.status == "curse" then
				if not (inst.components.follower.leader and inst.components.follower.leader:HasTag("player") and not SK_CURSE_ENABLE) and math.random(1, 10) >= 5 then
					PlayablePets.InflictStatus(inst, other, inst.status, t_values.INFLICT_CHANCE)
				else
					PlayablePets.InflictStatus(inst, other, "fire", t_values.INFLICT_CHANCE)
				end
			else
				PlayablePets.InflictStatus(inst, other, inst.status, t_values.INFLICT_CHANCE)
			end
		end	
	end
end

local function DieOnDay(inst)
	if TheWorld.state.isday then
		inst.sg:GoToState("attack")
	end
end

local function fncommon()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

	inst:SetPhysicsRadiusOverride(0.5)
    MakeFlyingCharacterPhysics(inst, 10, .5)

    inst.Transform:SetFourFaced()
	local scale = 2.5
	inst.Transform:SetScale(scale, scale, scale)

    inst:AddTag("scarytoprey")
	inst:AddTag("spookat")
	inst:AddTag("grimalkin")
	inst:AddTag("monster")

    inst.AnimState:SetBank("sk_grimalkin")
    inst.AnimState:SetBuild("sk_grimalkin_nrm")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.hit_recovery = t_values.HIT_RECOVERY

	--inst.shouldwalk = true
	inst.current_speed = 0
    inst.sounds = sounds

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = 0
    inst.components.locomotor.runspeed = 0
	
    inst:SetStateGraph("SGsk_grimalkin")

    inst:SetBrain(brain)
	
    inst:AddComponent("follower")	
	inst.components.follower.keepleaderonattacked = true
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(t_values.HEALTH)

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED

	inst:AddComponent("colouradder")
	inst:AddComponent("colourtweener")
	
    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(0)
    inst.components.combat:SetAttackPeriod(t_values.ATTACK_PERIOD)
	inst.components.combat:SetRange(t_values.ATTACK_RANGE, 0)
    inst.components.combat:SetRetargetFunction(1, retargetfn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst.components.combat:SetHurtSound(nil)
	inst.components.combat.onhitotherfn = OnHitOther
	
	MakeMediumFreezableCharacter(inst, "kat_head")
	MakeMediumBurnableCharacter(inst, "kat_head")
	
    inst:AddComponent("lootdropper")

    inst:AddComponent("inspectable")

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0) --fire, acid, poison
	PlayablePets.SetCommonStatusResistances(inst, 0, 0, 0, 0, 0, 0) --(inst, stun, fire, shock, poison, freeze, curse, sleep)
	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) --inst, prefaboverride

    return inst
end

local function fndefault()
    local inst = fncommon()
	
	inst:AddTag("NOCLICK")

    if not TheWorld.ismastersim then
        return inst
    end

	inst.components.health:SetInvincible(true)

	inst.persists = false
	
	inst.variant = ""
	
	inst:WatchWorldState("isday", DieOnDay)
	inst:DoTaskInTime(0, DieOnDay)
    return inst
end

return Prefab("sk_grimalkin", fndefault, assets, prefabs)
