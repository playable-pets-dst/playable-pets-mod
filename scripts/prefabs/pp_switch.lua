local prefabs =
{

}

local assets =
{
    Asset("ANIM", "anim/pp_switch.zip"),
}

local function RegisterTriggerables(inst, range)
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, range or 300, {"triggerable"}, {"notarget"})
    inst._targets = {}
    for i, v in ipairs(ents) do
        if v and v._trigger_id and v._trigger_id == inst._trigger_id and v._triggerfn then
            table.insert(inst._targets, v)
        end
    end
end

local function ActivateTriggerables(inst)
    for i, v in ipairs(inst._targets) do
        v:_triggerfn()
    end
end

local function Activate(inst)
    inst._isactive = true
    inst.AnimState:PlayAnimation("activate")
end

local function Deactivate(inst)
    inst._isactive = false
    inst.AnimState:PlayAnimation("deactivate")
end

local function ChangeState(inst, state)
    inst._switchstate = state
    inst.AnimState:PlayAnimation("switch")
    inst:AddTag("busy")
    ActivateTriggerables(inst)
    inst.AnimState:SetSymbolMultColour("arm_light", unpack(inst.colours[inst._switchstate+1]))
end

local function OnAttacked(inst, data)
    --Can only be activated by players.
    if not inst:HasTag("busy") and inst._isactive and data.attacker and data.attacker:HasTag("player") then
        ChangeState(inst, inst._switchstate == 0 and 1 or 0)        
        if inst._duration > 0 and not inst._resettask then 
            inst._isactive = false
            inst._resettask = inst:DoTaskInTime(inst._duration, function(inst)
                ChangeState(inst, inst._switchstate == 0 and 1 or 0)  
                inst._isactive = true
                inst._resettask:Cancel()
                inst._resettask = nil
            end)
        end
    end
end

local function OnSave(inst, data)
    data.duration = inst.duration
    data.colours = inst.colours
end

local function OnLoad(inst, data, newents)
    if data then
        inst._trigger_id = data.id or "" --this is for pairing with buttons
        inst._isactive = data.active or true
        if data.duration then --so pp_switch_timer can keep its default value.
            inst._duration = data.duration
        end
        if data.build then
            inst.AnimState:SetBuild(data.build)
        end
    end
end

local function CreateSwitch(name, data)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddNetwork()
        inst.entity:AddAnimState()

        inst.AnimState:SetBank("pp_switch")
        inst.AnimState:SetBuild("pp_switch")
        inst.AnimState:PlayAnimation("active")

        inst.AnimState:SetSymbolLightOverride("arm_light", 1)

        inst:AddTag("structure")
        ------------------------------------------
        inst.entity:SetPristine()
        ------------------------------------------
        if not TheWorld.ismastersim then
            return inst
        end

        inst._isactive = true
        inst._switchstate = 0
        inst.colours = data.colours or {{1, 0, 0, 1}, {0, 1, 0, 1}} 
        inst._triggerid = ""
        inst._duration = data.duration or 0 --negatives mean permanent toggle, 0 means its a toggle, else is self explanatory

        local _c = inst.colours[inst._switchstate+1]
        inst.AnimState:SetSymbolMultColour("arm_light", _c[1], _c[2], _c[3], _c[4])

        inst:AddComponent("inspectable")

        inst:AddComponent("health")
        inst.components.health:SetMinHealth(1)

        inst:AddComponent("combat")

        inst:DoTaskInTime(0, function(inst)
            RegisterTriggerables(inst, inst._range or nil)
        end)

        inst:ListenForEvent("attacked", OnAttacked)
        inst:ListenForEvent("animover", function(inst) inst:RemoveTag("busy") end)

        inst.OnLoad = OnLoad
        
        return inst
    end
    return Prefab("pp_switch"..name, fn, assets)
end

return CreateSwitch("", {}),
    CreateSwitch("_timer", {duration = 8, colours = {{245/255, 239/255, 66/255, 1}, {149/255, 66/255, 245/255, 1}}})
	
