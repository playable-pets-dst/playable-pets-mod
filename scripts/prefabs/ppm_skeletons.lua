local assets =
{
	Asset("ANIM", "anim/cotl_skeletons.zip"),
}

local function getdesc(inst, viewer)
    if inst.char ~= nil and not viewer:HasTag("playerghost") then
        local mod = GetGenderStrings(inst.char)
        local desc = GetDescription(viewer, inst, mod)
        local name = inst.playername or STRINGS.NAMES[string.upper(inst.char)]

        --no translations for player killer's name
        if inst.pkname ~= nil then
            return string.format(desc, name, inst.pkname)
        end

        --permanent translations for death cause
        if inst.cause == "unknown" then
            inst.cause = "shenanigans"
        elseif inst.cause == "moose" then
            inst.cause = math.random() < .5 and "moose1" or "moose2"
        end

        --viewer based temp translations for death cause
        local cause =
            inst.cause == "nil"
            and (viewer == "waxwell" and
                "charlie" or
                "darkness")
            or inst.cause

        return string.format(desc, name, STRINGS.NAMES[string.upper(cause)] or STRINGS.NAMES.SHENANIGANS)
    end
end

local function decay(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    inst:Remove()
    SpawnPrefab("ash").Transform:SetPosition(x, y, z)
    SpawnPrefab("collapse_small").Transform:SetPosition(x, y, z)
end

local function SetSkeletonDescription(inst, char, playername, cause, pkname, userid)
    inst.char = char
    inst.playername = playername
	inst.userid = userid
    inst.pkname = pkname
    inst.cause = pkname == nil and cause:lower() or nil
    inst.components.inspectable.getspecialdescription = getdesc
end

local function SetSkeletonAvatarData(inst, client_obj)
    inst.components.playeravatardata:SetData(client_obj)
end

local function onhammered(inst)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("rock")
    inst:Remove()
end

local function onsave(inst, data)
    data.anime = inst.anime
end

local function onload(inst, data)
    if data ~= nil and data.anime ~= nil then
        inst.anime = data.anime
        inst.AnimState:PlayAnimation(inst.anime)
    end
end

local function onsaveplayer(inst, data)
    onsave(inst, data)

    data.char = inst.char
    data.playername = inst.playername
	data.userid = inst.userid
    data.pkname = inst.pkname
    data.cause = inst.cause
    if inst.skeletonspawntime ~= nil then
        local time = GetTime()
        if time > inst.skeletonspawntime then
            data.age = time - inst.skeletonspawntime
        end
    end
end

local function onloadplayer(inst, data)
    onload(inst, data)

    if data ~= nil and data.char ~= nil and (data.cause ~= nil or data.pkname ~= nil) then
        inst.char = data.char
        inst.playername = data.playername --backward compatibility for nil playername
		inst.userid = data.userid
        inst.pkname = data.pkname --backward compatibility for nil pkname
        inst.cause = data.cause
        if inst.components.inspectable ~= nil then
            inst.components.inspectable.getspecialdescription = getdesc
        end
        if data.age ~= nil and data.age > 0 then
            inst.skeletonspawntime = -data.age
        end

        if data.avatar ~= nil then
            --Load legacy data
            inst.components.playeravatardata:OnLoad(data.avatar)
        end
    end
end

local function MakeSkeleton(name, data)
	local function fn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()
		inst.entity:AddSoundEmitter()

		MakeSmallObstaclePhysics(inst, data.physoverride or 0.25)
		MakeInventoryFloatable(inst, data.floatsize or "med", 0.05, 0.68)

		inst.AnimState:SetBank(data.bank)
   		inst.AnimState:SetBuild(data.build)

		inst:AddTag("playerskeleton")
        inst:AddTag("bone")

   		inst:AddComponent("playeravatardata")
   		inst.components.playeravatardata:AddPlayerData(true)

		inst:SetPrefabNameOverride(data.prefaboverride or "skeleton")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		inst.anime = data.animationoverride or (data.animoverride and data.animoverride..tostring(math.random(data.animnum)) or "piece"..tostring(math.random(data.animnum)))
		inst.AnimState:PlayAnimation(inst.anime, not data.animloop)

        if data.animloop then
            inst.AnimState:PushAnimation(data.animloop, true)
        end

		inst:AddComponent("inspectable")
    	inst.components.inspectable:RecordViews()

   		inst:AddComponent("lootdropper")
    	inst.components.lootdropper:SetChanceLootTable('skeleton')

		inst:AddComponent("workable")
   		inst.components.workable:SetWorkAction(data.actionoverride or ACTIONS.HAMMER)
   		inst.components.workable:SetWorkLeft(data.workleft or 3)
    	inst.components.workable:SetOnFinishCallback(onhammered)

		inst.OnLoad = onloadplayer
		inst.OnSave = onsaveplayer
		inst.SetSkeletonDescription = SetSkeletonDescription
		inst.SetSkeletonAvatarData = SetSkeletonAvatarData
		inst.Decay = decay
		inst.skeletonspawntime = GetTime()
		TheWorld:PushEvent("ms_skeletonspawn", inst)

		inst:DoTaskInTime(0.5, function(inst)
			if not PlayablePets.IsAboveLand(inst:GetPosition()) then
				inst:Remove()
			end
		end)

		return inst
	end
	return Prefab("pp_skeleton_"..name, fn, assets)
end

return MakeSkeleton("cotl_grunt", {bank = "cotl_skeletons", build = "cotl_skeletons", animnum = 2, animoverride = "grunt"}),
MakeSkeleton("cotl_ranged", {bank = "cotl_skeletons", build = "cotl_skeletons", animationoverride = "grunt3"})
