local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
local tuning_values = TUNING.PP.CENSOREDP

local assets = 
{
	Asset("ANIM", "anim/CENSORED.zip"),
	Asset("ANIM", "anim/CENSORED_babySMOOTHED.zip"),
}



local prefabs = 
{	

}
	
local prefabname = "lc_censoredp"	

local tuning_values = TUNING.PP.LC_CENSOREDP
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.WALKSPEED,
	walkspeed = tuning_values.WALKSPEED,
	
	attackperiod = tuning_values.ATTACKPERIOD,
	damage = tuning_values.DAMAGE,
	range = tuning_values.RANGE,
	hit_range = tuning_values.HIT_RANGE,
	
	bank = "lc_censored",
	shiny = "fumo_reimu",
	build = "CENSORED",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGlc_censoredp",
	minimap = "lc_censoredp.tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
 
})
------------------------------------------------------
 
local soundpath = "lobcorp/creatures/censored/"
local sounds =
{
    hit = soundpath.."attack",
    attack = soundpath.."attack",
    execution = soundpath.."execution", 
    heal = soundpath.."heal",
}

local function CalcSanityAura(inst)
    return -TUNING.SANITYAURA_HUGE*1.2
end

local function SpawnMinion(inst, pos)
	local minion = SpawnPrefab("lc_censoredbaby_npc")
	minion.Transform:SetPosition(pos:Get())
end

local function OnKill(inst, data) 
	if data.victim and not data.victim:HasTag("insect") 
	and not data.victim:HasTag("smallcreature") and not data.victim:HasTag("bird") then
		inst:SpawnMinion(data.victim:GetPosition())
	end
end

local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPM_FORGE.PITPIG)
	
	inst.mobsleep = false	
	
	inst:RemoveTag("LA_mob") --you're a traitor now
	--inst.acidmult = 1.25
	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:AddTag("lc")
    inst:AddTag("CENSORED")
	
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
end

local master_postinit = function(inst) 
    inst.sounds = sounds
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, 0, 0, 9999) --fire, acid, poison
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Variables--	
	--inst.taunt = true
	inst.mobsleep = true
    inst.shouldwalk = true

	inst.hit_recovery = 0.75

	inst.SpawnMinion = SpawnMinion
	
	local body_symbol = "body_0"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("body_0", 0, 0, 0)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!

	inst:AddComponent("sanityaura")
    inst.components.sanityaura.aurafn = CalcSanityAura
	----------------------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 50, 1.5)
    inst.DynamicShadow:SetSize(6, 2)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("kill", OnKill)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		--inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
