local assets=
{

}

local prefabs =
{
	--"explode_large",
	--"explodering_fx",
}

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "shadow"}
	end
end

local function projectile_onland(inst)
	if inst.onhitfn then
		inst.onhitfn(inst)
	else
		local fx = SpawnPrefab("ground_chunks_breaking")
    	fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
	end	
	
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x,0,z, 2.5, nil, GetExcludeTagsp(inst)) or {}
	for k, v in pairs(ents) do
		if inst.thrower and v.components.combat and v.components.health and not v.components.health:IsDead() and v ~= inst.thrower and not (not inst.target or ((v:HasTag("undead") or v:HasTag("wall")) and v ~= inst.target)) then
			v.components.combat:GetAttacked(inst.thrower, 30)
		end
	end	

	inst:Remove()
end

local function commonfn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("pm_drybones")
	inst.AnimState:SetBuild("pm_drybones_build")
	inst.AnimState:PlayAnimation("spin_loop", true)
	
	MakeInventoryPhysics(inst)

    inst.AnimState:SetScale(2, 2)

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	inst:AddTag("NOCLICK")

	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(60)

	inst:AddComponent("complexprojectile")
    inst.components.complexprojectile:SetHorizontalSpeed(30)
    inst.components.complexprojectile:SetGravity(-100)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(0, 3, 0))
    inst.components.complexprojectile:SetOnHit(projectile_onland)
	
    inst.SoundEmitter:PlaySound("pm/common/hammer_throw_lp", "throw_lp")

	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility
	
	--inst:DoTaskInTime(0, function(inst)  end)
	
	inst:DoTaskInTime(10, function(inst) inst:Remove() end)

	return inst
end

return Prefab("pm_drybones_projectilep", commonfn, assets, prefabs)
