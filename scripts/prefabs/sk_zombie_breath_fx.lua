
local assets =
{
    Asset("ANIM", "anim/sk_mist_fx.zip"),
	Asset("ANIM", "anim/sk_smoke_fx.zip"),
}

local prefabs =
{
    
}


local function common_fog_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    RemovePhysicsColliders(inst)
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")
	
	inst.AnimState:SetScale(3, 3)
	
	local anim = math.random(1, 2)

    inst.AnimState:SetBank("sk_mist_fx")
    inst.AnimState:SetBuild("sk_mist_fx")
    inst.AnimState:PlayAnimation(anim, false)
	inst.AnimState:PushAnimation("idle"..anim, true)
	
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	
	inst.AnimState:SetMultColour(229/255, 239/255, 161/255, 0.5)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst:AddComponent("colourtweener")
	
	inst:DoTaskInTime(0.5, function(inst)
		inst.components.colourtweener:StartTween({0, 0, 0, 0}, 1, inst.Remove)
	end)
	
	inst:DoTaskInTime(0, function(inst)
		inst.Physics:SetMotorVelOverride(3.5, -2, 0)
	end)

    return inst
end

local function common_particle_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    RemovePhysicsColliders(inst)
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.AnimState:SetBank("sk_smoke_fx")
    inst.AnimState:SetBuild("sk_smoke_fx")
    inst.AnimState:PlayAnimation("spawn", false)
	inst.AnimState:PushAnimation("spin_loop", true)
	
	inst.Transform:SetTwoFaced()
	
	inst.AnimState:SetMultColour(229/255, 239/255, 161/255, 1)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst:AddComponent("colourtweener")
	
	inst:DoTaskInTime(0.5, function(inst)
		inst.components.colourtweener:StartTween({0, 0, 0, 0}, 1, inst.Remove)
	end)
	
	inst:DoTaskInTime(0, function(inst)
		inst.Physics:SetMotorVelOverride(math.random(2, 5), -3, 0)
	end)

    return inst
end

--Fire----------------------------

local function fog_fire(inst)
	local inst = common_fog_fn()
	
	inst.AnimState:SetMultColour(176/255, 65/255, 24/255, 0.5)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

local function particle_fire(inst)
	local inst = common_particle_fn()
	
	inst.AnimState:SetMultColour(255/255, 174/255, 34/255, 1)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

--Curse---------------------------
local function fog_curse(inst)
	local inst = common_fog_fn()
	
	inst.AnimState:SetMultColour(150/255, 0, 200/255, 0.5)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

local function particle_curse(inst)
	local inst = common_particle_fn()
	
	inst.AnimState:SetMultColour(255/255, 58/255, 98/255, 1)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

--Freeze----------------------------

local function fog_freeze(inst)
	local inst = common_fog_fn()
	
	inst.AnimState:SetMultColour(132/255, 164/255, 208/255, 0.5)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

local function particle_freeze(inst)
	local inst = common_particle_fn()
	
	inst.AnimState:SetMultColour(168/255, 233/255, 255/255, 1)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

--Poison----------------------------

local function fog_poison(inst)
	local inst = common_fog_fn()
	
	inst.AnimState:SetMultColour(58/255, 153/255, 79/255, 0.5)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

local function particle_poison(inst)
	local inst = common_particle_fn()
	
	inst.AnimState:SetMultColour(74/255, 228/255, 108/255, 1)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

--Shock----------------------------

local function fog_shock(inst)
	local inst = common_fog_fn()
	
	inst.AnimState:SetMultColour(62/255, 231/255, 240/255, 0.5)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

local function particle_shock(inst)
	local inst = common_particle_fn()
	
	inst.AnimState:SetMultColour(62/255, 231/255, 240/255, 1)
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	return inst
end

return Prefab("sk_zombie_breath_fx", common_fog_fn, assets, prefabs),
Prefab("sk_zombie_breath_particle_fx", common_particle_fn, assets, prefabs),
Prefab("sk_zombie_breath_fire_fx", fog_fire, assets, prefabs),
Prefab("sk_zombie_breath_fire_particle_fx", particle_fire, assets, prefabs),
Prefab("sk_zombie_breath_curse_fx", fog_curse, assets, prefabs),
Prefab("sk_zombie_breath_curse_particle_fx", particle_curse, assets, prefabs),
Prefab("sk_zombie_breath_freeze_fx", fog_freeze, assets, prefabs),
Prefab("sk_zombie_breath_freeze_particle_fx", particle_freeze, assets, prefabs),
Prefab("sk_zombie_breath_shock_fx", fog_shock, assets, prefabs),
Prefab("sk_zombie_breath_shock_particle_fx", particle_shock, assets, prefabs),
Prefab("sk_zombie_breath_poison_fx", fog_poison, assets, prefabs),
Prefab("sk_zombie_breath_poison_particle_fx", particle_poison, assets, prefabs)
