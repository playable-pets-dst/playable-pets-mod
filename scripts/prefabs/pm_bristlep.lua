local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
local tuning_values = TUNING.PP.PM_BRISTLEP

local assets = 
{
	Asset("ANIM", "anim/pm_bristle_basic.zip"),
    Asset("ANIM", "anim/pm_bristle_build.zip"),
	Asset("ANIM", "anim/pm_bristle_dark_build.zip"),
}



local prefabs = 
{	

}
	
local prefabname = "pm_bristlep"	
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.RUNSPEED/2,
	walkspeed = tuning_values.WALKSPEED/2,
	
	attackperiod = tuning_values.ATTACKPERIOD,
	damage = tuning_values.DAMAGE*2,
	range = tuning_values.RANGE,
	hit_range = tuning_values.HIT_RANGE,
	
	bank = "pm_bristle",
	shiny = "pm_bristle",
	build = "pm_bristle_build",
	scale = 2,
	--build2 = "alternate build here",
	stategraph = "SGpm_bristlep",
	minimap = "pm_bristlep.tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable("pm_bristlep",
-----Prefab---------------------Chance------------
{
 
})
------------------------------------------------------
 
local soundpath = "pm/creatures/bristle/"
local sounds =
{
    attack = soundpath.."attack",
    attack2 = soundpath.."attack2",
    unflip = soundpath.."unflip", 
	flip = soundpath.."hit", 
    death = "pm/creatures/common/death",
	death_pst = "pm/creatures/common/death_pst",
	hit = "pm/creatures/common/hit",
	talk = "pm/common/talk",
}

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
end 

local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
--==============================================
--					Mob Functions
--==============================================
local function GetTriggerExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "wall"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "wall"}
	end
end

local function IsTriggerReady(inst)
	return not inst.sg:HasStateTag("busy") and inst._autotrigger and inst._manualtrigger and not inst:HasTag("_inbattle")
end

local function DoTrigger(inst)
	if IsTriggerReady(inst) then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x, 0, pos.z, tuning_values.HIT_RANGE, {"_combat", "_health"}, GetTriggerExcludeTags(inst))
		local targets = {}
		if #ents > 0 then
			for i, v in ipairs(ents) do
				if v:IsValid() and v ~= inst and v.prefab ~= inst.prefab and v.components.health and not v.components.health:IsDead() then
					table.insert(targets, v)
				end
			end
		end
		if #targets > 0 then
			inst.sg:GoToState("attack", targets)
		end
	end
end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPM_FORGE.PITPIG)
	
	inst.mobsleep = false	
	
	inst:RemoveTag("LA_mob") --you're a traitor now
	--inst.acidmult = 1.25
	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 

	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:AddTag("pm")
    inst:AddTag("bristle")

	--inst.components.talker.font = PAPER_MARIO_TALK
	--inst.components.talker.symbol = "bristle_body"
	inst.components.talker:SetOffsetFn(function(inst) --for some reason the talker component isn't taking offset, but it will take the fn...
		return Vector3(0,-200,0)	
	end)

	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
	
	--PlayablePets.SetNoCC(inst)
end

local master_postinit = function(inst) 
    inst.sounds = sounds
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 0, nil, 0, 9999) --fire, acid, poison
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Variables--	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
    inst.shouldwalk = true
	inst._autotrigger = true
	inst._manualtrigger = true

	inst.DoTrigger = DoTrigger
	inst.GetTriggerExcludeTags = GetTriggerExcludeTags

	inst._triggertask = inst:DoPeriodicTask(0.5, DoTrigger)

	inst.hit_recovery = 0.75
	
	local body_symbol = "bristle_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("swap_debuff", 0, 0, 0)

	PlayablePets.SetStormImmunity(inst)
	----------------------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	----------------------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(3, .75)
    inst.Transform:SetTwoFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", Equip) --Shows head when hats make heads disappear.
    inst:ListenForEvent("unequip", Unequip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		--inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter("pm_bristlep", prefabs, assets, common_postinit, master_postinit, start_inv)
