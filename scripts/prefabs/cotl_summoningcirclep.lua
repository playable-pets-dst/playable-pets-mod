require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/cotl_summoningcircle.zip"),
}

local prefabs =
{

}

local ACTIVE_COLOR = {1, 0, 0, 1}
local INACTIVE_COLOR = {89/255, 38/255, 38/255, 1}

local function MakeActive(inst, symbol)
    if inst.circle then
        inst.circle.AnimState:SetMultColour(unpack(ACTIVE_COLOR))
        inst.circle.AnimState:SetLightOverride(1)
    end
end

local function MakeInactive(inst, symbol)
    if inst.circle then
        inst.circle.AnimState:SetMultColour(unpack(INACTIVE_COLOR))
        inst.circle.AnimState:SetLightOverride(0)
    end
end

local function onhammered(inst, worker)
    inst.components.lootdropper:DropLoot()
    local fx = SpawnPrefab("collapse_small")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function OnRemove(inst)
    if inst.circle and inst.circle:IsValid() then
        inst.circle:Remove()
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("cotl_summoningcircle")
    inst.AnimState:SetBuild("cotl_summoningcircle")
    inst.AnimState:PlayAnimation("idle")
        
    inst:AddTag("structure")
    inst:AddTag("sacrificial_table")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")
    inst:AddComponent("lootdropper")

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
    inst.components.workable:SetWorkLeft(3)
    inst.components.workable:SetOnFinishCallback(onhammered)

    inst:DoTaskInTime(0, function(inst)
        inst.circle = SpawnPrefab("cotl_summoningcirclep")
        inst.circle.Transform:SetPosition(inst:GetPosition():Get())
        inst.circle.Transform:SetRotation(inst.Transform:GetRotation())
    end)

    inst.MakeActive = MakeActive
    inst.MakeInactive = MakeInactive

    inst:ListenForEvent("onremove", OnRemove)

    return inst
end

local function CreateStage(target)
    local inst = CreateEntity()

    inst:AddTag("DECOR") --"FX" will catch mouseover
    inst:AddTag("NOCLICK")
    --[[Non-networked entity]]
    inst.entity:SetCanSleep(false)
    inst.persists = false

    inst.entity:AddTransform()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("cotl_summoningcircle")
    inst.AnimState:SetBuild("cotl_summoningcircle")
    inst.AnimState:PlayAnimation("ground_center")

    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
    inst.AnimState:SetSortOrder(3)
    inst.AnimState:SetScale(3, 3)

    inst.entity:SetParent(target.entity)

    return inst
end

local function circlefn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank("cotl_summoningcircle")
    inst.AnimState:SetBuild("cotl_summoningcircle")
    inst.AnimState:PlayAnimation("ground_idle")

    inst.AnimState:SetMultColour(unpack(INACTIVE_COLOR))

    inst.AnimState:Hide("center")

    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetSortOrder(3)
        
    inst:AddTag("DECOR")
    inst:AddTag("NOCLICK")

    CreateStage(inst)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("colourtweener")

    inst.persists = false

    return inst
end
return Prefab("cotl_summoningtablep", fn, assets, prefabs),
    Prefab("cotl_summoningcirclep", circlefn, assets, prefabs)
