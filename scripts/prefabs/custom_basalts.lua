local assets =
{
    Asset("ANIM", "anim/stupidratbasalt.zip"),
    Asset("ANIM", "anim/cotl_cultist.zip"),
    Asset("ANIM", "anim/pig_knight_fat.zip"),
}

local function onsave(inst, data)
    data.anim = inst.animname
end

local function onload(inst, data)
    if data and data.anim then
        inst.animname = data.anim
        inst.AnimState:PlayAnimation(inst.animname)
    end
end

local function makebasalt(name, bank, build, anim, face)
    local function common_fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        MakeObstaclePhysics(inst, 1)

        if face then
            inst.Transform:SetFourFaced()
        end

        inst.MiniMapEntity:SetIcon("basalt.png")

        inst.AnimState:SetBank(bank or "blocker")
        inst.AnimState:SetBuild(build or "blocker")
        inst.AnimState:PlayAnimation(anim)

        inst:SetPrefabNameOverride("basalt")

        MakeSnowCoveredPristine(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end

        inst:AddComponent("inspectable")
        inst.components.inspectable.nameoverride = "BASALT"

        MakeSnowCovered(inst)
        return inst
    end
    return Prefab(name, common_fn, assets)
end

return makebasalt("stupidratbasalt", "stupidratbasalt", "stupidratbasalt", "idle1"),
    makebasalt("stupidratbasalt_tall", "stupidratbasalt", "stupidratbasalt", "idle2")