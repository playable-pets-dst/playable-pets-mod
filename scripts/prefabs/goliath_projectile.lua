
local assets =
{
    Asset("ANIM", "anim/goliath_projectile.zip"),
	Asset("ANIM", "anim/lavaarena_heal_projectile.zip"),
}

local prefabs =
{
    
}

local sounds =
{
	
}

local function OnHit(inst, owner, target)
	inst.popping = true
	local pos = inst:GetPosition()
	inst.targeting_task:Cancel()
	inst.targeting_task = nil
	inst.Physics:Stop()
	inst.AnimState:PlayAnimation("cast")
	if target and target.components.health and not target.components.health:IsDead() and owner and target.components.combat then
		target.components.combat:SetShouldAvoidAggro(owner)
		target.components.combat:GetAttacked(owner, 15)
		target.components.combat:RemoveShouldAvoidAggro(owner)
		PlayablePets.KnockbackOther(inst, target, 2)
	end
	inst:ListenForEvent("animover", inst.Remove)
end

local function CheckToPop(inst)
	local ent = FindEntity(inst, 2, nil, {"_combat"}, inst.NOTAGS or {"notarget", "INLIMBO", "playerghost"})
	if ent and ent:IsValid() and inst.owner and ent ~= inst.owner then
		OnHit(inst, inst.owner, ent)
	end
end

local function CheckForTargets(inst)
	CheckToPop(inst)
	local ent = FindEntity(inst, 15, nil, {"_combat"}, inst.NOTAGS or {"notarget", "INLIMBO", "playerghost"})
	if ent and ent:IsValid() and inst.owner and ent ~= inst.owner then
			--OnHit(inst, inst.owner, ent)
		inst.target = ent
	end
end

local function OnUpdate(inst)
	if inst.target and inst.target:IsValid() then
		local pos = inst.target:GetPosition()
		inst:ForceFacePoint(pos.x, 0, pos.z)
	end
	if inst.components.locomotor.walkspeed > 0 and not inst.popping then
		inst.components.locomotor.walkspeed = math.max(0, inst.components.locomotor.walkspeed - (1/10))
		inst.Physics:SetMotorVel(inst.components.locomotor.walkspeed, 0, 0)
	end
end

local function common_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 0.1, 0.1)
    RemovePhysicsColliders(inst)
	
    inst:AddTag("NOCLICK")

    inst.AnimState:SetBank("lavaarena_heal_projectile")
    inst.AnimState:SetBuild("goliath_projectile")
    inst.AnimState:PlayAnimation("idle_loop", true)
	inst.AnimState:SetScale(1, 1, 1)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("locomotor")	
	inst.components.locomotor.walkspeed = 6 + (math.random() * 2)
	--inst.components.locomotor:WalkForward()

	inst.target = nil
	inst.targeting_task = inst:DoPeriodicTask(0.01, CheckForTargets)
	
	
	inst:DoTaskInTime(0, function(inst)
		if not inst.permanent then
			inst.persists = false
			inst:DoTaskInTime(10, function(inst)
				inst.AnimState:PlayAnimation("disappear")
				inst:ListenForEvent("animover", inst.Remove)
			end)
		end
	end)

	inst:DoPeriodicTask(0, OnUpdate)

    return inst
end

return Prefab("goliath_projectilep", common_fn, assets, prefabs)
