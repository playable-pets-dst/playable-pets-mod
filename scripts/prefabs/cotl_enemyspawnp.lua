require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/cotl_enemyspawn.zip"),
}

local prefabs =
{

}

local function DoSpawn(inst, child)

end

local function DoDespawn(inst, child)
    inst.AnimState:PlayAnimation("idle_pst", false)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    inst.entity:AddLight()
        
    inst.Light:SetFalloff(1)
    inst.Light:SetIntensity(.5)
    inst.Light:SetRadius(1)
    inst.Light:Enable(false)
    inst.Light:SetColour(1, 0, 0)

    inst.AnimState:SetBank("cotl_enemyspawn")
    inst.AnimState:SetBuild("cotl_enemyspawn")
    inst.AnimState:PlayAnimation("idle_pre")
    inst.AnimState:PushAnimation("idle_loop", true)
    inst.AnimState:SetScale(2, 2)
    inst.AnimState:SetMultColour(1, 0, 0, 1)
    inst.AnimState:SetLightOverride(1)
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")

    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetSortOrder(3)
        
    inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.persists = false

    inst.DoSpawn = DoSpawn
    inst.DoDespawn = DoDespawn

    inst:ListenForEvent("animqueueover", inst.Remove)

    return inst
end
return Prefab("cotl_enemyspawnp", fn, assets, prefabs)
