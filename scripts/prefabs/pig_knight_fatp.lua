local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--Performs a role for other pigmen:
--Excels at destorying structures, machines, and Giants.
---------------------------

local prefabname = "pig_knight_fatp"

local assets = 
{
	Asset("ANIM", "anim/pig_knight_fat.zip"),
	Asset("ANIM", "anim/pig_knight_fat_loyalist.zip"),
}

local prefabs = 
{	

}

local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local MOB_TUNING = TUNING[string.upper(prefabname)]
local tuning_values = TUNING[string.upper(prefabname)]
local mob = 
{
	health       = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger       = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate   = TUNING.WILSON_HUNGER_RATE, 
	sanity       = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed     = MOB_TUNING.RUNSPEED,
	walkspeed    = MOB_TUNING.WALKSPEED,
	damage       = MOB_TUNING.DAMAGE,
	range        = MOB_TUNING.RANGE, 
	hit_range    = MOB_TUNING.HIT_RANGE,
	attackperiod = MOB_TUNING.ATTACK_PERIOD,
    scale        = 1,
	
	bank         = "pig_knight_fat",
	build        = "pig_knight_fat_loyalist",
    	
	stategraph   = "SG"..prefabname,
	minimap      = prefabname..".tex",	
}


SetSharedLootTable(prefabname,
{
    {"meat",  1.00},
	{"meat",  1.00},
	{"meat",  1.00},
	{"pigskin",  1.00},
})

local s_path = "pig_knight_fat/creatures/pig_knight_fat/"
local sounds = {
    --t  = "path",
	grunt = s_path.."grunt",
	hurt = s_path.."hurt",
	step = s_path.."step",
	swing = s_path.."swing",
	attack = s_path.."axe_attack",
	smash = s_path.."smash",
	bodyfall = s_path.."bodyfall",
	death = s_path.."death",
}

local FORGE_STATS = MOB_TUNING.FORGE
--==============================================
--					Local Functions
--==============================================
local function ShouldCrit(inst, target)
	--deal triple damage to constructs/giants.
	return (target:HasTag("construct") or target:HasTag("structure") or target:HasTag("epic"))
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		--inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	--data.isshiny = inst.isshiny or 0
end

--==============================================
--					Reforged
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst.mobsleep = false	
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "armors"})
	end)
	
	inst.components.health:SetAbsorptionAmount(0.8)
	
	inst:AddComponent("buffable")
	--inst.components.buffable:AddBuff("winona_passive", {{name = "cooldown", val = -0.1, type = "add"}})
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
        if ThePlayer then
            inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
        end
    end)
	----------------------------------
	--Tags--
	inst:AddTag("pig")
	inst:AddTag("pig_knight_fat")
	----------------------------------
end

--TODO I guess the new compiler did this...?
local head_symbols = {
	"head-0",
	"ear-0",
	"mouth-0",
	"eyes-0"
}

local function UpdateHats(inst)
	local hat = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	if not hat then
		inst.AnimState:Hide("swap_hat")
		inst.AnimState:Show("swap_helmet-0")
		for i, v in ipairs(head_symbols) do
			inst.AnimState:Hide(v)
		end
	else
		inst.AnimState:Hide("swap_helmet-0")
		for i, v in ipairs(head_symbols) do
			inst.AnimState:Show(v)
		end
		if hat then
			inst.AnimState:Show("swap_hat")
		else
			inst.AnimState:Hide("swap_hat")
		end
	end
end

local axe_attacks = {
	weapon_slam = {
		stats = 
		{
			blunt = 0, 
			slash = tuning_values.AXE_DAMAGE, 
		 	piercing = 0
		}, 
		poise = 40, 
		offset = tuning_values.AXE_RANGE_OFFSET, 
		range = tuning_values.AXE_RANGE,
	},
	butt_slam = {stats = {blunt = tuning_values.MAX_JUMP_DAMAGE, slash = 0, piercing = 0, poise = 80}, range = tuning_values.JUMP_RANGE}
}

local master_postinit = function(inst)
	--Stats--
    inst.sounds = sounds --sounds need to exist before stategraph
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier, ignoreweb
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, nil, 0.5) --fire, acid, poison, freeze
	----------------------------------
	--Variables	
    inst.shouldwalk = false
	inst._attacks = axe_attacks
	inst.ShouldCrit = ShouldCrit
	inst.OnSkill3 = function(inst, cursor_pos) inst.sg:GoToState("attack2", cursor_pos) end
	
    ----------------------------------
    --Playable Pets
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SLEEP, PP_ABILITIES.PP_SLEEP)
    inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL1, PP_ABILITIES.SKILL1)
	inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL2, PP_ABILITIES.SKILL2)
	inst.components.playablepet:AddAbility(PP_ABILITY_SLOTS.SKILL3, PP_ABILITIES.SKILL3)		
	----------------------------------
	--Debuff Symbol
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
    ----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	if inst.components.factionp then
		inst.components.factionp:SetFaction("gold") --automatically set faction to pig faction.
	end

	inst:AddComponent("groundpounder")
	inst.components.groundpounder.destroyer = false
	inst.components.groundpounder.groundpounddamagemult = 1
	inst.components.groundpounder.damageRings = 0
	inst.components.groundpounder.destructionRings = MOBFIRE == "Enable" and 0 or 2
	inst.components.groundpounder.platformPushingRings = 3
	inst.components.groundpounder.numRings = 3
	inst.components.groundpounder.radiusStepDistance = 2
	inst.components.groundpounder.ringWidth = 1.5
	----------------------------------
	--Eater--

	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 450, 1.5)
    inst.DynamicShadow:SetSize(4, 2.5)
	inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", UpdateHats) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", UpdateHats)
	---------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		--inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) UpdateHats(inst) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function()
        PlayablePets.RevRestore(inst, mob) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)

    PlayablePets.SetCommonSaveAndLoad(inst, OnSave, OnLoad)
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)