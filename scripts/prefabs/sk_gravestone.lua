--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_gravestone.zip"),
	Asset("ANIM", "anim/sk_graveyard_path.zip"),
}

local prefabs =
{
    
}

local sounds =
{
	
}

SetSharedLootTable('sk_gravestone',
{
    {"rocks",   			 1.00},
	{"rocks",   			 1.00},
	{"rocks",   			 1.00},
})

local MAX_GRAVES = 4

local function ChangeColours(inst)
	if inst.colour then
		inst.AnimState:SetMultColour(inst.colour[1], inst.colour[2], inst.colour[3], inst.colour[4])
	end
	if inst.fx and inst.light_colour then
		inst.fx.AnimState:SetMultColour(inst.light_colour[1], inst.light_colour[2], inst.light_colour[3], 1)
	end
end

local function SetColours(inst, colour, light_colour)
	inst.colour = colour or {125/255,143/255,150/255, 1}
	inst.light_colour = light_colour or {147/255, 255/255, 64/255}
	if not inst.id then
		inst.id = tostring(math.random(1, MAX_GRAVES))
	end
	ChangeColours(inst)
end

local function SetRandomColour(inst)
	inst.colour = {math.random(), math.random(), math.random(), 1}
	inst.light_colour = {math.random(), math.random(), math.random()}
	ChangeColours(inst)
end

local function RefreshBuilds(inst)
	inst.AnimState:PlayAnimation("gravestone"..inst.id)
	if inst.fx then
		inst.fx.AnimState:PlayAnimation("cracks"..inst.id)
	end
end

local function Deactivate(inst)
	if inst.fx then
		inst.fx:Remove()
		inst.fx = nil
	end
	if inst.active_task then
		inst.active_task:Cancel()
		inst.active_task = nil
	end
	inst.active = nil
	inst.spawned = nil
end

local function GetExcludeTags(inst)
	return {"playerghost", "INLIMBO", "undead", "grimalkin", "zombie", "spookat"}
end

local zombies = {
	spring = {"_poison", "_shock"},
	summer = {"_fire"},
	autumn = {""},
	winter = {"_freeze"},
}

local function PickZombie(inst)
	if math.random() < 0.001 then
		return "sk_zombie_curse"
	else
		return math.random() < 0.2 and "sk_bombie"..zombies[TheWorld.state.season][math.random(1, #zombies[TheWorld.state.season])] or "sk_zombie"..zombies[TheWorld.state.season][math.random(1, #zombies[TheWorld.state.season])]
	end
end

local function LookforTargets(inst)
	if not inst.spawned then
		local pos = inst:GetPosition()
		local ents = TheSim:FindEntities(pos.x, 0, pos.z, 5, {"player"}, GetExcludeTags(inst))
		if #ents > 0 then
			inst.spawned = true
			inst:DoTaskInTime(math.random() + 1, function(inst)
				local minionposx, minionposy, minionposz = pos.x + math.random(-2, 2), 0, pos.z + math.random(-2, 2)
				local minion = SpawnPrefab(PickZombie(inst))
				minion.sg:GoToState("spawn")
				minion.Transform:SetPosition(minionposx, 0, minionposz)
				if ents[1] and ents[1]:IsValid() then
					minion.components.combat:SetTarget(ents[1])
				end			
			end)
		end
	end	
end

local function Activate(inst)
	if not inst.fx then
		inst.fx = SpawnPrefab("sk_gravestone_cracksp")
		inst.fx.AnimState:PlayAnimation("cracks"..inst.id or "1")
		inst.fx.entity:SetParent(inst.entity)
		if inst.light_colour then
			inst.fx.AnimState:SetMultColour(inst.light_colour[1], inst.light_colour[2], inst.light_colour[3], 1)
		end
	end
	inst.active = true
	inst.active_task = inst:DoPeriodicTask(0.5, LookforTargets)
end

local function OnPhaseChanged(inst)
	if TheWorld.state.isday	then
		Deactivate(inst)
	elseif TheWorld.state.isnight then
		if math.random() < 0.1 then
			Activate(inst)
		end		
	end
end

local function OnSave(inst, data)
	data.id = inst.id or "1"
	data.colour = inst.colour or {1, 1, 1, 1}
	data.light_colour = inst.light_colour or {1, 1, 1}
end

local function OnLoad(inst, data)
	if data then
		inst.id = data.id or "1"
		inst.colour = data.colour
		inst.light_colour = data.light_colour
	end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeObstaclePhysics(inst, 0.75)

    --inst.Transform:SetOneFaced()
	local scale = 1.2
	inst.Transform:SetScale(scale, scale, scale)

    inst:AddTag("structure")
	inst:AddTag("sk_gravestone")

    inst.AnimState:SetBank("sk_gravestone")
    inst.AnimState:SetBuild("sk_gravestone")
    inst.AnimState:PlayAnimation("gravestone1", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("inspectable")
	
	SetColours(inst)
	inst:DoTaskInTime(0, ChangeColours)
	inst:DoTaskInTime(0, RefreshBuilds)
	
	inst.SetColours = SetColours
	inst.SetRandomColour = SetRandomColour
	inst.Activate = Activate
	inst.Deactivate = Deactivate
	
	inst:WatchWorldState("isday", OnPhaseChanged)
  	inst:WatchWorldState("isdusk", OnPhaseChanged)
	inst:WatchWorldState("isnight", OnPhaseChanged)
	OnPhaseChanged(inst)

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    return inst
end

return Prefab("sk_gravestone", fn, assets, prefabs)
