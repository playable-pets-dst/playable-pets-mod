local assets=
{
	Asset("ANIM", "anim/cotl_bomb.zip")
}

local prefabs =
{
	--"explode_large",
	--"explodering_fx",
}

local function AddFireFx(inst)
	if not inst.fuse_fx then
		inst.fuse_fx = SpawnPrefab("torchfire_rag")
		inst.fuse_fx.entity:AddFollower()
		inst.fuse_fx.Follower:FollowSymbol(inst.GUID, "fuse", 0, 0, 0)
		inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_fuse_LP", "hiss")
	end
end

local function removefirefx(inst)
    if inst.fuse_fx then
        inst.fuse_fx:Remove()
        inst.fuse_fx = nil
    end
end

local function onexplode(inst, scale)
	scale = scale or 1
	local pos = Vector3(inst.Transform:GetWorldPosition())
	inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_explo")

	if HAMLET_PP_MODE then
		--ATC feature, explosions cause cracks to reveal themselves.
		local interiorID = inst:GetCurrentInteriorID()
		if interiorID then
			local name = TheWorld.components.interiorspawner.interior_defs[interiorID].dungeon_name
			if name and not (name:find("pig_shop") or name:find("playerhouse") or name == "pig_palace") then -- maybe we should define interior quake rooms as constants instead?
				TheWorld:PushEvent("interior_startquake", {interiorID = interiorID, quake_level = INTERIOR_QUAKE_LEVELS.PILLAR_WORKED})
			end
		end
	end

	--local explode = SpawnPrefab("explode_large")
	--local ring = SpawnPrefab("explodering_fx")
	--local pos = inst:GetPosition()

	--ring.Transform:SetPosition(pos.x, pos.y, pos.z)
	--ring.Transform:SetScale(scale, scale, scale)

	--explode.Transform:SetPosition(pos.x, pos.y, pos.z)
	--explode.AnimState:SetBloomEffectHandle( "shaders/anim.ksh" )
	--explode.AnimState:SetLightOverride(1)
	--explode.Transform:SetScale(scale, scale, scale)
end

local function GetExcludeTags(inst)
	if MOBFIRE ~= "Disabled" then
		return {"INLIMBO", "playerghost", "notarget", "wall", "explodeimmune"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "explodeimmune"}
	end
end

local function projectile_onland(inst)
	removefirefx(inst)
	local fx_explosion = SpawnPrefab("cotl_explosion_fxp")
    fx_explosion.Transform:SetPosition(inst.Transform:GetWorldPosition())

	local fx = SpawnPrefab("cavein_dust_low")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx.AnimState:SetDeltaTimeMultiplier(1)
    fx.AnimState:SetMultColour(0.5, 0.5, 0.5, 1)

	ShakeAllCameras(CAMERASHAKE.FULL, .4, .02, .15, inst, 20)

	local fx_s = SpawnPrefab("cotl_scorched_groundp")
    fx_s.Transform:SetPosition(inst.Transform:GetWorldPosition())
	
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x,0,z, TUNING.PP.COTL_BOMB_RANGE, nil, GetExcludeTags(inst)) or {}
	for k, v in pairs(ents) do
		if v.components.combat and v.components.health and not v.components.health:IsDead() then
			v.components.combat:GetAttacked(inst.thrower or inst, 150, nil, "explosive")
		end
	end	

	inst:Remove()
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()

	inst.AnimState:SetBank("cotl_bomb")
	inst.AnimState:SetBuild("cotl_bomb")
	inst.AnimState:PlayAnimation("throw", true)

	inst.AnimState:Hide("fuse")
	
	MakeInventoryPhysics(inst)

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	inst:AddTag("NOCLICK")

	inst.entity:SetPristine()
	
	if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("combat")
	inst.components.combat:SetDefaultDamage(150)

	inst:AddComponent("complexprojectile")
    inst.components.complexprojectile:SetHorizontalSpeed(30)
    inst.components.complexprojectile:SetGravity(-50)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(0, 3, 0))
    inst.components.complexprojectile:SetOnHit(projectile_onland)

	AddFireFx(inst)

	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility
	
	--inst:DoTaskInTime(0, function(inst)  end)
	
	inst:DoTaskInTime(10, function(inst) inst:Remove() end)

	return inst
end

local function explode_fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddSoundEmitter()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	inst:AddTag("FX")
	inst:AddTag("NOCLICK")
	
	inst.AnimState:SetBank("explode")
    inst.AnimState:SetBuild("explode")
    inst.AnimState:PlayAnimation("small")
    inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	
	inst.SoundEmitter:PlaySound("dontstarve/common/blackpowder_explo")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst:DoTaskInTime(0, function(inst)
		local scale = inst.scale or 2
		inst.AnimState:SetScale(scale, scale)
	end)
	
	inst:ListenForEvent("animover", inst.Remove)

	return inst
end

local function scorched_fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddSoundEmitter()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	inst:AddTag("FX")
	inst:AddTag("NOCLICK")
	
	inst.AnimState:SetBuild("burntground")
    inst.AnimState:SetBank("burntground")
    inst.AnimState:PlayAnimation("idle")
    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_GROUND)
    inst.AnimState:SetSortOrder(3)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst:DoTaskInTime(0, function(inst)
		local scale = inst.scale or 2
		inst.AnimState:SetScale(scale, scale)
	end)
	
	inst.Transform:SetRotation(math.random() * 360)

	inst:AddComponent("colourtweener")
	inst.components.colourtweener:StartTween({0, 0, 0, 0}, 30, inst.Remove)

	return inst
end

return Prefab("cotl_bombp", fn, assets, prefabs),
Prefab("cotl_explosion_fxp", explode_fn, assets, prefabs),
Prefab("cotl_scorched_groundp", scorched_fn, assets, prefabs)
