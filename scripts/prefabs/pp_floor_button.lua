local prefabs =
{

}

local assets =
{
    Asset("ANIM", "anim/pp_floor_button.zip"),
}

local function RegisterTriggerables(inst, range)
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, range or 300, {"triggerable"}, {"notarget"})
    inst._targets = {}
    for i, v in ipairs(ents) do
        if v and v._trigger_id and v._trigger_id == inst._trigger_id and v._triggerfn then
            table.insert(inst._targets, v)
        end
    end
end

local function ActivateTriggerables(inst)
    for i, v in ipairs(inst._targets) do
        v:_triggerfn()
    end
end

local function OnNear(inst, player)
    if not inst._isactive then
        inst._isactive = true
        inst.AnimState:PlayAnimation("pressed")
        ActivateTriggerables(inst)
        if inst._duration > 0 and not inst._resettask then 
            inst._resettask = inst:DoTaskInTime(inst._duration, function(inst)
                inst._isactive = false
                inst.AnimState:PlayAnimation("unpressed")
                ActivateTriggerables(inst)
                inst._resettask:Cancel()
                inst._resettask = nil
            end)
        end
    end
end

local function OnFar(inst, player)
    if inst._duration == 0 then
        inst._isactive = false
        inst.AnimState:PlayAnimation("unpressed")
        ActivateTriggerables(inst)
    end
end

local function OnSave(inst, data, newents)
    data._duration = inst._duration or 4
    data.id = inst._trigger_id or ""
    data._range = inst._range or 300
end


local function OnLoad(inst, data, newents)
    if data then
        inst.Transform:SetRotation(data.rotation or 0)
        inst._trigger_id = data.id or "" --this is for pairing with buttons
        inst._isactive = data.active or false
        inst._duration = data.duration or 4
        inst._range = data._range or 300
        if data.build then
            inst.AnimState:SetBuild(data.build)
        end
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddNetwork()
    inst.entity:AddAnimState()

    inst.AnimState:SetBank("pp_floor_button")
    inst.AnimState:SetBuild("pp_floor_button")
    inst.AnimState:PlayAnimation("unpressed")

    inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
    inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetSortOrder(3)

	inst:AddTag("NOCLICK")
    ------------------------------------------
    inst.entity:SetPristine()
    ------------------------------------------
    if not TheWorld.ismastersim then
        return inst
    end

    inst._isactive = false
    inst._triggerid = ""
    inst._duration = 4 --negatives mean permanent toggle, 0 means it stay active as long as players stand on it, else is self explanatory

    inst:AddComponent("playerprox")
    inst.components.playerprox:SetOnPlayerNear(OnNear)
    inst.components.playerprox:SetOnPlayerFar(OnFar)
    inst.components.playerprox:SetDist(1.5, 1.51)

    inst:DoTaskInTime(0, function(inst)
        RegisterTriggerables(inst, inst._range or nil)
    end)

	inst.OnLoad = OnLoad
    inst.OnSave = OnSave
    ------------------------------------------
    return inst
end

return Prefab("pp_floor_button", fn, assets)