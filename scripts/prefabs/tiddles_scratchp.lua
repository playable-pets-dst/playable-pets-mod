local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "tiddles_scratchp"

local assets = 
{
	Asset("ANIM", "anim/tiddle_squawk.zip"),
    Asset("ANIM", "anim/tiddle_scratch.zip"),
    Asset("ANIM", "anim/tiddle_spit.zip"),
    Asset("ANIM", "anim/tiddle_trail.zip"),
    Asset("ANIM", "anim/tiddles_plaguegoo.zip"),
    Asset("ANIM", "anim/brightmare_gestalt.zip"),
}

local getskins = {}

local prefabs = 
{	

}

local tuning_values = TUNING.TIDDLE_PIGZOMBIE_FAT
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = TUNING.TIDDLER_SPEED,
	walkspeed = TUNING.TIDDLER_SPEED,
	damage = 0,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "krampus",
	build = "tiddle_scratch",
	scale = 1.1,
	--build2 = "chester_shadow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGtiddles_plaguebringerp",
	minimap = "tiddles_scratchp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
})

local FORGE_STATS = PPM_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local function IsFacing(inst, other)
	--Check if they're facing what they run into
 local x, y, z = other.Transform:GetWorldPosition()
                    local rot = inst.Transform:GetRotation()
                    local rot1 = inst:GetAngleToPoint(x, y, z)
                    local drot = math.abs(rot - rot1)
                    while drot > 180 do
                        drot = math.abs(drot - 360)
                    end

        return other ~= nil and (drot < 90)

end

local function dofx(inst, other)
	--Boom
    	ShakeAllCameras(CAMERASHAKE.SIDE, .5, .05, .1, inst, 40)
	other:PushEvent("knockback", {knocker = inst, radius = 150, strengthmult = 1})
	local pt = Vector3(other.Transform:GetWorldPosition())
	SpawnPrefab("fx_boat_pop").Transform:SetPosition(pt.x, pt.y+1, pt.z)
	inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/dragonfly/buttstomp")
end

local function ClearRecentlyCharged(inst, other)
    inst.recentlycharged[other] = nil
end


local function onothercollide(inst, other)
    local t = GetTime()
	if not other:IsValid() or inst.recentlycharged[other] then
            return

	elseif other:HasTag("smashable") and other.components.health ~= nil then
            --other.Physics:SetCollides(false)
            other.components.health:Kill()

	elseif other.components.workable ~= nil
        and other.components.workable:CanBeWorked()
        and other.components.workable.action ~= ACTIONS.NET then
	    --It's a workable. Check if it should be insantly destroyed or just worked by one
	    --Also don't be knocked back by bushes; just uproot them
		if not other:HasTag("bush") then
		    inst:PushEvent("knockback", {knocker = other, radius = 150, strengthmult = 1})
		end
            inst.knockout_time = t + 4
	    dofx(inst, other)
		inst:DoTaskInTime(0, function()
		    if other.prefab:match("fence") or other:HasTag("wall") or other:HasTag("tree") then
		    other.components.workable:Destroy(inst)
		    else
		    other.components.workable:WorkedBy(inst, 1)
		    end
		end)
        		if other:IsValid() and other.components.workable ~= nil and other.components.workable:CanBeWorked() then
            		    inst.recentlycharged[other] = true
            		    inst:DoTaskInTime(1, ClearRecentlyCharged, other)
        		end

	--It's something they can smack. Smack it.
	elseif (other.components.health ~= nil and not other.components.health:IsDead()) and other.components.combat ~= nil and not other:HasTag("tiddleplaguebringer") then
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(1, ClearRecentlyCharged, other)
	    other.components.combat:GetAttacked(inst, other:HasTag("player") and TUNING.TIDDLER_SLAM_DAMAGE_PLAYER or TUNING.TIDDLER_SLAM_DAMAGE)
            inst.charge_time = t + 5
	    inst:ClearBufferedAction()
	    dofx(inst, other)
		if other:HasTag("largecreature") or other:HasTag("structure") then
		    inst:PushEvent("knockback", {knocker = other, radius = 150, strengthmult = 1})
		end

	--Pig King is THICC. Bounce off of him as well as each other
	elseif other:HasTag("tiddleplaguebringer") or other:HasTag("structure") or other.prefab == "pigking" then
	    inst:PushEvent("knockback", {knocker = other, radius = 150, strengthmult = 1})
            inst.recentlycharged[other] = true
            inst:DoTaskInTime(5, ClearRecentlyCharged, other)
            inst.knockout_time = t + 5
	    dofx(inst, other)
    	end

end

local function oncollide(inst, other)
	--MANY checks required considering this is their primary means of attack
	--Colliding with any structure is on a cooldown to avoid chain-smashing
	--Must be moving at a certain speed AND running to avoid players being thrown back as the two being knocked back is supposed to be an opportunity to strike
	--Checks for angles so running behind them can't knock you back
            local t = GetTime()
    if not (other ~= nil and other:IsValid() and inst:IsValid())
	or ((other:HasTag("tiddleplaguebringer")) and t < inst.knockout_time )
        or inst.recentlycharged[other]
	or inst.sg:HasStateTag("knockout")
	or other:HasTag("shadow")
	or other:HasTag("shadowminion")
	or (other:HasTag("playerghost") or other.components.health ~= nil and other.components.health:IsDead())
        or Vector3(inst.Physics:GetVelocity()):LengthSq() < 42
	or inst.components.locomotor.isrunning ~= true
	or not IsFacing(inst, other) then
        return
    end
        inst:DoTaskInTime(1 * FRAMES, onothercollide, other)
end

local function SummonCrows(inst)
    local target = inst.components.combat.target
            local pt = target ~= nil and Vector3(target.Transform:GetWorldPosition()) or Vector3(inst.Transform:GetWorldPosition())
    local var = target ~= nil and 2 or 5
    local bird = SpawnPrefab("tiddle_plaguecrow")
    bird.Physics:Teleport(pt.x+math.random()*var -var/var, 20, pt.z+math.random()*var -var/var)
    bird:DoTaskInTime(0, function()
        bird.sg:GoToState("divebomb")
    end)
end


local function LaunchGoo(inst)
	local theta = math.random() * 2 * PI
	local r = inst:GetPhysicsRadius(0) + 0.25 + math.sqrt(math.random()) * TUNING.TIDDLER_SQUAWK_SPIT_DIST_VAR
	local x, y, z = inst.Transform:GetWorldPosition()
	local dest_x, dest_z = math.cos(theta) * r + x, math.sin(theta) * r + z

	local goo = SpawnPrefab("tiddles_plaguebomb2")
    	goo.Transform:SetPosition(x, 0, z)
	goo.Transform:SetRotation(theta / DEGREES)
	goo._caster = inst

	inst.SoundEmitter:PlaySound("wintersfeast2019/creatures/gingerbread_vargr/whoosh")
	Launch2(goo, inst, 1.5, 1, 1, .75)

end

--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.isalt = data.isalt or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("tiddlevirusimmune")
	inst:AddTag("epic")
	inst:AddTag("tiddleplaguebringer")
	inst:AddTag("tiddleplaguebearer")
	inst:AddTag("hostile")
	inst:AddTag("monster")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
	----------------------------------
	if not TheNet:IsDedicated() then
		inst.base = SpawnPrefab("tiddle_plaguebringerbase")
		inst.base.entity:SetParent(inst.entity) --prevent 1st frame sleep on clients
		inst.base.Follower:FollowSymbol(inst.GUID, "krampus_torso", 0, 102.5, -0.1)
	
		--inst:DoPeriodicTask(0, function(inst) inst.base.Transform:SetRotation(inst.Transform:GetRotation()) end)

	    inst.highlightchildren = { inst.base }

	end
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = fatsounds
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Variables			
	inst.isshiny = 0
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	
	inst.AnimState:Hide("SACK")
	
	inst.Erode = Erode
	inst.BecomeAlive = BecomeAlive
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 0) --fire, acid, poison
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, 'tiddlescratch') --inst, prefaboverride
	--inst:AddComponent("sanityaura")
	--inst.components.sanityaura.aura = CalcSanityAura
	
	inst:AddComponent("tiddlespreader")
	
	inst:AddComponent("timer") 
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater.strongstomach = true
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.75, 0.75)
    inst.Transform:SetFourFaced()
	inst.recentlycharged = {}
    inst.Physics:SetCollisionCallback(oncollide)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	
	inst:ListenForEvent("onhitother", spread)
    inst:ListenForEvent("attacked", OnAttacked)
	
	inst:ListenForEvent("death", BecomeCorpse)
    inst:ListenForEvent("tiddlerevived", BecomeAlive)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
