local assets = {
	Asset("ANIM", "anim/lc_debuff_enchantlure.zip"),
}

local prefabs = {}

local stages = {
	{colour = {255/255, 221/255, 0, 1}, strength = 0.67, duration = 40},
	{colour = {1, 85/255, 0, 1}, strength = 0.33, duration = 30},
	{colour = {1, 0, 0, 1}, strength = 0, duration = 12}
}

local SYMBOL_OFFSET = -300

local function OnAttached(inst, target)
    inst.entity:SetParent(target.entity)
    inst:ListenForEvent("death", function()
        inst.components.debuff:Stop()
    end, target)
	if target.components.debuffable.followsymbol and target.components.debuffable.followsymbol ~= ""  then		
		inst.entity:AddFollower()
		inst.Follower:FollowSymbol(target.GUID, target.components.debuffable.followsymbol, 0, SYMBOL_OFFSET, 0)
		inst.Transform:SetPosition(0, 0, 0)
	else
		inst.Transform:SetPosition(0, 3.5, 0)
	end
	target.components.locomotor:SetExternalSpeedMultiplier(inst, "lc_debuff_enchantlure", stages[inst.level].strength)
end

local function OnTimerDone(inst, data)
    if data.name == inst.prefab.."buffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer(inst.prefab.."buffover")
	if inst.level < #stages then
		inst.level = inst.level + 1
		inst.AnimState:SetMultColour(unpack(stages[inst.level].colour))
		target.components.locomotor:SetExternalSpeedMultiplier(inst, "lc_debuff_enchantlure", stages[inst.level].strength)
		if inst.level >= #stages then
			target:AddTag("lc_ismaxlured")
			if target.brain then
				target.brain:Stop()
			end
		end
	end
	inst.components.timer:StartTimer(inst.prefab.."buffover", stages[inst.level].duration or 40)
end

local function OnDetached(inst, target)
	target.components.locomotor:RemoveExternalSpeedMultiplier(target, "lc_debuff_enchantlure")
	target:RemoveTag("lc_ismaxlured")
	inst.AnimState:PlayAnimation("despawn")
	if target.brain then
		target.brain:Start()
	end
	inst:ListenForEvent("animover", inst.Remove)
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

    inst.AnimState:SetBank("lc_debuff_enchantlure")
    inst.AnimState:SetBuild("lc_debuff_enchantlure")
	inst.AnimState:PlayAnimation("spawn", false)
	inst.AnimState:PushAnimation("idle_loop", true)

	inst.AnimState:SetMultColour(unpack(stages[1].colour))
		
	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end
		
	inst:AddComponent("debuff")
	inst.components.debuff:SetAttachedFn(OnAttached)
	inst.components.debuff:SetDetachedFn(OnDetached)
	inst.components.debuff:SetExtendedFn(OnExtended)
	
	inst.level = 1
	inst.duration = stages[inst.level].duration

	inst:AddComponent("timer")
	inst:DoTaskInTime(0, function() -- in case we want to change duration
		inst.components.timer:StartTimer("lc_debuff_enchantlurebuffover", inst.duration)
	end)
	inst:ListenForEvent("timerdone", OnTimerDone)

	return inst
end

local function fxfn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

    inst.AnimState:SetBank("lc_debuff_enchantlure")
    inst.AnimState:SetBuild("lc_debuff_enchantlure")
	inst.AnimState:PlayAnimation("idle_loop", true)

	inst.AnimState:SetMultColour(unpack(stages[1].colour))
	inst.AnimState:SetLightOverride(1)
		
	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst.persists = false

	return inst
end
------------------------------------------------------------------------------

return Prefab("lc_debuff_enchantlure", fn, assets, prefabs),
Prefab("lc_lure_fx", fxfn, assets, prefabs)
