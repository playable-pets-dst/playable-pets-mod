require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/pw_banner.zip"),
    Asset("ANIM", "anim/pw_banner_loyalist.zip"),
    Asset("ANIM", "anim/pw_banner_shadow.zip"),
    Asset("ANIM", "anim/pw_banner_lunar.zip"),
    Asset("ANIM", "anim/pw_banner_peach.zip"),
}

local prefabs =
{
    --"merm",
    "collapse_big",

    --loot:
    "boards",
    "rocks",
    --"fish",
}

local loot =
{
    "boards",
    "rocks",
    
}

local function onhammered(inst, worker)
	if inst.components.sleepingbag then
		inst.components.sleepingbag:DoWakeUp()
	end	
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    if inst.components.lootdropper then
        inst.components.lootdropper:DropLoot()
    end
    local fx = SpawnPrefab("collapse_big")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle_loop", true)
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("build")
    inst.AnimState:PushAnimation("idle_loop", true)
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
end

local function onload(inst, data)
    if data ~= nil and data.burnt then
        inst.components.burnable.onburnt(inst)
    end
end

local function onignite(inst)

end

local function onburntup(inst)
    inst.AnimState:PlayAnimation("burnt")
end
local function CreateBanner(name, build)
    local function common_fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()
        inst.entity:AddLight()
        
        inst.Light:SetFalloff(1)
        inst.Light:SetIntensity(.5)
        inst.Light:SetRadius(1)
        inst.Light:Enable(false)
        inst.Light:SetColour(180/255, 195/255, 50/255)

        MakeObstaclePhysics(inst, 0.5)

        inst.AnimState:SetBank("pw_banner")
        inst.AnimState:SetBuild(build or "pw_banner_"..name)
        inst.AnimState:PlayAnimation("idle_loop", true)
        inst.AnimState:SetScale(1.2, 1.2)
        
        inst:AddTag("banner")
        inst:AddTag("structure")

        MakeSnowCoveredPristine(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
        
        inst:AddComponent("lootdropper")
        inst.components.lootdropper:SetLoot(loot)

        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(3)
        inst.components.workable:SetOnFinishCallback(onhammered)
        inst.components.workable:SetOnWorkCallback(onhit)

        inst:AddComponent("inspectable")

        inst:AddComponent("burnable")

        return inst
    end
    return Prefab("pw_banner_"..name, common_fn, assets, prefabs),
    MakePlacer("pw_banner_"..name.."_placer", "pw_banner", "pw_banner_"..name, "idle_loop")
end
return CreateBanner("loyalist"),
CreateBanner("shadow"),
CreateBanner("lunar"),
CreateBanner("peach")
