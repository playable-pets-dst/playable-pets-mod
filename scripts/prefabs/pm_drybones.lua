local brain = require("brains/pm_rangedbrain")

local assets =
{
	Asset("ANIM", "anim/pm_drybones_basic.zip"),
    Asset("ANIM", "anim/pm_drybones_build.zip"),
    Asset("ANIM", "anim/pm_drybones_dull_build.zip"),
    Asset("ANIM", "anim/pm_drybones_dark_build.zip"),
    Asset("ANIM", "anim/pm_drybones_red_build.zip"),
}

local function OnStatDirty(inst, stats)
	--make certain stats accessible client side, like hp amounts.
    --print("NPC OnStatDirty")
    if inst.replica.pm_unit then
        local stats = inst.replica.pm_unit
        inst._currenthp = stats.currenthp:value()
        inst._maxhp = stats.maxhp:value()
    end
end

local function OnStageDirty(inst)
	--might not need this for npcs.
end

local function PMUpdateStats(inst)
    if inst.components.pm_unit and inst.replica.pm_unit then
        if not TheNet:IsDedicated() then
            OnStatDirty(inst, inst.replica.pm_unit)
        end
    end
end

local soundpath = "pm/creatures/drybones/"
local sounds =
{
    shake = soundpath.."shake",
    shake_pst = soundpath.."shake_pst",
    dead = soundpath.."dead",
    bone_land = soundpath.."bone_land",
    bone_shuffle = soundpath.."bone_shuffle",
    death = "pm/creatures/common/death",
	death_pst = "pm/creatures/common/death_pst",
	hit = "pm/creatures/common/hit",
	talk = "pm/common/talk",
}

local function OnCollide(inst, other) 
	if other and other:IsValid() and other.components.pm_unit then
        inst.components.pm_unit:SendFightRequest(other)
    end
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()

    MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(3, .75)
    inst.Transform:SetTwoFaced()

	inst.AnimState:SetBank("pm_drybones")
   	inst.AnimState:SetBuild("pm_drybones_build")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.AnimState:SetScale(2, 2)

	inst:AddTag("pm_enemy")
    inst:AddTag("undead")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
        inst:ListenForEvent("pm_unit_statdirty", function(inst) OnStatDirty(inst) end)
		inst:ListenForEvent("pm_unit_stagedirty", function(inst) OnStageDirty(inst) end)
		return inst
	end

    inst.sounds = sounds
    inst.shouldwalk = true

    inst.persists = false --Remove later

    --local tuning_values = TUNING.PM.ENEMIES.DRYBONES
    --inst.PMUpdateStats = PMUpdateStats

	inst:AddComponent("inspectable")
    inst:AddComponent("health")
    inst.components.health.nofadeout = true

    inst:AddComponent("sizetweener")

    inst:AddComponent("locomotor")
    inst.components.locomotor.walkspeed = 4
    inst.components.locomotor.runspeed = 6

    inst:SetStateGraph("SGpm_drybones")
    inst:SetBrain(brain)

    --[[
    inst:AddComponent("pm_unit")
    inst.components.pm_unit:SetLevel(tuning_values.LEVEL)
    inst.components.pm_unit:SetMaxHp(tuning_values.MAXHP)]]

    --inst.Physics:SetCollisionCallback(OnCollide)

	return inst
end

return Prefab("pm_drybones", fn, assets)

