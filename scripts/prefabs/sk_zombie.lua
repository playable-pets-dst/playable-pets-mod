--require "scripts/sk_tuning"
--TODO clean this shit up!
local assets =
{
    Asset("ANIM", "anim/sk_zombie.zip"),
	Asset("ANIM", "anim/sk_zombie_dust.zip"),
	Asset("ANIM", "anim/sk_zombie_freeze.zip"),
	Asset("ANIM", "anim/sk_zombie_shock.zip"),
	Asset("ANIM", "anim/sk_zombie_poison.zip"),
	Asset("ANIM", "anim/sk_zombie_fire.zip"),
	Asset("ANIM", "anim/sk_zombie_curse.zip"),
	Asset("ANIM", "anim/sk_zombie_bellhop.zip"),
}

local prefabs =
{
    
}

local brain = require("brains/sk_zombiebrain")

local sounds =
{
	grunt = "sk/creatures/zombie/grunt",
    attack = "sk/creatures/zombie/attack",
	attack2 = "sk/creatures/zombie/attack2",
    hit = "sk/creatures/zombie/hit",
    death = "sk/common/hit/hit_zombie",
    spawn = "sk/creatures/zombie/spawn",
	attack3_pre = "sk/creatures/zombie/attack3_pre",
	attack3 = "sk/creatures/zombie/attack3",
}

SetSharedLootTable('sk_zombie',
{
    {"boneshard",       1.00},
    {"boneshard",   	1.00},
})

local t_values = TUNING.SK_ZOMBIES

local function IsBlocked(inst, afflicter)
	if afflicter then
		local anglediff = inst.Transform:GetRotation() - inst:GetAngleToPoint(afflicter.Transform:GetWorldPosition())
		return not (math.abs(anglediff) <= 90)
	else
		return false
	end
end

local function redirecthealth(inst, attacker, damage, weapon, stimuli)--(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    if amount < 0 and IsBlocked(inst, afflicter) then
		inst.isblocked = true
        inst.SoundEmitter:PlaySound("sk/common/hit/hit_invincible")
	elseif amount < 0 and not IsBlocked(inst, afflicter) then
		inst.isblocked = nil
    end   
	return inst
end

local function OnAttacked(inst, data)
	if not inst.components.follower.leader then
		inst.components.combat:SetTarget(data.attacker)
		inst.components.combat:ShareTarget(data.attacker, 30,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("zombie") or dude:HasTag("undead"))
                and data.attacker ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, t_values.TARGET_RANGE + 10)
	else
		if data.attacker and data.attacker ~= inst.components.follower.leader then
			inst.components.combat:SetTarget(data.attacker)
		end
	end
end

local function OnAttackOther(inst, data)
    inst.components.combat:ShareTarget(data.target, 20,
        function(dude)
            return not (dude.components.health ~= nil and dude.components.health:IsDead())
                and (dude:HasTag("zombie") or dude:HasTag("undead"))
                and data.target ~= (dude.components.follower ~= nil and dude.components.follower.leader or nil)
        end, t_values.TARGET_RANGE + 10)
end

local function OnKillOther(inst, data)
	if data.victim and data.victim:IsValid() and (data.victim:HasTag("character") or data.victim:HasTag("player")) and not (data.victim:HasTag("largecreature") or data.victim:HasTag("giant")) then
		local pos = data.victim:GetPosition()
		local zombie = SpawnPrefab(inst.prefab)
		zombie.Transform:SetPosition(pos.x, 0, pos.z)
		zombie.sg:GoToState("spawn")
		if inst.components.follower.leader then
			zombie.components.follower.leader = inst.components.follower.leader
		end
	
	elseif data.victim and data.victim:IsValid() and (data.victim:HasTag("character") or data.victim:HasTag("player")) and (data.victim:HasTag("largecreature") or data.victim:HasTag("giant")) then
		--Dreadnaughts spawn here
	end
end

local function OnNewTarget(inst, data)
    if inst.components.sleeper:IsAsleep() then
        inst.components.sleeper:WakeUp()
    end
end

local function GetExcludeTags(inst)
	if TheNet:GetServerGameMode() == "lavaarena" or inst:HasTag("companion") or (inst.components.follower.leader and inst.components.follower.leader:HasTag("player") and not TheNet:GetPVPEnabled()) then
		return	{ "wall", "player", "companion", "_isinheals", "smallcreature", "insect"}
	elseif inst:HasTag("bellhop") then
		return { "wall", "structure", "smallcreature", "insect", "undead", "player", "companion", "character", "animal" }
	else
		return { "wall", "zombie", "zombiefriend", "structure", "sk_enemy", "smallcreature", "insect", "undead" }
	end
end

local function retargetfn(inst)
    local leader = inst.components.follower.leader
    if leader and leader.components.combat and not leader.components.combat.target then
		return 
	elseif leader and leader.components.combat and leader.components.combat.target then
		return leader.components.combat.target or leader.current_target
	end
	return FindEntity(
                inst,
                t_values.TARGET_RANGE,
                function(guy)
                    return inst.components.combat:CanTarget(guy) and guy ~= leader
                end,
                nil,
                GetExcludeTags(inst)
            )
end

local function KeepTarget(inst, target)
    local leader = inst.components.follower.leader
    return (leader == nil or (leader and inst:IsNear(leader, t_values.FOLLOWER_RETURN_DISTANCE)))
        and inst.components.combat:CanTarget(target)
		and target ~= leader
        and not (TheNet:GetServerGameMode() == "lavaarena" and target:HasTag("_isinheals"))
end

local function OnSave(inst, data)

end

local function OnLoad(inst, data)

end

local function OnHitOther(inst, other, damage, stimuli)
    if other and (not stimuli or stimuli ~= "status") then
		if inst.status and inst.status == "freeze" and math.random() <= t_values.INFLICT_CHANCE then
			if other.components.freezable then
				other.components.freezable:Freeze(8)
			end
		elseif inst.status then
			if inst.status == "curse" then
				PlayablePets.InflictStatus(inst, other, "fire", t_values.INFLICT_CHANCE)
			else
				PlayablePets.InflictStatus(inst, other, inst.status, t_values.INFLICT_CHANCE)
			end
		end	
	end
end

local function fncommon()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

	inst:SetPhysicsRadiusOverride(0.5)
    MakeCharacterPhysics(inst, 10, .5)

    inst.DynamicShadow:SetSize(2.5, 1.5)
    inst.Transform:SetFourFaced()
	local scale = 0.9
	inst.Transform:SetScale(scale, scale, scale)

    inst:AddTag("scarytoprey")
	inst:AddTag("zombie")
	inst:AddTag("undead")
	inst:AddTag("monster")

    inst.AnimState:SetBank("sk_zombie")
    inst.AnimState:SetBuild("sk_zombie_dust")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.altattack = true
	inst.can_breath = true
	inst.shouldwalk = true
	
	inst.hit_recovery = t_values.HIT_RECOVERY

    inst.sounds = sounds

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
	inst.components.locomotor.walkspeed = t_values.WALK_SPEED
    inst.components.locomotor.runspeed = t_values.RUN_SPEED
    inst:SetStateGraph("SGsk_zombie")

    inst:SetBrain(brain)

    inst:AddComponent("follower")	
	inst.components.follower.keepleaderonattacked = true
	
    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(t_values.HEALTH)
	inst.components.health.nofadeout = true

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_MED

	inst:AddComponent("colouradder")
	inst:AddComponent("colourtweener")
	
    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(t_values.DAMAGE)
    inst.components.combat:SetAttackPeriod(t_values.ATTACK_PERIOD)
	inst.components.combat:SetRange(t_values.ATTACK_RANGE, t_values.HIT_RANGE)
    inst.components.combat:SetRetargetFunction(3, retargetfn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)
    inst.components.combat:SetHurtSound("sk/common/hit/hit_zombie")
	inst.components.combat.onhitotherfn = OnHitOther
	
    inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable('sk_zombie')

    inst:AddComponent("inspectable")
	
	inst:AddComponent("named")

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

	inst:ListenForEvent("killed", PlayablePets.ZombieOnKill)
    inst:ListenForEvent("attacked", OnAttacked)
	
	PlayablePets.SetCommonStatResistances(inst, 2, 2) --fire, acid, poison
	
	PlayablePets.SetFamily(inst, SK_FAMILIES.UNDEAD) --inst, prefaboverride

    return inst
end

local function fndefault()
    local inst = fncommon()

    if not TheWorld.ismastersim then
        return inst
    end

	inst.variant = ""
	
    MakeMediumFreezableCharacter(inst, "rib")
    MakeMediumBurnableCharacter(inst, "rib")

    return inst
end

local function fnfire()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_zombie_fire")
	
    if not TheWorld.ismastersim then
        return inst
    end

	inst.variant = "_fire"
	inst.status = "fire"
	
    MakeMediumFreezableCharacter(inst, "rib")

  	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(180/255, 90/255, 30/255)

    return inst
end

local function fncurse()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_zombie_curse")
	
    if not TheWorld.ismastersim then
        return inst
    end

    MakeMediumFreezableCharacter(inst, "rib")
    --inst.components.freezable:SetResistance(4) --because fire
	inst.variant = "_curse"
	inst.status = "curse"

  	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(244/255, 69/255, 255/255)
	
	local fx = SpawnPrefab("sk_enemy_shieldp") --todo, not showing up for clients
	fx.Transform:SetScale(1.25, 1.25, 1.25)
	fx.AnimState:PlayAnimation("back")
    fx.entity:SetParent(inst.entity)
	inst.fx = fx
	
	local _oldGetAttacked = inst.components.combat.GetAttacked
	inst.components.combat.GetAttacked = function(self, attacker, damage, weapon, stimuli)
		local absorb = 0
		if IsBlocked(inst, attacker) then
			SpawnPrefab("sk_hit_sparksp").Transform:SetPosition(inst:GetPosition():Get())
			inst.SoundEmitter:PlaySound("sk/common/hit/hit_invincible")
			absorb = 1
			inst.isblocked = true
		end
		if self.inst.components.health.absorb ~= absorb then self.inst.components.health:SetAbsorptionAmount(absorb) end
		return _oldGetAttacked(self, attacker, damage, weapon, stimuli)
	end

    return inst
end

local function fnshock()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_zombie_shock")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "_shock"
	inst.status = "shock"

    MakeMediumFreezableCharacter(inst, "rib")
	MakeMediumBurnableCharacter(inst, "rib")
	
	inst.Light:Enable(true)
	inst.Light:SetRadius(t_values.LIGHT_RADIUS)
	inst.Light:SetFalloff(t_values.LIGHT_FALLOFF)
	inst.Light:SetIntensity(t_values.LIGHT_INTENSITY)
	inst.Light:SetColour(69/255, 255/255, 202/255)

    return inst
end

local function fnice()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_zombie_freeze")
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = "_freeze"
	inst.status = "freeze"
	
	MakeMediumBurnableCharacter(inst, "rib")
	MakeMediumFreezableCharacter(inst, "rib")
	inst.components.freezable:SetResistance(9999)
	
	inst.Transform:SetScale(1.1, 1.1, 1.1)

    return inst
end

local function fnpoison()
    local inst = fncommon()

	inst.AnimState:SetBuild("sk_zombie_poison")
	
    if not TheWorld.ismastersim then
        return inst
    end

	inst.variant = "_poison"
	inst.status = "poison"
	
	inst.Transform:SetScale(0.8, 0.8, 0.8)
	
    MakeMediumFreezableCharacter(inst, "rib")
	MakeMediumBurnableCharacter(inst, "rib")

    return inst
end

local function RandomizeColor(inst)
	inst.AnimState:SetMultColour(math.random(), math.random(), math.random(), 1)
end

local function fnbellhop()
    local inst = fncommon()
	
	inst:AddTag("companion")
	inst:AddTag("bellhop")
	inst:RemoveTag("monster")
	inst:RemoveTag("scarytoprey")
	inst.AnimState:SetBuild("sk_zombie_bellhop")
	
	inst.AnimState:SetScale(0.8, 0.8)	
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.variant = ""
	
	inst.RandomizeColor = RandomizeColor
	
	inst.components.health:SetMaxHealth(2000)
	inst.components.health:StartRegen(10, 2)
	
	inst.components.locomotor.walkspeed = t_values.WALK_SPEED*0.8
	
    MakeMediumFreezableCharacter(inst, "rib")
	MakeMediumBurnableCharacter(inst, "rib")
	
	inst.components.sanityaura.aura = TUNING.SANITYAURA_TINY	

    return inst
end

return Prefab("sk_zombie", fndefault, assets, prefabs),
		Prefab("sk_zombie_fire", fnfire, assets, prefabs),
		Prefab("sk_zombie_curse", fncurse, assets, prefabs),
		Prefab("sk_zombie_shock", fnshock, assets, prefabs),
		Prefab("sk_zombie_freeze", fnice, assets, prefabs),
		Prefab("sk_zombie_poison", fnpoison, assets, prefabs),
		Prefab("sk_zombie_bellhop", fnbellhop, assets, prefabs)
