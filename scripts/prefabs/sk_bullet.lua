
local assets =
{
    Asset("ANIM", "anim/sk_bullet.zip"),
	Asset("ANIM", "anim/sk_bullet_fx.zip"),
}

local prefabs =
{
    
}

local sounds =
{
	
}

local function OnHit(inst, owner, target)
	local pos = inst:GetPosition()
	local hit_fx = SpawnPrefab("sk_bullet_explodep")
	hit_fx.Transform:SetPosition(pos.x, 1.5, pos.z)
	if inst.colour then
		hit_fx.AnimState:SetMultColour(inst.colour[1], inst.colour[2], inst.colour[3], 1)
	end
	
	if target and target.components.health and not target.components.health:IsDead() and owner and target.components.combat then
		target.components.combat:GetAttacked(owner, inst.damage or TUNING.SK.BULLET.DAMAGE, nil, inst.stimuli or nil)
		if inst.status then
			if inst.status and inst.status == "freeze" and math.random() <= (inst.status_chance or TUNING.SK.BULLET.STATUS_CHANCE) then
				if target.components.freezable then
					--target.components.freezable:Freeze(10)
				end
			else
				PlayablePets.InflictStatus(owner, target, inst.status, inst.status_chance or TUNING.SK.BULLET.STATUS_CHANCE)
			end	
		end
	end
	inst:Remove()
end

local function CheckForTargets(inst)
	local current_target = inst.components.projectile and inst.components.projectile.target or inst.current_target
	local ent = FindEntity(inst, TUNING.SK.BULLET.HIT_DIST, nil, {"_combat"}, inst.NOTAGS or {"notarget", "INLIMBO", "playerghost"})
	if ent and ent:IsValid() and inst.owner and ent ~= inst.owner and ent ~= current_target then
		OnHit(inst, inst.owner, ent)
	end
end

local function common_fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
	inst.entity:AddLight()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    RemovePhysicsColliders(inst)
	
	inst:AddTag("FX")
    inst:AddTag("NOCLICK")

    inst.AnimState:SetBank("sk_bullet")
    inst.AnimState:SetBuild("sk_bullet")
    inst.AnimState:PlayAnimation("idle_loop", true)
	inst.AnimState:SetScale(1.7, 1.7, 1.7)
	
	inst.AnimState:SetLightOverride(1)
	
	inst.Light:Enable(true)
	inst.Light:SetRadius(0.25)
	inst.Light:SetFalloff(1)
	inst.Light:SetIntensity(0.5)
	inst.Light:SetColour(255/255, 255/255, 255/255)

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:AddComponent("projectile")
	inst.components.projectile:SetSpeed(TUNING.SK.BULLET.SPEED)
	inst.components.projectile:SetRange(TUNING.SK.BULLET.RANGE)
	inst.components.projectile:SetStimuli(nil)
	inst.components.projectile:SetHitDist(TUNING.SK.BULLET.HIT_DIST)
	inst.components.projectile:SetLaunchOffset(TUNING.SK.BULLET.LAUNCH_OFFSET)
	inst.components.projectile:SetOnHitFn(OnHit)
	inst.components.projectile:SetOnMissFn(inst.Remove)
	inst.components.projectile:SetHoming(false)
	
	inst.targeting_task = inst:DoPeriodicTask(0.2, CheckForTargets)
	
	
	inst:DoTaskInTime(0, function(inst)
		if not inst.permanent then
			inst.persists = false
			inst:DoTaskInTime(inst.duration or TUNING.SK.BULLET.DURATION, inst.Remove)
		end
	end)

    return inst
end

local function fn_default()
	local inst = common_fn()

	inst.AnimState:SetMultColour(255/255, 252/255, 70/255, 1)
	inst.Light:SetColour(255/255, 252/255, 70/255)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.colour = {255/255, 252/255, 70/255}
	
	return inst
end

local function fn_shadow()
	local inst = common_fn()

	inst.AnimState:SetMultColour(132/255, 3/255, 252/255, 1)
	inst.Light:SetColour(132/255, 3/255, 252/255)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.colour = {132/255, 3/255, 252/255}
	
	return inst
end

local function fn_freeze()
	local inst = common_fn()

	inst.AnimState:SetMultColour(93/255, 255/255, 255/255, 1)
	inst.Light:SetColour(93/255, 255/255, 255/255)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.status = "freeze"
	inst.colour = {93/255, 255/255, 255/255}
	
	return inst
end

local function fn_fire()
	local inst = common_fn()

	inst.AnimState:SetMultColour(255/255, 156/255, 39/255, 1)
	inst.Light:SetColour(255/255, 156/255, 39/255)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.status = "fire"
	inst.colour = {255/255, 156/255, 39/255}
	
	return inst
end

local function fn_shock()
	local inst = common_fn()

	inst.AnimState:SetMultColour(93/255, 255/255, 200/255, 1)
	inst.Light:SetColour(93/255, 255/255, 200/255)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.status = "shock"
	inst.colour = {93/255, 255/255, 200/255}
	
	return inst
end

local function fn_poison()
	local inst = common_fn()

	inst.AnimState:SetMultColour(174/255, 255/255, 46/255, 1)
	inst.Light:SetColour(0/255, 255/255, 0/255)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.status = "poison"
	inst.colour = {174/255, 255/255, 46/255}
	
	return inst
end

local function fn_chaser()
	local inst = common_fn()

	inst.AnimState:SetMultColour(180/255, 0/255, 52/255, 1)
	inst.Light:SetColour(180/255, 0/255, 52/255)
	inst.AnimState:SetScale(2.2, 2.2)
	
    if not TheWorld.ismastersim then
        return inst
    end
	
	inst:DoPeriodicTask(0.04, function(inst)
		local pos = inst:GetPosition()
		local trail = SpawnPrefab("sk_bullet_trailp")
		trail.Transform:SetPosition(pos.x, 1.8, pos.z)
		trail.AnimState:SetScale(2.2, 2.2)
		trail.AnimState:SetMultColour(180/255, 0/255, 52/255, 1)
	end)
	
	inst.components.projectile:SetHoming(true)
	inst.duration = TUNING.SK.MARGREL.BULLET_LIFE
	inst.components.projectile:SetSpeed(TUNING.SK.MARGREL.BULLET_SPEED)
	inst.colour = {180/255, 0/255, 52/255}
	
	return inst
end

return Prefab("sk_bullet", fn_default, assets, prefabs),
	Prefab("sk_bullet_shadow", fn_shadow, assets, prefabs),
	Prefab("sk_bullet_shock", fn_shock, assets, prefabs),
	Prefab("sk_bullet_fire", fn_fire, assets, prefabs),
	Prefab("sk_bullet_freeze", fn_freeze, assets, prefabs),
	Prefab("sk_bullet_poison", fn_poison, assets, prefabs),
	Prefab("sk_bullet_chaser", fn_chaser, assets, prefabs)
