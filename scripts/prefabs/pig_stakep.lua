local assets =
{
	Asset("ANIM", "anim/pig_stakep.zip"),
	Asset("ANIM", "anim/pig_stakep_build.zip"),
    Asset("ANIM", "anim/pig_corpse_pile.zip"),
}

local function MakeStake(name, data)
	local function fn()
		local inst = CreateEntity()

		inst.entity:AddTransform()
		inst.entity:AddAnimState()
		inst.entity:AddNetwork()
		inst.entity:AddSoundEmitter()

		MakeObstaclePhysics(inst, 1)

		inst.AnimState:SetBank(data.bank)
   		inst.AnimState:SetBuild(data.build)
        inst.AnimState:PlayAnimation(data.animation, true)

		inst:AddTag("pig_stake")

		inst.entity:SetPristine()

		if not TheWorld.ismastersim then
			return inst
		end

		inst:AddComponent("inspectable")

        if data.pig then
            inst:DoTaskInTime(0, function(inst)
                inst.AnimState:AddOverrideBuild(data.pig)
            end)
        end

		return inst
	end
	return Prefab(name, fn, assets)
end

return MakeStake("pig_stake1p", {bank = "pig_stakep", build = "pig_stakep_build", animation = "empty1"}),
MakeStake("pig_stake_alivep", {bank = "pig_stakep", build = "pig_stakep_build", animation = "idle_loop", pig = "pig_build"}),
MakeStake("pig_stake_deadp", {bank = "pig_stakep", build = "pig_stakep_build", animation = "dead", pig = "pig_build"}),
MakeStake("pig_corpse_pilep", {bank = "pig_corpse_pile", build = "pig_corpse_pile", animation = "idle_loop"})

