local assets = {
	Asset("ANIM", "anim/sk_curse_aura.zip"),
}

local prefabs = {}

local function common_fn()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
		
	
	return inst
end

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","kat", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	end
end

local function FadeOut(inst)
	local pos = inst:GetPosition()
	local ents = TheSim:FindEntities(pos.x, 0, pos.z, inst.range or 6, {"_combat"}, GetExcludeTags(inst))
	if #ents > 0 and inst.owner then
		for i, v in ipairs(ents) do
			if v.components.health and not v.components.health:IsDead() and v:IsValid() and inst.owner ~= v and not inst.disabled and not v.debuffimmune and math.random() < TUNING.SK.CURSE_AURA_CHANCE*(v.curse_resist or 1) then
				PlayablePets.InflictStatus(inst.owner, v, "curse", 1, 1.5)
			end
		end
	end
	inst:DoTaskInTime(0, function(inst)
		inst.components.colourtweener:StartTween({0,0,0,0}, 0.25, inst.Remove)
	end)
end

local function FadeIn(inst)
	inst.components.colourtweener:StartTween(inst.colour, 0.25, FadeOut)
end


local function curse_fn() --for blackkats
	local inst = CreateEntity()
	inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
	
	inst.AnimState:SetBank("sk_curse_aura")
	inst.AnimState:SetBuild("sk_curse_aura")
	inst.AnimState:PlayAnimation("idle")
	
	inst.AnimState:SetRayTestOnBB(true)
	inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
	inst.AnimState:SetLayer(LAYER_BACKGROUND)
	inst.AnimState:SetSortOrder(3)
	inst.AnimState:SetLightOverride(1)
	
	inst.AnimState:SetMultColour(0, 0, 0, 0)
	
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
	
	inst.AnimState:SetScale(3.5, 3.5)
	
	inst:AddTag("FX")
	inst:AddTag("NOCLICK")
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false
	
	inst.Transform:SetRotation(math.random(1, 360))
	
	inst.colour = {180/255, 0/255, 52/255, 1}
	
	inst:AddComponent("colourtweener")
	inst:DoTaskInTime(0, FadeIn)
	
	return inst
end

return Prefab("sk_curse_aura", curse_fn, assets, prefabs)