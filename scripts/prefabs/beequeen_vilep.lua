local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
-- C no longer spawns minions, but rather a large puddle that inflicts poison.
-- Regular attack inflicts poison.
-- X can enrage herself, increased speed and reduced attack period? Maybe make its own buff?
-- 20% faster default speed?
-- smaller scale?
---------------------------

local prefabname = "beequeen_vilep"

local assets = 
{
	
}

local prefabs = {
}

local start_inv = {
}

local start_inv2 = 
{

}



if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.RUNSPEED,
	walkspeed = tuning_values.WALKSPEED,
	damage = tuning_values.DAMAGE,
	range = tuning_values.RANGE,
	hit_range = tuning_values.ATTACK_RANGE,
	attackperiod = tuning_values.ATTACK_PERIOD,
	bank = "bee_queen",
	build = "bee_queen_build",
	scale = 1.4,
	--build2 = "alternate build here",
	stategraph = "SG"..prefabname,
	minimap = "beequeenp.tex",
}

--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
       
})

local FORGE_STATS = PPM_FORGE[string.upper(prefabname)] --BEEQUEEN
--==============================================
--					Mob Functions
--==============================================

--Honey Code--
local MAX_HONEY_VARIATIONS = 7
local MAX_RECENT_HONEY = 4
local HONEY_PERIOD = .2
local HONEY_LEVELS =
{
    {
        min_scale = .5,
        max_scale = .8,
        threshold = 8,
        duration = 1.2,
    },
    {
        min_scale = .5,
        max_scale = 1.1,
        threshold = 2,
        duration = 2,
    },
    {
        min_scale = 1,
        max_scale = 1.3,
        threshold = 1,
        duration = 4,
    },
}

local function PickHoney(inst)
    local rand = table.remove(inst.availablehoney, math.random(#inst.availablehoney))
    table.insert(inst.usedhoney, rand)
    if #inst.usedhoney > MAX_RECENT_HONEY then
        table.insert(inst.availablehoney, table.remove(inst.usedhoney, 1))
    end
    return rand
end

local function DoHoneyTrail(inst)
    local level = HONEY_LEVELS[
        (not inst.sg:HasStateTag("moving") and 1) or
        (inst.components.locomotor.walkspeed <= TUNING.BEEQUEEN_SPEED and 2) or
        3
    ]

    inst.honeycount = inst.honeycount + 1

    if inst.honeythreshold > level.threshold then
        inst.honeythreshold = level.threshold
    end

    if inst.honeycount >= inst.honeythreshold then
        inst.honeycount = 0

        if inst.honeythreshold < level.threshold then
            inst.honeythreshold = math.ceil((inst.honeythreshold + level.threshold) * .5)
        end

        local fx = SpawnPrefab("vile_trailp")
        fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
        fx:SetVariation(PickHoney(inst), GetRandomMinMax(level.min_scale, level.max_scale), level.duration + math.random() * .5)
    end
end

local function StartHoney(inst)
    if inst.honeytask == nil then
        inst.honeythreshold = HONEY_LEVELS[1].threshold
        inst.honeycount = math.ceil(inst.honeythreshold * .5)
        inst.honeytask = inst:DoPeriodicTask(HONEY_PERIOD, DoHoneyTrail, 0)
    end
end

local function StopHoney(inst)
    if inst.honeytask ~= nil then
        inst.honeytask:Cancel()
        inst.honeytask = nil
    end
end

--==============================================
--				Custom Common Functions
--==============================================	
local function onhitother2(inst, other)
	if math.random() < tuning_values.POISON_CHANCE then
        PlayablePets.SetPoison(inst, other)
    end
end

local function OnAttackOther(inst, data)
    if data.target ~= nil then
        local fx = SpawnPrefab("vile_trailp")
        fx.Transform:SetPosition(data.target.Transform:GetWorldPosition())
        fx:SetVariation(PickHoney(inst), GetRandomMinMax(1, 1.3), 4 + math.random() * .5)
    end
end

local function OnMissOther(inst)
    local x, y, z = inst.Transform:GetWorldPosition()
    local angle = -inst.Transform:GetRotation() * DEGREES
    local fx = SpawnPrefab("vile_trailp")
    fx.Transform:SetPosition(x + TUNING.BEEQUEEN_ATTACK_RANGE * math.cos(angle), 0, z + TUNING.BEEQUEEN_ATTACK_RANGE * math.sin(angle))
    fx:SetVariation(PickHoney(inst), GetRandomMinMax(1, 1.3), 4 + math.random() * .5)
end

----PvP Teleport----

local function getwildposition(inst)
    local ground = TheWorld
    local centers = {}
    for i, node in ipairs(ground.topology.nodes) do
        if ground.Map:IsPassableAtPoint(node.x, 0, node.y) then
            table.insert(centers, {x = node.x, z = node.y})
        end
    end
    if #centers > 0 then
        local pos = centers[math.random(#centers)]
        return Point(pos.x, 0, pos.z)
    else
        return inst:GetPosition()
    end
end

local function PvPTeleport(inst)
if inst.mobteleported == nil or inst.mobteleported == false then
	local ground = TheWorld
	local pt = getwildposition(inst)--Point(inst.Transform:GetWorldPosition())
      if pt.y <= .1 then
         pt.y = 0
         inst.Physics:Stop()
         inst.Physics:Teleport(pt.x,pt.y,pt.z)
		 inst.mobteleported = true
      end
	end
end

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false) --PP doesn't work with movement prediction enabled, due to use of custom stategraphs
   end
end)
	----------------------------------
	--Tags--
    inst:AddTag("epic")
    inst:AddTag("bee")
    inst:AddTag("monster")
    inst:AddTag("flying")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
end



local master_postinit = function(inst)
	--Stats--
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20, 1) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 1, 0) --fire, acid, poison
	----------------------------------
	--Variables	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	inst.ghostbuild = "ghost_monster_build"
	
	
	inst.StartHoney = StartHoney
    inst.StopHoney = StopHoney
    inst.honeytask = nil
    inst.honeycount = 0
    inst.honeythreshold = 0
    inst.usedhoney = {}
    inst.availablehoney = {}
    for i = 1, MAX_HONEY_VARIATIONS do
        table.insert(inst.availablehoney, i)
    end
    inst:StartHoney()
	
	inst.hit_recovery = 1
	
	inst.SoundEmitter:PlaySound("dontstarve/creatures/together/bee_queen/wings_LP", "flying")
	
	local body_symbol = "hive_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	
	inst:AddComponent("timer")
	inst.focustarget_cd = TUNING.BEEQUEEN_FOCUSTARGET_CD[1]
    inst.spawnguards_cd = TUNING.BEEQUEEN_SPAWNGUARDS_CD[1]
    inst.spawnguards_threshold = TUNING.BEEQUEEN_TOTAL_GUARDS or 1
	
	inst:AddComponent("epicscare")
    inst.components.epicscare:SetRange(TUNING.BEEQUEEN_EPICSCARE_RANGE)
	inst.AnimState:PlayAnimation("idle_loop", true)
	----------------------------------
	--Eater--
	inst.components.eater:SetAbsorptionModifiers(1,3,3) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	---------------------------------
	--Physics and Shadows--
	MakeFlyingGiantCharacterPhysics(inst, 500, 1.4)
	PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
    inst.DynamicShadow:SetSize(4, 2)
    inst.Transform:SetSixFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("attacked", onattacked)
	inst:ListenForEvent("onattackother", OnAttackOther)
	inst:ListenForEvent("onmissother", OnMissOther)
	inst:ListenForEvent("death", StopHoney)
	inst.components.combat.onhitotherfn = onhitother2
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
		StartHoney(inst)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)