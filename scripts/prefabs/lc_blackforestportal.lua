local assets =
{
	Asset("ANIM", "anim/lc_blackforestportal.zip"),
}

local function TransformBirds(inst, birds)
    local apocbird = birds[math.random(3)]
    TheNet:Announce("Apocbird is born!")
    --apocbird.components.seamlessplayerswapper:_StartSwap("lc_apocbirdp")
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, 3, {"player"}, {"notarget", "INLIMBO", "playerghost"}, {"bigbird", "punishingbird", "judgementbird"})
    if #ents > 0 then
        ents[math.random(1, #ents)].components.seamlessplayerswapper:_StartSwap("lc_apocbirdp")
    end
    inst.components.lighttweener:StartTween(inst.Light, 0, 0, 0, {1, 0, 0}, 2)
    inst.components.colourtweener:StartTween({1, 1, 1, 0}, 3, inst.Remove)
end

local function FindBirds(inst)
    if not inst:HasTag("completed") then
        local pos = inst:GetPosition()
        local ents = TheSim:FindEntities(pos.x, 0, pos.z, 3, {"player"}, {"notarget", "INLIMBO", "playerghost"})
        if #ents > 0 then
            for i, v in ipairs(ents) do
                if v:IsValid() and not v.components.health:IsDead() then
                    if not inst.birds.bird1 and v.prefab == "lc_bigbirdp" then
                        inst.birds.bird1 = v
                    end
                    if not inst.birds.bird2 and v.prefab == "lc_judgementbirdp" then
                        inst.birds.bird2 = v
                    end
                    if not inst.birds.bird3 and v.prefab == "lc_punishingbirdp" then
                        inst.birds.bird3 = v
                    end
                end
            end
        end
        if inst.birds.bird1 and inst.birds.bird2 and inst.birds.bird3 then
            inst:AddTag("completed")
            inst.portal_task:Cancel()
            TransformBirds(inst, {inst.birds.bird1, inst.birds.bird2, inst.birds.bird3})
        else
            inst.birds = {}
        end
    end
end

local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()

	inst.AnimState:SetBank("lc_blackforestportal")
   	inst.AnimState:SetBuild("lc_blackforestportal")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst.AnimState:SetMultColour(1, 1, 1, 0)

	inst:AddTag("blackforestportal")

    inst.entity:AddLight()
    inst.Light:SetIntensity(0)
	inst.Light:SetRadius(0)
	inst.Light:SetFalloff(0)
	inst.Light:SetColour(1, 0, 0)
    inst.Light:Enable(true)

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

    TheNet:Announce("The Black Forest Portal has appeared in the world!")

    inst.birds = {}

	inst:AddComponent("inspectable")

    inst:AddComponent("colourtweener")
    inst.components.colourtweener:StartTween({1, 1, 1, 1}, 2)

    inst:AddComponent("lighttweener")
    inst.components.lighttweener:StartTween(inst.Light, 10, 0.9, 0.5, {1, 0, 0}, 5)

    inst.portal_task = inst:DoPeriodicTask(0.2, FindBirds)

	return inst
end

return Prefab("lc_blackforestportal", fn, assets)

