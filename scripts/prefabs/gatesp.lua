local prefabs =
{

}

local assets =
{
    Asset("ANIM", "anim/pp_gate_end.zip"),
    Asset("ANIM", "anim/pp_gate_pole.zip"),
    Asset("ANIM", "anim/pp_gate_base.zip"),
}

--TODO component maybe?
-----------------------------------------------
--Trigger Functions

local function KillGateSound(inst)
    inst.SoundEmitter:KillSound("power")
    inst.SoundEmitter:PlaySound("dontstarve/common/together/catapult/hit")
end

local function MakeObstacle(inst)
    inst.Physics:SetActive(true)
    --inst._ispathfinding:set(true)
end

local function ClearObstacle(inst)
    inst.Physics:SetActive(false)
    --inst._ispathfinding:set(false)
end

local function OpenTheGates(inst)
    inst._isactive = true
    inst.SoundEmitter:PlaySound("dontstarve/common/together/catapult/ratchet_LP", "power")
    for i, v in ipairs(inst.gate_children.gate_poles) do
        v.AnimState:PlayAnimation("open_pre")
        v.AnimState:PushAnimation("open", false)
        v:ListenForEvent("animqueueover", function() KillGateSound(inst) end)
        ClearObstacle(v)
    end
end

local function CloseTheGates(inst)
    inst._isactive = false
    inst.SoundEmitter:PlaySound("dontstarve/common/together/catapult/ratchet_LP", "power")
    for i, v in ipairs(inst.gate_children.gate_poles) do
        v.AnimState:PlayAnimation("closed_pre")
        v.AnimState:PushAnimation("closed", false)
        v:ListenForEvent("animqueueover", function() KillGateSound(inst) end)
        MakeObstacle(v)
    end
end

local function OnTrigger(inst, force)
    local active = force or inst._isactive
    if active then
        CloseTheGates(inst)
    else
        OpenTheGates(inst)
    end
end
-----------------------------------
--Load Functions
local function OnSave(inst, data, newents)
    data.size = inst.size or 1
    data.id = inst._trigger_id or ""
    data.rotation = inst.Transform:GetRotation() --why do I need to save this??? figure it out later...
end

local function OnLoad(inst, data, newents)
    if data then
        inst.Transform:SetRotation(data.rotation or 0)
        inst._trigger_id = data.id or nil --this is for pairing with buttons
        inst._isactive = data.active or false
        inst.size = data.size or 1
    end
end

local function SpawnGateStuff(inst)
    local pos = inst:GetPosition()
    local rotation = inst.Transform:GetRotation() - 90
    local angle = -rotation * DEGREES
    --If using parenting, you'll need to create custom physics

    --Gate Ends
    local end_offset = 2.7 * (inst.size - (-0.2+(0.2*inst.size))) --TODO set a range.
    local end_offset_pos1 = Vector3(math.cos(angle), 0, math.sin(angle)) * end_offset
    local end_offset_pos2 = Vector3(math.cos(angle), 0, math.sin(angle)) * -end_offset
    local gate_end1 = SpawnPrefab("pp_gate_end")
    gate_end1.Transform:SetPosition(pos.x + (-end_offset * math.cos(angle)), 0, pos.z + (-end_offset * math.sin(angle)))
    --gate_end1.AnimState:SetMultColour(1, 0, 0, 1)
    gate_end1.Transform:SetRotation(inst.Transform:GetRotation())
    local gate_end2 = SpawnPrefab("pp_gate_end")
    gate_end2.Transform:SetPosition(pos.x + (end_offset * math.cos(angle)), 0, pos.z + (end_offset * math.sin(angle)))
    --gate_end2.AnimState:SetMultColour(0, 0, 1, 1)
    gate_end2.Transform:SetRotation(inst.Transform:GetRotation())

    inst.gate_children.gate_ends[1] = gate_end1
    inst.gate_children.gate_ends[2] = gate_end2

    --Gate Poles
    --type1
    local num_poles = 3 * math.floor(inst.size+0.5)
    for i = 1, num_poles do
        local pole = SpawnPrefab("pp_gate_pole")
        local pole_offset = 1.55 - (0.023*inst.size)
        --local pole_offset_pos = Vector3(math.cos(angle), 0, math.sin(angle)) * (pole_offset*i)

        --local pole_offset_pos = Vector3(pos.x - end_offset, 0, pos.z) * pole_offset
        --pole.Transform:SetPosition(pos.x - end_offset - 0.3 + (pole_offset*i), 0, pos.z)
        pole.Transform:SetPosition(pos.x + (-(-(end_offset + 0.4) + (pole_offset*i)) * math.cos(angle)), 0, pos.z + (-(-(end_offset + 0.4) + (pole_offset*i)) * math.sin(angle)))
        pole.Transform:SetRotation(inst.Transform:GetRotation())
        inst.gate_children.gate_poles[i] = pole
    end
end

-----------------------------------------------------

local function CreateGate(name, data)
    local function end_fn()
        local inst = CreateEntity()
    
        inst.entity:AddTransform()
        inst.entity:AddNetwork()
        inst.entity:AddAnimState()
        inst.entity:AddFollower()

        MakeObstaclePhysics(inst, 0.5)
    
        inst.AnimState:SetBank("pp_gate_end")
        inst.AnimState:SetBuild("pp_gate_end")
        inst.AnimState:PlayAnimation("idle_loop", false)

        inst.AnimState:SetScale(1.2, 1.5)
    
        inst:AddTag("NOCLICK")
        inst:AddTag("FX")
        ------------------------------------------
        inst.entity:SetPristine()
        ------------------------------------------
        if not TheWorld.ismastersim then
            return inst
        end

        inst.persists = false

        ------------------------------------------
        return inst
    end
    local function pole_fn()
        local inst = CreateEntity()
    
        inst.entity:AddTransform()
        inst.entity:AddNetwork()
        inst.entity:AddAnimState()
        inst.entity:AddFollower()
    
        inst.AnimState:SetBank("pp_gate_pole")
        inst.AnimState:SetBuild("pp_gate_pole")
        inst.AnimState:PlayAnimation("closed", false)

        MakeObstaclePhysics(inst, 0.5)

        inst.AnimState:SetScale(1, 1.3)

        inst.Transform:SetEightFaced()
    
        inst:AddTag("NOCLICK")
        inst:AddTag("FX")
        ------------------------------------------
        inst.entity:SetPristine()
        ------------------------------------------
        if not TheWorld.ismastersim then
            return inst
        end

        inst.persists = false

        ------------------------------------------
        return inst
    end
    local function base_fn()
        local inst = CreateEntity()
    
        inst.entity:AddTransform()
        inst.entity:AddNetwork()
        inst.entity:AddSoundEmitter()
        inst.entity:AddAnimState()
    
        inst.AnimState:SetBank("pp_gate_base")
        inst.AnimState:SetBuild("pp_gate_base")
        inst.AnimState:PlayAnimation("idle_loop", false)

        inst.AnimState:SetMultColour(0, 0, 0, 0.01) --so it can still be targeted by findents.
    
        inst.AnimState:SetOrientation(ANIM_ORIENTATION.OnGround)
        inst.AnimState:SetLayer(LAYER_BACKGROUND)
    
        inst:AddTag("NOCLICK")
        inst:AddTag("gate")
        inst:AddTag("triggerable")
        ------------------------------------------
        inst.entity:SetPristine()
        ------------------------------------------
        if not TheWorld.ismastersim then
            return inst
        end

        inst._isactive = false --false means the gates are up, true means they are down.
        inst._trigger_id = ""
        inst.size = 1
        inst.gate_children = {
            gate_poles = {},
            gate_ends = {}
        }
        inst._triggerfn = OnTrigger  --maybe make this a listenforevent instead
        inst.OpenTheGates = OpenTheGates
        inst.CloseTheGates = CloseTheGates

        inst:DoTaskInTime(0, function(inst)
            SpawnGateStuff(inst)
        end)

        inst.OnLoad = OnLoad
        inst.OnSave = OnSave

        ------------------------------------------
        return inst
    end

    return Prefab("pp_gate_base", base_fn, assets),
    Prefab("pp_gate_pole", pole_fn, assets),
    Prefab("pp_gate_end", end_fn, assets)
end

return CreateGate("", {})
	
