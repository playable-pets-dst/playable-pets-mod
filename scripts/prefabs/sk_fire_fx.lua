local assets=
{
	Asset("ANIM", "anim/fire.zip"),
    Asset("SOUND", "sound/common.fsb"),
}

local prefabs=
{
	
}

local function OnLoad(inst, data)
	inst:Remove()
end

local function OnIgnite(inst)
end

local function OnExtinguish(inst)
	inst:Remove()
end

local function fn(Sim)
	local inst = CreateEntity()
	inst.entity:AddTransform()
	inst.entity:AddSoundEmitter()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	
	inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
    inst.AnimState:SetBank("fire")
    inst.AnimState:SetBuild("fire")
	inst.AnimState:PlayAnimation("level4", true)
    inst.AnimState:SetRayTestOnBB(true)
    inst.AnimState:SetFinalOffset(-1)
	
	inst.SoundEmitter:PlaySound("dontstarve/common/fireBurstLarge")
	inst.SoundEmitter:PlaySound("dontstarve/common/forestfire", "fire")
	inst.SoundEmitter:SetParameter("fire", "intensity", 1)
	
	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end
	
	inst.persists = false

	inst:AddTag("fire")
	inst:AddTag("NOCLICK")
	
	local light = inst.entity:AddLight()
	light:SetFalloff(.2)
    light:SetIntensity(0.9)
    light:SetRadius(6)
    light:Enable(true)
    light:SetColour(255/255,190/255,121/255)
	
	inst.OnLoad = OnLoad

	return inst
end

return Prefab( "common/sk_fire_fx", fn, assets, prefabs)
