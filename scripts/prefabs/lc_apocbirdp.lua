local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "lc_apocbirdp"

local assets = 
{
	Asset("ANIM", "anim/apocbird.zip"),
	Asset("ANIM", "anim/apocbird_pt1.zip"),
	Asset("ANIM", "anim/apocbird_pt2.zip"),
	Asset("ANIM", "anim/apocbird_pt3.zip"),
	Asset("ANIM", "anim/apocbird_pt4.zip"),
	Asset("ANIM", "anim/apocbird_puppet.zip"),
	Asset("ANIM", "anim/lc_laserprojectile.zip"),
}
local symbols = {
	{"arm", "body", "fingers", "hand", "head"},
	{"mouth", "neck", "teleport", "mouthattack", "zeye", "shine", "scale"},
	{"wings", "luresfx", "beamattack"},
	{"teleport"},
}


local getskins = {}

local prefabs = 
{	

}

local tuning_values = TUNING.PP.LC_APOCBIRDP
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.RUNSPEED,
	walkspeed = tuning_values.WALKSPEED,
	damage = tuning_values.DAMAGE,
	range = tuning_values.RANGE,
	hit_range = tuning_values.ATTACK_RANGE,
	attackperiod = tuning_values.ATTACK_PERIOD,
	bank = "lc_apocbird",
	build = "apocbird_pt1",
	scale = 1,
	stategraph = "SGlc_apocbirdp",
	minimap = "lc_apocbirdp.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
	{'monstermeat',             1.00},
	{'monstermeat',             1.00},
	{'monstermeat',             1.00},
	{'monstermeat',             1.00}, 
	{'monstermeat',             1.00},
	{'monstermeat',             1.00},
	{'monstermeat',             1.00},
	{'monstermeat',             1.00}, 
	{'monstermeat',             1.00},
	{'monstermeat',             1.00},
})

local FORGE_STATS = PPM_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local soundpath = "lobcorp/creatures/apocbird/"
local sounds =
{
    cast = soundpath.."cast",
	ambience = soundpath.."ambience",
	crackle = soundpath.."crackle",
	laser_fire = soundpath.."laser_fire",
	laser_boom = soundpath.."laser_boom",
	bite = "lobcorp/creatures/bigbird/bigdamage",
	death = soundpath.."death",
	scream = soundpath.."scream",
	slam = soundpath.."slam",
	laser_cast = soundpath.."laser_cast",
	step = "lobcorp/creatures/bigbird/step",
	teleport = soundpath.."teleport",
}

local function OnHitOther(inst, data)
	if inst.sg:HasStateTag("biting") then
		inst.SoundEmitter:PlaySound(inst.sounds.attack_hit)
	elseif inst.sg:HasStateTag("bigbiting") then
		inst.SoundEmitter:PlaySound(inst.sounds.attack2_hit)
	end
	if inst.sg:HasStateTag("biting") or inst.sg:HasStateTag("bigbiting") then
		if data.target and data.target:IsValid() and data.target.components.sanity and data.damage then
			data.target.components.sanity:DoDelta(data.damage/2, false)
		end
	end
end

local function CalcSanityAura(inst, observer)
    return (inst.components.health ~= nil and not inst.components.health:IsDead() and -TUNING.SANITYAURA_LARGE)
        or 0
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow", "lobcorp_mob"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "lobcorp_mob"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "lobcorp_mob"}
	end
end

local function CastProjectiles(inst)
	inst.SoundEmitter:PlaySound(inst.sounds.laser_fire)
	local pos = inst:GetPosition()
	local range = tuning_values.PROJECTILE_RANGE
	local ents = TheSim:FindEntities(pos.x,0,pos.z, range, {"_health", "_combat"}, GetExcludeTagsp(inst))
	if #ents > 0 then
		inst:DoTaskInTime(tuning_values.FIRE_DELAY, function(inst)
			for i = 0, tuning_values.MAX_PROJECTILES do
				local target = #ents > 0 and ents[math.random(1, #ents)] or nil
				inst:DoTaskInTime(i*tuning_values.PROJ_DELAY, function(inst) 
					local targetpos = (target and target:IsValid() and not target.components.health:IsDead()) and target:GetPosition() or {x = pos.x + math.random(-range/2, range/2), y = 0, z = pos.z + math.random(-range/2, range/2)}
					local projectile = SpawnPrefab("lc_apocbird_projectile")
					projectile.owner = inst
					projectile.Transform:SetPosition(targetpos.x, 0, targetpos.z)
				end)				
			end
		end)
	end
end

--==============================================
--				Custom Common Functions
--==============================================	

--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		
	end
end

local function OnSave(inst, data)
	
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================
local function NoHoles(pt)
    return not TheWorld.Map:IsGroundTargetBlocked(pt) 
end

local function ReticuleTargetFn(inst)
    local rotation = inst.Transform:GetRotation() * DEGREES
    local pos = inst:GetPosition()
    pos.y = 0
    for r = 13, 4, -.5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    for r = 13.5, 16, .5 do
        local offset = FindWalkableOffset(pos, rotation, r, 1, false, true, NoHoles)
        if offset ~= nil then
            pos.x = pos.x + offset.x
            pos.z = pos.z + offset.z
            return pos
        end
    end
    pos.x = pos.x + math.cos(rotation) * 13
    pos.z = pos.z - math.sin(rotation) * 13
    return pos
end

local function GetPointSpecialActions(inst, pos, useitem, right)
    if right and useitem == nil then
        local rider = inst.replica.rider
        if rider == nil or not rider:IsRiding() then
            return { ACTIONS.HULK_TELEPORT}
        end
    end
    return {}
end

local function OnSetOwner(inst)
    if inst.components.playeractionpicker ~= nil and TheNet:GetServerGameMode() ~= "lavaarena" then
        inst.components.playeractionpicker.pointspecialactionsfn = GetPointSpecialActions
    end
end

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("lobcorp_mob")
	inst:AddTag("apocbird")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)

	inst:ListenForEvent("setowner", OnSetOwner)
	
	inst:AddComponent("reticule")
    inst.components.reticule.targetfn = ReticuleTargetFn
    inst.components.reticule.ease = true
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Variables			
	inst.isshiny = 0
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	inst.taunt3 = true
	inst.shouldwalk = false
	inst.can_tele = true
	inst.attack2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"

	inst.CastProjectiles = CastProjectiles

	PlayablePets.ApplyMultiBuild(inst, symbols, "apocbird")
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
    inst.components.burnable:SetOnBurntFn(nil)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 9999) --fire, acid, poison

	inst.SoundEmitter:PlaySound(inst.sounds.ambience, "amb_loop")
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Eater--
	inst.components.eater:SetDiet({ FOODGROUP.OMNI }, { FOODGROUP.OMNI })
    inst.components.eater:SetCanEatHorrible()
    inst.components.eater:SetCanEatRaw()
    inst.components.eater.strongstomach = true
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 1000, 2)
    inst.DynamicShadow:SetSize(8, 3)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip)
	--inst:ListenForEvent("onhitother", OnHitOther)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	


	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) inst.SoundEmitter:PlaySound(inst.sounds.ambience, "amb_loop") end)
    end)
	inst.OnSave = OnSave
	inst.OnLoad = OnLoad
	--PlayablePets.SetCommonSaveAndLoad(inst, OnSave, OnLoad)
	
    return inst	
end

------------------------------
--Projectile--

local function ProjectileExplode(inst)
	local targetpos = inst:GetPosition()
	local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 2, nil, GetExcludeTagsp(inst))
	local targets = {}
    local poundfx = SpawnPrefab("pp_ground_fx")
    poundfx.Transform:SetPosition(targetpos.x, 0, targetpos.z)
	poundfx.AnimState:SetLightOverride(1)
	poundfx.AnimState:SetSaturation(0)
	poundfx.AnimState:SetMultColour(1, 218/255, 81/255, 1)
	poundfx.AnimState:SetScale(0.25, 0.25)
	poundfx.SoundEmitter:PlaySound(sounds.laser_boom)
	ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 8)
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= (inst.owner or inst) then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst.owner or inst, tuning_values.LASER_DAMAGE/(v:HasTag("player") and 2 or 1))
			end
		end
	end
	inst:Remove()
end

local function projfn()
	local inst = CreateEntity()
    ------------------------------------------
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()
    ------------------------------------------
	inst.AnimState:SetBank("lc_apocbird_projectile")
    inst.AnimState:SetBuild("lc_laserprojectile")
    inst.AnimState:PlayAnimation("fall")

	inst.AnimState:Hide("impactholder")
	inst.AnimState:Hide("lureholder")

	inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

	inst:ListenForEvent("animover", ProjectileExplode)

	return inst
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv),
	Prefab("lc_apocbird_projectile", projfn, assets, prefabs)
