require "prefabutil"

local assets =
{
    Asset("ANIM", "anim/pig_cage.zip"),
    Asset("ANIM", "anim/pig_cage_build.zip"),
}

local prefabs =
{
    --"merm",
    "collapse_big",

    --loot:
    "boards",
    "rocks",
    --"fish",
}

local loot =
{
    "boards",
    "rocks",
    
}

local function onhammered(inst, worker)
	if inst.components.sleepingbag then
		inst.components.sleepingbag:DoWakeUp()
	end	
    if inst.components.burnable ~= nil and inst.components.burnable:IsBurning() then
        inst.components.burnable:Extinguish()
    end
    if inst.components.lootdropper then
        inst.components.lootdropper:DropLoot()
    end
    local fx = SpawnPrefab("collapse_big")
    fx.Transform:SetPosition(inst.Transform:GetWorldPosition())
    fx:SetMaterial("wood")
    inst:Remove()
end

local function onhit(inst, worker)
    if not inst:HasTag("burnt") then
        inst.AnimState:PlayAnimation("hit")
        inst.AnimState:PushAnimation("idle_loop", true)
    end
end

local function onbuilt(inst)
    inst.AnimState:PlayAnimation("build")
    inst.AnimState:PushAnimation("idle_loop", true)
end

local function onsave(inst, data)
    if inst:HasTag("burnt") or (inst.components.burnable ~= nil and inst.components.burnable:IsBurning()) then
        data.burnt = true
    end
end

local function onload(inst, data)
    if data ~= nil and data.burnt then
        inst.components.burnable.onburnt(inst)
    end
end

local function onignite(inst)

end

local function onburntup(inst)
    inst.AnimState:PlayAnimation("burnt")
end
local function CreateCage(name, build, anim, pig_build)
    local function common_fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddSoundEmitter()
        inst.entity:AddMiniMapEntity()
        inst.entity:AddNetwork()

        MakeObstaclePhysics(inst, 1)

        inst.AnimState:SetBank("pig_cage")
        inst.AnimState:SetBuild(build or "pig_cage_build")
        inst.AnimState:PlayAnimation(anim or "idle_loop", true)
        
        inst:AddTag("cage")
        inst:AddTag("structure")

        if pig_build then
            inst.AnimState:AddOverrideBuild(pig_build)
            inst.is_occupied = true
        end

        MakeSnowCoveredPristine(inst)

        inst.entity:SetPristine()

        if not TheWorld.ismastersim then
            return inst
        end
        --[[
        inst:AddComponent("lootdropper")
        inst.components.lootdropper:SetLoot(loot)
    
        inst:AddComponent("workable")
        inst.components.workable:SetWorkAction(ACTIONS.HAMMER)
        inst.components.workable:SetWorkLeft(3)
        inst.components.workable:SetOnFinishCallback(onhammered)
        inst.components.workable:SetOnWorkCallback(onhit)]]

        inst:AddComponent("inspectable")

        return inst
    end
    return Prefab("pw_cage_"..name, common_fn, assets, prefabs)
end
return CreateCage("empty"),
CreateCage("pig", nil, nil, "pig_slave_build"),
CreateCage("pig_sleeping", nil, "sleep_loop", "pig_slave_build")
