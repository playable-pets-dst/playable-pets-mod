local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
local tuning_values = TUNING.PP.COTL_HEALERP

local assets = 
{
	Asset("ANIM", "anim/cotl_sorceror.zip"), 
    Asset("ANIM", "anim/cotl_healer_build.zip"), 
    Asset("ANIM", "anim/cotl_enemyspawn.zip"),   
}

local prefabs = 
{	

}
	
local prefabname = "cotl_healerp"	
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.RUNSPEED/1.4,
	walkspeed = tuning_values.WALKSPEED/1.4,
	
	attackperiod = tuning_values.ATTACKPERIOD,
	damage = tuning_values.DAMAGE,
	range = tuning_values.RANGE,
	hit_range = tuning_values.HIT_RANGE,
	
	bank = "cotl_sorceror",
	shiny = "cotl_sorceror",
	build = "cotl_healer_build",
	scale = 1.4,
	--build2 = "alternate build here",
	stategraph = "SGcotl_sorcerorp",
	minimap = prefabname..".tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
 
})
------------------------------------------------------
 
local soundpath = "pp_sounds/creatures/cotl_cultist/guardian/"
local sounds =
{
    hit = soundpath.."attack",
    attack = soundpath.."attack",
	death = soundpath.."attack",
	attack_slash = "pp_sounds/creatures/cotl_cultist/attack_slash",
    teleport_in = soundpath.."teleport_in", 
	teleport_out = soundpath.."teleport_out", 
    talk = soundpath.."attack2",
}

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound(inst.sounds.talk)
end

local function DoSpecial(inst, pos)
    if pos then
        local fx = SpawnPrefab("pp_ground_fx")
        fx.Transform:SetPosition(pos.x, pos.y, pos.z)
        fx.AnimState:SetLightOverride(1)
        fx.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
        fx.AnimState:SetScale(0.25, 0.25)
        fx.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/spawn", nil, .5)
        fx.owner = inst

        fx:DoTaskInTime(5*FRAMES, function(inst)
            local heal_ents = {}
            local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, tuning_values.HEAL_RANGE, {"_health", "_combat"}, {"structure", "wall", "notarget", "INLIMBO", "playerghost"})
            if #ents > 0 then
                fx.SoundEmitter:PlaySound("dontstarve/characters/wortox/soul/heal")
                for i, v in ipairs(ents) do
                    if v:IsValid() and v ~= fx.owner and v.components.health and not v.components.health:IsDead() then
                        v.components.health:DoDelta(tuning_values.HEAL_AMOUNT)
                        local heal_fx = SpawnPrefab("wortox_soul_heal_fx")
                        heal_fx.entity:SetParent(v.entity)
                        heal_fx.AnimState:SetAddColour(1, 1, 1, 0)
                        heal_fx.AnimState:SetLightOverride(1)
                        heal_fx.AnimState:SetBloomEffectHandle("shaders/anim.ksh")  
                        if v:HasTag("epic") or v:HasTag("largecreature") then
                            heal_fx.AnimState:SetScale(3, 3)
                        end
                        if v.components.colouradder then
                            v.components.colouradder:PushColour("cotl_heal", 0.5, 0.5, 0.5, 0)
                            v:DoTaskInTime(0.25, function(inst)
                                v.components.colouradder:PopColour("cotl_heal")
                            end)
                        end                
                    end
                end
            end
        end)
    end
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		--for now, we'll just use the default swap object in the build
		inst.AnimState:ClearOverrideSymbol("swap_object")
	end 
end 

local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local function ThrowProjectile(inst, pos)
	if pos ~= nil then
		local spit = SpawnPrefab("cotl_bombp")
        if inst._bomboverride then
            spit.AnimState:SetBuild(inst._bomboverride)
        end
        spit.Transform:SetPosition(inst:GetPosition():Get())
		spit.thrower = inst
		spit.components.complexprojectile:Launch(pos, inst)
    end
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPM_FORGE.PITPIG)
	
	inst.mobsleep = false	
	
	inst:RemoveTag("LA_mob") --you're a traitor now
	--inst.acidmult = 1.25
	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst.AnimState:SetSymbolLightOverride("sorc_eyes", 1)

	inst:AddTag("cotl")
    inst:AddTag("cotl_oldfaith")
	inst:AddTag("healer")
	inst:AddTag("monster")
	
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.

    PlayablePets.AddRMBAbility(inst)
end

local master_postinit = function(inst) 
    inst.sounds = sounds
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Variables--

	inst.taunt = true
    inst.taunt2 = true
	inst.mobsleep = true
    inst.shouldwalk = true

    inst.DoSpecial = DoSpecial
    --inst.components.ppskin_manager:SetSkinDefaultFn(SetSkinDefault)

	PlayablePets.SetSkeleton(inst, "pp_skeleton_cotl_ranged")

	inst.hit_recovery = 0.75
	
	local body_symbol = "cultist_body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("cultist_head", 0, 0, 0)
	PlayablePets.SetCommonStatResistances(inst, nil, nil, nil, 9999) --fire, acid, poison
	----------------------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	----------------------------------------------
	--Physics and Scale--
	MakeFlyingCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(3, 1)
    inst.Transform:SetSixFaced()
    PlayablePets.SetAmphibious(inst, nil, nil, true, nil, true)
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", Equip) --Shows head when hats make heads disappear.
    inst:ListenForEvent("unequip", Unequip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		--inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--

	inst.components.talker.ontalk = ontalk
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, true, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, true, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
