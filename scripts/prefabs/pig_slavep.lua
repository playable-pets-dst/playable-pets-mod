local MakePlayerCharacter = require "prefabs/player_common"

---------------------------


---------------------------
local prefabname = "pig_slavep"

local assets = 
{
	Asset("ANIM", "anim/pig_slave_build.zip"),
    Asset("ANIM", "anim/ds_pig_boat_jump.zip"),
    Asset("ANIM", "anim/ds_pig_basic.zip"),
    Asset("ANIM", "anim/ds_pig_actions.zip"),
    Asset("ANIM", "anim/ds_pig_attacks.zip"),
    Asset("SOUND", "sound/merm.fsb"),
}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 5,
	walkspeed = TUNING.PIG_WALK_SPEED,
	damage = 30,
	attackperiod = 0,
	range = 2,
	hitrange = 3,
	bank = "pigman",
	shiny = "pig",
	build = "pig_slave_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGpig_commonp",
	minimap = "pigmanplayer.tex",
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'meat',		0.5},
	{'pigskin',		0.5},
})

local sounds = {
	grunt = "dontstarve/pig/grunt",
    attack = "dontstarve/pig/attack",
    hit = "dontstarve/pig/hurt",
    death = "dontstarve/pig/death",
    talk = "dontstarve/pig/grunt",
	sleep = "dontstarve/pig/sleep",
} 

local function ontalk(inst, script)
    inst.SoundEmitter:PlaySound(inst.sounds.talk)
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
end 
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end

local ex_fns = require "prefabs/player_common_extensions"

local function OnEat(inst, food)
	if food and food:HasTag("monstermeat") then
		inst.curselvl = inst.curselvl + 1
		--inst.components.sanity:DoDelta(-10, false)
		if inst.curselvl >= 4 then
			SetWerePig(inst, true)
		end	
	end
end

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PP_FORGE.PIG)
	
	inst.mobsleep = false
	--inst.acidmult = 1.25
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"books"}, {"staves"}, {"darts"})
	end)
	
	inst.components.revivablecorpse.revivespeedmult = 1
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)
	inst:AddTag("pig")
	inst:AddTag("slave")

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)
	
end

local master_postinit = function(inst) 
	------------------------------------------
	--Stats--
	inst.sounds = sounds
    PlayablePets.SetCommonStats(inst, mob, true, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 1.2, 1.2, 1.2) --fire, acid, poison, freeze (flat value, not a multiplier)
	----------------------------------
	--Loot drops--
	inst:AddComponent("lootdropper")
    inst.components.lootdropper:SetChanceLootTable(prefabname)
	----------------------------------
	--Tags--
	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
	
	inst.isshiny = 0
	
	local body_symbol = "pig_torso"
	inst.poisonsymbol = body_symbol
	MakeMediumBurnableCharacter(inst, body_symbol)
    MakeMediumFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)	
	----------------------------------
	--PvP Mode Stuff--
	inst.mobteleported = false
	----------------------------------
	--Eater--
	--inst.components.eater:SetOnEatFn(OnEat)
	---------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.5, .75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", PlayablePets.CommonOnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("equip", Equip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", Unequip)
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	inst.components.talker.ontalk = ontalk
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob, nil, nil, nil, true) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
