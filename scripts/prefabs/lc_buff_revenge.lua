local assets = {
	
}

--NOTE! This is hardcoded for punishing bird, it will break when applied to other mobs!
local prefabs = {}

local tuning_values = TUNING.PP.LC_PUNISHINGBIRDP

local function OnKillTarget(inst, data) 
	if inst.components.debuffable then
		inst.components.debuffable:RemoveDebuff("lc_buff_revenge")
	end
end

local function OnAttached(inst, target)
	target.AnimState:SetBuild("lc_punishingbird_enraged_build")
    inst.entity:SetParent(target.entity)
	target:AddTag("lc_revenge")
    inst:ListenForEvent("death", function()
        inst.components.debuff:Stop()
    end, target)
	target:ListenForEvent("killed", OnKillTarget)
	target.components.combat:SetDefaultDamage(tuning_values.ALT_DAMAGE)
	target.components.combat:SetRange(tuning_values.ALT_RANGE, tuning_values.ALT_HIT_RANGE)
	target.components.combat:SetAttackPeriod(tuning_values.ALT_ATTACK_PERIOD)
	target.components.locomotor:SetExternalSpeedMultiplier(inst, "lc_buff_revenge", tuning_values.ENRAGE_SPEED_MULT)
end

local function OnTimerDone(inst, data)
    if data.name == inst.prefab.."buffover" then
        inst.components.debuff:Stop()
    end
end

local function OnExtended(inst, target)
    inst.components.timer:StopTimer(inst.prefab.."buffover")
	inst.components.timer:StartTimer(inst.prefab.."buffover", tuning_values.ENRAGE_DURATION)
end

local function OnDetached(inst, target)
	target.AnimState:SetBuild("lc_punishingbird")
	target.components.locomotor:RemoveExternalSpeedMultiplier(target, "lc_buff_revenge")
	target.components.combat:SetDefaultDamage(target.mob_table.damage)
	target.components.combat:SetRange(target.mob_table.range, target.mob_table.hit_range)
	target.components.combat:SetAttackPeriod(target.mob_table.attackperiod)
	target:RemoveTag("lc_revenge")
	target:RemoveEventCallback("killed", OnKillTarget)
	inst:Remove()
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

    inst.AnimState:SetBank("lc_debuff_enchantlure")
    inst.AnimState:SetBuild("lc_debuff_enchantlure")
	inst.AnimState:PlayAnimation("spawn", false)
	inst.AnimState:PushAnimation("idle_loop", true)
		
	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end
		
	inst:Hide()

	inst:AddComponent("debuff")
	inst.components.debuff:SetAttachedFn(OnAttached)
	inst.components.debuff:SetDetachedFn(OnDetached)
	inst.components.debuff:SetExtendedFn(OnExtended)

	inst.duration = tuning_values.ENRAGE_DURATION

	inst:AddComponent("timer")
	inst:DoTaskInTime(0, function() -- in case we want to change duration
		inst.components.timer:StartTimer("lc_buff_revengebuffover", inst.duration)
	end)
	inst:ListenForEvent("timerdone", OnTimerDone)

	return inst
end
------------------------------------------------------------------------------

return Prefab("lc_buff_revenge", fn, assets, prefabs)
