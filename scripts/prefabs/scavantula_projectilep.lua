local assets=
{
	Asset("ANIM", "anim/hound_thrown_actions.zip"),
}

local prefabs = {}

local function projectile_onland(inst)	
	local x, y, z = inst:GetPosition():Get()
	local ents = TheSim:FindEntities(x,0,z, 3, nil, {"notarget", "INLIMBO", "shadow", "playerghost"})
	for k,v in pairs(ents) do
		if v.components.combat and ((inst.owner and v ~= inst.owner) or not inst.owner) then
			v.components.combat:GetAttacked((inst.owner and inst.owner:IsValid()) and inst.owner or inst, 100)
		end
	end
	
	if inst.hound_prefab then
		local hound = SpawnPrefab(inst.hound_prefab)
		hound.Transform:SetPosition(x, 0, z)
		hound.components.health:SetInvincible(false)
		hound.components.health:Kill()
	end

	if inst._thrown_player and inst._thrown_player:IsValid() then
		local target = inst._thrown_player
		target.Transform:SetPosition(x, 0, z)
		target.components.health:SetInvincible(false)
		target:RemoveTag("notarget")
		target:Show()
		target.components.health:Kill()
	end
	inst:Remove()
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	inst.entity:AddNetwork()
	inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	
	MakeInventoryPhysics(inst)

	inst.AnimState:SetBank("hound")
	inst.AnimState:SetBuild("hound")
	inst.AnimState:PlayAnimation("spin_loop", true)
	--inst:SetPrefabNameOverride("peghook")
	
	inst.entity:SetPristine()

	inst:AddTag("thrown")
	inst:AddTag("projectile")
	inst:AddTag("NOCLICK")

	inst.persists = false

	if not TheWorld.ismastersim then
		return inst
	end	

	inst.SetSource = PlayablePets.SetProjectileSource --needed for Reforged compatibility

	inst:AddComponent("complexprojectile")
	--TODO: Set Tuning variables.
    inst.components.complexprojectile:SetHorizontalSpeed(30)
    inst.components.complexprojectile:SetGravity(-50)
    inst.components.complexprojectile:SetLaunchOffset(Vector3(0, 2, 0))
    inst.components.complexprojectile:SetOnHit(projectile_onland)

	inst:DoTaskInTime(20, inst.Remove)

	return inst
end


return Prefab("scavantula_projectilep", fn, assets, prefabs)