local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
----------==Notes==--------
--
---------------------------
local prefabname = "roach_beetlep"

local assets = 
{
	Asset("ANIM", "anim/hf_roach_beetle.zip"),
	Asset("ANIM", "anim/hf_roach_beetle_build.zip"),
	Asset("ANIM", "anim/hf_roach_beetle_projectile.zip"),
	---
	Asset("SOUNDPACKAGE", "sound/hf.fev"), 
	Asset( "SOUND", "sound/hf_bank00.fsb"),
}

local getskins = {}

local prefabs = 
{	

}
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--'prefab',
	--"spear",
}

local start_inv2 =
{
   --"beehome",
}

if MOBHOUSE== "Enable1" or MOBHOUSE== "Enable3" then
	start_inv = start_inv2
end
-----------------------
--Stats--
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = 8,
	walkspeed = 8,
	damage = 20*2,
	range = 2,
	hit_range = 3,
	attackperiod = 0,
	bank = "roach_beetle",
	build = "hf_roach_beetle_build",
	scale = 0.8,
	--build2 = "chester_shadow_build",
	--build3 = "chester_snow_build",
	stategraph = "SGroach_beetlep",
	minimap = "roach_beetlep.tex",
	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable(prefabname,
-----Prefab---------------------Chance------------
{
    {'monstermeat',             1.00}, 
})

local FORGE_STATS = PPM_FORGE[string.upper(prefabname)] 
--==============================================
--					Mob Functions
--==============================================
local sound_path = "hf/creatures/roach_beetle/"
local sounds = {
		taunt    = sound_path .. "taunt",
		hit   	 = sound_path .. "hit",
		sleep_in = sound_path .. "sleep_in",
		sleep_out = sound_path .. "sleep_out",
		death	 = sound_path .. "death",
		idle = sound_path .. "idle",
		attack_pre = sound_path .. "attack",
		step	 = "dontstarve/creatures/spider/walk_spider",
		shell	 = "dontstarve/creatures/lava_arena/turtillus/shell_impact"
}
--==============================================
--				Custom Common Functions
--==============================================	
local function OnEquip(inst)
	local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	inst.AnimState:Show("HEAD")
	if head then
		inst.AnimState:HideSymbol("atenna")
	else
		inst.AnimState:ShowSymbol("atenna")
	end 
end
--==============================================
--					Loading/Saving
--==============================================
 
local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
		inst.isalt = data.isalt or false
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
	data.isalt = inst.isalt or false
end

--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, FORGE_STATS)
	
	inst:DoTaskInTime(0, function(inst)
	inst:AddComponent("itemtyperestrictions")
	inst.components.itemtyperestrictions:SetRestrictions({"melees", "books", "staves", "darts"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

--==============================================
--					Common/Master
--==============================================

local common_postinit = function(inst) 
	inst.MiniMapEntity:SetIcon(mob.minimap)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
	  --ThePlayer.HUD.controls.status.brain:Hide()
		if MONSTERHUNGER== "Disable" then
			--ThePlayer.HUD.controls.status.stomach:Hide()
		end
   end
end)
	----------------------------------
	--Tags--
	----------------------------------
	inst:AddTag("HF_mob")
    inst:AddTag("LA_mob")
	inst:AddTag("roach_beetle")
	inst:AddTag("monster")
	inst:AddTag("tiddlevirusimmune")
	----------------------------------
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst)	
end

local master_postinit = function(inst)  
	--Stats--
	inst.sounds = sounds
	PlayablePets.SetCommonStats(inst, mob) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, 3, 3) --fire, acid, poison
	----------------------------------
	--Variables			
	inst.isshiny = 0
	
	inst.mobsleep = true
	inst.taunt = true
	inst.taunt2 = true
	
	inst.mobplayer = true
	inst.ghostbuild = "ghost_monster_build"
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeLargeFreezableCharacter(inst)
	inst.components.debuffable:SetFollowSymbol(body_symbol, 0, 0, 0)
	----------------------------------
	--Components
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	inst:AddComponent("sanityaura")
	inst.components.sanityaura.aura = -TUNING.SANITYAURA_TINY
	----------------------------------
	--Eater--
	PlayablePets.SetCanEatAll(inst)
	---------------------------------
	--Physics and Shadows--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(1.75, 0.75)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", OnEquip) --Shows head when hats make heads disappear.
	inst:ListenForEvent("unequip", OnEquip)
	---------------------------------
	--Forge--
	if PP_FORGE_ENABLED then
		inst.forge_fn = SetForge(inst)
	end
    ------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
