local MakePlayerCharacter = require "prefabs/player_common"

---------------------------
local tuning_values = TUNING.PP.GOLIATH_GARLICP

local assets = 
{
	Asset("ANIM", "anim/goliath_garlic.zip"),
    Asset("ANIM", "anim/goliath_garlic_build.zip"),
	Asset("ANIM", "anim/goliath_cloud.zip"),
	Asset("ANIM", "anim/goliath_projectile.zip"),
}



local prefabs = 
{	
	"goliath_cloudp",
}
	
local prefabname = "goliath_garlicp"	
	
--Note: Becareful when adding starting items.
--People are likely to spam changecharacter to get more of them.	
local start_inv = 
{
	--"prefab",
}

local start_inv2 = 
{
	--"prefabhome",
}

if MOBHOUSE == "Enable1" or MOBHOUSE == "Enable3" then
	start_inv = start_inv2
end
-----------------------
local mob = 
{
	health = TUNING[string.upper(prefabname.."_HEALTH")],
	hunger = TUNING[string.upper(prefabname.."_HUNGER")],
	hungerrate = TUNING.WILSON_HUNGER_RATE, 
	sanity = TUNING[string.upper(prefabname.."_SANITY")],
	runspeed = tuning_values.RUNSPEED,
	walkspeed = tuning_values.WALKSPEED,
	
	attackperiod = tuning_values.ATTACKPERIOD,
	damage = tuning_values.DAMAGE,
	range = tuning_values.RANGE,
	hit_range = tuning_values.HIT_RANGE,
	
	bank = "goliath_garlic",
	shiny = "fumo_reimu",
	build = "goliath_garlic_build",
	scale = 1,
	--build2 = "alternate build here",
	stategraph = "SGgoliath_garlicp",
	minimap = "goliath_garlicp.tex",	
}
-----------------------
--Loot that drops when you die, duh.
SetSharedLootTable("goliath_garlicp",
-----Prefab---------------------Chance------------
{
	{"garlic_oversized",       1.00},
	{"lightbulb",       1.00},
	{"lightbulb",       1.00},
})
------------------------------------------------------
 
local soundpath = "pp_sounds/creatures/goliath_garlic/"
local sounds =
{
    hit = soundpath.."hit",
	chew = soundpath.."chew",
	bubble = soundpath.."bubble",
	step = soundpath.."step",
    attack = soundpath.."attack_pre",
    attack_ground = soundpath.."attack_ground", 
    death = soundpath.."death",
	foley = "dontstarve/creatures/leif/foley",
}

local function DoSpit(inst)
	local cloud = SpawnPrefab("goliath_cloudp")
	cloud.Transform:SetPosition(inst:GetPosition():Get())
	
	for i = 1, 6 do
		inst:DoTaskInTime(0.1 + (0.1*i), function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.bubble)
			local spit = SpawnPrefab("goliath_projectilep")
			local posx, posy, posz = inst.Transform:GetWorldPosition()
			local angle = -inst.Transform:GetRotation() * DEGREES
			local offset = 0.5
			local max_angle = 30
			local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
			spit.Transform:SetPosition(targetpos.x, 0, targetpos.z)
			spit.Transform:SetRotation(inst.Transform:GetRotation() + ((math.random(-1, 1) * math.random())*max_angle))
			spit.owner = inst
			local s = 1 + (math.random()-0.5)
			spit.Transform:SetScale(s, s, s)
		end)
	end
end

local function Equip(inst)
    local head = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HEAD)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if head then
		inst.AnimState:Show("HEAD")
	end 
	if hands then
		inst.AnimState:Show("swap_object")
	end 	
end

local function Unequip(inst)
	local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
	if not hands then
		inst.AnimState:Hide("swap_object")
	end 
end 

local function OnLoad(inst, data)
	if data ~= nil then
		inst.mobteleported = data.mobteleported or false
		inst.isshiny = data.isshiny or 0
	end
end

local function OnSave(inst, data)
	data.mobteleported = inst.mobteleported or false
	data.isshiny = inst.isshiny or 0
end
--==============================================
--					Forged Forge
--==============================================

local ex_fns = require "prefabs/player_common_extensions"

local function SetForge(inst)
	PlayablePets.SetForgeStats(inst, PPM_FORGE.PITPIG)
	
	inst.mobsleep = false	
	
	inst:RemoveTag("LA_mob") --you're a traitor now
	--inst.acidmult = 1.25
	inst.components.health:SetAbsorptionAmount(0.85)
	
	inst:DoTaskInTime(0, function(inst)
		inst:AddComponent("itemtyperestrictions")
		inst.components.itemtyperestrictions:SetRestrictions({"books", "staves", "darts", "melees", "armors"})
	end)
	
	inst.components.combat:SetDamageType(1)
	
	inst.components.revivablecorpse.revivespeedmult = 0.5
	
	inst:RemoveEventCallback("respawnfromcorpse", ex_fns.OnRespawnFromPlayerCorpse)
	inst:ListenForEvent("respawnfromcorpse", PlayablePets.OnRespawnFromMobCorpse)
end

local common_postinit = function(inst) 
	--inst.soundsname = "beefalo"

	inst.MiniMapEntity:SetIcon(mob.minimap)
	--inst.AnimState:SetMultColour(1, 1, 1, 0.5)

	inst:DoTaskInTime(0, function() 
   if ThePlayer then
      inst:EnableMovementPrediction(false)
   end
end)

	inst:AddTag("goliath_garlic")
	
	--Night Vision that toggles, might be a better way to do this.
	--Enables at night, disables during the day.
	inst:WatchWorldState( "isday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isdusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "isnight", function() PlayablePets.SetNightVision(inst)  end)
	inst:WatchWorldState( "iscaveday", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavedusk", function() PlayablePets.SetNightVision(inst) end)
  	inst:WatchWorldState( "iscavenight", function() PlayablePets.SetNightVision(inst)  end)
	
	PlayablePets.SetNightVision(inst) --This is here to make sure the NightVision is enabled when a player rejoins.
end

local master_postinit = function(inst) 
    inst.sounds = sounds
	------------------------------------------
	--Stats--
    PlayablePets.SetCommonStats(inst, mob, nil, true) --mob table, ishuman, ignorepvpmultiplier
	PlayablePets.SetCommonWeatherResistances(inst, 60, 20) --heat, cold, wetness
	PlayablePets.SetCommonStatResistances(inst, 3, nil, 3) --fire, acid, poison
	----------------------------------
	--Loot drops--
	PlayablePets.SetCommonLootdropper(inst, prefabname) --inst, prefaboverride
	----------------------------------
	--Variables--	
	inst.taunt = true
	inst.taunt2 = true
	inst.mobsleep = true
    inst.shouldwalk = true
	inst.hit_recovery = 0.75

	inst.AnimState:SetSymbolLightOverride("eye", 1)
	inst.AnimState:SetSymbolLightOverride("glow", 1)

	inst.DoSpit = DoSpit
	
	local body_symbol = "body"
	inst.poisonsymbol = body_symbol
	MakeLargeBurnableCharacter(inst, body_symbol)
    MakeSmallFreezableCharacter(inst, body_symbol)
	inst.components.debuffable:SetFollowSymbol("body", 0, 0, 0)
	
	PlayablePets.SetStormImmunity(inst)
	----------------------------------------------
	--Eater--
    inst.components.eater:SetAbsorptionModifiers(1,1,1) --This might multiply food stats.
	inst.components.eater:SetCanEatHorrible() --check to make them eat monster meat.
	inst.components.eater.strongstomach = true -- can eat monster meat!
	----------------------------------------------
	--Physics and Scale--
	MakeCharacterPhysics(inst, 50, .5)
    inst.DynamicShadow:SetSize(6, 3)
    inst.Transform:SetFourFaced()
	---------------------------------
	--Listeners--
	inst:ListenForEvent("equip", Equip) --Shows head when hats make heads disappear.
    inst:ListenForEvent("unequip", Unequip) --Shows head when hats make heads disappear.
	------------------------------------------------------
	--Forge--
	if TheNet:GetServerGameMode() == "lavaarena" then
		--inst.forge_fn = SetForge(inst)
	end
	------------------------------------------------------
	--Respawning and Initializing functions--
	
	inst:ListenForEvent("respawnfromghost", function(inst) PlayablePets.RevRestore(inst, mob) end) --(inst, mob, isflying, iswebimmune, noshadow, ishuman)

    inst:DoTaskInTime(0, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
	inst:DoTaskInTime(3, function(inst) PlayablePets.SetSkin(inst, mob) end)
    inst:ListenForEvent("respawnfromghost", function() 
        inst:DoTaskInTime(5, function(inst) PlayablePets.CommonSetChar(inst, mob) end)
		inst:DoTaskInTime(5.1, function(inst) PlayablePets.SetSkin(inst, mob) end)
		inst:DoTaskInTime(5, function(inst) PlayablePets.RevRestore(inst, mob) end)
    end)
	
	inst.OnSave = OnSave
    inst.OnLoad = OnLoad
	
    return inst
	
end

return MakePlayerCharacter(prefabname, prefabs, assets, common_postinit, master_postinit, start_inv)
