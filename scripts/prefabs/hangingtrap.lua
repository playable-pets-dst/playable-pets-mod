local assets = 
{
	Asset("ANIM", "anim/hangingtrap.zip"),
}

local prefabs = 
{	

}

--MAKE THESE TUNING VALUES! I WILL STRANGLE YOU IF YOU KEEP THESE HERE
local DAMAGE = 300
local RANGE = 3

local function OnGroundPound(inst)
	local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, RANGE, {"_health"}, {"notarget", "shadow", "playerghost", "INLIMBO"})
	if #ents > 0 then
		for i, v in ipairs(ents) do
			if v.components.combat and v:IsValid() and v ~= owner then
				v.components.combat:GetAttacked(inst, DAMAGE)
			end
		end
	end
end

local function DoDrop(inst)
	inst:RemoveTag("ready")
	inst.AnimState:PlayAnimation("drop")
	inst:DoTaskInTime(12*FRAMES, function(inst)
		inst.components.groundpounder:GroundPound()
	end)
end

local function SetReady(inst)
	inst.AnimState:PlayAnimation("idle_raised", true)
	inst:AddTag("ready")
end

local function SetUp(inst)
	inst.AnimState:PlayAnimation("raise")
	inst:ListenForEvent("animover", SetReady)
end
	
local function fn()
	local inst = CreateEntity()

	inst.entity:AddTransform()
	inst.entity:AddAnimState()
	inst.entity:AddNetwork()
	inst.entity:AddSoundEmitter()

	inst.AnimState:SetBank("hangingtrap")
   	inst.AnimState:SetBuild("hangingtrap")
    inst.AnimState:PlayAnimation("idle", true)

	inst:AddTag("hangingtrap")

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

	inst:AddComponent("inspectable")

	inst:AddComponent("inventory")

	inst:AddComponent("combat")

	
	inst:DoPeriodicTask(0, function(inst)
		if inst:HasTag("ready") then
			DoDrop(inst)
		end
	end)

	inst:AddComponent("groundpounder")
    inst.components.groundpounder.destroyer = false
    inst.components.groundpounder.damageRings = 0
    inst.components.groundpounder.destructionRings = 0
    inst.components.groundpounder.platformPushingRings = 0
    inst.components.groundpounder.numRings = 2
    inst.components.groundpounder.groundpoundFn = OnGroundPound

	return inst
end

return Prefab("hangingtrap", fn, assets, prefabs)
