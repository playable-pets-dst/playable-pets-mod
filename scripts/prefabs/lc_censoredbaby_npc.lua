local assets =
{
    Asset("ANIM", "anim/CENSORED_babySMOOTHED.zip"),
}

local prefabs =
{

}

local brain = require("brains/censoredbrain")

local WAKE_TO_FOLLOW_DISTANCE = 8
local SLEEP_NEAR_HOME_DISTANCE = 10
local SHARE_TARGET_DIST = 30
local HOME_TELEPORT_DIST = 30

local NO_TAGS = { "FX", "NOCLICK", "DECOR", "INLIMBO" }

local soundpath = "lobcorp/creatures/censored/"
local sounds =
{
    hit = soundpath.."babyattack",
    attack = soundpath.."babyattack",
    execution = soundpath.."execution", 
    heal = soundpath.."heal",
    born = soundpath.."born",
}

local function retargetfn(inst)
    return FindEntity(
            inst,
            13,
            function(guy)
                return inst.components.combat:CanTarget(guy)
            end,
            nil,
            { "wall", "structure", "CENSORED" }
        )
end

local function KeepTarget(inst, target)
    return inst.components.combat:CanTarget(target) and inst:IsNear(target, 30) and not target:HasTag("CENSORED")
end

local function OnAttacked(inst, data)
	if inst.components.follower.leader ~= data.attacker or data.attacker.prefab ~= "boarriorp" then
    inst.components.combat:SetTarget(data.attacker)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("CENSORED") and dude.components.combat and not dude.components.combat.target and not dude.components.health:IsDead() end, 30)
	end
end

local function OnAttackOther(inst, data)
    inst.components.combat:ShareTarget(data.attacker, SHARE_TARGET_DIST, function(dude) return dude:HasTag("CENSORED") and dude.components.combat and not dude.components.combat.target and not dude.components.health:IsDead() end, 30)
end

local function OnSave(inst, data)
    
end

local function OnLoad(inst, data)

end

local function SpawnMinion(inst, pos)
	local minion = SpawnPrefab("lc_censoredbaby_npc")
	minion.Transform:SetPosition(pos:Get())
end

local function OnKill(inst, data) 
	if data.victim and not data.victim:HasTag("insect") 
	and not data.victim:HasTag("smallcreature") and not data.victim:HasTag("bird") then
		inst:SpawnMinion(data.victim:GetPosition())
	end
end

local function master_immunity(inst, amount, overtime, cause, ignore_invincible, afflicter, ignore_absorb)
    return afflicter ~= nil and (afflicter.prefab == "boarriorp" or inst.components.follower.leader == afflicter)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddDynamicShadow()
    inst.entity:AddNetwork()

    MakeCharacterPhysics(inst, 50, .5)

    inst.DynamicShadow:SetSize(1.75, 0.75)
    inst.Transform:SetFourFaced()

    inst:AddTag("scarytoprey")
    inst:AddTag("monster")
    inst:AddTag("hostile")
    inst:AddTag("CENSORED")	
	
	inst.is_npc = true

    inst.AnimState:SetBank("lc_censored_baby")
    inst.AnimState:SetBuild("CENSORED_babySMOOTHED")
    inst.AnimState:PlayAnimation("idle_loop", true)

    inst:AddComponent("spawnfader")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.sounds = sounds
    inst.SpawnMinion = SpawnMinion

    inst:AddComponent("locomotor") -- locomotor must be constructed before the stategraph
    inst.components.locomotor.walkspeed = 2
    inst.components.locomotor.runspeed = 2
    inst:SetStateGraph("SGlc_censoredbaby_npc")

    inst:SetBrain(brain)

    inst:AddComponent("follower")

    inst:AddComponent("health")
    inst.components.health:SetMaxHealth(500)

    inst:AddComponent("sanityaura")
    inst.components.sanityaura.aura = -TUNING.SANITYAURA_LARGE

    inst:AddComponent("combat")
    inst.components.combat:SetDefaultDamage(35)
    inst.components.combat:SetAttackPeriod(2)
    inst.components.combat:SetRetargetFunction(1, retargetfn)
    inst.components.combat:SetKeepTargetFunction(KeepTarget)

    inst:AddComponent("lootdropper")
    --inst.components.lootdropper:SetChanceLootTable('hound')

    inst:AddComponent("inspectable")
	
	MakeMediumBurnableCharacter(inst, "body")

    inst.OnSave = OnSave
    inst.OnLoad = OnLoad

    inst:ListenForEvent("attacked", OnAttacked)
    inst:ListenForEvent("onattackother", OnAttackOther)
    inst:ListenForEvent("killed", OnKill)

    return inst
end

return Prefab("lc_censoredbaby_npc", fn, assets, prefabs)
