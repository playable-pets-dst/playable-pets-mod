local SKINS = {
	--[[
	prefab = {
		skinname = {
			name = "", --this is for the purpose of printing
			fname = "Name of Skin",
			build = "build name",
			teen_build = "",
			adult_build = "adult build name",
			etc_build = "blah blah build", --add as many as these as you need
			fn = functionhere,
			owners = {}, --userids go here
			locked = true, --this skin can't be used.
		},
	},
	]]
	mewkatp = {
		fancy = {
			name = "fancy",
			fname = "Moorcraft Kat",
			build = "sk_spookat_moorcraft",
			scale = 2,
		}
	},
	zombie_dustp = {
		bellhop = {
			name = "bellhop",
			fname = "Zombie Bellhop",
			build = "sk_zombie_bellhop",
		},
		swarm = {
			name = "swarm",
			fname = "Void Zombie",
			build = "sk_zombie_swarm",
		}
	},
	pm_shroobp = {
		blaze = {
			name = "blaze",
			fname = "Blazing Shroob",
			build = "pm_shroob_blazing_build",
		},
		dr = {
			name = "dr",
			fname = "Dr. Shroob",
			build = "pm_shroob_dr_build",
		},
		diver = {
			name = "diver",
			fname = "Diver Shroob",
			build = "pm_shroob_diver_build",
		},
		rc = {
			name = "rc",
			fname = "RC Shroob",
			build = "pm_shroob_rc_build",
		},
		guard = {
			name = "guard",
			fname = "Guardian Shroob",
			build = "pm_shroob_guardian_build",
		},
	},
	pm_duplighostp = {
		doopliss = {
			name = "doopliss",
			fname = "Doopliss",
			build = "pm_doopliss_build",
		},
	},
	cotl_scampp = {
		pink = {
			name = "pink",
			fname = "Pink Scamp",
			build = "cotl_scamp_peach_build",
		},
	},
	cotl_bomb_scoutp = {
		pink = {
			name = "pink",
			fname = "Pink Bomb Scout",
			build = "cotl_bomb_scout_peach_build",
			proj_build = "cotl_bomb_peach_build",
		},
	},
	fumop = {
		cirno = {
			name = "cirno",
			fname = "Cirno Fumo",
			build = "fumo_cirno",
		},
	},
	pm_bristlep = {
		dark = {
			name = "dark",
			fname = "Dark Bristle",
			build = "pm_bristle_dark_build",
		},
	},
	pm_drybonesp = {
		dull = {
			name = "dull",
			fname = "Dull Bones",
			build = "pm_drybones_dull_build",
		},
		dark = {
			name = "dark",
			fname = "Dark Bones",
			build = "pm_drybones_dark_build",
		},
		red = {
			name = "red",
			fname = "Red Bones",
			build = "pm_drybones_red_build",
		},
		blue = {
			name = "blue",
			fname = "Blue Shoes Drybones",
			build = "pm_drybones_build",
			symbol_data = {
				{
					symbol = "bone_foot",
					hue = 0.55,
				},
			}
		},
		umbra = {
			name = "umbra",
			fname = "Umbra Drybones",
			build = "pm_drybones_build",
			symbol_data = {
				{
					symbol = "bone_foot",
					colour = {0.4, 0.4, 0.4, 1},
					sat = 0,
				},
				{
					symbol = "bone_hand",
					colour = {0.2, 0.2, 0.3, 1}
				},
				{
					symbol = "bone_head",
					colour = {0.5, 0.4, 0.5, 1}
				},
				{
					symbol = "bone_body",
					colour = {0.4, 0.3, 0.4, 1}
				},
				{
					symbol = "bone_eyes",
					colour = {1, 1, 0.2, 1},
					lightoverride = 1,
				},
			}
		},
		spectral = {
			name = "spectral",
			fname = "Spectral Bones",
			build = "pm_drybones_build",
			symbol_data = {
				{
					symbol = "bone_foot",
					colour = {0.3, 0.4, 0.6, 0.7},
					sat = 0,
				},
				{
					symbol = "bone_hand",
					colour = {0.5, 0.6, 1, 0.7}
				},
				{
					symbol = "bone_head",
					colour = {0.3, 0.4, 0.6, 0.7}
				},
				{
					symbol = "bone_body",
					colour = {0.3, 0.4, 0.6, 0.7}
				},
				{
					symbol = "bone_eyes",
					colour = {152/255, 131/255, 230/255, 1}
				},
			}
		},
	},
}

return SKINS