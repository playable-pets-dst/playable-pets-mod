SK_HEALTH = {}

local BASE_HEALTH = {150, 350, 675}

SK_HEALTH.SetDangerMob = function (inst, multiplier)
	inst.components.health:SetMaxHealth(inst.components.health.maxhealth * multiplier)
	inst.components.combat:SetDefaultDamage(inst.components.combat.defaultdamage * multiplier)
	inst.isdanger = true
end

SK_HEALTH.GetHealth = function (inst, class, tier, multiplier)
	return BASE_HEALTH[tier or 3] * (class or 1) * (multiplier or 1) * SK_DIFFICULTY
end

return SK_HEALTH