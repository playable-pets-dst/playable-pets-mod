require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "pickup"
local shortaction = "action"
local workaction = "work"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.PICKUP, "pickup"),
    ActionHandler(ACTIONS.PICK, "pickup"),
    ActionHandler(ACTIONS.HARVEST, "pickup"),
    ActionHandler(ACTIONS.DROP, "pickup"),
    ActionHandler(ACTIONS.GIVE, "pickup"),
    ActionHandler(ACTIONS.GIVEALLTOPLAYER, "pickup"),
    ActionHandler(ACTIONS.ATTACK, "attack_pre"),
    --ActionHandler(ACTIONS.PP_DODGE, "idle"),
   -- ActionHandler(ACTIONS.PP_RANGEDSPECIAL, "idle"),
}

local tuning_values = TUNING.PP.COTL_SCAMPP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst, data) 
        --TODO this event doesn't get pushed on a killing blow.
        inst._stored_damage = data.damage or 0
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function DoRunStep(inst)
    local pos = inst:GetPosition()
    inst:DoTaskInTime(0.1, function(inst)
        local fx = SpawnPrefab("cavein_dust_low")
        fx.Transform:SetPosition(pos:Get())
        fx.Transform:SetScale(0.1, 0.1, 0.1)
        fx.AnimState:SetDeltaTimeMultiplier(3.5)
        fx.AnimState:SetMultColour(92/255, 67/255, 57/255, 1)
    end)
    PlayFootstep(inst)
end

local function DoDodgeFx(inst)
    local pos = inst:GetPosition()
    inst:DoTaskInTime(0.1, function(inst)
        local fx = SpawnPrefab("cavein_dust_low")
        fx.Transform:SetPosition(pos:Get())
        fx.Transform:SetScale(0.3, 0.3, 0.3)
        fx.AnimState:SetDeltaTimeMultiplier(3.5)
        fx.AnimState:SetMultColour(92/255, 67/255, 57/255, 1)
    end)
end

local function ActivateRollState(inst)
    --For Debugging
    --inst.AnimState:SetMultColour(0, 1, 0, 1)
    inst.sg:AddStateTag("temp_invincible")
end

local function DeactivateRollState(inst)
    --For Debugging
    --inst.AnimState:SetMultColour(1, 0, 0, 1)
    inst.sg:RemoveStateTag("temp_invincible")
end

local function IsRangedAttack(inst)
    local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    return hands and (hands:HasTag("blowdart") or hands:HasTag("rangedweapon")) or inst:HasTag("bomb_scout")
end

local states =
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "emote",
        tags = {"idle", "canrotate"},
        onenter = function(inst, data)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            if data.anim and data.anim == "emoteXL_happycheer" then
                inst.AnimState:PlayAnimation("pray2_pre")
                inst.AnimState:PushAnimation("pray2_loop", true)
            else
                inst.AnimState:PlayAnimation("pray_pre")
                inst.AnimState:PushAnimation("pray_loop", true)
            end
        end,

    },

    State{
        name = "pp_rangedspecial",
        tags = {"attack", "busy"},
		
		onenter = function(inst, pos)
            inst.sg.statemem.target_pos = pos
            inst:ForceFacePoint(pos.x, 0, pos.z)
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("bomb_throw")
        end,
        
        onexit = function(inst)
            inst.components.combat:StartAttack()
        end,
        
        timeline=
        {
            TimeEvent(15*FRAMES, function(inst) 
                if inst.ThrowProjectile and inst.sg.statemem.target_pos then
                    local pos = inst.sg.statemem.target_pos
                    inst:ThrowProjectile(pos)
                end
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "attack_pre",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack_pre_fast")
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            --TimeEvent(16*FRAMES, function(inst) PlayablePets.DoWork(inst, 3) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("attack") end),
        },
	
    },

    State{
        name = "attack",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            if target ~= nil then
                if target:IsValid() and not inst:IsNear(target, inst.components.combat.hitrange-0.5) then
                    inst:ForceFacePoint(target:GetPosition())
                    inst.sg.statemem.attacktarget = target
                    inst.Physics:SetMotorVel(IsRangedAttack(inst) and 0 or 20, 0, 0)
                end
            end
            inst.AnimState:PlayAnimation("attack")
            inst.SoundEmitter:PlaySound(inst.sounds.attack_slash)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(3*FRAMES, function(inst) 
                PlayablePets.DoWork(inst, 3) 
                
            end),
            TimeEvent(6*FRAMES, function(inst)
                inst.Physics:Stop()
            end)
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("attack_pst") end),
        },
	
    },

    State{
        name = "attack_pst",
        tags = {"busy"},
		
		onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack_pst")
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(1*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end)
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "work",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack_pre_fast")
            inst.AnimState:PushAnimation("attack", false)
            inst.AnimState:PushAnimation("attack_pst", false)
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(16*FRAMES, function(inst) PlayablePets.DoWork(inst, 3) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "pickup",
        tags = {"busy"},
		
		onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("pickup")
	    end,
	
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst:PerformBufferedAction() end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound(inst.sounds.hit) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1", --dodge roll?
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "dodge", --dodge roll?
        tags = {"busy", "dodge_roll"},

        onenter = function(inst, pos)
            if pos then
                inst:ForceFacePoint(pos.x, 0, pos.z)
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("dodge")
            inst.Physics:SetMotorVel(inst.components.locomotor.runspeed*1.5, 0, 0)
            DeactivateRollState(inst) --for Debugging mostly.
            DoDodgeFx(inst)
        end,

		timeline=
        {
            TimeEvent(3*FRAMES, function(inst) ActivateRollState(inst) end),
            TimeEvent(6*FRAMES, DoDodgeFx),
            TimeEvent(8*FRAMES, function(inst) DeactivateRollState(inst) end),
            TimeEvent(9*FRAMES, DoDodgeFx),
            TimeEvent(12*FRAMES, DoDodgeFx),
        },

        onexit = function(inst)
            inst.can_dodge = false
            if inst._dodgetask then
                inst._dodgetask:Cancel()
                inst._dodgetask = nil
            end
            inst._dodgetask = inst:DoTaskInTime(TUNING.PP.COTL_SWORDMANP.DODGE_CD, function(inst)
                inst.can_dodge = true
            end)
        end,

        events=
        {
			EventHandler("animover", function(inst) 
                inst.sg:GoToState("idle")             
            end),
        },
    },

    State{
		name = "special_atk2", --walk toggle
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
            PlayablePets.ToggleWalk(inst)
            --inst.SoundEmitter:PlaySound(inst.sounds.fumo) 
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst, data)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            if data.afflicter and data.afflicter:IsValid() then
                inst.skeleton_prefab = nil
                local skel = SpawnPrefab("pp_skeleton_cotl_grunt")
                MakeInventoryPhysics(skel)
                skel.Transform:SetPosition(inst:GetPosition():Get())
                skel.Transform:SetRotation(inst.Transform:GetRotation())

                skel:DoTaskInTime(10, function(skel)
                    MakeSmallObstaclePhysics(skel, 0.25)
                end)

                skel:SetSkeletonDescription(inst.prefab, inst:GetDisplayName(), inst.deathcause, inst.deathpkname, inst.userid)
                skel:SetSkeletonAvatarData(inst.deathclientobj)
                Launch(skel, data.afflicter, inst._stored_damage and inst._stored_damage/5 or 0)
            end
            PlayablePets.DoDeath(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					--PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
            TimeEvent(8*FRAMES, PlayFootstep),
            TimeEvent(25*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
    
    State
    {
        name = "run_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
            TimeEvent(5*FRAMES, DoRunStep),
            TimeEvent(10*FRAMES, DoRunStep),
            TimeEvent(15*FRAMES, DoRunStep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, "pray2_pre", "pray2_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "walk_loop"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)




    
return StateGraph("cotl_cultistp", states, events, "idle", actionhandlers)

