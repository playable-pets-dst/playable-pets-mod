require("stategraphs/commonstates")
require("stategraphs/ppstates")

--[[
local colors = {
	["_fire"],
	["_curse"],
	["_poison"],
	["_freeze"],
	["_shock"],
}]]

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "zombie", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","zombie", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "zombie", "undead"}
	end
end


function TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst, data)
		if inst:HasTag("bombie") then
			return "attack4"
		else
			if inst.altattack and not inst:IsNear(data.target, 2.5) then
				return "attack2"
			else
				return "attack"
			end
		end
	end),
	ActionHandler(ACTIONS.REVIVE_CORPSE, function(inst, data)
		if inst:HasTag("bombie") then
			return "run_pst"
		else
			return "revivecorpse"
		end
	end),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SetSleeperAwakeState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:RemoveImmunity("sleeping")
    end
	if inst.components.shedder ~= nil then
	inst.components.shedder:StartShedding(360)
	end
	
    if inst.components.talker ~= nil then
        inst.components.talker:StopIgnoringAll("sleeping")
    end
    if inst.components.firebug ~= nil then
        inst.components.firebug:Enable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(true)
        inst.components.playercontroller:Enable(true)
    end
    inst:OnWakeUp()
    inst.components.inventory:Show()
    inst:ShowActions(true)
	--inst.sg:GoToState("taunt")
end

local function SetSleeperSleepState(inst)
    if inst.components.grue ~= nil then
        inst.components.grue:AddImmunity("sleeping")
    end
    if inst.components.talker ~= nil then
        inst.components.talker:IgnoreAll("sleeping")
    end
	
	if inst.components.shedder ~= nil then
	inst.components.shedder:StopShedding()
	end
	
    if inst.components.firebug ~= nil then
        inst.components.firebug:Disable()
    end
    if inst.components.playercontroller ~= nil then
        inst.components.playercontroller:EnableMapControls(false)
        inst.components.playercontroller:Enable(false)
    end
    inst:OnSleepIn()
    inst.components.inventory:Hide()
    inst:PushEvent("ms_closepopups")
    inst:ShowActions(false)
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
		print(distsq)
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(0))
        if dist > 0 then
            return math.min(10, dist) / (18 * FRAMES)
        end
    end
    return 0
end

local function SetJumpPhysics(inst)
	--ToggleOffCharacterCollisions(inst)
end

local function SetNormalPhysics(inst)
	--ToggleOnCharacterCollisions(inst)
end

local function ExplosionShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 1, inst, 20)
end

local events=
{
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("attacked", function(inst) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("parrying") then 
            if not inst.sg:HasStateTag("attack") and not inst.isblocked and (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime() and not inst.sg:HasStateTag("nointerrupt") then -- don't interrupt attack or exit shield
                inst.sg:GoToState("hit") -- can still attack
            end
			inst.isblocked = nil
        end 
    end),
    EventHandler("doattack", function(inst, data) 
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
            inst.sg:GoToState("attack", data.target) 
        end 
    end),
    EventHandler("death", function(inst, data)
        if inst.sleepingbag ~= nil and (inst.sg:HasStateTag("bedroll") or inst.sg:HasStateTag("tent")) then -- wakeup on death to "consume" sleeping bag first
            inst.sleepingbag.components.sleepingbag:DoWakeUp()
            inst.sleepingbag = nil
        end

        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
			if inst:HasTag("_isinrez") then
				inst.sg:GoToState("fake_death")
			else
				inst.sg:GoToState("death")
			end
        end
    end),
	EventHandler("respawnfromcorpse", function(inst, reviver) 
		if inst.sg:HasStateTag("death") then 
			inst.sg:GoToState("corpse_rebirth") 
		end 
	end),	
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("entershield", function(inst) inst.sg:GoToState("shield") end),
    EventHandler("exitshield", function(inst) inst.sg:GoToState("shield_end") end),
    
	
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

local states=
{
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },
	
	State{
		name = "spawn",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("spawn")
			inst.DynamicShadow:Enable(false)		
        end,
		
		onexit = function(inst)
			inst.components.inventory:Open()
			inst.components.health:SetInvincible(false) --failsafe
			inst.DynamicShadow:Enable(true)
			if inst.glow then
				inst.Light:Enable(true)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("back")
			end
			if inst:HasTag("bombie") and not inst.fuse then
				local fx = SpawnPrefab("torchfire_rag")
				fx.entity:SetParent(inst.entity)
				fx.entity:AddFollower()
				fx.Follower:FollowSymbol(inst.GUID, "swap_fuse", 0, 0, 0)
				inst.fuse = fx
			end
		end,
	
		timeline=
		{
			TimeEvent(3*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound(inst.sounds.spawn) 
				if inst.components.health and not inst.components.health:IsDead() then
					inst.components.health:SetInvincible(true)
					--Leo: We make it invincible like it is in spiral knights, only making them take damage at a certain point in the animation.
				end
			end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt)
				if inst.glow then
					inst.Light:Enable(true)
				end
			end),
			TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(66*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt) end),
			TimeEvent(72*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) inst.DynamicShadow:Enable(true) end),
			
		},

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
        name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pst")
			inst.components.locomotor:StopMoving()
			if not inst.shouldwalk or inst.shouldwalk == false then
				inst.shouldwalk = true
			else
				inst.shouldwalk = false
			end
        end,

        timeline=
        {

        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.DynamicShadow:Enable(false)
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end
			if inst.fuse  then
				inst.fuse:Remove()
				inst.fuse = nil
			end
            inst.AnimState:PlayAnimation("death_old")
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.noskeleton = true
                    PlayablePets.DoDeath(inst)
                end
            end),
        },

        

    },    
	
	State{
        name = "fake_death", --I could use the corpse state. But I'm shaky about using forge states outside of it, plus the NPC will need this state anyways.
        tags = {"busy", "corpse"},
        
        onenter = function(inst)
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end
			if inst.fuse  then
				inst.fuse:Remove()
				inst.fuse = nil
			end
            inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death", false)
            inst.Physics:Stop()
            --RemovePhysicsColliders(inst)            
            --inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
			
			inst.sg:SetTimeout(15)
        end,

        ontimeout = function(inst) --didn't revive, give up.
            if MOBGHOST == "Enable" then
                inst:PushEvent(inst.ghostenabled and "makeplayerghost" or "playerdied", { skeleton = false })
			else
				TheWorld:PushEvent("ms_playerdespawnanddelete", inst)
			end
        end,        
    },
	
	State{
        name = "attack", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound(inst.sounds.grunt)
        inst.AnimState:PlayAnimation("attack_pre")
		inst.AnimState:PushAnimation("attack", false)
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {

			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack)
			--inst.SoundEmitter:PlaySound("dontstarve/wilson/attack_weapon")
			end),
            TimeEvent(21*FRAMES, function(inst) PlayablePets.DoWork(inst, 3.5) end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "special_atk1", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
			inst.components.combat:StartAttack()
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.SoundEmitter:PlaySound(inst.sounds.attack3_pre)
			inst.AnimState:PlayAnimation("breath_pre")
			inst.AnimState:PushAnimation("breath_loop", true)
			inst.sg:SetTimeout(TUNING.SK_ZOMBIES.BREATH_TIME)
			inst.fx_task = inst:DoPeriodicTask(TUNING.SK_ZOMBIES.BREATH_FOG_TICK, function(inst)
				if inst.sg.statemem.breathing then
					local pos = inst:GetPosition()
					local angle = -inst.Transform:GetRotation() * DEGREES
					local offset = TUNING.SK_ZOMBIES.BREATH_FOG_OFFSET
					local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
					local fx = SpawnPrefab("sk_zombie_breath"..inst.variant.."_fx")
					fx.Transform:SetPosition(targetpos.x, TUNING.SK_ZOMBIES.BREATH_FOG_Y_OFFSET, targetpos.z)
					fx.Transform:SetRotation(inst.Transform:GetRotation())
				end
			end)
			inst.breathing_task = inst:DoPeriodicTask(TUNING.SK_ZOMBIES.BREATH_PARTICLE_TICK, function(inst)
				if inst.sg.statemem.breathing then
					local pos = inst:GetPosition()
					local angle = -inst.Transform:GetRotation() * DEGREES
					local offset = TUNING.SK_ZOMBIES.BREATH_AOE_OFFSET
					local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
					local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, TUNING.SK_ZOMBIES.BREATH_AOE_RANGE, nil, GetExcludeTags(inst))
					local targets = {}
					
					if ents and #ents > 0 then
						for i, v in ipairs(ents) do
							if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
								if inst.variant == "_freeze" and v.components.freezable then
									v.components.freezable:Freeze(4)
								else
									PlayablePets.InflictStatus(inst, v, inst.variant == "" and "stun" or string.gsub(inst.variant, "_", ""), 1)
								end
							end
						end
					end	
				end
			end)
		end,

		onupdate = function(inst)
			if inst.sg.statemem.breathing and math.random() < TUNING.SK_ZOMBIES.BREATH_PARTICLE_CHANCE then
				local pos = inst:GetPosition()
				local angledif = TUNING.SK_ZOMBIES.BREATH_PARTICLE_ANGLEDIF
				local current_angle = -inst.Transform:GetRotation() * DEGREES
				local offset = TUNING.SK_ZOMBIES.BREATH_PARTICLE_OFFSET
				local offset_pos = Point(pos.x + (offset*math.cos(current_angle)), TUNING.SK_ZOMBIES.BREATH_PARTICLE_Y_OFFSET, pos.z + (offset*math.sin(current_angle)))
				local fx = SpawnPrefab("sk_zombie_breath"..inst.variant.."_particle_fx")
				fx.Transform:SetPosition(offset_pos:Get())
				fx.Transform:SetRotation(inst.Transform:GetRotation() + math.random(-angledif, angledif))
			end
		end,
		
		onexit = function(inst)
			inst.sg.statemem.breathing = nil
			if inst.breathing_task then
				inst.breathing_task:Cancel()
				inst.breathing_task = nil
			end
			if inst.fx_task then
				inst.fx_task:Cancel()
				inst.fx_task = nil
			end
			inst.taunt = false
			inst.taunt_cd = inst:DoTaskInTime(TUNING.SK_ZOMBIES.BREATH_COOLDOWN, function(inst) inst.taunt = true end)
		end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("attack3_pst")
		end,
		
		timeline=
		{
			TimeEvent(60*FRAMES, function(inst) 
				inst.sg.statemem.breathing = true
				inst.SoundEmitter:PlaySound(inst.sounds.attack3)
			end),
		},

        events=
        {
            --EventHandler("attacked", function(inst) inst.sg:GoToState("hit") end),
        },
	
    },
	
	State{
        name = "attack3_pst", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
			inst.components.combat:StartAttack()
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("breath_pst")
		end,
	
		onexit = function(inst)

		end,
		
		timeline=
		{

		},

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack2", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		local buffaction = inst:GetBufferedAction()
		local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
		inst.altattack = nil
		
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound(inst.sounds.grunt)
        inst.AnimState:PlayAnimation("attack2_pre")
		inst.AnimState:PushAnimation("attack2", false)
	end,
	
	onexit = function(inst)
		inst.sg.statemem.jump = nil
		inst.sg.statemem.attacktarget = nil
		inst.sg.mem.speed = nil
		inst.components.combat:SetRange(2, 3)
		inst:DoTaskInTime(10, function(inst) inst.altattack = true inst.components.combat:SetRange(7, 3) end)
    end,
	
	onupdate = function(inst)
        if inst.sg.statemem.jump then
            inst.Physics:SetMotorVel(inst.sg.mem.speed and inst.sg.mem.speed * 1.2 or 5, 0, 0)
        end
    end,
	
    timeline=
    {
		TimeEvent(18*FRAMES, function(inst) 
			inst.sg.statemem.jump = true
			SetJumpPhysics(inst)
			inst.sg:AddStateTag("nointerrupt")
			if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid() then
				inst.sg.mem.speed = CalcJumpSpeed(inst, inst.sg.statemem.attacktarget)
			end
		end),

		TimeEvent(18*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound(inst.sounds.attack2)
		end),
        TimeEvent(36*FRAMES, function(inst) 
			inst:PerformBufferedAction() 
			inst.sg.statemem.jump = false 
			inst.Physics:Stop()
			SetNormalPhysics(inst)
		end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack4", 
        tags = {"attack", "busy", "nointerrupt"},
		
		onenter = function(inst, target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound(inst.sounds.attack)
        inst.AnimState:PlayAnimation("attack4")
	end,
	
	onexit = function(inst)
		if inst.glow then
			inst.Light:Enable(false)
		end
		if inst.fx then
			inst.fx.AnimState:PlayAnimation("deactive")
		end
		if inst.fuse  then
			inst.fuse:Remove()
			inst.fuse = nil
		end
    end,
	
    timeline=
    {

    },

        events=
        {
            EventHandler("animover", function(inst) 

				----
				local explosion = SpawnPrefab("explode_small_slurtlehole")
				ExplosionShake(inst)
				explosion.Transform:SetPosition(inst:GetPosition():Get())
				explosion.Transform:SetScale(2.5, 2.5, 2.5)
				if inst.explosion_colour then
					explosion.AnimState:SetMultColour(inst.explosion_colour.r, inst.explosion_colour.g, inst.explosion_colour.b)
				end
				inst.SoundEmitter:PlaySound(inst.sounds.explode)
				inst.components.combat:DoAreaAttack(inst, inst.components.combat.hitrange, nil, nil, inst.variant == "_shock" and "electric" or "explosive",  GetExcludeTags(inst))
				inst.components.health:SetPercent(0)
			end),
        },
	
    },
	
	------------------SLEEPING-----------------
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("despawn",false)
			inst.DynamicShadow:Enable(false)
        end,
		
		timeline = 
		{
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(47*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(62*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn)
				if inst.glow then
					inst.Light:Enable(false)
				end
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("deactive")
				end
				if inst.fuse  then
					inst.fuse:Remove()
					inst.fuse = nil
				end
			end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
		
		onexit = function(inst)
			inst.MiniMapEntity:SetIcon("")
			inst:AddTag("noplayerindicator")
			inst.DynamicShadow:Enable(true)
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end
			if inst.fuse  then
				inst.fuse:Remove()
				inst.fuse = nil
			end
		end,
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.DynamicShadow:Enable(false)
				if inst.glow then
					inst.Light:Enable(false)
				end
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("deactive")
				end
				if inst.fuse  then
					inst.fuse:Remove()
					inst.fuse = nil
				end
				--inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/bearger/sleep")
				--inst.AnimState:PlayAnimation("sleep_loop")
				
				inst.sg:SetTimeout(1)
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline =
		{
			
		},
		
		ontimeout = function(inst)
			inst.sg:GoToState("sleeping")
		end,
		
		onexit = function(inst)
			inst.DynamicShadow:Enable(true)
			if inst.glow then
				inst.Light:Enable(true)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("back")
			end
			if inst:HasTag("bombie") then
				local fx = SpawnPrefab("torchfire_rag")
				fx.entity:SetParent(inst.entity)
				fx.entity:AddFollower()
				fx.Follower:FollowSymbol(inst.GUID, "swap_fuse", 0, 0, 0)
				inst.fuse = fx
			end
		end,

        events =
        {
            --EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end
			if inst.fuse  then
				inst.fuse:Remove()
				inst.fuse = nil
			end
			inst.DynamicShadow:Enable(false)
            inst.AnimState:PlayAnimation("spawn")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline = 
		{
			TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt)
				if inst.glow then
					inst.Light:Enable(true)
				end
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("back")
				end
				if inst:HasTag("bombie") then
					local fx = SpawnPrefab("torchfire_rag")
					fx.entity:SetParent(inst.entity)
					fx.entity:AddFollower()
					fx.Follower:FollowSymbol(inst.GUID, "swap_fuse", 0, 0, 0)
					inst.fuse = fx
				end
			end),
			TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(66*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt) inst.components.health:SetInvincible(false) end),
			TimeEvent(72*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) inst.DynamicShadow:Enable(true) end),
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
		
		onexit = function(inst)
			inst.DynamicShadow:Enable(true)
			if inst.glow then
				inst.Light:Enable(true)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("back")
			end
			if inst:HasTag("bombie") then
				local fx = SpawnPrefab("torchfire_rag")
				fx.entity:SetParent(inst.entity)
				fx.entity:AddFollower()
				fx.Follower:FollowSymbol(inst.GUID, "swap_fuse", 0, 0, 0)
				inst.fuse = fx
			end
			inst.MiniMapEntity:SetIcon(inst.prefab..".tex")
			inst:RemoveTag("noplayerindicator")
		end,
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			--inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			TimeEvent(12*FRAMES, function(inst) inst.components.locomotor:WalkForward() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			if math.random(1,10) <= 3.33 then
				inst.SoundEmitter:PlaySound(inst.sounds.grunt)
			end
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep),
			TimeEvent(23*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
	
	--
	
	State
    {
        name = "run_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(12*FRAMES, function(inst) inst.components.locomotor:WalkForward() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			if math.random(1,10) <= 1 then
				inst.SoundEmitter:PlaySound(inst.sounds.grunt)
			end
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep),
			TimeEvent(7*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
	
}




CommonStates.AddFrozenStates(states)

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) 
				if inst.glow then
					inst.Light:Enable(false)
				end
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("deactive")
				end
				if inst.fuse then
					inst.fuse:Remove()
					inst.fuse = nil
				end
			end),
		},
		
		corpse_taunt =
		{
			TimeEvent(3*FRAMES, function(inst) 
				inst.SoundEmitter:PlaySound(inst.sounds.spawn) 
			end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt) end),
			TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(66*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt) end),
			TimeEvent(72*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) 
				inst.DynamicShadow:Enable(true)
				if inst.glow then
					inst.Light:Enable(true)
				end
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("back")
				end
				if inst:HasTag("bombie") then
					local fx = SpawnPrefab("torchfire_rag")
					fx.entity:SetParent(inst.entity)
					fx.entity:AddFollower()
					fx.Follower:FollowSymbol(inst.GUID, "swap_fuse", 0, 0, 0)
					inst.fuse = fx
				end
			end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "spawn"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
	
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death_old")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)

PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pst",
	leap_loop = "attack2_pre",
	leap_pst = "attack2",
	
	lunge_pre = simpleanim,
	lunge_loop = simpleanim,
	lunge_pst = simpleanim,
	
	superjump_pre = simpleanim,
	superjump_loop = simpleanim,
	superjump_pst = simpleanim,
	
	parry_loop = "idle_loop",
	
	castspelltime = 10,
})

    
return StateGraph("zombiep", states, events, "spawn", actionhandlers)

