require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.ATTACK, "attack"),
    ActionHandler(ACTIONS.PP_TRANSFORM, "mimic"),
}

local tuning_values = TUNING.PP.PM_DUPLIGHOSTP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function DoTalkSound(inst)
    --[[
    inst.SoundEmitter:PlaySound(inst.sounds.talk, "talk")]]
    if inst.talking_task then
        inst.talking_task:Cancel()
        inst.talking_task = nil
    end
    inst.talking_task = inst:DoPeriodicTask(0, function(inst)
        inst.SoundEmitter:PlaySound("pm/common/talk_oneshot")
    end)
end

local function CalcBonkSpeed(inst, target)
	--local target = inst.components.combat.target
    if target and target:IsValid() then
        local x, y, z = target.Transform:GetWorldPosition()
        local distsq = inst:GetDistanceSqToPoint(x, y, z)
        if distsq > 0 then
            inst:ForceFacePoint(x, y, z)
            local dist = math.sqrt(distsq) - (target.Physics ~= nil and inst.Physics:GetRadius() + target.Physics:GetRadius() or inst.Physics:GetRadius())
            if dist > 0 then
                return math.min(8, dist) / (10 * FRAMES)
            end
        end
    end
    return 0
end

local function StopTalkSound(inst)
    --inst.SoundEmitter:KillSound("talk")
    if inst.talking_task then
        inst.talking_task:Cancel()
        inst.talking_task = nil
    end
end

local function StopPMTask(inst)
    if inst._pm_attack_task then
        inst._pm_attack_task:Cancel()
        inst._pm_attack_task = nil
    end
    inst.components.locomotor:SetReachDestinationCallback(nil)
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("ontalk", function(inst) if not inst.sg:HasStateTag("busy") then inst.sg:GoToState("talk") end end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --headbonk
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.target = target
				end
			end
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack_pre") --26 FRAMES
            inst.AnimState:PushAnimation("attack_loop", false) --10 FRAMES each
            inst.AnimState:PushAnimation("attack_loop", false)
            inst.AnimState:PushAnimation("attack", false) -- 13-14 FRAMES
            inst.SoundEmitter:PlaySound(inst.sounds.rise)
            --Turn off Character Physics here
	    end,
	
        onexit = function(inst)
            inst.components.health:SetInvincible(false)
        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.components.health:SetInvincible(true) end),
            TimeEvent(46*FRAMES, function(inst) 
                --Do targeted lunge here.
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
                if inst.sg.statemem.target then
                    inst:FacePoint(inst.sg.statemem.target:GetPosition())
                    inst.Physics:SetMotorVel(CalcBonkSpeed(inst, inst.sg.statemem.target), 0, 0)
                else
                    inst.Physics:SetMotorVel(inst.components.locomotor.runspeed * 1.3, 0, 0)
                end
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst)
                --Do attack check here I guess. Its a bit weird to have it in the animqueueover but should be fine. 
                local ent = FindEntity(inst, tuning_values.HIT_RANGE, nil, {"_combat", "_health"}, PlayablePets.GetExcludeTags(inst))
                if ent and ent:IsValid() then
                    inst.components.combat:DoAttack(ent)
                    inst.sg:GoToState("attack_pst")
                else
                    inst.sg:GoToState("attack_miss") 
                end
            end),
        },
	
    },

    State{
        name = "attack_pst",
        tags = {"attack", "busy", "canrotate"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack_pst")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
            inst.Physics:SetMotorVel(6, 0, 0)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(14*FRAMES, function(inst) inst.Physics:Stop() end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack_miss",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.AnimState:PlayAnimation("attack_miss")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
            inst.sg.statemem.miss_speed = 5
        end,
        
        onexit = function(inst)
            inst.sg.statemem.miss_speed = nil
        end,

        onupdate = function(inst)
            if inst.sg.statemem.miss_speed then
                inst.sg.statemem.miss_speed = math.max(0, inst.sg.statemem.miss_speed - 1/5)
                inst.Physics:SetMotorVel(inst.sg.statemem.miss_speed, 0, 0)
            end
        end,
        
        timeline=
        {
            TimeEvent(14*FRAMES, function(inst) inst.sg:RemoveStateTag("busy") end),
        },

        events=
        {
            EventHandler("locomote", function(inst) inst.sg:GoToState("recover") end),
        },
    },

    State{
        name = "recover",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack_recover")
            --jump sound here
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,
        
        timeline=
        {

        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "mimic",
        tags = {"attack", "busy", "canrotate"},
		
		onenter = function(inst, target)
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.target = target
				end
			end
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("transform_pre") --16 FRAMES each
            inst.AnimState:PushAnimation("transform_pre", false)
            inst.AnimState:PushAnimation("transform_pre", false)
            inst.AnimState:PushAnimation("transform_loop", true)
            inst.SoundEmitter:PlaySound(inst.sounds.transform_pre)
            inst.sg:SetTimeout(10) --fail safe
        end,
        
        onexit = function(inst)

        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,
        
        timeline=
        {
            TimeEvent(16*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(inst.sounds.transform_pre)
            end),
            TimeEvent(32*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(inst.sounds.transform_pre)
            end),
            TimeEvent(40*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(inst.sounds.laugh)
            end),
            TimeEvent(64*FRAMES, function(inst) 
                if inst.sg.statemem.target and inst.sg.statemem.target:IsValid() then
                    inst:DoTransform(inst.sg.statemem.target)
                end
            end),
        },

        events=
        {
            --EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"idle"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("transform_pre", false)
            inst.AnimState:PushAnimation("transform_loop", true)
            inst.SoundEmitter:PlaySound(inst.sounds.transform_pre)
        end,

		timeline=
        {
            TimeEvent(8*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(inst.sounds.laugh)
            end),
        },

        onexit = function(inst)

        end,

        events=
        {

        },
    },

    State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
            PlayablePets.ToggleWalk(inst)
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("hit", true)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            inst.sg:SetTimeout(2)
        end,

        timeline = {

        },

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death_pst)
            PlayablePets.DoDeath(inst)
        end,

        events =
        {

        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
            TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	

    State
    {
        name = "run_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {

		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	

    State{
        name = "talk",
        tags = { "idle", "talking" },

        onenter = function(inst, noanim)
            inst.AnimState:PlayAnimation("talk", true)
            DoTalkSound(inst)
            inst.sg:SetTimeout(0.5)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,

        events =
        {
            EventHandler("donetalking", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

        onexit = StopTalkSound,
    },
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "attack_miss",
		corpse_taunt = "attack_recover"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "idle_loop"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "hit")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)
    
return StateGraph("pm_duplighostp", states, events, "idle", actionhandlers)

