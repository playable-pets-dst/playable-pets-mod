require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "attack"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.PICKUP, "attack"),
	ActionHandler(ACTIONS.HARVEST, "attack"),
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

function ForceEatItem(inst, item)
	if item and item.components.edible and inst.components.eater:PrefersToEat(item) then
		inst.components.eater:Eat(item)
	else
		if item and item:IsValid() then
			item:Remove()
			inst.components.health:DoDelta(1)
			inst.components.hunger:DoDelta(10)
			inst.components.sanity:DoDelta(1)
		end
	end
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("stun", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.AddCommonHandlers(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnDeath(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnLocomoteAdvanced(), 
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --swipe
        tags = {"attack", "busy"},
		
		  onenter = function(inst, target)
        local buffaction = inst:GetBufferedAction()
        local target = buffaction ~= nil and buffaction.target or nil
        inst.components.combat:SetTarget(target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("bite")
        if target ~= nil then
            if target:IsValid() then
                inst:FacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {
			TimeEvent(0*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.attack_pre)
			end),
			TimeEvent(10*FRAMES, function(inst)
				if inst.bufferedaction.action == ACTIONS.PP_EAT and inst:IsNear(inst.sg.statemem.attacktarget, inst.mob_table.hit_range) then
					ForceEatItem(inst, inst.sg.statemem.attacktarget)
				else
					PlayablePets.DoWork(inst, 2)
					inst.sg:RemoveStateTag("pre_attack")
				end
			end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle", "atk_pst")  end),
        },
	
    },
	
	State{
		name = "stun",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
            inst.Physics:Stop()
			inst.sg.statemem.stimuli = stimuli or nil
			if stimuli and stimuli == "electric" then
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/stun")
				inst.AnimState:PlayAnimation("stun_loop", true)
				inst.sg:SetTimeout(1)
			else
				inst.AnimState:PlayAnimation("stun_loop")
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/hit")
			end
        end,

		timeline=
        {

        },
		
		onexit = function(inst)
            
        end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil)
		end,
		
		

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("stun_pst", inst.sg.statemem.stimuli or nil) end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy", "nofreeze"},

        onenter = function(inst, stimuli)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) 
				inst.sg:GoToState("idle")
			end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
			--inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
			inst.SoundEmitter:PlaySound(inst.sounds.taunt)
        end,

		timeline=
        {

        },

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
			--inst.SoundEmitter:PlaySound("dontstarve/pig/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State{
        name = "eat",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
			inst.SoundEmitter:PlaySound(inst.sounds.attack_pre)
            inst.AnimState:PlayAnimation("bite")
        end,

        onexit = function(inst)
        	inst:ClearBufferedAction()
        end,
        
        events = {
            EventHandler("animover", function(inst)
				inst:PerformBufferedAction()
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State{
        name = "land",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
			inst.SoundEmitter:PlaySound(inst.sounds.shell)
            inst.AnimState:PlayAnimation("flip_pre", false)
			inst.AnimState:PushAnimation("flip_loop", false)
			inst.AnimState:PushAnimation("flip_loop", false)
			inst.AnimState:PushAnimation("flip_loop", false)
			inst.AnimState:PushAnimation("flip_pst", false)
        end,
        
        events = {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			TimeEvent(0, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.sleep_in)
			end),
			TimeEvent(20*FRAMES, function(inst)
				inst.SoundEmitter:PlaySound(inst.sounds.sleep_out)
			end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "stun",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_loop")
        end,

		timeline=
        {
			--TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/stun") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("stun_pst") end),
        },
    },
	
	State{
		name = "stun_pst",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("stun_pst")
        end,

		timeline=
        {
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/turtillus/hide_pst") end),
        },
		
		onexit = function(inst)
            
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "taunt", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle_loop")
local simpleanim = "run_pst"
local simpleidle = "idle_loop"
local simplemove = "run"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)

CommonStates.AddWalkStates(states, {
	runtimeline = {
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
	},
})
CommonStates.AddRunStates(states, {
	runtimeline = {
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(13*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
		TimeEvent(19*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
	},
})
    
return StateGraph("roach_beetlep", states, events, "idle", actionhandlers)

