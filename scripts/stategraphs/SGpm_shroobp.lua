require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "work"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.PICKUP, "pick"),
    ActionHandler(ACTIONS.PICK, "pick"),
    ActionHandler(ACTIONS.HARVEST, "pick"),
    ActionHandler(ACTIONS.DROP, "pick"),
    ActionHandler(ACTIONS.GIVE, "pick"),
    ActionHandler(ACTIONS.GIVEALLTOPLAYER, "pick"),
    ActionHandler(ACTIONS.ATTACK, "attack"),
}

local tuning_values = TUNING.PP.PM_SHROOBP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("ontalk", function(inst) inst.SoundEmitter:PlaySound(inst.sounds.talk) if inst.sg:HasStateTag("idle") then inst.sg:GoToState("talk") end end),
    EventHandler("pm_doaction", function(inst, data) 
        local targets = data.targets
        local skill = data.skill
        if skill == "Attack" then
            inst.components.pm_unit.stage.components.pm_battlemanager:PlayAction(inst)
            local origin = inst:GetPosition()
            local tarpos = targets[1]:GetPosition()
            inst.components.locomotor:GoToPoint(Point(tarpos.x, 0, tarpos.z)) --TODO do proper movement calc
            inst._pm_attack_task = inst:DoPeriodicTask(0, function(inst)
                if inst:IsNear(targets[1], 2) and not inst.sg:HasStateTag("busy") then
                    inst.sg:GoToState("pm_attack", {mypos = origin, target = targets[1]})
                end
            end)
        end
    end),
    EventHandler("pm_hit", function(inst, data) 
        inst.sg:GoToState("hit")
    end),
    EventHandler("pm_death", function(inst, data) 
        inst.sg:GoToState("pm_death")
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "work",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
	    end,
	
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(7*FRAMES, function(inst) 
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "land",
        tags = {"busy"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("land")
        end,

    },

    State{
        name = "getup",
        tags = {"busy"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("getup_pre")
            inst.AnimState:PushAnimation("getup", false)
        end,

        onexit = function(inst)

        end,
        
        timeline=
        {

        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "attack",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
	    end,
	
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(7*FRAMES, function(inst) 
                inst:PerformBufferedAction()
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "pp_rangedspecial",
        tags = {"attack", "busy"},
		
		onenter = function(inst, pos)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("laser")
            if pos then
                inst:ForceFacePoint(pos.x, 0, pos.z)
            end
	    end,
	
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(7*FRAMES, function(inst) 
                inst.AnimState:Show("ARM_carry")
                inst.SoundEmitter:PlaySound(inst.sounds.charge)

            end),
            TimeEvent(11*FRAMES, function(inst) 
                local fx = SpawnPrefab("shroob_laser_charge")
                fx.entity:AddFollower()
                fx.Follower:FollowSymbol(inst.GUID, "raygun", 80, -30, 0)                
            end),
            TimeEvent(23*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
                inst:FireProjectile()
            end),
            TimeEvent(47*FRAMES, function(inst)
                local hands = inst.components.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
                if not hands then 
                    inst.AnimState:Hide("ARM_carry")
                end
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"idle"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.SoundEmitter:PlaySound(inst.sounds.laugh)
            inst.AnimState:PlayAnimation("laugh_pre", false)
            inst.AnimState:PushAnimation("laugh_loop", true)

        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			--EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
            PlayablePets.ToggleWalk(inst)
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("hit", true)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            inst.sg:SetTimeout(2)
        end,

        timeline = {

        },

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death_pst)
            PlayablePets.DoDeath(inst)
        end,

        events =
        {

        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
            --TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.step) end),
            TimeEvent(5*FRAMES, PlayFootstep),
            TimeEvent(18*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	

    State
    {
        name = "run_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
            TimeEvent(0*FRAMES, PlayFootstep),
            TimeEvent(7*FRAMES, PlayFootstep),
            TimeEvent(14*FRAMES, PlayFootstep),
            TimeEvent(21*FRAMES, PlayFootstep),
            TimeEvent(38*FRAMES, PlayFootstep),            
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	

    State
    {
        name = "pick",
        tags = {"busy"},

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("pick")
            inst:PerformBufferedAction()            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
    State
    {
        name = "eat",
        tags = {"busy"},

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("eat")          
        end,

        timeline = {
            TimeEvent(9*FRAMES, function(inst) inst:PerformBufferedAction() end),
            --TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.eat) end),
            --TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.eat) end),
            --TimeEvent(36*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.eat) end),
        },

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },		

    State{
        name = "talk",
        tags = { "idle", "talking" },

        onenter = function(inst, noanim)
            inst.AnimState:PlayAnimation("talk_loop", true)
            inst.sg:SetTimeout(0.5)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,

        events =
        {
            EventHandler("donetalking", function(inst)
                inst.sg:GoToState("idle")
            end),
        },
    },
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"pick", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "land",
		corpse_taunt = "laugh_loop"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddHomeState(states, nil, "pick", "walk_pst", true)
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "idle_loop"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = "jump_pre", loop = "jump_loop", pst = "jump_pst"}, nil, "hit")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)
    
return StateGraph("pm_shroobp", states, events, "idle", actionhandlers)

