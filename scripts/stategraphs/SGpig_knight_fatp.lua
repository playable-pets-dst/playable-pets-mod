require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "pickup"
local shortaction = "pickup"
local workaction = "work"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.HARVEST, "pickup"),
    ActionHandler(ACTIONS.PICK, "pickup"),
}

local tuning_values = TUNING.PIG_KNIGHT_FATP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function DoStep(inst)
    inst.SoundEmitter:PlaySound(inst.sounds.step)
    ShakeAllCameras(CAMERASHAKE.FULL, .25, .015, .25, inst, 10)
end

local function SmallShake(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, .25, .015, .25, inst, 10)
end

local function ShakePound(inst)
    ShakeAllCameras(CAMERASHAKE.FULL, 0.5, .03, .5, inst, 30)
end

local function DoWeaponAttack(inst, data)
    local data = data or {}
    local posx, posy, posz = inst.Transform:GetWorldPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = data.offset or 4.25
    local targetpos = Point(posx + (offset * math.cos(angle)), 0, posz + (offset * math.sin(angle)))
    inst.components.groundpounder.numRings = 2
    inst.components.groundpounder:GroundPound(targetpos)
    local ents = TheSim:FindEntities(targetpos.x, 0, targetpos.z, data.range or inst.components.combat.hitrange, {"_health", "_combat"}, PlayablePets.GetExcludeTags(inst))
    for i, v in ipairs(ents) do
        if v and not v.components.health:IsDead() and v ~= inst then
            if not v:HasTag("player") and v.components.factionp and v.components.factionp.faction == inst.components.factionp.faction then
                return
            end
            local attack = data
            inst.components.combat:DoAdvancedAttack(v, attack, inst:ShouldCrit(v) and 3 or 1)
        end
    end
end

local function DoSlamAttack(inst, data)
    local data = data or {}
    local pos = inst:GetPosition()
    inst.components.groundpounder.numRings = 4
    inst.components.groundpounder:GroundPound(pos)
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, data.range or inst.components.combat.hitrange, {"_health", "_combat"}, PlayablePets.GetExcludeTags(inst))
    for i, v in ipairs(ents) do
        if v and not v.components.health:IsDead() and v ~= inst then
            if not v:HasTag("player") and v.components.factionp and v.components.factionp.faction == inst.components.factionp.faction then
                return
            end
            local dist = inst:GetDistanceSqToPoint(v:GetPosition())
            inst.components.combat:DoAdvancedAttack(v, data)
        end
    end
    inst.components.playablepet:CooldownAbility(PP_ABILITY_SLOTS.SKILL3, tuning_values.JUMP_CD)
end

local function CalcJumpSpeed(inst, targetpos, dist_limit, frames)
    local pos = inst:GetPosition()
    local distsq = inst:GetDistanceSqToPoint(targetpos)
    local dist = math.sqrt(distsq)
    if dist > 0 then
        return math.min(dist_limit, dist) / (frames * FRAMES)
    end
end

local HIT_RECOVERY = tuning_values.HIT_RECOVERY or 5

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
        elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and (inst.sg.mem.last_hit_time or 0) + HIT_RECOVERY < GetTime() then
            inst.sg:GoToState("hit")
        end
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
			local target = buffaction ~= nil and buffaction.target or nil
			if target ~= nil and target:IsValid() then
				inst.sg.statemem.target = target
				inst.sg.statemem.targetpos = target:GetPosition()
				inst:ForceFacePoint(inst.sg.statemem.targetpos)
			end
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
            inst.SoundEmitter:PlaySound(inst.sounds.grunt)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            FrameEvent(5, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.swing) end),
            FrameEvent(22, function(inst) DoWeaponAttack(inst, inst._attacks.weapon_slam)
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
                --inst.SoundEmitter:PlaySound(inst.sounds.smash)
                ShakePound(inst)
                --fx here            
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "work",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
            inst.SoundEmitter:PlaySound(inst.sounds.grunt)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            FrameEvent(5, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.swing) end),
            FrameEvent(22, function(inst) PlayablePets.DoWork(inst, 20)
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
                --inst.SoundEmitter:PlaySound(inst.sounds.smash)
                ShakePound(inst)
                --fx here            
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "attack2", --butt slam
        tags = {"attack", "busy"},
		
		onenter = function(inst, targetpos)
            if targetpos then
                inst.sg.statemem.targetpos = targetpos
                inst:ForceFacePoint(inst.sg.statemem.targetpos)
            end
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack2")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            FrameEvent(22, function(inst)
                if inst.sg.statemem.targetpos then
                    inst:ForceFacePoint(inst.sg.statemem.targetpos)
                    local speed = CalcJumpSpeed(inst, inst.sg.statemem.targetpos, 5, 24)
                    inst.Physics:SetMotorVel(speed, 0, 0)
                end
            end),
            FrameEvent(46, function(inst) 
                DoSlamAttack(inst, inst._attacks.butt_slam)
                inst.SoundEmitter:PlaySound(inst.sounds.bodyfall)
                inst.SoundEmitter:PlaySound(inst.sounds.smash)
                ShakePound(inst)
                inst.Physics:Stop()
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "interact",
        tags = {"busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("interact")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) PlayablePets.DoWork(inst, 8) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "pickup",
        tags = {"busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("pickup")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(16*FRAMES, function(inst) PlayablePets.DoWork(inst, 8) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "eat",
        tags = {"busy"},
		
		onenter = function(inst)
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat")
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(21*FRAMES, function(inst) PlayablePets.DoWork(inst, 8) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.sg.mem.last_hit_time = GetTime()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound(inst.sounds.hurt) 
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {
            FrameEvent(17, function(inst)
                --weapon hits the ground
                DoStep(inst)
            end),
            FrameEvent(36, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.grunt)
            end)
        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "special_atk2",
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation(inst.shouldwalk and "walk_pst" or "run_pst")
            PlayablePets.ToggleWalk(inst)
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            inst.sg:SetTimeout(4)
        end,

        timeline = {
            FrameEvent(6, function(inst)
                --weapon hits ground.
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
                SmallShake(inst)
            end),
            FrameEvent(35, function(inst)
                inst.SoundEmitter:PlaySound(inst.sounds.bodyfall)
                ShakePound(inst)
            end)
        },

        ontimeout = function(inst)
            PlayablePets.DoDeath(inst)
        end,

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_loop_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_loop_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            if math.random() > 0.8 then
                inst.SoundEmitter:PlaySound(inst.sounds.grunt)
            end
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = 
        {
            TimeEvent(0*FRAMES, DoStep),
            TimeEvent(22*FRAMES, DoStep),
		},
		
        events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
		},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
    
    State
    {
        name = "run_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run")
        end,
		
		timeline = {
            FrameEvent(7, DoStep),
            FrameEvent(19, DoStep),
		},
		
        events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
		},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "walk_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "walk_loop"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)




    
return StateGraph("pig_knight_fatp", states, events, "idle", actionhandlers)

