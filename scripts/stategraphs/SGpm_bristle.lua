require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

local tuning_values = TUNING.PP.FUMOP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function DoTalkSound(inst)
    --[[
    inst.SoundEmitter:PlaySound(inst.sounds.talk, "talk")]]
    inst.talking_task = inst:DoPeriodicTask(0, function(inst)
        inst.SoundEmitter:PlaySound("pm/common/talk_oneshot")
    end)
end

local function StopTalkSound(inst)
    --inst.SoundEmitter:KillSound("talk")
    if inst.talking_task then
        inst.talking_task:Cancel()
        inst.talking_task = nil
    end
end

local function StopPMTask(inst)
    print("Stopping Task")
    if inst._pm_attack_task then
        inst._pm_attack_task:Cancel()
        inst._pm_attack_task = nil
    end
    inst.components.locomotor:SetReachDestinationCallback(nil)
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("ontalk", function(inst) if not inst.sg:HasStateTag("busy") then inst.sg:GoToState("talk") end end),
    EventHandler("pm_onact", function(inst, data) 
        --Since I'm an NPC, I can just do whatever the fuck I want, hud be damned.
        print("NPC turn start")
        local stage = inst.components.pm_unit.stage.components.pm_battlemanager
        local enemyteam = stage:GetOpposingTeamOfUnit(inst)
        local targets = stage:GetAllUnitsOnTeam(enemyteam)
        inst:PushEvent("pm_doaction", {targets = targets, skill = "Attack"})
    end),
    EventHandler("pm_doaction", function(inst, data) 
        local targets = data.targets
        local skill = data.skill
        if skill == "Attack" then
            local origin = inst:GetPosition()
            local tarpos = targets[1]:GetPosition()
            inst.components.locomotor:GoToPoint(Point(tarpos.x, 0, tarpos.z)) --TODO do proper movement calc
            inst._pm_attack_task = inst:DoPeriodicTask(0, function(inst)
                if inst:IsNear(targets[1], 2) and not inst.sg:HasStateTag("busy") then
                    inst.sg:GoToState("pm_attack", {mypos = origin, target = targets[1]})
                end
            end)
            --inst.sg:GoToState("pm_attack", targets[1])
        end
    end),
    EventHandler("pm_hit", function(inst, data) 
        inst.sg:GoToState("hit")
    end),
    EventHandler("pm_death", function(inst, data) 
        inst.sg:GoToState("pm_death")
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "pm_attack",
        tags = {"busy", "pm_attacking"},
        onenter = function(inst, data)
            StopPMTask(inst)
            inst.components.locomotor:Stop()

            inst.sg.mem.origin = data.mypos
            inst.sg.mem.target = data.target
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,

        --onexit = StopPMTask,
        timeline=
        {
            TimeEvent(6*FRAMES, function(inst) inst.components.pm_unit:DoDamage(inst.sg.mem.target, 1) end),
        },

        events=
        {
            EventHandler("animover", function(inst) 
                inst.sg:RemoveStateTag("busy")
                local mypos = inst.sg.mem.origin
                inst.components.locomotor:GoToPoint(Point(mypos.x, 0, mypos.z))
                inst.components.locomotor:SetReachDestinationCallback(function(inst)
                    inst.Transform:SetRotation(inst.components.pm_unit.team == 1 and 0 or 180)
                    StopPMTask(inst)
                    inst.components.pm_unit.stage:PushEvent("pm_unit_endturn", {prevactor = inst})
                end)
            end),
        },

    },

    State{
        name = "attack",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
        inst.AnimState:PlayAnimation("attack")
        inst.SoundEmitter:PlaySound(inst.sounds.attack)
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {
        TimeEvent(6*FRAMES, function(inst) PlayablePets.DoWork(inst, 3) end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "attack2",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack2_pre")
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(6*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack2) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("attack2_loop") end),
        },
    },

    State{
        name = "attack2_loop",
        tags = {"attack", "spinning", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack2_loop", true)
            inst.Physics:SetMotorVel(8, 0, 0)
            inst.sg:SetTimeout(0.7)
        end,
        
        onexit = function(inst)

        end,

        ontimeout = function(inst)
            inst.sg:GoToState("attack2_pst")
        end,
        
        timeline=
        {
            --TimeEvent(10*FRAMES, function(inst) PlayablePets.DoWork(inst, 3) end),
        },

        events=
        {
            
        },
    },

    State{
        name = "attack2_pst",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack2_pst")
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("action")
            PlayablePets.ToggleWalk(inst)
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("hit", true)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
            inst.sg:SetTimeout(2)
        end,

        timeline = {

        },

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death_pst)
            --PlayablePets.DoDeath(inst)
        end,

        events =
        {

        },

    },

    State{
        name = "pm_death",
        tags = {"busy"},

        onenter = function(inst, killer)
            if killer then
                inst.sg.mem.killer = killer
            end
            inst.AnimState:PlayAnimation("hit", true)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
            inst.sg:SetTimeout(3)
        end,

        timeline = {
            TimeEvent(1, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
            TimeEvent(2, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death_pst) 
                inst.components.pm_unit:AwardExp(inst.sg.mem.killer)
                inst.components.sizetweener:StartTween(0.05, 0.75)
            end)
        },

        ontimeout = function(inst)
            --DoDeath
            inst:Hide()
            --inst.DynamicShadow:Enable(false)
            --PlayablePets.DoDeath(inst)
        end,

        events =
        {

        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("idle_static_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation("idle_static")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("idle_shake", true)
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            inst.sg:SetTimeout(2)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,

        events =
        {

        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("idle_loop")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("idle_loop")
        end,
		
		timeline = {

		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("action")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	

    State{
        name = "talk",
        tags = { "idle", "talking" },

        onenter = function(inst, noanim)
            inst.AnimState:PlayAnimation("talk", true)
            DoTalkSound(inst)
            inst.sg:SetTimeout(0.5)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,

        events =
        {
            EventHandler("donetalking", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

        onexit = StopTalkSound,
    },
}

return StateGraph("pm_bristle", states, events, "idle", actionhandlers)

