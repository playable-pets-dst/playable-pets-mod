require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "attack"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	ActionHandler(ACTIONS.ATTACK, function(inst)
        if inst.attack2 then
            return "attack2"
        else
            return "attack"
        end		
	end),
    ActionHandler(ACTIONS.HULK_TELEPORT, "action"),
}

local tuning_values = TUNING.PP.LC_APOCBIRDP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow", "battlestandard", "lobcorp_mob"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "lobcorp_mob"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "lobcorp_mob"}
	end
end

local function DoLure(inst)
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, tuning_values.LURE_RANGE, {"_combat"}, GetExcludeTagsp(inst))
    if #ents > 0 then
        for i, v in ipairs(ents) do
            if v ~= inst and v.components.locomotor and v:IsValid() and v.components.health and not v.components.health:IsDead() then
                v:AddDebuff("lc_debuff_enchantlure", "lc_debuff_enchantlure")
            end
        end
    end
end

local function DoStep(inst)
    inst.SoundEmitter:PlaySound(inst.sounds.step)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 30)
end

local function DoSlam(inst)
    inst.SoundEmitter:PlaySound(inst.sounds.slam)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .05, .5, inst, 30)
    local pos = inst:GetPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 10
	local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 8, nil, GetExcludeTagsp(inst))
	local targets = {}
    local fx = SpawnPrefab("groundpoundring_fx")
    fx.Transform:SetPosition(targetpos.x, 0, targetpos.z)
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst, tuning_values.ALT_DAMAGE/(v:HasTag("player") and 2 or 1))
				inst:PushEvent("onattackother", { target = v })
			end
		end
	else
		inst:PushEvent("onmissother")		
	end
end

local function DoBite(inst)
    inst.SoundEmitter:PlaySound(inst.sounds.bite)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .05, .5, inst, 30)
    local pos = inst:GetPosition()
	local angle = -inst.Transform:GetRotation() * DEGREES
	local offset = 10
	local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
	local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, 3, nil, GetExcludeTagsp(inst))
	local targets = {}
	if ents and #ents > 0 then
		for i, v in ipairs(ents) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
				table.insert(targets, v)
			end
		end
	end	
	if targets and #targets > 0 then
		for i, v in ipairs(targets) do
			if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat then
				v.components.combat:GetAttacked(inst, tuning_values.BITE_DAMAGE/(v:HasTag("player") and 2 or 1))
                inst.components.health:DoDelta(tuning_values.BITE_DAMAGE/2)
				inst:PushEvent("onattackother", { target = v })
			end
		end
	else
		inst:PushEvent("onmissother")		
	end
end

local function DoJudgement(inst, target)
    if target:IsValid() and target.components.health and not target.components.health:IsDead() and target.components.combat then
        target.components.combat:GetAttacked(inst, 1, nil, "strong")
        if target.components.health.currenthealth <= target.components.health.maxhealth*tuning_values.JUDGEMENT_PERCENT and target:HasTag("player") then
            --should be killing blow. Hacky way to do this... seems like dodelta doesn't run killed event on jb, this WILL cause issues with mobs that uses multiple skeletons.
            local pl = target
            local old_skeleton = pl.skeleton_prefab
		    pl.skeleton_prefab = "pp_skeleton_lynch"
		    pl:ListenForEvent("respawnfromghost", function(pl) 
			     pl.skeleton_prefab = old_skeleton
		    end)
        end
        target.components.health:DoDelta(-target.components.health.maxhealth*tuning_values.JUDGEMENT_PERCENT, false, nil, nil, inst, true)
    end
end

local function CallJudgement(inst)
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, tuning_values.JUDGEMENT_RANGE, {"_combat"}, GetExcludeTagsp(inst))
    if #ents > 0 then
        for i, v in ipairs(ents) do
            if v ~= inst then
                DoJudgement(inst, v)
            end
        end
    end
end

local function DoLure(inst)
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, tuning_values.LURE_RANGE, {"_combat"}, GetExcludeTagsp(inst))
    if #ents > 0 then
        for i, v in ipairs(ents) do
            if v ~= inst and v.components.locomotor and v:IsValid() and v.components.health and not v.components.health:IsDead() then
                v:AddDebuff("lc_debuff_enchantlure", "lc_debuff_enchantlure")
            end
        end
    end
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,

    },

    State{
        name = "attack", --attack 
        tags = {"attack", "busy", "biting"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
            inst.SoundEmitter:PlaySound(inst.sounds.crackle)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(29*FRAMES, function(inst) 
                PlayablePets.DoWork(inst, 3) 
                inst.SoundEmitter:PlaySound(inst.sounds.bite)
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack2", --heavy attack
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.attack2 = false
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack2")
            inst.SoundEmitter:PlaySound(inst.sounds.crackle)
    	end,
	
        timeline=
        {
            TimeEvent(48*FRAMES, function(inst) 
                DoSlam(inst)
            end),
        },

        onexit = function(inst)
            inst.attack2 = false
            if inst.attack2_task then
                inst.attack2_task:Cancel()
                inst.attack2_task = nil
            end
            inst.attack2_task = inst:DoTaskInTime(tuning_values.ATTACK2_CD, function(inst)
                inst.attack2 = true
            end)
        end,

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1", --attack 4/laser
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack4")
            --inst.taunt = false
            inst.SoundEmitter:PlaySound(inst.sounds.teleport)
        end,

		timeline=
        {
			TimeEvent(71*FRAMES, function(inst) inst:CastProjectiles() end)
        },

        onexit = function(inst)
            inst.taunt = false
            if inst.tauntcd_task then
                inst.tauntcd_task:Cancel()
                inst.tauntcd_task = nil
            end
            inst.tauntcd_task = inst:DoTaskInTime(tuning_values.LASER_CD, function(inst) inst.taunt = true end)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "special_atk2", --attack 5
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack5")
            --inst.taunt = false
            inst.SoundEmitter:PlaySound(inst.sounds.crackle)
            inst.SoundEmitter:PlaySound("lobcorp/creatures/judgementbird/attack")
        end,

		timeline=
        {
			TimeEvent(34*FRAMES, function(inst) inst.SoundEmitter:PlaySound("lobcorp/creatures/judgementbird/down") end),
            TimeEvent(42*FRAMES, function(inst) 
                CallJudgement(inst) 
                inst.SoundEmitter:PlaySound("lobcorp/creatures/judgementbird/stun")
            end)
        },

        onexit = function(inst)
            inst.taunt2 = false
            if inst.taunt2cd_task then
                inst.taunt2cd_task:Cancel()
                inst.taunt2cd_task = nil
            end
            inst.taunt2cd_task = inst:DoTaskInTime(tuning_values.LYNCH_CD, function(inst) inst.taunt2 = true end)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "special_atk3", --lure
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack6")
            --inst.taunt = false
            inst.SoundEmitter:PlaySound(inst.sounds.crackle)
            inst.fx = SpawnPrefab("lc_lure_fx")
            inst.fx.AnimState:SetScale(4, 4)
	        inst.fx.entity:AddFollower()
	        inst.fx.Follower:FollowSymbol(inst.GUID, "luresfx", 0, 0, 0)
            inst.fx.Transform:SetPosition(0, 0, 0)
        end,

		timeline=
        {
			TimeEvent(40*FRAMES, function(inst) 
                DoLure(inst) 
            end)
        },

        onexit = function(inst)
            inst.taunt3 = false
            if inst.taunt3cd_task then
                inst.taunt3cd_task:Cancel()
                inst.taunt3cd_task = nil
            end
            inst.taunt3cd_task = inst:DoTaskInTime(tuning_values.LURE_CD, function(inst) inst.taunt3 = true end)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    inst.SoundEmitter:KillSound("amb_loop")
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = {"busy"}, --Attack 3

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("attack3")
        end,

        timeline=
        {
			TimeEvent(16*FRAMES, function(inst) DoStep(inst) end),
            TimeEvent(46*FRAMES, function(inst) DoBite(inst) end)
        },

        onexit = function(inst)
            inst.mobsleep = false
            if inst.sleepcd_task then
                inst.sleepcd_task:Cancel()
                inst.sleepcd_task = nil
            end
            inst.sleepcd_task = inst:DoTaskInTime(tuning_values.BITE_CD, function(inst) inst.mobsleep = true end)
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "portal_jumpin",  
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst, pos)
			inst.can_tele = false
			if pos then
				inst.tele_pos = pos
			end
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("teleport_pre")
            inst.SoundEmitter:PlaySound(inst.sounds.teleport)
            inst.DynamicShadow:Enable(false)
        end,

        events =
        {   
            EventHandler("animover", function(inst) inst.sg:GoToState("teleport_loop", inst.tele_pos or nil) end ),        
        },
		
		onexit = function(inst)
			inst:DoTaskInTime(tuning_values.TELE_CD, function(inst) inst.can_tele = true end)
		end,

        timeline=
        {

        },
    },

    State{
        name = "teleport_loop",  
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst)
            inst:Hide()
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("run_pst")          
        end,            
        
        events =
        {   
            EventHandler("animover", function(inst)                
                inst.sg:GoToState("teleport_pst", inst.tele_pos or nil)
            end ),        
        },

        timeline=
        {
            
        },
    },

    State{
        name = "teleport_pst",  
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst, pos)
            inst:Show()
            inst.DynamicShadow:Enable(true)
            if pos or inst.tele_pos then
                inst.Transform:SetPosition(inst.tele_pos.x, 0 ,inst.tele_pos.z) 
            end
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("teleport_pst")  
            inst.SoundEmitter:PlaySound(inst.sounds.teleport)
        end,            
        
        events =
        {   
            EventHandler("animover", function(inst)                
                inst.sg:GoToState("idle", inst.tele_pos or nil)
            end ),        
        },

        timeline=
        {
            
        },
    },
	
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "run_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			--inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_pre")			
        end,
		
		timeline = 
		{
			TimeEvent(25*FRAMES, function(inst) DoStep(inst) end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
            TimeEvent(26*FRAMES, function(inst) inst.Physics:Stop() end ),
            TimeEvent(65*FRAMES, function(inst) DoStep(inst) end ),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
        end,

        timeline = {
            TimeEvent(24*FRAMES, function(inst) DoStep(inst) end ),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"run_pst", nil, nil, "idle", "run_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "idle"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "run_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle")
PP_CommonStates.AddSailStates(states, {}, "run_pst", "idle")
local simpleanim = "run_pst"
local simpleidle = "idle"
local simplemove = "run"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "run_pst",
}
)
    
return StateGraph("lc_apocbirdp", states, events, "idle", actionhandlers)

