require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.ATTACK, "idle"), --Summoner can attack, the others cannot.
    --ActionHandler(ACTIONS.PP_DODGE, "idle"),
   -- ActionHandler(ACTIONS.PP_RANGEDSPECIAL, "idle"),
}

local tuning_values = TUNING.PP.COTL_SCAMPP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local events=
{
    EventHandler("attacked", function(inst, data) 
        --TODO this event doesn't get pushed on a killing blow.
        inst._stored_damage = data.damage or 0
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	--CommonHandlers.OnHop(),
	--PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
    EventHandler("emote", function(inst, data)
        if not (inst.sg:HasStateTag("busy") or
                inst.sg:HasStateTag("nopredict") or
                inst.sg:HasStateTag("sleeping"))
            and not inst.components.inventory:IsHeavyLifting()
            and (data.mounted or not inst.components.rider:IsRiding())
            and (data.beaver or not inst:HasTag("beaver"))
            and (not data.requires_validation or TheInventory:CheckClientOwnership(inst.userid, data.item_type)) then
            inst.sg:GoToState("emote", data)
        end
    end),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function OnDisappear(inst)
    inst:Hide()
    inst:AddTag("invisible")
	inst:AddTag("notarget")
	inst.components.health:SetInvincible(true)
	inst.noactions = true
    inst.DynamicShadow:Enable(false)
    inst.MiniMapEntity:SetIcon("")
	inst:RemoveTag("scarytoprey")
    local speed = 2
    inst.components.locomotor:SetExternalSpeedMultiplier(inst, speed, speed)  
end

local function OnAppear(inst)
    inst:Show()
    inst.noactions = nil
    local speed = 2
    inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, speed)
	inst.components.health:SetInvincible(false)
    inst.DynamicShadow:Enable(true)
	inst:RemoveTag("invisible")
	inst:RemoveTag("notarget")
	inst:AddTag("scarytoprey") 
    inst.MiniMapEntity:SetIcon(inst.mob_table.minimap)
end

local states =
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "emote",
        tags = {"idle", "canrotate"},
        onenter = function(inst, data)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            if data.anim and data.anim == "emoteXL_happycheer" then
                inst.AnimState:PlayAnimation("pray2_pre")
                inst.AnimState:PushAnimation("pray2_loop", true)
            else
                inst.AnimState:PlayAnimation("pray_pre")
                inst.AnimState:PushAnimation("pray_loop", true)
            end
        end,

    },

    State{
        name = "pp_rangedspecial",
        tags = {"attack", "busy"},
		
		onenter = function(inst, pos)
            inst.sg.statemem.target_pos = pos
            inst:ForceFacePoint(pos.x, 0, pos.z)
            inst.SoundEmitter:PlaySound(inst.sounds.talk)
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("cast")
        end,
        
        onexit = function(inst)
            inst.components.combat:StartAttack()
        end,
        
        timeline=
        {
            TimeEvent(25*FRAMES, function(inst) 
                if inst.DoSpecial then 
                    inst:DoSpecial(inst.sg.statemem.target_pos)
                end
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
            inst.SoundEmitter:PlaySound(inst.sounds.hit) 
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1", --dodge roll?
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("action")
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "special_atk2", --dodge roll?
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            if inst.noactions then
                inst.sg:GoToState("spawn")
            else
                inst.sg:GoToState("despawn")
            end
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "spawn",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("action", true)
            inst.sg.statemem.portal = SpawnPrefab("cotl_enemyspawnp")
            local portal = inst.sg.statemem.portal
            portal.Transform:SetPosition(inst:GetPosition():Get())
        end,

		timeline=
        {
            TimeEvent(10*FRAMES, function(inst)
                inst.AnimState:PushAnimation("spawn", false)
                OnAppear(inst)
                inst.AnimState:SetMultColour(0,0,0,1)
                inst.components.colourtweener:StartTween({1,1,1,1}, 1)
                inst.SoundEmitter:PlaySound(inst.sounds.teleport_in)
            end)
        },

        onexit = function(inst)
            if inst.sg.statemem.portal then
                inst.sg.statemem.portal:DoDespawn(inst)
            end
            inst.sg.statemem.portal = nil
        end,

        events=
        {
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "despawn",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("despawn")
            inst.components.colourtweener:StartTween({0,0,0,1}, 0.25)
            inst.sg.statemem.portal = SpawnPrefab("cotl_enemyspawnp")
            local portal = inst.sg.statemem.portal
            portal.Transform:SetPosition(inst:GetPosition():Get())
            inst.SoundEmitter:PlaySound(inst.sounds.teleport_out)
        end,

		timeline=
        {

        },

        onexit = function(inst)
            OnDisappear(inst)
            if inst.sg.statemem.portal then
                inst.sg.statemem.portal:DoTaskInTime(10*FRAMES, function(other) other:DoDespawn(inst) end)
            end
            inst.sg.statemem.portal = nil
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst, data)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            if data.afflicter and data.afflicter:IsValid() then
                inst.skeleton_prefab = nil
                local skel = SpawnPrefab("pp_skeleton_cotl_grunt")
                MakeInventoryPhysics(skel)
                skel.Transform:SetPosition(inst:GetPosition():Get())
                skel.Transform:SetRotation(inst.Transform:GetRotation())

                skel:DoTaskInTime(10, function(skel)
                    MakeSmallObstaclePhysics(skel, 0.25)
                end)

                skel:SetSkeletonDescription(inst.prefab, inst:GetDisplayName(), inst.deathcause, inst.deathpkname, inst.userid)
                skel:SetSkeletonAvatarData(inst.deathclientobj)
                Launch(skel, data.afflicter, inst._stored_damage and inst._stored_damage/5 or 0)
            end
            PlayablePets.DoDeath(inst)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					--PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("idle_loop")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("idle_loop")
        end,
		
		timeline = {

		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("action")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"action", nil, "pray2_pre", "pray2_loop", "action") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "spawn"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "action")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "action", "idle_loop")
local simpleanim = "action"
local simpleidle = "idle_loop"
local simplemove = "idle_loop"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "action",
}
)




    
return StateGraph("cotl_sorcerorp", states, events, "idle", actionhandlers)

