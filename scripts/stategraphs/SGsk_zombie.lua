require("stategraphs/commonstates")
require("stategraphs/ppstates")

local function GetExcludeTags(inst)
	if TheNet:GetServerGameMode() == "lavaarena" or inst:HasTag("companion") then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","zombie", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "zombie", "undead", "bombie"}
	end
end

local actionhandlers = 
{
    ActionHandler(ACTIONS.ATTACK, "attack")
}

local function ExplosionShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 1, inst, 20)
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
		print(distsq)
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(0))
        if dist > 0 then
            return math.min(10, dist) / (18 * FRAMES)
        end
    end
    return 0
end

local function SetJumpPhysics(inst)
	ToggleOffCharacterCollisions(inst)
end

local function SetNormalPhysics(inst)
	ToggleOnCharacterCollisions(inst)
end

local events=
{
    EventHandler("attacked", function(inst) 
        if not inst.components.health:IsDead() then 
            if not inst.sg:HasStateTag("attack") and not inst.isblocked and (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery < GetTime() and not inst.sg:HasStateTag("nointerrupt") then -- don't interrupt attack or exit shield
                inst.sg:GoToState("hit") -- can still attack
            end
			inst.isblocked = nil
        end 
    end),
    EventHandler("doattack", function(inst, data) 
		if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) and data.target:IsValid() then 
			if inst:HasTag("bellhop") and data.target:HasTag("player") then
				inst.components.combat:SetTarget(nil)
			elseif inst:HasTag("bombie") then
				inst.sg:GoToState("attack4", data.target)
			else
				if inst.altattack and not inst:IsNear(data.target, TUNING.SK_ZOMBIES.ATTACK_MELEE_RANGE) then
					inst.sg:GoToState("attack2", data.target)
				elseif inst.can_breath then
					inst.sg:GoToState("attack3", data.target)
				else
					inst.sg:GoToState("attack", data.target)
				end	
			end	
		end 
	end),
    EventHandler("death", function(inst, data)
        if (data ~= nil and data.cause == "file_load") or inst.components.revivablecorpse ~= nil then
            inst.sg:GoToState("corpse", true)
        else
			if inst:HasTag("_isinrez") then
				inst.sg:GoToState("fake_death")
			else
				inst.sg:GoToState("death")
			end
        end
    end),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnKnockback(),
	EventHandler("locomote", function(inst)
        local is_moving = inst.sg:HasStateTag("moving")
        local is_running = inst.sg:HasStateTag("running")
        local is_idling = inst.sg:HasStateTag("idle")

        local should_move = inst.components.locomotor:WantsToMoveForward()
        local should_run = inst.components.locomotor:WantsToRun() and inst:HasTag("bombie")

        if is_moving and not should_move then
            inst.sg:GoToState(is_running and "run_stop" or "walk_stop")
        elseif (is_idling and should_move) or (is_moving and should_move and is_running ~= should_run) then
            if should_run then
                inst.sg:GoToState("run_start")
            else
                inst.sg:GoToState("walk_start")
            end
        end
    end),
	CommonHandlers.OnHop()
	--PP_CommonHandlers.OnLocomoteAdvanced(),    
}

local function DisableAttack2(inst)
	inst.altattack = false
	if not inst.can_breath then
		inst.components.combat:SetRange(TUNING.SK_ZOMBIES.ATTACK_MELEE_RANGE, TUNING.SK_ZOMBIES.HIT_RANGE)
	end
	inst:DoTaskInTime(TUNING.SK_ZOMBIES.ALTATTACK_COOLDOWN, function(inst)
		inst.altattack = true
		inst.components.combat:SetRange(TUNING.SK_ZOMBIES.ATTACK_RANGE, TUNING.SK_ZOMBIES.HIT_RANGE)
	end)
end

local function DisableBreath(inst)
	inst.can_breath = false
	if not inst.altattack then
		inst.components.combat:SetRange(TUNING.SK_ZOMBIES.ATTACK_MELEE_RANGE, TUNING.SK_ZOMBIES.HIT_RANGE)
	end
	inst:DoTaskInTime(TUNING.SK_ZOMBIES.BREATH_COOLDOWN, function(inst)
		inst.can_breath = true
		inst.components.combat:SetRange(TUNING.SK_ZOMBIES.ATTACK_RANGE, TUNING.SK_ZOMBIES.HIT_RANGE)
	end)
end

local states=
{
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },
	
	State{
		name = "spawn",
        tags = {"busy", "nointerrupt"},

        onenter = function(inst)
            inst.Physics:Stop()
			inst.AnimState:PlayAnimation("spawn")
			inst.DynamicShadow:Enable(false)
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end	
        end,
		
		onexit = function(inst)
			inst.components.health:SetInvincible(false) --failsafe
			inst.DynamicShadow:Enable(true)
			if inst.glow then
				inst.Light:Enable(true)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("back")
			end
		end,
	
		timeline=
		{
			TimeEvent(3*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) inst.components.health:SetInvincible(true) end),
			TimeEvent(27*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(33*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt)
				if inst.glow then
					inst.Light:Enable(true)
				end
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("back")
				end
			end),
			TimeEvent(48*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(66*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.grunt) end),
			TimeEvent(72*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) inst.DynamicShadow:Enable(true) end),
			
		},

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
			inst.sg.mem.last_hit_time = GetTime()
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.DynamicShadow:Enable(false)
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end
			if inst.fuse then
				inst.fuse:Remove()
			end
            inst.AnimState:PlayAnimation("death_old")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
			if TheNet:GetServerGameMode() ~= "lavaarena" then
				inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			end
        end,

        events=
        {
            EventHandler("animqueueover", function(inst) inst:Remove() end),
        },     

    },    
	
	State{
        name = "fake_death", --I could use the corpse state. But I'm shaky about using forge states outside of it, plus the NPC will need this state anyways.
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.DynamicShadow:Enable(false)
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end
			if inst.fuse then
				inst.fuse:Remove()
			end
            inst.AnimState:PlayAnimation("death", false)
            inst.Physics:Stop()
			
			inst.sg:SetTimeout(TUNING.SK_ZOMBIES.REVIVE_WAIT_TIME)
        end,

        ontimeout = function(inst) --didn't revive, give up.
			SpawnPrefab("collapse_small").Transform:SetPosition(inst:GetPosition():Get())
            inst:Remove()
        end,        
    },
	
	State{
        name = "attack", 
        tags = {"attack", "busy"},
		
	onenter = function(inst, target)
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound(inst.sounds.grunt)
        inst.AnimState:PlayAnimation("attack_pre")
		inst.AnimState:PushAnimation("attack", false)
	end,
	
	onexit = function(inst)

    end,
	
    timeline=
    {

			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack) end),
            TimeEvent(21*FRAMES, function(inst) inst.components.combat:DoAttack() end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack2", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		inst.altattack = false
		if inst.brain then
			inst.brain:Stop()
		end
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
                inst.sg.statemem.attacktarget = target
            end
        end
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound(inst.sounds.grunt)
        inst.AnimState:PlayAnimation("attack2_pre")
		inst.AnimState:PushAnimation("attack2", false)
	end,
	
	onexit = function(inst)
		if inst.brain then
			inst.brain:Start()
		end
		inst.sg.statemem.jump = nil
		inst.sg.statemem.attacktarget = nil
		inst.sg.mem.speed = nil
		DisableAttack2(inst)
    end,
	
	onupdate = function(inst)
        if inst.sg.statemem.jump then
            inst.Physics:SetMotorVel(inst.sg.mem.speed and inst.sg.mem.speed * 1.2 or 5, 0, 0)
        end
    end,
	
    timeline=
    {
		TimeEvent(18*FRAMES, function(inst) 
			inst.sg.statemem.jump = true
			SetJumpPhysics(inst)
			inst.sg:AddStateTag("nointerrupt")
			if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid() then
				inst.sg.mem.speed = CalcJumpSpeed(inst, inst.sg.statemem.attacktarget)
			end
		end),

		TimeEvent(18*FRAMES, function(inst) 
			inst.SoundEmitter:PlaySound(inst.sounds.attack2)
		end),
        TimeEvent(36*FRAMES, function(inst) 
			inst.components.combat:DoAttack() 
			inst.sg.statemem.jump = false 
			inst.Physics:Stop()
			SetNormalPhysics(inst)
		end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack3", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
			inst.components.combat:StartAttack()
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.SoundEmitter:PlaySound(inst.sounds.attack3_pre)
			inst.AnimState:PlayAnimation("breath_pre")
			inst.AnimState:PushAnimation("breath_loop", true)
			inst.sg:SetTimeout(TUNING.SK_ZOMBIES.BREATH_TIME)
			inst.can_breath = false
			--TODO make this more clean! Unneeded redudancy and looks just damn messy!
			inst.fx_task = inst:DoPeriodicTask(TUNING.SK_ZOMBIES.BREATH_FOG_TICK, function(inst)
				if inst.sg.statemem.breathing then
					local pos = inst:GetPosition()
					local angle = -inst.Transform:GetRotation() * DEGREES
					local offset = TUNING.SK_ZOMBIES.BREATH_FOG_OFFSET
					local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
					local fx = SpawnPrefab("sk_zombie_breath"..inst.variant.."_fx")
					fx.Transform:SetPosition(targetpos.x, TUNING.SK_ZOMBIES.BREATH_FOG_Y_OFFSET, targetpos.z)
					fx.Transform:SetRotation(inst.Transform:GetRotation())
				end
			end)
			inst.breathing_task = inst:DoPeriodicTask(TUNING.SK_ZOMBIES.BREATH_PARTICLE_TICK, function(inst)
				if inst.sg.statemem.breathing then
					local pos = inst:GetPosition()
					local angle = -inst.Transform:GetRotation() * DEGREES
					local offset = TUNING.SK_ZOMBIES.BREATH_AOE_OFFSET
					local targetpos = {x = pos.x + (offset * math.cos(angle)), y = 0, z = pos.z + (offset * math.sin(angle))} 
					local ents = TheSim:FindEntities(targetpos.x,0,targetpos.z, TUNING.SK_ZOMBIES.BREATH_AOE_RANGE, nil, GetExcludeTags(inst))
					local targets = {}
					
					if ents and #ents > 0 then
						for i, v in ipairs(ents) do
							if v:IsValid() and v.components.health and not v.components.health:IsDead() and v.components.combat and v ~= inst then
								if inst.variant == "_freeze" and v.components.freezable then
									v.components.freezable:Freeze(4)
								else
									PlayablePets.InflictStatus(inst, v, inst.variant == "" and "stun" or string.gsub(inst.variant, "_", ""), 1)
								end
							end
						end
					end	
				end
			end)
		end,

		onupdate = function(inst)
			if inst.sg.statemem.breathing and math.random() < TUNING.SK_ZOMBIES.BREATH_PARTICLE_CHANCE then
				local pos = inst:GetPosition()
				local angledif = TUNING.SK_ZOMBIES.BREATH_PARTICLE_ANGLEDIF
				local current_angle = -inst.Transform:GetRotation() * DEGREES
				local offset = TUNING.SK_ZOMBIES.BREATH_PARTICLE_OFFSET
				local offset_pos = Point(pos.x + (offset*math.cos(current_angle)), TUNING.SK_ZOMBIES.BREATH_PARTICLE_Y_OFFSET, pos.z + (offset*math.sin(current_angle)))
				local fx = SpawnPrefab("sk_zombie_breath"..inst.variant.."_particle_fx")
				fx.Transform:SetPosition(offset_pos:Get())
				fx.Transform:SetRotation(inst.Transform:GetRotation() + math.random(-angledif, angledif))
			end
		end,
		
		onexit = function(inst)
			inst.sg.statemem.breathing = nil
			if inst.breathing_task then
				inst.breathing_task:Cancel()
				inst.breathing_task = nil
			end
			if inst.fx_task then
				inst.fx_task:Cancel()
				inst.fx_task = nil
			end
			DisableBreath(inst)
		end,
		
		ontimeout = function(inst)
			inst.sg:GoToState("attack3_pst")
		end,
		
		timeline=
		{
			TimeEvent(60*FRAMES, function(inst) 
				inst.sg.statemem.breathing = true
				inst.SoundEmitter:PlaySound(inst.sounds.attack3)
			end),
		},

        events=
        {
            EventHandler("attacked", function(inst) inst.sg:GoToState("hit") end),
        },
	
    },
	
	State{
        name = "attack3_pst", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
			inst.components.combat:StartAttack()
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("breath_pst")
		end,
	
		onexit = function(inst)

		end,
		
		timeline=
		{

		},

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack4", --bombie explode
        tags = {"attack", "busy", "nointerrupt"},
		
		onenter = function(inst, target)
			inst.components.combat:StartAttack()
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.SoundEmitter:PlaySound(inst.sounds.attack)
			inst.AnimState:PlayAnimation("attack4")
		end,
	
		onexit = function(inst)
			if inst.glow then
				inst.Light:Enable(false)
			end
			if inst.fx then
				inst.fx.AnimState:PlayAnimation("deactive")
			end
			if inst.fuse then
				inst.fuse:Remove()
			end
		end,
	
		timeline =
		{

		},

        events=
        {
            EventHandler("animover", function(inst) 
				local explosion = SpawnPrefab("explode_small_slurtlehole")
				ExplosionShake(inst)
				explosion.Transform:SetPosition(inst:GetPosition():Get())
				explosion.Transform:SetScale(2.5, 2.5, 2.5)
				if inst.explosion_colour then
					explosion.AnimState:SetMultColour(inst.explosion_colour.r, inst.explosion_colour.g, inst.explosion_colour.b)
				end
				inst.SoundEmitter:PlaySound(inst.sounds.explode)
				inst.components.combat:DoAreaAttack(inst, inst.components.combat.hitrange, nil, nil, inst.variant == "_shock" and "electric" or "explosive",  GetExcludeTags(inst))
				inst.components.health:SetPercent(0)
			end),
        },
	
    },

	State {
        name = "despawn",
        tags = { "busy"}, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("despawn")
			inst.DynamicShadow:Enable(false)
			
        end,
		
		timeline = 
		{
			TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(16*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(35*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(47*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn) end),
			TimeEvent(62*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.spawn)
				if inst.glow then
					inst.Light:Enable(true)
				end
				if inst.fx then
					inst.fx.AnimState:PlayAnimation("back")
				end
				if inst:HasTag("bombie") then
					local fx = SpawnPrefab("torchfire_rag")
					fx.entity:SetParent(inst.entity)
					fx.entity:AddFollower()
					fx.Follower:FollowSymbol(inst.GUID, "swap_fuse", 0, 0, 0)
					inst.fuse = fx
				end
			end),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst:Remove() end ),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			--inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			TimeEvent(12*FRAMES, function(inst) inst.components.locomotor:WalkForward() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			if math.random(1,10) <= 1.5 then
				inst.SoundEmitter:PlaySound(inst.sounds.grunt)
			end
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep),
			TimeEvent(23*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
	
	--
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:StopMoving()
            inst.AnimState:PlayAnimation("run_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(12*FRAMES, function(inst) inst.components.locomotor:WalkForward() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			if math.random(1,10) <= 1 then
				inst.SoundEmitter:PlaySound(inst.sounds.grunt)
			end
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep),
			TimeEvent(7*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")
			
            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
	
}




CommonStates.AddFrozenStates(states)

PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death_old")
    
return StateGraph("sk_zombie", states, events, "idle", actionhandlers)

