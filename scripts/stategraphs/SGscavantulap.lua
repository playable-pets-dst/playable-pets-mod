require("stategraphs/commonstates")
require("stategraphs/ppstates")
local tuning_values = TUNING.PP.SCAVANTULAP

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "groundpoundimmune"}
	else	
		return {"player", "companion", "INLIMBO", "playerghost", "notarget", "shadow", "groundpoundimmune"}
	end
end

local function ShakeIfClose(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .15, .05, .75, inst, 15)
end

local function CalcJumpSpeed(inst, target)
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (inst:GetPhysicsRadius(0) + target:GetPhysicsRadius(-1))
        if dist > 0 then
            return math.min(8, dist) / (10 * FRAMES)
        end
    end
    return 0
end

local longaction = "taunt"
local shortaction = "action"
local workaction = "attack"
local otheraction = "eat"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
	--ActionHandler(ACTIONS.CAST_NET, longaction),
	ActionHandler(ACTIONS.ATTACK, function(inst)
		local buffaction = inst:GetBufferedAction()
		local target = buffaction ~= nil and buffaction.target or nil
		if target and target:IsValid() and not inst:IsNear(target, tuning_values.HIT_RANGE) then
			return "attack2"
		else
			return "attack"
		end	
	end),
    ActionHandler(ACTIONS.PP_DESTROY, "attack")
}

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function SpawnMoveFx(inst)
    if inst.noactions then
        SpawnPrefab("mole_move_fx").Transform:SetPosition(inst.Transform:GetWorldPosition())
    end
end

local function GrabHound(inst, hound)
    if hound and hound:HasTag("hound") and not (hound:HasTag("epic") or hound:HasTag("largecreature")) and hound:IsValid() and not hound.components.health:IsDead() then
        inst._isgrabbing = true
        if hound:HasTag("player") then
            inst._grabbed_player = hound
            inst._grabbed_build = hound.AnimState:GetBuild()
            inst.AnimState:AddOverrideBuild(inst._grabbed_build)
            hound.sg:GoToState("pp_empty")
            inst.sg:GoToState("grab_attack_pre")
            --hide player here. Need an empty state?
        else
            inst._grabbed_prefab = hound.prefab
            inst._grabbed_build = hound.AnimState:GetBuild()
            inst.AnimState:AddOverrideBuild(inst._grabbed_build)
            hound:Remove()
            inst.sg:GoToState("grab_attack_pre")
        end
    end
end

local function ShakeIfClose(inst)
	for i, v in ipairs(AllPlayers) do
		v:ShakeCamera(CAMERASHAKE.FULL, .35, .02, 1.25, inst, 40)
	end
end

local function ThrowHound(inst)
    inst._isgrabbing = nil
    local posx, posy, posz = inst.Transform:GetWorldPosition()
    local ents = TheSim:FindEntities(posx,0,posz, 13, {"hound"}, {"notarget", "INLIMBO", "shadow", "playerghost"})
    local proj = SpawnPrefab("scavantula_projectilep")
    proj.owner = inst
    proj.Transform:SetPosition(posx, posy, posz)
    proj._thrown_player = (inst._grabbed_player and inst._grabbed_player:IsValid()) and inst._grabbed_player or nil
    proj.hound_prefab = inst._grabbed_prefab 
    proj.AnimState:SetBuild(inst._grabbed_build)
    local mr = 13
    local target_pos = Point(posx + math.random(-mr, mr), 0, posz + math.random(-mr, mr))
    if #ents > 0 then
        target_pos = ents[math.random(1, #ents)]:GetPosition()
    end
    proj.components.complexprojectile:Launch(target_pos, inst, inst)
end

local function SetHiddenState(inst, var)
    if var then
        inst.MiniMapEntity:SetIcon("")
	    inst:AddTag("noplayerindicator")
        inst:Hide()
        inst.noactions = true
        inst.taunt2_exit = "spawn"
        inst:AddTag("notarget")
        inst.components.health:SetInvincible(true)
        inst.DynamicShadow:Enable(false)
        inst.components.locomotor:SetExternalSpeedMultiplier(inst, "dig_speed", tuning_values.DIG_SPEED_MULT)
    else
        inst:Show()
        inst.MiniMapEntity:SetIcon(inst.prefab..".tex")
	    inst:RemoveTag("noplayerindicator")
        inst.noactions = nil
        inst.taunt2_exit = nil
        inst:RemoveTag("notarget")
        inst.components.health:SetInvincible(false)
        inst.taunt2 = false
        inst:DoTaskInTime(10, function(inst) inst.taunt2 = true end)
        inst.components.locomotor:RemoveExternalSpeedMultiplier(inst, "dig_speed")
    end    
end

local events=
{
	EventHandler("attacked", function(inst, data) 
		if data.stimuli and data.stimuli == "strong" and not (inst.sg:HasStateTag("transform") or inst.sg:HasStateTag("nointerrupt")) and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit")
		elseif data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") and not inst.sg:HasStateTag("nointerrupt") then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OpenGift(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.AddCommonHandlers(),
	--PP_CommonHandlers.OnLocomoteAdvanced(), 
	PP_CommonHandlers.OnDeath(),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}

local function SoundPath(inst, event)
    local   creature = "cavespider"
    return "dontstarve/creatures/" .. creature .. "/" .. event
end

 local states=
{
	
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            if inst._isgrabbing then
                ThrowHound(inst)
            end
            inst.SoundEmitter:PlaySound(SoundPath(inst, "die"))
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
			inst.components.inventory:DropEverything(true)
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
                    PlayablePets.DoDeath(inst)
                end
            end),
        },
    },

    State{
        name = "spawn",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/emerge")
            ShakeIfClose(inst)
            inst.Physics:Stop()
            inst.DynamicShadow:Enable(false)
            SetHiddenState(inst)
            inst.AnimState:PlayAnimation("dig_pst")
        end,

        timeline = {
            TimeEvent(16*FRAMES, function(inst) 
                inst.DynamicShadow:Enable(true)
            end),
        },

        onexit = function(inst)
            inst.DynamicShadow:Enable(true)
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    }, 
	
	State{
        name = "special_atk1",
        tags = {"busy"},
        
        onenter = function(inst)
            if inst.noactions then
                inst.sg:GoToState("idle")
            else
                inst.Physics:Stop()
                ShakeIfClose(inst)
                inst.AnimState:PlayAnimation("taunt")
                inst.SoundEmitter:PlaySound(SoundPath(inst, "scream"))
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    }, 
    
	State{
        name = "special_atk2", 
        tags = {"busy", "idle"},
        
        onenter = function(inst)
            inst.DynamicShadow:Enable(false)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("dig_pre")
        end,

        onexit = function(inst)
            SetHiddenState(inst, true)
        end,

        timeline = {
            TimeEvent(7*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") ShakeIfClose(inst) end),
            TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") end),
            TimeEvent(17*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") end),
            TimeEvent(21*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/retract") end),
        },
        
        events=
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },		
    }, 

    State{
        name = "grab_attack_pre",
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("hound_attack_pre")		
        end,

        onexit = function(inst)

        end,

        events=
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("grab_attack_loop")
            end),
        },		
    },   

    State{
        name = "grab_attack_loop",
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("hound_attack_loop", true)		
            inst.sg:SetTimeout(1.5)
        end,

        onexit = function(inst)

        end,

        ontimeout = function(inst)
            inst.sg:GoToState("grab_attack_pst")
        end,

        events=
        {
            
        },		
    },       

    State{
        name = "grab_attack_pst",
        tags = {"busy", "nointerrupt"},
        
        onenter = function(inst)
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("hound_attack_pst")		
        end,

        onexit = function(inst)
            inst._grabbed_build = nil
            inst._grabbed_player = nil
            inst._grabbed_prefab = nil
        end,

        timeline=
        {
            TimeEvent(5*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "attack_grunt")) 
                ThrowHound(inst)
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst)
                inst.sg:GoToState("idle")
            end),
        },		
    },  
    
    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        
        onenter = function(inst, start_anim)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("slow_idle", true)
        end,
    },
    
    State{
        name = "eat",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat")
        end,
        
        events=
        {
            EventHandler("animover", function(inst)
                if inst:PerformBufferedAction() then
                    inst.sg:GoToState("eat_loop")
                else
                    inst.sg:GoToState("idle")
                end
            end),
        },
    },  
    
    
	State{
        name = "born",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("taunt")
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },      
    
    State{
        name = "eat_loop",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_loop", false)
            inst.AnimState:PushAnimation("eat_pst", false)            
        end,
        
        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },       
    },  

    State{
        name = "taunt",
        tags = {"busy"},
        
        onenter = function(inst)
            if inst.noactions then
                inst.sg:GoToState("idle")
            else
                inst.Physics:Stop()
                ShakeIfClose(inst)
                inst.AnimState:PlayAnimation("taunt")
                inst.SoundEmitter:PlaySound(SoundPath(inst, "scream"))
                inst:PerformBufferedAction()
            end
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },    
    
    State{
        name = "investigate",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
            inst.SoundEmitter:PlaySound(SoundPath(inst, "scream"))
        end,
        
        events=
        {
            EventHandler("animover", function(inst)
                inst:PerformBufferedAction()
                inst.sg:GoToState("idle")
            end),
        },
    },    
    
    State{
        name = "attack",
        tags = {"attack", "busy"},
        
        onenter = function(inst)
			if inst.noactions then
                inst.sg:GoToState("idle")
            else
                inst.components.combat:StartAttack()
                inst.Physics:Stop()
                inst.AnimState:PlayAnimation("atk")
            end
        end,
        
        timeline=
        {
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "Attack")) end),
            TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "attack_grunt") ) end),
            TimeEvent(25*FRAMES, function(inst) 
                if inst.bufferedaction and inst.bufferedaction.action == ACTIONS.PP_DESTROY then
                    inst.bufferedaction.action = ACTIONS.HAMMER
                end
                PlayablePets.DoWork(inst, 3) 
                inst.sg:RemoveStateTag("attack") 
            end),
        },
        
        events=
        {
            EventHandler("animover", function(inst)
                inst.sg:GoToState("idle")
            end),			
        },
    },

    State{
        name = "attack2",
        tags = {"attack", "canrotate", "busy", "jumping"},
        
        
		onenter = function(inst, target)
            if inst.noactions then
                inst.sg:GoToState("idle")
            else
                local buffaction = inst:GetBufferedAction()
                local target = buffaction ~= nil and buffaction.target or nil
                inst.components.combat:SetTarget(target)
                inst.components.locomotor:Stop()
                inst.components.locomotor:EnableGroundSpeedMultiplier(false)

                inst.components.combat:StartAttack()
                inst.AnimState:PlayAnimation("warrior_atk")
                inst.sg.statemem.target = target
            end
        end,

        onexit = function(inst)
            inst.components.locomotor:Stop()
            inst.components.locomotor:EnableGroundSpeedMultiplier(true)
        end,
        
        timeline =
        {
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "attack_grunt")) end),
            TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "Jump")) end),
            TimeEvent(8*FRAMES, function(inst) inst.Physics:SetMotorVelOverride(15,0,0) end),
            TimeEvent(9*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "Attack")) end),
            TimeEvent(19*FRAMES, function(inst) inst.components.combat:DoAttack(inst.sg.statemem.target)
                GrabHound(inst, inst.sg.statemem.target)
            end),
            TimeEvent(20*FRAMES,
				function(inst)
                    inst.Physics:ClearMotorVelOverride()
					inst.components.locomotor:Stop()
				end),
        },
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
        name = "spitter_attack",
        tags = {"attack", "canrotate", "busy", "spitting"},

        onenter = function(inst, target)
            if inst.components.locomotor then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("spit")
        end,

        onexit = function(inst)
		
        end,

        timeline =
        {
            TimeEvent(7*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound(SoundPath(inst, "attack_grunt")) 

			end),

            TimeEvent(21*FRAMES, function(inst) inst:PerformBufferedAction()
            --inst.SoundEmitter:PlaySound(SoundPath(inst, "spit_voice"))
            end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("taunt") end),
        },
    },

    State{
        name = "hit",
        
        onenter = function(inst)
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },    
    
    State{
        name = "hit_stunlock",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(SoundPath(inst, "hit_response"))
            inst.AnimState:PlayAnimation("hit")
            inst.Physics:Stop()            
        end,
        
        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },  
	
	------------------SLEEPING-----------------
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,
		
		timeline = 
		{
		    TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "fallAsleep")) end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping" },
		
		onenter = function(inst)
			inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("sleep_loop")
		end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline =
		{
			TimeEvent(35*FRAMES, function(inst) 
                inst.SoundEmitter:PlaySound(SoundPath(inst, "sleeping")) 
                PlayablePets.SleepHeal(inst)
            end ),
		},

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end

            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline = 
		{
		TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "wakeUp")) end ),
		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },    

    State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("slow_walk_loop")
            if not inst.SoundEmitter:PlayingSound("move") and inst.noactions then
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/move", "move")
            end
        end,
		
		timeline = {
		    TimeEvent(2*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(6*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(10*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(15*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(0*FRAMES,  SpawnMoveFx),
            TimeEvent(3*FRAMES,  SpawnMoveFx),
            TimeEvent(6*FRAMES,  SpawnMoveFx),
            TimeEvent(9*FRAMES, SpawnMoveFx),
            TimeEvent(12*FRAMES, SpawnMoveFx),
            TimeEvent(15*FRAMES, SpawnMoveFx),
            TimeEvent(18*FRAMES, SpawnMoveFx),
            TimeEvent(21*FRAMES, SpawnMoveFx),
            TimeEvent(24*FRAMES, SpawnMoveFx),
            TimeEvent(27*FRAMES, SpawnMoveFx),
		},
		
        events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
		},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
            inst.SoundEmitter:KillSound("move")
        end,

        timeline = {
		    
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	

    State
    {
        name = "run_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("walk_loop")
            if not inst.SoundEmitter:PlayingSound("move") and inst.noactions then
                inst.SoundEmitter:PlaySound("dontstarve_DLC001/creatures/mole/move", "move")
            end
        end,
		
		timeline = {
            TimeEvent(0*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(3*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(7*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(12*FRAMES, function(inst) if not inst.noactions then inst.SoundEmitter:PlaySound(SoundPath(inst, "walk_spider")) end end),
            TimeEvent(0*FRAMES,  SpawnMoveFx),
            TimeEvent(3*FRAMES,  SpawnMoveFx),
            TimeEvent(6*FRAMES,  SpawnMoveFx),
            TimeEvent(9*FRAMES, SpawnMoveFx),
            TimeEvent(12*FRAMES, SpawnMoveFx),
            TimeEvent(15*FRAMES, SpawnMoveFx),
            TimeEvent(18*FRAMES, SpawnMoveFx),
            TimeEvent(21*FRAMES, SpawnMoveFx),
            TimeEvent(24*FRAMES, SpawnMoveFx),
		},
		
        events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
		},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
            inst.SoundEmitter:KillSound("move")
        end,

        timeline = {
		    
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

CommonStates.AddFrozenStates(states)
PP_CommonStates.AddHomeState(states, nil, "eat", "taunt", true)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)
PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "taunt", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, true, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "die")) end),
		},
		
		corpse_taunt =
		{
			TimeEvent(0*FRAMES, function(inst) inst.SoundEmitter:PlaySound(SoundPath(inst, "scream")) end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{
		--corpse = "dontstarve_DLC003/creatures/enemy/grabbing_vine/death"
	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "taunt")
CommonStates.AddHopStates(states, false, {pre = "walk_pre", loop = "walk_loop", pst = "walk_pst"}, nil, "death")
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = "cower",
	plank_idle_loop = "cower_loop",
	plank_idle_pst = "walk_pst",
	
	plank_hop_pre = "walk_pre",
	plank_hop = "walk_loop",
	
	steer_pre = "walk_pst",
	steer_idle = "idle",
	steer_turning = "taunt",
	stop_steering = "walk_pst",
	
	row = "walk_pst",
}
)
PP_CommonStates.AddAoeStates(states, nil, 
{
	generic = "walk_pst",
	
	leap_pre = "walk_pre",
	leap_loop = "walk_loop",
	leap_pst = "walk_pst",
	
	castspelltime = 10,
})
    
return StateGraph("spiderp", states, events, "taunt", actionhandlers)

