require("stategraphs/commonstates")

local actionhandlers = 
{		
	
}

local events=
{
    EventHandler("attacked", function(inst) if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then inst.sg:GoToState("hit") end end),
    CommonHandlers.OnDeath(),
    EventHandler("doattack", function(inst, data) 
		if not inst.sg:HasStateTag("busy") then 
			inst.sg:GoToState("attack", data.target) 
		end 
	end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnLocomote(true,false),
    CommonHandlers.OnFreeze(),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle", true)
        end,

    },

    State{
        name = "attack",
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {

			TimeEvent(12*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.attack)
                inst.components.combat:DoAttack(inst.components.combat.target)
		    end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.SoundEmitter:PlaySound(inst.sounds.hit)
			--inst.SoundEmitter:PlaySound("dontstarve/pig/grunt")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/pig/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			--inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			--inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then

                end
            end),
        },

    },
	
	State
    {
        name = "run_start",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("walk_pre")			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "running", "canrotate" },

        onenter = function(inst)
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk")
        end,
		
		timeline = {
		    TimeEvent(23*FRAMES, PlayFootstep),
            TimeEvent(23*FRAMES, function(inst) inst.components.locomotor:RunForward() end)
		},
		
        events=
		{
			EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
		},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}

--CommonStates.AddFrozenStates(states)



    
return StateGraph("lc_censoredbaby_npc", states, events, "idle", actionhandlers)

