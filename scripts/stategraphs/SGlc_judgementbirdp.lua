require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "action"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 

}

local tuning_values = TUNING.PP.LC_JUDGEMENTBIRDP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function GetExcludeTagsp(inst)
	if TheNet:GetPVPEnabled() then
		return {"notarget", "INLIMBO", "shadow", "battlestandard", "lobcorp_mob"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "lobcorp_mob"}
	else	
		return {"player", "companion", "INLIMBO", "notarget", "shadow", "lobcorp_mob"}
	end
end

local function DoAttack(inst, target)
    if target:IsValid() and target.components.health and not target.components.health:IsDead() and target.components.combat then
        target.components.combat:GetAttacked(inst, 1, nil, "strong")
        if target.components.health.currenthealth <= target.components.health.maxhealth*tuning_values.DAMAGE_PERCENT and target:HasTag("player") then
            --should be killing blow. Hacky way to do this... seems like dodelta doesn't run killed event on jb, this WILL cause issues with mobs that uses multiple skeletons.
            local pl = target
            local old_skeleton = pl.skeleton_prefab
		    pl.skeleton_prefab = "pp_skeleton_lynch"
		    pl:ListenForEvent("respawnfromghost", function(pl) 
			     pl.skeleton_prefab = old_skeleton
		    end)
        end
        target.components.health:DoDelta(-target.components.health.maxhealth*tuning_values.DAMAGE_PERCENT, false, nil, nil, inst, true)
    end
end

local function DoAoeAttack(inst)
    local pos = inst:GetPosition()
    local ents = TheSim:FindEntities(pos.x, 0, pos.z, tuning_values.HIT_RANGE, {"_combat"}, GetExcludeTagsp(inst))
    if #ents > 0 then
        for i, v in ipairs(ents) do
            if v ~= inst then
                DoAttack(inst, v)
            end
        end
    end
end

local function DoStep(inst)
    inst.SoundEmitter:PlaySound(inst.sounds.step)
    ShakeAllCameras(CAMERASHAKE.FULL, .5, .02, .2, inst, 30)
end

local attack_timeline=
{

        TimeEvent(33*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end),
        TimeEvent(74*FRAMES, function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.down)
        end),
        TimeEvent(118*FRAMES, function(inst) 
            inst.SoundEmitter:PlaySound(inst.sounds.stun)
            if inst.sg:HasStateTag("aoe_attack") then
                DoAoeAttack(inst)
            else
                if inst.sg.statemem.attacktarget and inst.sg.statemem.attacktarget:IsValid() and inst:IsNear(inst.sg.statemem.attacktarget, tuning_values.HIT_RANGE) then
                    DoAttack(inst, inst.sg.statemem.attacktarget)
                end
            end
        end),
}

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "attack", --single target damage
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            local buffaction = inst:GetBufferedAction()
            local target = buffaction ~= nil and buffaction.target or nil
            if target ~= nil then
                if target:IsValid() then
                    inst:FacePoint(target:GetPosition())
                    inst.sg.statemem.attacktarget = target
                end
            end
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
            inst.taunt2 = false --the taunt is tied to the same cooldown.
	    end,
	
        onexit = function(inst)
            inst.taunt2 = false
            if inst.taunt2_task then
                inst.taunt2_task:Cancel()
                inst.taunt2_task = nil
            end
            inst.taunt2_task = inst:DoTaskInTime(tuning_values.ATTACK_PERIOD, function(inst) inst.taunt2 = true end)
        end,
	
        timeline=attack_timeline,

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "special_atk2", --heavy attack
        tags = {"attack", "busy", "aoe_attack"},
		
		onenter = function(inst, target)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()

            inst.AnimState:PlayAnimation("attack")
            inst.taunt2 = false            
    	end,
	
        timeline=attack_timeline,

        onexit = function(inst)
            inst.taunt2 = false
            if inst.taunt2_task then
                inst.taunt2_task:Cancel()
                inst.taunt2_task = nil
            end
            inst.taunt2_task = inst:DoTaskInTime(tuning_values.ATTACK_PERIOD, function(inst) inst.taunt2 = true end)
        end,       

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle")  end),
        },
	
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	
	State{
		name = "special_atk1",
        tags = {"taunting"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("taunt")
        end,

		timeline=
        {

        },

        onexit = function(inst)
        
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")
            inst.AnimState:PlayAnimation("death")
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
        end,

        events =
        {
            EventHandler("animover", function(inst)
                if inst.AnimState:AnimDone() then
					PlayablePets.DoDeath(inst)
                end
            end),
        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				--inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep")
				PlayablePets.SleepHeal(inst)
				inst.AnimState:PlayAnimation("sleep_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("sleep_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
		    TimeEvent(0*FRAMES, PlayFootstep),
            TimeEvent(33*FRAMES, PlayFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")
			
            
        end,

        timeline = {
		    TimeEvent(7*FRAMES, PlayFootstep),
		},

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },	
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "taunt", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "death",
		corpse_taunt = "taunt"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "walk"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove.."_pre", loop = simplemove.."_loop", pst = simplemove.."_pst"}, nil, "death")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove.."_pre",
	plank_hop = simplemove.."_loop",
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)




    
return StateGraph("lc_bigbirdp", states, events, "idle", actionhandlers)

