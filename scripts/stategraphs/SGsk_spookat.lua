require("stategraphs/commonstates")
require("stategraphs/ppstates")

local MAX_SPEED = 10 -- TODO adjust?
local MAX_DASH_TIME = 0.6 -- seconds?
local ACCELERATION = MAX_SPEED/12 -- TODO how was this determined?

local function GetExcludeTags(inst)
	if TheNet:GetPVPEnabled() then
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	elseif TheNet:GetServerGameMode() == "lavaarena" then
		return {"player", "companion", "playerghost", "INLIMBO", "notarget", "shadow","kat", "undead"}
	else	
		return {"INLIMBO", "playerghost", "notarget", "shadow", "kat", "undead"}
	end
end

local t_values = TUNING.SK_SPOOKATS

local actionhandlers = {}

local function CalcChompSpeed(inst, target)
	--local target = inst.components.combat.target
    local x, y, z = target.Transform:GetWorldPosition()
    local distsq = inst:GetDistanceSqToPoint(x, y, z)
    if distsq > 0 then
        inst:ForceFacePoint(x, y, z)
        local dist = math.sqrt(distsq) - (target.Physics ~= nil and inst.Physics:GetRadius() + target.Physics:GetRadius() or inst.Physics:GetRadius())
        if dist > 0 then
            return inst:HasTag("blackkat") and math.min(8, dist) / (28 * FRAMES) or math.min(8, dist) / (17 * FRAMES)
        end
    end
    return 0
end

local function RemoveMinion(minion)
	if minion.owner and minion.owner.minions and minion.owner.minions[minion] then
		--print("Minion removed, total minions left: "..tostring(#minion.owner.minions))
		minion.owner.minions[minion] = nil
	end
end

local function SpawnMinions(inst)
	local num_minions = 2
	local pos = inst:GetPosition()
	for i = 1, num_minions do
		local position = {x = pos.x + math.random(-5, 5), y = 0, z = pos.z + math.random(-5, 5)}
		if inst.minions and inst.components.leader:CountFollowers() < t_values.MAX_MINIONS and not IsOceanTile(TheWorld.Map:GetTileAtPoint(position.x, position.y, position.z)) then
			local minion = SpawnPrefab(inst.minionoverride or "sk_zombie_curse")
			inst.components.leader:AddFollower(minion)
			minion.Transform:SetPosition(position.x, position.y, position.z)
			minion.owner = inst
			minion.sg:GoToState("spawn")
			minion.components.lootdropper:SetLoot({})
			inst.minions[minion] = minion
			minion:ListenForEvent("onremove", RemoveMinion)
			minion:ListenForEvent("death", RemoveMinion)
		end		
	end	
end

local function ExplosionShake(inst)
    ShakeAllCameras(CAMERASHAKE.VERTICAL, .5, .03, 1, inst, 20)
end

local events=
{
	PP_CommonHandlers.AddCommonHandlers(),
    EventHandler("attacked", function(inst) 
		if inst:HasTag("mewkat") then
			return
		end
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
            if not inst.sg:HasStateTag("attack") and not inst.isblocked and (inst.sg.mem.last_hit_time or 0) + inst.hit_recovery*2 < GetTime() and not inst.sg:HasStateTag("nointerrupt") then -- don't interrupt attack or exit shield
                inst.sg:GoToState("hit") -- can still attack
            end
			inst.isblocked = nil
        end 
    end),
    EventHandler("doattack", function(inst, data) 
		if inst:HasTag("mewkat") then
			return
		end
        if not inst.components.health:IsDead() and not inst.sg:HasStateTag("busy") then 
			if inst.can_summon and inst.components.leader:CountFollowers() < t_values.MAX_MINIONS then
				inst.sg:GoToState("summon")
			else
				inst.sg:GoToState("attack", data.target) 
			end
        end 
    end),
    PP_CommonHandlers.OnDeath(),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	PP_CommonHandlers.OnKnockback(),
	PP_CommonHandlers.OnLocomoteAdvanced(),
}

if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	table.insert(events, PP_CommonHandlers.OnSink())
	table.insert(events, CommonHandlers.OnHop())
end

local function GetAnimation(inst, str)
	return inst.isangry and str.."_angry" or str
end

local states=
{
    
	State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation(GetAnimation(inst, "idle_loop"), true)
        end,
		
		events =
		{
			EventHandler("animover", function(inst) 
				if inst.isangry then
					inst.sg:GoToState("idle")
				else
					inst.sg:GoToState(math.random() < (0.2*(inst:HasTag("mewkat") and 2 or 1)) and "idle_taunt" or "idle")
				end
			end)
		}
    },
	
	State{
        name = "idle_taunt",
        tags = {"taunt"},

        onenter = function(inst)
            inst.AnimState:PlayAnimation("idle_taunt")
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			
			inst.SetAngry(inst, false)
        end,

        timeline=
        {
			TimeEvent(4*FRAMES, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.taunt) end)
        },

        events=
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit")
			inst.sg.mem.last_hit_time = GetTime()
			inst.SetAngry(inst, true)
			--inst.SoundEmitter:PlaySound(inst.sounds.hit)
        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
    
    State{
        name = "death",
        tags = {"busy"},
        
        onenter = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death)
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.DynamicShadow:Enable(false)
            inst.AnimState:PlayAnimation("death")
            RemovePhysicsColliders(inst)            
            inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))   
         
			if inst.components.playercontroller ~= nil then
               inst.components.playercontroller:RemotePausePrediction()
            end
        end,
    },    
	
	State{
        name = "attack", --dash
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
			if inst.brain then
				inst.brain:Stop()
			end
			if target and target:IsValid() then
				inst.current_target = target
				inst:FacePoint(target:GetPosition())
				inst.components.combat:SetTarget(target)
				if inst.SetMinionTarget then
					inst.SetMinionTarget(inst, target)
				end
			end
			inst.components.combat:StartAttack()

			inst.sg.statemem.dash_state = "accelerating"
			inst.sg.statemem.current_speed = 0
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
				
			inst.SetAngry(inst, true)
				
			local target_pos = target:GetPosition()
			inst:ForceFacePoint(target_pos)
			inst.Physics:SetMotorVel(inst.sg.statemem.current_speed, 0, 0)
				
			inst.sg.statemem.target_pos = target_pos
			inst.sg.statemem.dash_start_time = GetTime()

			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pre"), false)
			inst.AnimState:PushAnimation(GetAnimation(inst, "walk_loop"), true)
		end,
		
		onupdate = function(inst)
			--TODO put move tweening in here
            if not inst.sg.statemem.end_dash then
                if inst.sg.statemem.dash_state == "accelerating" then
                    inst.sg.statemem.current_speed = math.min(inst.sg.statemem.current_speed + ACCELERATION, MAX_SPEED  * (inst:HasTag("blackkat") and 1 or 1.2))
                    if inst.sg.statemem.current_speed >= MAX_SPEED * (inst:HasTag("blackkat") and 1 or 1.2) then
                        inst.sg.statemem.dash_state = "moving" -- TODO better name?
                    end
                elseif inst.sg.statemem.dash_state == "decelerating" then
                    inst.sg.statemem.current_speed = math.max(inst.sg.statemem.current_speed - ACCELERATION, 0)
                    if inst.sg.statemem.current_speed <= 0 then
                        inst.sg.statemem.dash_state = "stopped"
                    end
                elseif inst.sg.statemem.dash_state == "moving" and (GetTime() - inst.sg.statemem.dash_start_time > MAX_DASH_TIME) then
                    inst.sg.statemem.dash_state = "decelerating"
                elseif inst.sg.statemem.dash_state == "stopped" then
                    -- Stop spinning if reached max amount of rams or if sleep is queued
					if inst.current_target and inst.current_target:IsValid() and inst:IsNear(inst.current_target, inst:HasTag("blackkat") and 7 or 5) then
						inst.sg:GoToState("attack_bite", inst.current_target)
					elseif inst.current_target and inst.current_target:IsValid() then
						inst.sg:GoToState("attack_spit", inst.current_target)
					else
						inst.sg:GoToState("idle")
					end
                end
				if inst.sg.statemem.current_speed then
					inst.Physics:SetMotorVel(inst.sg.statemem.current_speed, 0, 0)
				end
            end
		end,
	
		onexit = function(inst)
			if inst.brain then
				inst.brain:Start()
			end
		end,
	
		timeline=
		{
		
		},

        events=
        {
            --dash state is timedout
        },
	
    },
	
	State{
        name = "attack_bite", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
			if inst.brain then
				inst.brain:Stop()
			end
			if target ~= nil then
				if target:IsValid() then
					inst:ForceFacePoint(target:GetPosition())
					inst.current_target = target
					inst.sg.statemem.speed = CalcChompSpeed(inst, target)
					inst.sg.statemem.jump = true
				end
			end
			inst.components.combat:StartAttack()
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.SoundEmitter:PlaySound(inst.sounds.grunt)
			inst.AnimState:PlayAnimation("attack_bite")
		end,
	
		onexit = function(inst)
			if inst.brain then
				inst.brain:Start()
			end
		end,
	
		onupdate = function(inst)
            if inst.sg.statemem.jump then
                inst.Physics:SetMotorVel(inst.sg.statemem.speed, 0, 0)
            end
        end,
	
		timeline=
		{
			TimeEvent(18*FRAMES, function(inst) 
				local fx = SpawnPrefab("sk_bite_fxp")
				local posx, posy, posz = 0, 0, 0
				local angle = -inst.Transform:GetRotation() * DEGREES
				local offset = 1
				local targetpos = {x = posx + offset, y = 1.5, z = posz} 
				fx.AnimState:SetMultColour(1, 26/255, 1, 1)
				fx.entity:SetParent(inst.entity)
				fx.Transform:SetPosition(targetpos.x, targetpos.y, targetpos.z)			
			end),
			TimeEvent(23*FRAMES, function(inst) 
				inst.components.combat:DoAttack()
				inst.SoundEmitter:PlaySound(inst.sounds.attack)
				inst.Physics:Stop()
				inst.sg.statemem.jump = nil
			end),
		},

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "attack_spit", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		if inst.brain then
			inst.brain:Start()
		end
		if target ~= nil then
            if target:IsValid() then
                inst:ForceFacePoint(target:GetPosition())
            end
        end
        inst.components.combat:StartAttack()
        inst.components.locomotor:Stop()
        inst.Physics:Stop()
		inst.SoundEmitter:PlaySound(inst.sounds.grunt)
		if inst:HasTag("blackkat") then
			inst.SoundEmitter:PlaySound(inst.sounds.spit)
		end
        inst.AnimState:PlayAnimation("attack_spit")
	end,
	
	onexit = function(inst)
		if inst.brain then
			inst.brain:Start()
		end
    end,
	
	onupdate = function(inst)
        
    end,
	
    timeline=
    {
		TimeEvent(18*FRAMES, function(inst) 
				
		end),
        TimeEvent(23*FRAMES, function(inst) 
			if inst.components.combat.target and inst.components.combat.target:IsValid() then
				inst:ForceFacePoint(inst.components.combat.target:GetPosition())
				inst:FireProjectile(inst.components.combat.target)
			end
			if not inst:HasTag("blackkat") then
				inst.SoundEmitter:PlaySound(inst.sounds.spit)
			end
			inst.Physics:Stop()
		end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "summon", 
        tags = {"attack", "busy"},
		
		onenter = function(inst, target)
		inst.components.locomotor:Stop()
		inst.Physics:Stop()
		inst.SoundEmitter:PlaySound(inst.sounds.grunt)
		inst.SoundEmitter:PlaySound(inst.sounds.spit)
		inst.AnimState:PlayAnimation("attack_spit")
	end,
	
	onexit = function(inst)
		inst.can_summon = false
		inst.queued_summon = nil
		if inst.summon_task then
			inst.summon_task:Cancel()
			inst.summon_task = nil
		end
		inst.summon_task = inst:DoTaskInTime(TUNING.SK_BLACKKAT.SUMMON_COOLDOWN, function(inst) inst.can_summon = true end)
    end,
	
	onupdate = function(inst)
        
    end,
	
    timeline=
    {
        TimeEvent(23*FRAMES, function(inst) 
			SpawnMinions(inst)
			inst.Physics:Stop()
		end),
    },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "idle_to_angry", 
        tags = {"busy"},
		
		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("idle_to_angry")
			inst.SetAngry(inst, true)
		end,
	
		onexit = function(inst)
			
		end,
		
		onupdate = function(inst)
			
		end,
		
		timeline=
		{

		},

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	State{
        name = "angry_to_idle", 
        tags = {"busy"},
		
		onenter = function(inst, target)
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
			inst.AnimState:PlayAnimation("angry_to_idle")
			inst.SetAngry(inst, false)
		end,
	
		onexit = function(inst)
			
		end,
		
		onupdate = function(inst)
			
		end,
		
		timeline=
		{

		},

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },
	
	------------------SLEEPING-----------------
	State {
        name = "sleep",
        tags = { "busy", "sleeping" }, 

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.components.locomotor:Stop()
			inst.Physics:Stop()
            inst.AnimState:PlayAnimation(GetAnimation(inst, "sleep_pre"))
        end,
		
		timeline = 
		{
			
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
		
		onexit = function(inst)

		end,
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },

        --onenter = onentersleeping,
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				inst.AnimState:PlayAnimation(GetAnimation(inst, "sleep_loop"))
			end,

        --timeline = timelines ~= nil and timelines.sleeptimeline or nil,
		timeline =
		{
			
		},
		
		onexit = function(inst)

		end,

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
			inst.AnimState:PlayAnimation(GetAnimation(inst, "sleep_pst"))
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,
		
		timeline = 
		{

		},
		
		events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
		
		onexit = function(inst)

		end,
    },
	
	State
    {
        name = "eat",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
            inst.AnimState:PlayAnimation("eat_pre", false)	
			inst.AnimState:PushAnimation("eat_loop", false)		
			inst.AnimState:PushAnimation("eat_loop", false)	
			inst.AnimState:PushAnimation("eat_pst", false)	
			inst:PerformBufferedAction()
			inst.Physics:Stop()
        end,
		
		onexit = function(inst)
			inst.SetAngry(inst, true)
		end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
            inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pre"))			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
			if inst:HasTag("blackkat") and math.random() < 0.1 then
				inst.SoundEmitter:PlaySound(inst.sounds.taunt)
			end
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_loop"))
        end,
		
		timeline = {
		
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pst"))            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },		
	
	State
    {
        name = "run_start",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
            inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pre"))			
        end,
		
		timeline = 
		{

		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate", "running" },

        onenter = function(inst)
			if inst.isangry then
				inst.components.locomotor:RunForward()
			else
				inst.components.locomotor:WalkForward()
			end
			if inst:HasTag("blackkat") and math.random() < 0.1 then
				inst.SoundEmitter:PlaySound(inst.sounds.taunt)
			end
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_loop"))
        end,
		
		timeline = {
		
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation(GetAnimation(inst, "walk_pst"))            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
}




CommonStates.AddFrozenStates(states)
PP_CommonStates.AddKnockbackState(states, nil, "hit") --(states, timeline, anim, sounds, fns)

    
return StateGraph("sk_spookat", states, events, "idle", actionhandlers)

