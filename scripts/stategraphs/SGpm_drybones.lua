require("stategraphs/commonstates")
require("stategraphs/ppstates")

local longaction = "action"
local shortaction = "action"
local workaction = "work"
local otheraction = "action"

local actionhandlers = PP_CommonStates.GetCommonActions(longaction, shortaction, workaction, otheraction)

local actionhandler_overrides = 
{ 
    ActionHandler(ACTIONS.ATTACK, "attack"),
}

local tuning_values = TUNING.PP.PM_DRYBONESP

if #actionhandler_overrides > 0 then
	for i, v in ipairs(actionhandler_overrides) do
		table.insert(actionhandlers, v)
	end
end

local function DoGenericFootstep(inst)
    PlayFootstep(inst)
end

local function DoTalkSound(inst)
    --[[
    inst.SoundEmitter:PlaySound(inst.sounds.talk, "talk")]]
    inst.talking_task = inst:DoPeriodicTask(0, function(inst)
        inst.SoundEmitter:PlaySound("pm/common/talk_oneshot")
    end)
end

local function StopTalkSound(inst)
    --inst.SoundEmitter:KillSound("talk")
    if inst.talking_task then
        inst.talking_task:Cancel()
        inst.talking_task = nil
    end
end

local function StopPMTask(inst)
    print("Stopping Task")
    if inst._pm_attack_task then
        inst._pm_attack_task:Cancel()
        inst._pm_attack_task = nil
    end
    inst.components.locomotor:SetReachDestinationCallback(nil)
end

local events=
{
    EventHandler("attacked", function(inst, data) 
		if data.stimuli and (data.stimuli == "electric" or data.stimuli == "explosive") and not inst.components.health:IsDead() then
			inst.sg:GoToState("hit", data.stimuli)
		elseif not inst.components.health:IsDead() and (data.stimuli and data.stimuli == "strong" or not inst.sg:HasStateTag("busy")) then 
			inst.sg:GoToState("hit") 
		end 
	end),
    EventHandler("ontalk", function(inst) if not inst.sg:HasStateTag("busy") then inst.sg:GoToState("talk") end end),
    EventHandler("pm_doaction", function(inst, data) 
        local targets = data.targets
        local skill = data.skill
        if skill == "Attack" then
            inst.components.pm_unit.stage.components.pm_battlemanager:PlayAction(inst)
            local origin = inst:GetPosition()
            local tarpos = targets[1]:GetPosition()
            inst.components.locomotor:GoToPoint(Point(tarpos.x, 0, tarpos.z)) --TODO do proper movement calc
            inst._pm_attack_task = inst:DoPeriodicTask(0, function(inst)
                if inst:IsNear(targets[1], 2) and not inst.sg:HasStateTag("busy") then
                    inst.sg:GoToState("pm_attack", {mypos = origin, target = targets[1]})
                end
            end)
        end
    end),
    EventHandler("pm_hit", function(inst, data) 
        inst.sg:GoToState("hit")
    end),
    EventHandler("pm_death", function(inst, data) 
        inst.sg:GoToState("pm_death")
    end),
	PP_CommonHandlers.AddCommonHandlers(),
    PP_CommonHandlers.OnDeath(),
    PP_CommonHandlers.OnLocomoteAdvanced(),
    EventHandler("doattack", function(inst, data) if not inst.components.health:IsDead() and (inst.sg:HasStateTag("hit") or not inst.sg:HasStateTag("busy")) then inst.sg:GoToState("attack", data.target) end end),
    CommonHandlers.OnSleep(),
    CommonHandlers.OnFreeze(),
	CommonHandlers.OnHop(),
	PP_CommonHandlers.OnSink(),
	PP_CommonHandlers.OnKnockback(),
	
	 EventHandler("ms_opengift",
        function(inst)
            if not inst.sg:HasStateTag("busy") then
                inst.sg:GoToState("opengift")
            end
        end),
		
	EventHandler("respawnfromghost", function(inst)  
			if inst.components.playercontroller ~= nil then
                inst.components.playercontroller:Enable(true)
            end

            inst.components.health:SetInvincible(false)
            inst:ShowHUD(true)
            inst:SetCameraDistance()

            SerializeUserSession(inst) end),	
}


 local states=
{

    State{
        name = "idle",
        tags = {"idle", "canrotate"},
        onenter = function(inst, playanim)
            --inst.SoundEmitter:PlaySound("dontstarve/creatures/hound/pant")
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_loop", true)
        end,

    },

    State{
        name = "pm_attack",
        tags = {"busy", "pm_attacking"},
        onenter = function(inst, data)
            StopPMTask(inst)
            inst.components.locomotor:Stop()

            inst.sg.mem.origin = data.mypos
            inst.sg.mem.target = data.target
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("attack")
            inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,

        --onexit = StopPMTask,
        timeline=
        {
            TimeEvent(6*FRAMES, function(inst) inst.components.pm_unit:DoDamage(inst.sg.mem.target, 2) end),
        },

        events=
        {
            EventHandler("animover", function(inst) 
                inst.sg:RemoveStateTag("busy")
                local mypos = inst.sg.mem.origin
                inst.components.locomotor:GoToPoint(Point(mypos.x, 0, mypos.z))
                inst.components.locomotor:SetReachDestinationCallback(function(inst)
                    inst.Transform:SetRotation(inst.components.pm_unit.team == 1 and 0 or 180)
                    StopPMTask(inst)
                    inst.components.pm_unit.stage:PushEvent("pm_unit_endturn", {prevactor = inst})
                end)
            end),
        },

    },

    State{
        name = "attack",
        tags = {"attack", "busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("throw")
            inst.AnimState:PushAnimation("throw_pst")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
	    end,
	
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(13*FRAMES, function(inst) 
                --throw
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "work",
        tags = {"busy"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("throw")
            inst.AnimState:PushAnimation("throw_pst")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
	    end,
	
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(13*FRAMES, function(inst) 
                PlayablePets.DoWork(inst, 2)
            end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
	
    },

    State{
        name = "bite",
        tags = {"attack", "busy", "canrotate"},
		
		onenter = function(inst)
            inst.components.combat:StartAttack()
            inst.components.locomotor:Stop()
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("bite")
            --inst.SoundEmitter:PlaySound(inst.sounds.attack)
        end,
        
        onexit = function(inst)

        end,
        
        timeline=
        {
            TimeEvent(15*FRAMES, function(inst) PlayablePets.DoWork(inst, 2) end),
        },

        events=
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
	State{
		name = "hit",
        tags = {"busy", "hit"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("hit", true)
            inst.SoundEmitter:PlaySound(inst.sounds.hit)
            inst.sg:SetTimeout(0.75)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,
    },
	
	--build?
	State{
		name = "special_atk1",
        tags = {"busy", "taunting"},

        onenter = function(inst, cb)
            if inst._manualtrigger then
                inst._manualtrigger = false
                inst.SoundEmitter:PlaySound(inst.sounds.flip)
            else
                inst._manualtrigger = true
                inst.SoundEmitter:PlaySound(inst.sounds.attack)
            end
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("idle_shake")
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },

    State{
		name = "special_atk2",
        tags = {"busy"},

        onenter = function(inst, cb)
            inst.Physics:Stop()
            inst.AnimState:PlayAnimation("walk_pst")
            PlayablePets.ToggleWalk(inst)
        end,

		timeline=
        {

        },

        onexit = function(inst)

        end,

        events=
        {
			EventHandler("animover", function(inst) inst.sg:GoToState("idle") end),
        },
    },
	
    State{
        name = "death",
        tags = {"busy"},

        onenter = function(inst)
			inst.SoundEmitter:PlaySound(inst.sounds.death)
            inst.AnimState:PlayAnimation("hit", true)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
			inst.components.lootdropper:DropLoot(Vector3(inst.Transform:GetWorldPosition()))
			inst.components.inventory:DropEverything(true)
            inst.sg:SetTimeout(2)
        end,

        timeline = {

        },

        ontimeout = function(inst)
            inst.SoundEmitter:PlaySound(inst.sounds.death_pst)
            PlayablePets.DoDeath(inst)
        end,

        events =
        {

        },

    },

    State{
        name = "pm_death",
        tags = {"busy"},

        onenter = function(inst, killer)
            if killer then
                inst.sg.mem.killer = killer
            end
            inst.AnimState:PlayAnimation("hit", true)
            inst.Physics:Stop()
            RemovePhysicsColliders(inst)     
            inst.sg:SetTimeout(3)
        end,

        timeline = {
            TimeEvent(1, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death) end),
            TimeEvent(2, function(inst) inst.SoundEmitter:PlaySound(inst.sounds.death_pst) 
                inst.components.sizetweener:StartTween(0.05, 0.75)
            end)
        },

        ontimeout = function(inst)
            --DoDeath
            inst:Hide()
            inst.components.pm_unit:AwardExp(inst.sg.mem.killer)
            --inst.DynamicShadow:Enable(false)
            --PlayablePets.DoDeath(inst)
        end,

        events =
        {

        },

    },
	
	State {
        name = "sleep",
        tags = { "sleeping", "busy" }, --add tag "busy" if you hate sliding

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("dead_pre")
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("sleeping") end ),
            EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "sleeping",
        tags = { "sleeping", "busy" },
		
		onenter = function(inst)
				inst.components.locomotor:StopMoving()
				PlayablePets.SleepHeal(inst, 0.5)
				inst.AnimState:PlayAnimation("dead_loop")
			end,
			
		onexit = function(inst)
		
		end,

		timeline=
        {
			--TimeEvent(20*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/sleep") end),
        },

        events =
        {
            EventHandler("animover", function(inst) inst.sg:GoToState("sleeping") end ),
			EventHandler("onwakeup", function(inst) inst.sg:GoToState("wake") end),
        },
    },

    State
    {
        name = "wake",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("dead_shake", true)
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
            inst.sg:SetTimeout(0.75)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("dead_pst")
        end,

        events =
        {

        },
    },

    State
    {
        name = "dead_pst",
        tags = { "busy", "waking" },

        onenter = function(inst)
            if inst.components.locomotor ~= nil then
                inst.components.locomotor:StopMoving()
            end
            inst.AnimState:PlayAnimation("dead_pst")
            if inst.components.sleeper ~= nil and inst.components.sleeper:IsAsleep() then
                inst.components.sleeper:WakeUp()
            end
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") end ),
        },
    },
	
    --Don't use commonstate's as they will eventually desync.
	State
    {
        name = "walk_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
            inst.AnimState:PlayAnimation("walk_pre")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
        },
    },
	
	State
    {
        name = "walk",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:WalkForward()
			inst.AnimState:PlayAnimation("walk_loop")
        end,
		
		timeline = {
            TimeEvent(7*FRAMES, DoGenericFootstep),
            TimeEvent(14*FRAMES, DoGenericFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("walk") end ),
			},

    },
	
	State
    {
        name = "walk_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("walk_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") DoGenericFootstep(inst) end ),
        },
    },	

    State
    {
        name = "run_start",
        tags = { "moving", "canrotate"},

        onenter = function(inst)
			inst.components.locomotor:RunForward()
            inst.AnimState:PlayAnimation("run_loop")
			
        end,
		
		timeline = 
		{
			--TimeEvent(0*FRAMES, function(inst) inst.Physics:Stop() end ),
		},
		
        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
        },
    },
	
	State
    {
        name = "run",
        tags = { "moving", "canrotate" },

        onenter = function(inst)
			inst.components.locomotor:RunForward()
			inst.AnimState:PlayAnimation("run_loop")
        end,
		
		timeline = {
            TimeEvent(4*FRAMES, DoGenericFootstep),
            TimeEvent(8*FRAMES, DoGenericFootstep),
		},
		
        events=
			{
				EventHandler("animqueueover", function(inst) inst.sg:GoToState("run") end ),
			},

    },
	
	State
    {
        name = "run_stop",
        tags = { "idle" },

        onenter = function(inst) 
            inst.components.locomotor:StopMoving()
			inst.AnimState:PlayAnimation("run_pst")            
        end,

        events =
        {
            EventHandler("animqueueover", function(inst) inst.sg:GoToState("idle") DoGenericFootstep(inst) end ),
        },
    },	

    State{
        name = "talk",
        tags = { "idle", "talking" },

        onenter = function(inst, noanim)
            inst.AnimState:PlayAnimation("talk", true)
            DoTalkSound(inst)
            inst.sg:SetTimeout(0.5)
        end,

        ontimeout = function(inst)
            inst.sg:GoToState("idle")
        end,

        events =
        {
            EventHandler("donetalking", function(inst)
                inst.sg:GoToState("idle")
            end),
        },

        onexit = StopTalkSound,
    },
}

PP_CommonStates.AddActionStates(states, 
	{
		TimeEvent(0 * FRAMES, function(inst)
            inst:PerformBufferedAction()
        end),
	}, 
	"walk_pst", nil, nil, "idle_loop", "walk_pst") --(states, timelines, anim, anim2, enteranim, loopanim, exitanim, noanim, events)
PP_CommonStates.AddCorpseStates(states, nil, 
	--timelines = 
	{
		corpse =
		{
			TimeEvent(0*FRAMES, function(inst) 
                --inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/death")             
            end),
		},
		
		corpse_taunt =
		{
			--TimeEvent(10*FRAMES, function(inst) inst.SoundEmitter:PlaySound("dontstarve/creatures/lava_arena/boaron/taunt") end),
		},
	
	},
	--anims = 
	{
		corpse = "dead_pre",
		corpse_taunt = "dead_pst"
	},
	--sounds =
	{

	},
	--fns =
	{

	}
	) ---(states, isflying, timelines, anims, sounds, fns, events, nofx)
PP_CommonStates.AddJumpInStates(states, nil, "walk_pst")
PP_CommonStates.AddOpenGiftStates(states, "idle_loop")
PP_CommonStates.AddSailStates(states, {}, "walk_pst", "idle_loop")
local simpleanim = "walk_pst"
local simpleidle = "idle_loop"
local simplemove = "idle_loop"
if CurrentRelease.GreaterOrEqualTo(ReleaseID.IDs.R08_ROT_TURNOFTIDES) then
	CommonStates.AddHopStates(states, false, {pre = simplemove, loop = simplemove, pst = simplemove}, nil, "hit")
end
PP_CommonStates.AddSailorStates(states, nil, 
{
	plank_idle = simpleanim,
	plank_idle_loop = simpleidle,
	plank_idle_pst = simpleanim,
	
	plank_hop_pre = simplemove,
	plank_hop = simplemove,
	
	steer_pre = simpleanim,
	steer_idle = simpleidle,
	steer_turning = simpleanim,
	stop_steering = simpleanim,
	
	row = "walk_pst",
}
)
    
return StateGraph("pm_drybonesp", states, events, "idle", actionhandlers)

