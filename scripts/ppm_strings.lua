local prefab = ""
------------------------------------------------------------------
-- Recipe strings
------------------------------------------------------------------

--STRINGS.RECIPE_DESC.PIGHOUSE_PLAYER = "A house for best pigs"

------------------------------------------------------------------
-- Character select screen strings
------------------------------------------------------------------
------------------------------------------------------------------
prefab = "pig_knight_fatp"
------------------------------------------------------------------
STRINGS.CHARACTER_TITLES[prefab] = "Ham Breaker"
STRINGS.CHARACTER_NAMES[prefab] = STRINGS.CHARACTER_TITLES[prefab]
STRINGS.CHARACTER_DESCRIPTIONS[prefab] = "*Is a pig.\n*Is bulky, but slow.\n*Deals critical damage against structures and bosses."
STRINGS.CHARACTER_QUOTES[prefab] = "Breaking Bacon."
STRINGS.CHARACTER_SURVIVABILITY[prefab] = "Good"

STRINGS.CHARACTER_TITLES.lc_schadenfreudep = "Schadenfreude"
STRINGS.CHARACTER_NAMES.lc_schadenfreudep = STRINGS.CHARACTER_TITLES.lc_schadenfreudep
STRINGS.CHARACTER_DESCRIPTIONS.lc_schadenfreudep = "*Is from another dimension. \n*Has chainsaws. \n*Actually can't transform."
STRINGS.CHARACTER_QUOTES.lc_schadenfreudep = "Thanks to this mob, I've learned to reliably spell Schadenfreude."
STRINGS.CHARACTER_SURVIVABILITY.lc_schadenfreudep = "Pretty Alright"

STRINGS.CHARACTER_TITLES.goliath_garlicp = "Goliath Garlic"
STRINGS.CHARACTER_NAMES.goliath_garlicp = STRINGS.CHARACTER_TITLES.goliath_garlicp
STRINGS.CHARACTER_DESCRIPTIONS.goliath_garlicp = "*Is from another dimension. \n*Spits funny little bubbles. \n*Wards off vampires."
STRINGS.CHARACTER_QUOTES.goliath_garlicp = "A literal nightmare for vampires."
STRINGS.CHARACTER_SURVIVABILITY.goliath_garlicp = "Pretty Alright"

STRINGS.CHARACTER_TITLES.lc_censoredp = "CENSORED"
STRINGS.CHARACTER_NAMES.lc_censoredp = STRINGS.CHARACTER_TITLES.lc_censoredp
STRINGS.CHARACTER_DESCRIPTIONS.lc_censoredp = "*Is from another dimension. \n*[CENSORED]."
STRINGS.CHARACTER_QUOTES.lc_censoredp = "[CENSORED]"
STRINGS.CHARACTER_SURVIVABILITY.lc_censoredp = "[CENSORED]"

STRINGS.CHARACTER_TITLES.pm_duplighostp = "Duplighost"
STRINGS.CHARACTER_NAMES.pm_duplighostp = STRINGS.CHARACTER_TITLES.pm_duplighostp
STRINGS.CHARACTER_DESCRIPTIONS.pm_duplighostp = "*Is from another dimension. \n*Is a ghost? \n*Can transform into (most) things."
STRINGS.CHARACTER_QUOTES.pm_duplighostp = "Masters of indentity fraud."
STRINGS.CHARACTER_SURVIVABILITY.pm_duplighostp = "Okay"
STRINGS.CHARACTERS.PM_DUPLIGHOSTP = STRINGS.CHARACTERS.GENERIC --undoes "silent" speech

STRINGS.CHARACTER_TITLES.pm_shroobp = "Shroob"
STRINGS.CHARACTER_NAMES.pm_shroobp = STRINGS.CHARACTER_TITLES.pm_shroobp
STRINGS.CHARACTER_DESCRIPTIONS.pm_shroobp = "*Is from another dimension. \n*Is an alien race. \n*Highly intelligent, thats why they have rayguns."
STRINGS.CHARACTER_QUOTES.pm_shroobp = "They are coping that they will never get their remake."
STRINGS.CHARACTER_SURVIVABILITY.pm_shroobp = "Pretty Alright"
STRINGS.CHARACTERS.PM_SHROOBP = STRINGS.CHARACTERS.GENERIC --undoes "silent" speech

STRINGS.CHARACTER_TITLES.pm_peachp = "Princess Peach"
STRINGS.CHARACTER_NAMES.pm_peachp = STRINGS.CHARACTER_TITLES.pm_peachp
STRINGS.CHARACTER_DESCRIPTIONS.pm_peachp = "*Is from another dimension. \n*Is a human(?). \n*Ruler of the Mushroom Kingdom."
STRINGS.CHARACTER_QUOTES.pm_peachp = "Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, I love you, oh Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, Peaches, I love you, oh -"
STRINGS.CHARACTER_SURVIVABILITY.pm_peachp = "Humanish"
STRINGS.CHARACTERS.PM_PEACHP = STRINGS.CHARACTERS.GENERIC --undoes "silent" speech

STRINGS.CHARACTER_TITLES.pig_slavep = "Enslaved Pig"
STRINGS.CHARACTER_NAMES.pig_slavep = STRINGS.CHARACTER_TITLES.pig_slavep
STRINGS.CHARACTER_DESCRIPTIONS.pig_slavep = "*Is a weakened pigman. \n*Not much else tbh."
STRINGS.CHARACTER_QUOTES.pig_slavep = "Helpers."
STRINGS.CHARACTER_SURVIVABILITY.pig_slavep = "Not Great"
STRINGS.CHARACTERS.PIG_SLAVEP = STRINGS.CHARACTERS.GENERIC

STRINGS.CHARACTER_TITLES.pm_drybonesp = "Dry Bones"
STRINGS.CHARACTER_NAMES.pm_drybonesp = STRINGS.CHARACTER_TITLES.pm_drybonesp
STRINGS.CHARACTER_DESCRIPTIONS.pm_drybonesp = "*Is from another dimension. \n*Is an undead koopa. \n*Throws bones and can revive itself."
STRINGS.CHARACTER_QUOTES.pm_drybonesp = "A visual representation of your love life."
STRINGS.CHARACTER_SURVIVABILITY.pm_drybonesp = "High"
STRINGS.CHARACTERS.PM_DRYBONESP = STRINGS.CHARACTERS.GENERIC

STRINGS.CHARACTER_TITLES.pm_bristlep = "Bristle"
STRINGS.CHARACTER_NAMES.pm_bristlep = STRINGS.CHARACTER_TITLES.pm_bristlep
STRINGS.CHARACTER_DESCRIPTIONS.pm_bristlep = "*Is from another dimension. \n*Is an ancient robot. \n*Does a bit of a spin attack."
STRINGS.CHARACTER_QUOTES.pm_bristlep = "Hello Bristle from Paper Mario 2, otherwise known as Paper Mario: The Thousand-Year Door."
STRINGS.CHARACTER_SURVIVABILITY.pm_bristlep = "Bristle"

STRINGS.CHARACTER_TITLES.cotl_scampp = "Scamp"
STRINGS.CHARACTER_NAMES.cotl_scampp = STRINGS.CHARACTER_TITLES.cotl_scampp
STRINGS.CHARACTER_DESCRIPTIONS.cotl_scampp = "*Is from another dimension.\n*Has knowledge of the occult.\n*Weak but quick, can't dodge roll."
STRINGS.CHARACTER_QUOTES.cotl_scampp = "Aw whatcha up to you lil Scamp? Demonic Rituals? Ha, kids these days."
STRINGS.CHARACTER_SURVIVABILITY.cotl_scampp = "Low"
STRINGS.CHARACTERS.COTL_SCAMPP = STRINGS.CHARACTERS.GENERIC

STRINGS.CHARACTER_TITLES.cotl_swordmanp = "Swordman"
STRINGS.CHARACTER_NAMES.cotl_swordmanp = STRINGS.CHARACTER_TITLES.cotl_swordmanp
STRINGS.CHARACTER_DESCRIPTIONS.cotl_swordmanp = "*Is from another dimension.\n*Has knowledge of the occult.\n*Can dodge roll."
STRINGS.CHARACTER_QUOTES.cotl_swordmanp = "Its what all the cool kids are into... right?"
STRINGS.CHARACTER_SURVIVABILITY.cotl_swordmanp = "Average"
STRINGS.CHARACTERS.COTL_SWORDMANP = STRINGS.CHARACTERS.GENERIC

STRINGS.CHARACTER_TITLES.cotl_bomb_scoutp = "Bomb Scout"
STRINGS.CHARACTER_NAMES.cotl_bomb_scoutp = STRINGS.CHARACTER_TITLES.cotl_bomb_scoutp
STRINGS.CHARACTER_DESCRIPTIONS.cotl_bomb_scoutp = "*Is from another dimension.\n*Has knowledge of the occult.\n*Can throw dangerous high-damage explosives, but can't dodge roll."
STRINGS.CHARACTER_QUOTES.cotl_bomb_scoutp = "Why care which faction you're in when you can just throw BOMBS!"
STRINGS.CHARACTER_SURVIVABILITY.cotl_bomb_scoutp = "Low"
STRINGS.CHARACTERS.COTL_BOMB_SCOUTP = STRINGS.CHARACTERS.GENERIC

STRINGS.CHARACTER_TITLES.fumop = "Fumo"
STRINGS.CHARACTER_NAMES.fumop = STRINGS.CHARACTER_TITLES.fumop
STRINGS.CHARACTER_DESCRIPTIONS.fumop = "*Fumo.\n*Fumo.\n*Fumo.\n*Fumo.\n*Fumo.\n*Fumo.\n*Fumo."
STRINGS.CHARACTER_QUOTES.fumop = "Fumo."
STRINGS.CHARACTER_SURVIVABILITY.fumop = "Fumo"

STRINGS.CHARACTER_TITLES.scavantulap = "Scavantula"
STRINGS.CHARACTER_NAMES.scavantulap = STRINGS.CHARACTER_TITLES.scavantulap
STRINGS.CHARACTER_DESCRIPTIONS.scavantulap = "*Is a spider\n*Can eat meat, but prefers bones\n*Absolutely hates hounds"
STRINGS.CHARACTER_QUOTES.scavantulap = "Maybe if you didn't die so much, they wouldn't be here!"
STRINGS.CHARACTER_SURVIVABILITY.scavantulap = "High"

STRINGS.CHARACTER_TITLES.lc_punishingbirdp = "Punishing Bird"
STRINGS.CHARACTER_NAMES.lc_punishingbirdp = "Punishing Bird"
STRINGS.CHARACTER_DESCRIPTIONS.lc_punishingbirdp = "*Is from another dimension\n*Very weak\n*Don't fight back"
STRINGS.CHARACTER_QUOTES.lc_punishingbirdp = "Does this look like the face of mercy?"
STRINGS.CHARACTER_SURVIVABILITY.lc_punishingbirdp = "Low"

STRINGS.CHARACTER_TITLES.lc_bigbirdp = "Big Bird"
STRINGS.CHARACTER_NAMES.lc_bigbirdp = "Big Bird"
STRINGS.CHARACTER_DESCRIPTIONS.lc_bigbirdp = "*Is from another dimension\n*Can lure nearby enemies\n*Does a heavy attack on lured enemies"
STRINGS.CHARACTER_QUOTES.lc_bigbirdp = "No, not that one"
STRINGS.CHARACTER_SURVIVABILITY.lc_bigbirdp = "Medium"

STRINGS.CHARACTER_TITLES.lc_judgementbirdp = "Judgement Bird"
STRINGS.CHARACTER_NAMES.lc_judgementbirdp = "Judgement Bird"
STRINGS.CHARACTER_DESCRIPTIONS.lc_judgementbirdp = "*Is from another dimension\n*Judgement attack cuts 35% of foe's HP\n*Is slow..."
STRINGS.CHARACTER_QUOTES.lc_judgementbirdp = "He'll judge you for your sins, and yes he has seen your internet history."
STRINGS.CHARACTER_SURVIVABILITY.lc_judgementbirdp = "Medium"

STRINGS.CHARACTER_TITLES.roach_beetlep = "Roach Beetle"
STRINGS.CHARACTER_NAMES.roach_beetlep = "Roach Beetle"
STRINGS.CHARACTER_DESCRIPTIONS.roach_beetlep = "*Is from another dimension\n*Can eat just about anything\n*Is 3x weak to acid, fire, and poison"
STRINGS.CHARACTER_QUOTES.roach_beetlep = "Looks more like a scarab?"
STRINGS.CHARACTER_SURVIVABILITY.roach_beetlep = "Roach-like"

STRINGS.CHARACTER_TITLES.gravepigp = "Grave Pig"
STRINGS.CHARACTER_NAMES.gravepigp = "GRAVE PIG"
STRINGS.CHARACTER_DESCRIPTIONS.gravepigp = "*Is from another dimension\n*Weaker than a pitpig, but can be revived by necromancy\n*Immune to poison, but is 3x weak to fire"
STRINGS.CHARACTER_QUOTES.gravepigp = "This li'l piggy is already dead!"
STRINGS.CHARACTER_SURVIVABILITY.gravepigp = "Medium"

STRINGS.CHARACTER_TITLES.necrommanderp = "Necrommander"
STRINGS.CHARACTER_NAMES.necrommanderp = STRINGS.CHARACTER_TITLES.necrommanderp
STRINGS.CHARACTER_DESCRIPTIONS.necrommanderp = "*Is from another dimension\n*Is a necromancer and caster of the dark arts\n*Immune to poison, but is 3x weak to fire"
STRINGS.CHARACTER_QUOTES.necrommanderp = "When you're too into the dead."
STRINGS.CHARACTER_SURVIVABILITY.necrommanderp = "Medium"

STRINGS.CHARACTER_TITLES.tiddles_zombiepigp = "Zombie Pigman"
STRINGS.CHARACTER_NAMES.tiddles_zombiepigp = "Zombie Pigman"
STRINGS.CHARACTER_DESCRIPTIONS.tiddles_zombiepigp = "*Is a diseased pigman\n*Can only truly die when burned\n*Immune to poison, but is 3x weak to fire"
STRINGS.CHARACTER_QUOTES.tiddles_zombiepigp = "They don't smell much worse."
STRINGS.CHARACTER_SURVIVABILITY.tiddles_zombiepigp = "Low"

STRINGS.CHARACTER_TITLES.tiddles_zombiepig_werep = "Zombie Were-Pig"
STRINGS.CHARACTER_NAMES.tiddles_zombiepig_werep = "Zombie Were-Pig"
STRINGS.CHARACTER_DESCRIPTIONS.tiddles_zombiepig_werep = "*Is a diseased werepig\n*Can only truly die when burned\n*Immune to poison, but is 3x weak to fire"
STRINGS.CHARACTER_QUOTES.tiddles_zombiepig_werep = "Its gone rabid!"
STRINGS.CHARACTER_SURVIVABILITY.tiddles_zombiepig_werep = "Medium"

STRINGS.CHARACTER_TITLES.tiddles_zombiepig_fatp = "Pig Zombie Carrier"
STRINGS.CHARACTER_NAMES.tiddles_zombiepig_fatp = "Pig Zombie Carrier"
STRINGS.CHARACTER_DESCRIPTIONS.tiddles_zombiepig_fatp = "*Is a diseased fatpig\n*Explodes when they die.\n*Immune to poison, but is 3x weak to fire"
STRINGS.CHARACTER_QUOTES.tiddles_zombiepig_fatp = "BOOMER!"
STRINGS.CHARACTER_SURVIVABILITY.tiddles_zombiepig_fatp = "Medium"

--SK
STRINGS.CHARACTER_TITLES.grimalkin_cursep = "Margrel"
STRINGS.CHARACTER_NAMES.grimalkin_cursep = "Margrel"
STRINGS.CHARACTER_DESCRIPTIONS.grimalkin_cursep = "*Is a Giant\n*Has a passive curse aura\n*Can summon Black Kats\n*Summons bullets passively"
STRINGS.CHARACTER_QUOTES.grimalkin_cursep = "Despite being the unluckiest kat, you need an awful lot of luck to fight him."

STRINGS.CHARACTER_TITLES.grimalkinp = "Grimalkin"
STRINGS.CHARACTER_NAMES.grimalkinp = "Grimalkin"
STRINGS.CHARACTER_DESCRIPTIONS.grimalkinp = "*Is a Grimalkin\n*Ignores shields\n*Hates day time"
STRINGS.CHARACTER_QUOTES.grimalkinp = "Charlie but cooler."

STRINGS.CHARACTER_TITLES.mewkatp = "Mew Kat"
STRINGS.CHARACTER_NAMES.mewkatp = "Mew Kat"
STRINGS.CHARACTER_DESCRIPTIONS.mewkatp = "*Is always calm\n*Is adorable\n*Will never attack"
STRINGS.CHARACTER_QUOTES.mewkatp = "Mew Kat believes in you."

STRINGS.CHARACTER_TITLES.spookat_blackp = "Black Kat"
STRINGS.CHARACTER_NAMES.spookat_blackp = "Black Kat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_blackp = "*Is always angry\n*Loves eating books\n*Can summon zombies"
STRINGS.CHARACTER_QUOTES.spookat_blackp = "He would LOVE to eat your homework."

STRINGS.CHARACTER_TITLES.spookat_poisonp = "Hurkat"
STRINGS.CHARACTER_NAMES.spookat_poisonp = "Hurkat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_poisonp = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_poisonp = "The only cure is death."

STRINGS.CHARACTER_TITLES.spookat_firep = "Pepperkat"
STRINGS.CHARACTER_NAMES.spookat_firep = "Pepperkat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_firep = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_firep = "The only cure is death."

STRINGS.CHARACTER_TITLES.spookat_freezep = "Bloogato"
STRINGS.CHARACTER_NAMES.spookat_freezep = "Bloogato"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_freezep = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_freezep = "The only cure is death."

STRINGS.CHARACTER_TITLES.spookat_nrmp = "Spookat"
STRINGS.CHARACTER_NAMES.spookat_nrmp = "Spookat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_nrmp = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_nrmp = "Cute until its not."

STRINGS.CHARACTER_TITLES.spookat_shockp = "Statikat"
STRINGS.CHARACTER_NAMES.spookat_shockp = "Statikat"
STRINGS.CHARACTER_DESCRIPTIONS.spookat_shockp = "*Is a spookat\n*Has a dark side\n*Is a ghost"
STRINGS.CHARACTER_QUOTES.spookat_shockp = "Shocking in more ways than one."

STRINGS.CHARACTER_TITLES.zombie_dustp = "Dust Zombie"
STRINGS.CHARACTER_NAMES.zombie_dustp = "DUST ZOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_dustp = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_dustp = "The original bony boi"

STRINGS.CHARACTER_TITLES.zombie_firep = "Slag Walker"
STRINGS.CHARACTER_NAMES.zombie_firep = "SLAG WALKER"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_firep = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_firep = "The most familiar Zombie of all"

STRINGS.CHARACTER_TITLES.zombie_poisonp = "Droul"
STRINGS.CHARACTER_NAMES.zombie_poisonp = "DROUL"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_poisonp = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_poisonp = "Hes just DROULing to meet you"

STRINGS.CHARACTER_TITLES.zombie_freezep = "Frozen Shambler"
STRINGS.CHARACTER_NAMES.zombie_freezep = "FROZEN SHAMBLER"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_freezep = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_freezep = "what 99% of wilson's turn into"

STRINGS.CHARACTER_TITLES.zombie_shockp = "Frankenzom"
STRINGS.CHARACTER_NAMES.zombie_shockp = "FRANKEZOM"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_shockp = "*Is a zombie\n*Raises fallen opponents as Zombies\n*wip"
STRINGS.CHARACTER_QUOTES.zombie_shockp = "A zombie mixed with Frankenstein monster? Shocking."

STRINGS.CHARACTER_TITLES.zombie_cursep = "Carnavon"
STRINGS.CHARACTER_NAMES.zombie_cursep = "CARNAVON"
STRINGS.CHARACTER_DESCRIPTIONS.zombie_cursep = "*Is a zombie\n*Raises fallen opponents as Zombies\n*Blocks attacks behind him"
STRINGS.CHARACTER_QUOTES.zombie_cursep = "He curses a lot. Fitting for how edgy he looks."

STRINGS.CHARACTER_TITLES.bombie_dustp = "Bombie"
STRINGS.CHARACTER_NAMES.bombie_dustp = "BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_dustp = "*Is a bombie\n*Explodes when attacking\n*Can stun enemies"
STRINGS.CHARACTER_QUOTES.bombie_dustp = "The original bombie boi"

STRINGS.CHARACTER_TITLES.bombie_firep = "Burning Bombie"
STRINGS.CHARACTER_NAMES.bombie_firep = "BURNING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_firep = "*Is a bombie\n*Explodes when attacking\n*Can set enemies on fire"
STRINGS.CHARACTER_QUOTES.bombie_firep = "When you need more destruction in your explosions"

STRINGS.CHARACTER_TITLES.bombie_poisonp = "Choking Bombie"
STRINGS.CHARACTER_NAMES.bombie_poisonp = "CHOKING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_poisonp = "*Is a bombie\n*Explodes when attacking\n*Can poison enemies"
STRINGS.CHARACTER_QUOTES.bombie_poisonp = "How can he choke people when he has no arms?"

STRINGS.CHARACTER_TITLES.bombie_freezep = "Freezing Bombie"
STRINGS.CHARACTER_NAMES.bombie_freezep = "FREEZING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_freezep = "*Is a bombie\n*Explodes when attacking\n*Can freeze enemies"
STRINGS.CHARACTER_QUOTES.bombie_freezep = "Works well with other bombies for obvious reasons"

STRINGS.CHARACTER_TITLES.bombie_shockp = "Surging Bombie"
STRINGS.CHARACTER_NAMES.bombie_shockp = "SURGING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_shockp = "*Is a bombie\n*Explodes when attacking\n*Can shock enemies"
STRINGS.CHARACTER_QUOTES.bombie_shockp = "Hes just surging with excitement!"

STRINGS.CHARACTER_TITLES.bombie_cursep = "Cursing Bombie"
STRINGS.CHARACTER_NAMES.bombie_cursep = "CURSING BOMBIE"
STRINGS.CHARACTER_DESCRIPTIONS.bombie_cursep = "*Is a bombie\n*Explodes when attacking\n*Can curse enemies"
STRINGS.CHARACTER_QUOTES.bombie_cursep = "Hes going to drop the F bomb on you"
------------------------------------------------------------------
-- Forge
------------------------------------------------------------------
--SK
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_dustp = "*Raises fallen opponents as Zombies\n*20% to inflict stun\n*Remembers skills of a prior life\n\nExpertise:\nBooks, Staves"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_cursep = "*Raises fallen opponents as Zombies\n*20% to inflict fire or curse\n*Has a 80% shield on his back\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_shockp = "*Raises fallen opponents as Zombies\n*20% to inflict shock\n*Shocks enemies when attacked\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_freezep = "*Raises fallen opponents as Zombies\n*20% to inflict freeze\n*Cannot be knocked back\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_poisonp = "*Raises fallen opponents as Zombies\n*20% to inflict poison\n*Immune to acid\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.zombie_firep = "*Raises fallen opponents as Zombies\n*20% to inflict fire\n*Attack damage increased by 50%\n\nExpertise:\nNone"

STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_dustp = "*Explodes when attacking\n*50% to inflict stun\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_firep = "*Explodes when attacking\n*50% to inflict fire\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_freezep = "*Explodes when attacking\n*50% to inflict freeze\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_poisonp = "*Explodes when attacking\n*50% to inflict poison\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_shockp = "*Explodes when attacking\n*50% to inflict shock\n*Can't revive others\n\nExpertise:\nNone"
STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.bombie_cursep = "*Explodes when attacking\n*25% to inflict curse\n*Can't revive others\n\nExpertise:\nNone"
------------------------------------------------------------------
-- The prefab names as they appear in-game
------------------------------------------------------------------

--SK
STRINGS.NAMES.SK_GRIM_TOTEM = "Grim Totem"

STRINGS.NAMES.SK_ZOMBIE = "Dust Zombie"
STRINGS.NAMES.SK_ZOMBIE_FIRE = "Slag Walker"
STRINGS.NAMES.SK_ZOMBIE_POISON = "Droul"
STRINGS.NAMES.SK_ZOMBIE_FREEZE = "Frozen Shambler"
STRINGS.NAMES.SK_ZOMBIE_SHOCK = "Frankenzom"
STRINGS.NAMES.SK_ZOMBIE_CURSE = "Carnavon"
STRINGS.NAMES.SK_ZOMBIE_BELLHOP = "Zombie Bellhop"

STRINGS.NAMES.SK_BOMBIE = "Bombie"
STRINGS.NAMES.SK_BOMBIE_FIRE = "Burning Bombie"
STRINGS.NAMES.SK_BOMBIE_POISON = "Choking Bombie"
STRINGS.NAMES.SK_BOMBIE_FREEZE = "Freezing Bombie"
STRINGS.NAMES.SK_BOMBIE_SHOCK = "Surging Bombie"

STRINGS.NAMES.SK_SPOOKAT = "Spookat"
STRINGS.NAMES.SK_SPOOKAT_FIRE = "Pepperkat"
STRINGS.NAMES.SK_SPOOKAT_POISON = "Hurkat"
STRINGS.NAMES.SK_SPOOKAT_FREEZE = "Bloogato"
STRINGS.NAMES.SK_SPOOKAT_SHOCK = "Statikat"
STRINGS.NAMES.SK_SPOOKAT_BLACK = "Black Kat"
STRINGS.NAMES.SK_SPOOKAT_BLACK_T2 = STRINGS.NAMES.SK_SPOOKAT_BLACK
STRINGS.NAMES.SK_SPOOKAT_MEW = "Mew Kat"
STRINGS.NAMES.SK_SPOOKAT_RITUAL = "Ritual Kat"
--------------------------------------------------

STRINGS.NAMES.PM_SHROOBFO = "UFO"
STRINGS.NAMES.PP_SWITCH = "Switch"
STRINGS.NAMES.PP_SWITCH_TIMER = STRINGS.NAMES.PP_SWITCH

STRINGS.NAMES.PIG_STAKE_ALIVEP = "Hanged Pig"
STRINGS.NAMES.PIG_STAKE_DEADP = "Stake"
STRINGS.NAMES.PIG_STAKE_EMPTYP = "Stake"
STRINGS.NAMES.PW_BANNER_LOYALIST = "Loyalist Banner"
STRINGS.NAMES.PW_BANNER_SHADOW = "Dread Banner"
STRINGS.NAMES.PW_BANNER_LUNAR = "Brilliant Banner"
STRINGS.NAMES.PW_BANNER_PEACH = "Mushroom Kingdom Banner"
STRINGS.NAMES.PW_CAGE_EMPTY = "Empty Cage"
STRINGS.NAMES.PW_CAGE_PIG = "Caged Pig"
STRINGS.NAMES.PW_CAGE_PIG_SLEEPING = STRINGS.NAMES.PW_CAGE_PIG
STRINGS.NAMES.LC_BLACKFORESTPORTAL = "Entrance to the Black Forest"

STRINGS.NAMES.LC_CENSOREDBABY_NPC = "CENSORED"


------------------------------------------------------------------
--Reforged Strings
------------------------------------------------------------------
--STRINGS.LAVAARENA_CHARACTER_DESCRIPTIONS.houndplayer = "*Is very fast\n*Can taunt by pressing x \n*Not very bulky\n\nExpertise:\nMelees, Darts"

------------------------------------------------------------------
-- Pigman-specific speech strings
------------------------------------------------------------------

--STRINGS.CHARACTERS.PIGMANPLAYER = require "speech_pigmanplayer"

------------------------------------------------------------------
-- The default responses of characters examining the prefabs
------------------------------------------------------------------

return STRINGS