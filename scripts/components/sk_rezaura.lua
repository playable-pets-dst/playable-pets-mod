local RezAura = Class(function(self, inst)
    self.inst = inst
    self.range = 12 --TODO: make rez ring dynamic in size.
	self.default_circle_size = 0.65
	self.cache = {}
    self.aurafn = nil
	
	local fx = SpawnPrefab("sk_rez_circlep")
	fx.AnimState:SetScale(self.range * self.default_circle_size , self.range * self.default_circle_size)
	fx.AnimState:PlayAnimation("spawn")
	fx.AnimState:PushAnimation("idle", false)
    fx.entity:SetParent(self.inst.entity)
	self.circle = fx
	
	self.inst:DoTaskInTime(0, inst.StartUpdatingComponent, self)
end)

function RezAura:SetCircle(circle, radius)
	--print("RezAura DEBUG: SetCircle ran!")
	local radius = radius or self.range
	--local current_scale_width, current_scale_height = self.inst.Transform:GetScale()
	local scale = radius * self.default_circle_size or 0.65
	circle.AnimState:SetScale(scale, scale)
end

function RezAura:SetRange(range)
	if range and self.circle then
		--print("RezAura DEBUG: SetRange ran!")
		self.range = range
		self:SetCircle(self.circle, range)
	end
end

function RezAura:DoPulse()
	--print("RezAura DEBUG: DoPulse ran!")
	local pos = self.inst:GetPosition()
	if self.circle then
		self.circle.AnimState:PlayAnimation("pulse")
	end
	local ents = TheSim:FindEntities(pos.x, 0, pos.z, self.range, {"zombie"}) -- TODO could exclude "_isinheals" since they would be set already
	--print("RezAura DEBUG: DoPulse ents found: "..#ents)
	for i, v in ipairs(ents) do
		if v and v.sg and v.components.health:IsDead() then
			v:DoTaskInTime(0.5, function(inst)
				local pos = v:GetPosition()
				local text = SpawnPrefab("sk_status_textp")
				text.AnimState:OverrideSymbol("stun", "status_text", "arise")
				text.AnimState:PlayAnimation("buff")
				text.Transform:SetPosition(pos.x, 3, pos.z)
				SpawnPrefab("collapse_small").Transform:SetPosition(v:GetPosition():Get())
				v.components.health:SetPercent(1)
				v.sg:GoToState("spawn")
			end)
		end
	end
end

function RezAura:InRange(ent)
	local pos = ent:GetPosition()
	return distsq(pos.x, pos.z, self.pos.x, self.pos.z) < self.range * self.range
end

local function IsEntityAlive(ent)
	return ent.components.health and not ent.components.health:IsDead()
end

function RezAura:OnUpdate(dt) -- TODO use mob radius????
	self.current_time = GetTime()
	if not self.pos then self.pos = self.inst:GetPosition() end
	local ents = TheSim:FindEntities(self.pos.x, 0, self.pos.z, self.range, {"zombie"}) -- TODO could exclude "_isinheals" since they would be set already
	for _, ent in pairs(ents) do
		if not self.cache[ent] then
			self.cache[ent] = true
			self:OnEntEnter(ent)
		end
	end
	for ent in pairs(self.cache) do
		if not self:InRange(ent) then
			self:OnEntLeave(ent)
		end
	end
end

function RezAura:OnEntEnter(ent)
	ent:AddTag("_isinrez") -- for targeting and shield behaviours -- TODO change tag name to a more generic term
	ent.rez_aura_count = (ent.rez_aura_count or 0) + 1
end

function RezAura:OnEntLeave(ent)
	if not self.cache[ent] then
		print("ERROR: Tried to remove non-exsisting ent!")
		return
	end
	ent.rez_aura_count = ent.rez_aura_count - 1
	if ent.rez_aura_count <= 0 then
		if ent:HasTag("_isinrez") then
			ent:RemoveTag("_isinrez")
		end
	end
	self.cache[ent] = nil
end

function RezAura:Start()
	self.inst:StartUpdatingComponent(self)
	if self.circle then
		self:SetCircle(self.circle, self.range)
		self.circle.AnimState:PlayAnimation("spawn")
	end
end

function RezAura:Stop()
	self.inst:StopUpdatingComponent(self)
	for ent in pairs(self.cache) do
		self:OnEntLeave(ent)
	end
	if self.circle then
		self.circle.AnimState:PlayAnimation("despawn")
	end
end

RezAura.OnRemoveEntity = RezAura.Stop
RezAura.OnRemoveFromEntity = RezAura.Stop

return RezAura
