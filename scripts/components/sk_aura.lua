local Aura = Class(function(self, inst)
    self.inst = inst
	self.isenabled = false
    self.range = 6 --TODO: make rez ring dynamic in size.
	self.chance = 1
	self.cache = {}
	self.notags = {}
    self.onhitfn = nil
	self.onstartfn = nil
	self.onendfn = nil
	
	self.inst:DoTaskInTime(0, inst.StartUpdatingComponent, self)
end)

function Aura:SetOnStartFn(fn)
	self.onstartfn = onstartfn
end

function Aura:SetOnEndFn(fn)
	self.onendfn = onendfn
end

function Aura:SetOnHitFn(fn)
	self.onhitfn = onhitfn
end

function Aura:SetRange(range)
	if range and self.circle then
		self.range = range
	end
end

function Aura:Hit(target)
	if self.onhitfn then
		self.onhitfn(self.inst, target)
	end
	
end

function Aura:InRange(ent)
	local pos = ent:GetPosition()
	return distsq(pos.x, pos.z, self.pos.x, self.pos.z) < self.range * self.range
end

local function IsEntityAlive(ent)
	return ent.components.health and not ent.components.health:IsDead()
end

function Aura:OnUpdate(dt) -- TODO use mob radius????
	self.current_time = GetTime()
	if not self.pos then self.pos = self.inst:GetPosition() end
	local ents = TheSim:FindEntities(self.pos.x, 0, self.pos.z, self.range, {"_combat"}, self.notags and self.notags or {"shadow", "INLIMBO", "notarget"}) -- TODO could exclude "_isinheals" since they would be set already
	for _, ent in pairs(ents) do
		if not self.cache[ent] then
			self.cache[ent] = true
			self:OnEntEnter(ent)
			if math.random() <= self.chance then
				self:Hit(ent)
			end
		end
	end
	for ent in pairs(self.cache) do
		if not self:InRange(ent) then
			self:OnEntLeave(ent)
		end
	end
end

function Aura:OnEntEnter(ent)
	ent:AddTag("_isinaura")
	ent.aura_count = (ent.aura_count or 0) + 1
end

function Aura:OnEntLeave(ent)
	if not self.cache[ent] then
		print("ERROR: Tried to remove non-exsisting ent!")
		return
	end
	ent.aura_count = ent.aura_count - 1
	if ent.aura_count <= 0 then
		if ent:HasTag("_isinaura") then
			ent:RemoveTag("_isinaura")
		end
	end
	self.cache[ent] = nil
end

function Aura:Start()
	self.inst:StartUpdatingComponent(self)
	if self.onstartfn and self.inst then
		self.onstartfn(self.inst)
	end
end

function Aura:Stop()
	self.inst:StopUpdatingComponent(self)
	if self.onendfn and self.inst then
		self.onendfn(self.inst)
	end
end

Aura.OnRemoveEntity = Aura.Stop
Aura.OnRemoveFromEntity = Aura.Stop

return Aura
