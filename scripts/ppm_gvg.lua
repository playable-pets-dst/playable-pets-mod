
GVG = {}

local RADIUS_LARGE = 40
local RADIUS = 20

GVG.FindNearbyGiants = function(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x, y, z, RADIUS_LARGE, { "locomotor" }, { "epic" })
	return ents 
end

GVG.FindNearbyPlayerGiants = function(inst)
	local x, y, z = inst.Transform:GetWorldPosition()
	local ents = TheSim:FindEntities(x, y, z, RADIUS_LARGE, { "player" }, { "epic", "giant" })
	return ents 
end

GVG.OnKillOther = function(inst, other)
	if other and other:HasTag("epic") and other:HasTag("player") and not (other.prefab == klausp and other.is_enraged == false) then
		inst.components.health:DoDelta(inst.components.health.maxhealth, false)
		if inst.kill_counter then
			inst.kill_counter = inst.kill_counter + 1
		end
		if inst.kill_count then
			inst.kill_count = inst.kill_count + 1
		end
	end
end
 
GVG.STATS =
{
	BEARGER_HEALTH = 6000,
	
	BEARGER_DAMAGE = 200,
	BEARGER_ATTACK_PERIOD = 3,
	BEARGER_DAMAGE_MULT = 1.5,
	BEARGER_RANGE = 6,
	
	
	BEARGER_HUNGER = 500,
	BEARGER_HUNGERRATE = 0.15,
	
	BEARGER_RUNSPEED = 7,
	BEARGER_WALKSPEED = 6,
}


return GVG